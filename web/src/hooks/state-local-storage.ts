import { useState } from "react";

/**
 * Hook to manage a variable in the local storage.
 *
 * @param key {string} Name of the variable
 * @param defaultValue {string} The default / initial value of the attribute (not set in the url)
 */
export function useStateLocalStorage<T>(key: string, defaultValue: T): [T, (value: T) => void] {
  const [variable, setVariable] = useState<T>(
    localStorage.getItem(key) !== null ? (JSON.parse(localStorage.getItem(key) || "") as T) : defaultValue,
  );

  /**
   * Given a parameter, it returns the setter for it.
   */
  function getSetVariable(key: string): (value: T) => void {
    return (value: T): void => {
      if (value !== null && value !== undefined) {
        localStorage.setItem(key, JSON.stringify(value));
        setVariable(value);
      } else {
        localStorage.removeItem(key);
      }
    };
  }
  return [variable, getSetVariable(key)];
}
