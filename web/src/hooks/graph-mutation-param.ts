import { useApolloClient } from "@apollo/client";

/**
 * Just a hook that reset the cache of apollo after a mutation.
 */
export function useGraphqlMutationParam(): { onCompleted: () => void } {
  const client = useApolloClient();
  return {
    onCompleted: (): void => {
      client.resetStore();
    },
  };
}
