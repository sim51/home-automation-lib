/* eslint @typescript-eslint/no-use-before-define: 0 */
/* eslint @typescript-eslint/no-explicit-any: 0 */
import { ComponentFragment, ModuleFragment } from "./graphql/types";

export function omitDeepArrayWalk(arr: Array<any>, key: string): Array<any> {
  return arr.map((val) => {
    if (Array.isArray(val)) return omitDeepArrayWalk(val, key);
    else if (typeof val === "object") return omitDeep(val, key);
    return val;
  });
}

export function omitDeep(obj: any, key: string): any {
  const keys: string[] = Object.keys(obj);
  const newObj: any = {};
  keys.forEach((i) => {
    if (i !== key) {
      const val: any = obj[i];
      if (val instanceof Date) newObj[i] = val;
      else if (Array.isArray(val)) newObj[i] = omitDeepArrayWalk(val, key);
      else if (typeof val === "object" && val !== null) newObj[i] = omitDeep(val, key);
      else newObj[i] = val;
    }
  });
  return newObj;
}

export function omitDeepEmpty(obj: any): any {
  const keys: string[] = Object.keys(obj);
  const newObj: any = {};
  keys.forEach((i) => {
    if (obj[i] !== undefined && obj[i] !== null) {
      const val: any = obj[i];
      if (val instanceof Date) newObj[i] = val;
      else if (Array.isArray(val) && val.length > 0) newObj[i] = val;
      else if (typeof val === "object" && val !== null) newObj[i] = omitDeepEmpty(val);
      else newObj[i] = val;
    }
  });
  return newObj;
}

export function template(template: string, params: { [key: string]: any } = {}): string {
  let result = template;
  Object.keys(params).forEach((key) => {
    result = result.replaceAll(`:${key}`, params[key] || ``);
  });
  return result;
}

export function tooltipPosition(coordinates: [number, number], element: HTMLElement): void {
  // constant
  const offset = 10;
  const maxX = window.innerWidth;
  const maxY = window.innerHeight;
  const mouseX = coordinates[0];
  const mouseY = coordinates[1];
  const elementWidth = element.offsetWidth;
  const elementHeight = element.offsetHeight;

  // compute diagonals
  const diagTopLeft = Math.pow(mouseX, 2) + Math.pow(mouseY, 2);
  const diagTopRight = Math.pow(maxX - mouseX, 2) + Math.pow(mouseY, 2);
  const diagBottomleft = Math.pow(mouseX, 2) + Math.pow(maxY - mouseY, 2);
  const diagBottomRight = Math.pow(maxX - mouseX, 2) + Math.pow(maxY - mouseY, 2);

  if (diagTopLeft > diagTopRight && diagTopLeft > diagBottomleft && diagTopLeft > diagBottomRight) {
    // display in top / Left
    element.style.top = `${mouseY - elementHeight - offset}px`;
    element.style.left = `${mouseX - elementWidth - offset}px`;
  } else if (diagTopRight > diagTopLeft && diagTopRight > diagBottomleft && diagTopRight > diagBottomRight) {
    // display in top / right
    element.style.top = `${mouseY - elementHeight - offset}px`;
    element.style.left = `${mouseX + offset}px`;
  } else if (diagBottomleft > diagTopLeft && diagBottomleft > diagTopLeft && diagBottomleft > diagBottomRight) {
    // display in bottom / left
    element.style.top = `${mouseY + offset}px`;
    element.style.left = `${mouseX - elementWidth - offset}px`;
  } else {
    // display in bottom / right;
    element.style.top = `${mouseY + offset}px`;
    element.style.left = `${mouseX + offset}px`;
  }
}

export function displayDateTime(date?: Date | string | null): string {
  if (!date || date === null) {
    return "";
  } else {
    const value: Date = new Date(date);
    const datePart = value.toISOString().split("T")[0];
    const timePart = value.toLocaleTimeString();
    return `${timePart} - ${datePart}`;
  }
}

export function displayDateTimeToDate(value: string): Date {
  const parts = value.split(" - ");
  const parsed = Date.parse(`${parts[1]} ${parts[0]}`);
  const date = new Date(parsed);
  return date;
}

/**
 * Round to its closest float with two decimals.
 * Exemples:
 *  - roundNumber(3.14159) => 3.14
 *  - roundNumber(null) => null
 *  - roundNumber(3) => 3
 */
export function roundNumber(x: number | undefined | null): number | undefined | null {
  if (x !== undefined && x !== null) {
    return Math.round(x * 100) / 100;
  }
  return x;
}

export function componentToLogLabel(component: ComponentFragment & { module: ModuleFragment }): string {
  return `HAL > ${component.module.name.toUpperCase()} > ${component.type.name.toUpperCase()} > ${component.name.toUpperCase()}`;
}

/**
 * Guess the type of a string and return it casted.
 * examples:
 *   - "true" => true
 *   - "10.1" => 10.1
 */
export function stringGuessCast(value: string): any {
  if (value !== null) {
    if (isNaN((value as unknown) as number) === false) {
      return parseFloat(value) % 1 === 0 ? parseInt(value) : (parseFloat(value) as unknown);
    }
    if (value === "false" || value === "true") {
      return (value === "true") as unknown;
    }
  }
  return value;
}

/**
 * Return the position on the page of an html element
 */
export function getElementPosition(element: HTMLElement): [number, number] {
  let x = 0;
  let y = 0;
  let ele = element;
  while (true) {
    x += ele.offsetLeft;
    y += ele.offsetTop;
    if (ele.offsetParent === null) {
      break;
    }
    ele = ele.offsetParent as HTMLElement;
  }
  return [x, y];
}
