import React from "react";

interface Props {
  name: string;
  label: string;
  value?: string;
  onChange: (data: any) => void;
}

export const TimeSelector: React.FC<Props> = (props: Props) => {
  const { name, label, value, onChange } = props;
  const values = [
    { value: "-1h", label: "Last hour" },
    { value: "-4h", label: "Last 4 hours" },
    { value: "-12h", label: "Last 12 hours" },
    { value: "-1d", label: "Last day" },
    { value: "-2d", label: "Last two days" },
    { value: "-3d", label: "Last three days" },
    { value: "-7d", label: "Last week" },
  ];

  return (
    <div className="field">
      <div className="label">
        <label htmlFor={name}>{label || name}</label>
      </div>
      <div className="input">
        <div className="select">
          <select id={name} onChange={onChange} value={value}>
            {value && !values.map((e) => e.value).includes(value) && <option value={value}>Custom range</option>}
            {values.map((e) => (
              <option key={e.value} value={e.value}>
                {e.label}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
};
