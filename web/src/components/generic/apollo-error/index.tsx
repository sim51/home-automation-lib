import React, { useState, useEffect } from "react";
import { ApolloError as Error } from "@apollo/client";

interface Props {
  title?: string;
  error?: Error | null;
}

export const ApolloError: React.FC<Props> = (props: Props) => {
  const { title, error } = props;
  const [closed, setClosed] = useState<boolean>(false);

  useEffect(() => {
    setClosed(false);
  }, [error]);

  if (error && error !== null && !closed) {
    return (
      <div className="alert-danger">
        <button type="button" onClick={() => setClosed(true)}>
          <i className="fa fa-times"></i>
        </button>
        <h4>{title ? title : "Error"}</h4>
        {error.graphQLErrors.length > 0 ? (
          <>
            {error.graphQLErrors.map((error, index) => {
              return (
                <div className="error" key={index}>
                  <h5>{error.extensions?.code}</h5>
                  <span>{error.message}</span>
                </div>
              );
            })}
          </>
        ) : (
          <div className="error">
            <span>{error.message}</span>
          </div>
        )}
      </div>
    );
  }
  return null;
};
