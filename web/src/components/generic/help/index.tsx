import React from "react";

interface Props {
  text?: string;
}

export const Help: React.FC<Props> = (props: Props) => {
  const { text } = props;
  if (!text) return null;
  return (
    <span className="help" title={text}>
      <i className="fa fa-question-circle" aria-hidden="true"></i>
    </span>
  );
};
