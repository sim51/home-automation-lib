import React from "react";
import * as CSS from "csstype";

export interface Column {
  prop: string;
  title?: string;
  format?: (data: any) => string;
  size: number;
  center?: boolean;
}
interface Props {
  columns: Array<Column>;
  className?: string;
  data: Array<{ [key: string]: any }>;
}

export const DataTable: React.FC<Props> = (props: Props) => {
  const { className, columns, data } = props;

  return (
    <>
      {data.length > 0 && (
        <table className={className}>
          <thead>
            <tr>
              {columns.map((column) => {
                const style: CSS.Properties = { width: column.size + "%" };
                return (
                  <th key={column.prop} scope="col" style={style}>
                    {column.title || column.prop}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {data.map((row, index) => {
              return (
                <tr key={index}>
                  {columns.map((column) => {
                    const style: CSS.Properties = {
                      width: column.size + "%",
                      textAlign: column.center ? "center" : "left",
                    };
                    return (
                      <td
                        key={index + "-" + column.prop}
                        style={style}
                        title={column.format ? column.format(row[column.prop]) : row[column.prop]}
                      >
                        {column.format ? column.format(row[column.prop]) : row[column.prop]}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
      {data.length === 0 && <div className="no-data">Empty result</div>}
    </>
  );
};
