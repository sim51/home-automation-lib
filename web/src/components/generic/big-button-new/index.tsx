import React from "react";
import { Link } from "react-router-dom";

type ActionFunction = () => void | Promise<void>;

interface Props {
  title: string;
  action?: string | ActionFunction;
}

export const BigButtonNew: React.FC<Props> = (props: Props) => {
  const { action, title } = props;
  if (typeof action === "string") {
    return (
      <Link className="big-button-new" title={title} to={action}>
        <i className="fa fa-plus" aria-hidden="true"></i>
      </Link>
    );
  } else {
    return (
      <button
        className="big-button-new"
        title={title}
        onClick={(e) => {
          if (typeof action === "function") {
            action();
          }
        }}
      >
        <i className="fa fa-plus" aria-hidden="true"></i>
      </button>
    );
  }
};
