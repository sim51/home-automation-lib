import React from "react";

interface Props {
  loading?: boolean;
}

// from https://theanam.github.io/css-only-loaders/
export const Spinner: React.FC<Props> = (props: Props) => {
  const { loading } = props;

  if (loading || loading === undefined) {
    return (
      <div className="spinner">
        <div className="lds-dual-ring"></div>
      </div>
    );
  } else {
    return null;
  }
};
