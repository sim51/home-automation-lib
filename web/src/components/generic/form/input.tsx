import React, { useState, useEffect } from "react";
import { Help } from "../help";
import { FieldProps } from "./field";

export const FormInput: React.FC<FieldProps<string>> = (props: FieldProps<string>) => {
  const { field, onChange } = props;
  const [value, setValue] = useState<any>("");

  useEffect(() => {
    if (props.value) {
      setValue(props.value);
    }
  }, [props]);

  return (
    <div className="field">
      <div className="label">
        <label htmlFor={field.name}>{field.label || field.name}</label>
        <Help text={field.help} />
      </div>
      <div className="input">
        <input
          {...field}
          defaultValue={value}
          id={field.name}
          onChange={(event) => onChange(event.target.value)}
          autoComplete={field.type === "password" ? "off" : "on"}
        />
      </div>
    </div>
  );
};
