import React, { useState, useEffect } from "react";
import { FormField } from "./field";
import { FieldFragment } from "../../../graphql/types";

interface Props {
  onSubmit: (item: any) => Promise<void>;
  onCancel?: () => void;
  schema: FieldFragment[];
  item: { [key: string]: any };
}

export const HalForm: React.FC<Props> = (props: Props) => {
  const { onSubmit, onCancel, schema, item } = props;
  const [error, setError] = useState<Error>();
  const [model, setModel] = useState<{ [key: string]: any }>(item);

  function onChangeField(key: string): (data: any) => void {
    return (data: any) => {
      setModel(Object.assign({}, model, { [key]: data }));
    };
  }

  useEffect(() => {
    setModel(props.item);
  }, [props]);

  return (
    <form
      onSubmit={async (event) => {
        event.preventDefault();
        try {
          await onSubmit(model);
        } catch (error) {
          setError(error);
        }
      }}
    >
      {error ? <div className="error">{error.toString()}</div> : null}

      <div className="fields">
        {schema.map((field: FieldFragment) => {
          return (
            <FormField key={field.name} field={field} value={model[field.name]} onChange={onChangeField(field.name)} />
          );
        })}
      </div>
      <div className="actions">
        <button title="Submit" type="submit">
          Save
        </button>
        {onCancel && (
          <button
            title="Cancel"
            className="cancel"
            onClick={(event) => {
              onCancel();
              event.preventDefault();
            }}
          >
            Cancel
          </button>
        )}
      </div>
    </form>
  );
};
