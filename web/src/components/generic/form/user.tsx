import React, { useState, useEffect } from "react";
import { FieldProps } from "./field";
import { Help } from "../help";
import { useGetUsersQuery, UserFragment } from "../../../graphql/types";

export const FormUser: React.FC<FieldProps<string>> = (props: FieldProps<string>) => {
  const { field, onChange } = props;
  const [value, setValue] = useState<any>("");
  const { data } = useGetUsersQuery();

  useEffect(() => {
    if (props.value) {
      setValue(props.value);
    }
  }, [props]);

  return (
    <div className="field">
      <div className="label">
        <label htmlFor={field.name}>{field.label || field.name}</label>
        <Help text={field.help} />
      </div>
      <div className="input">
        <div className="select">
          <select {...field} value={value} id={field.name} onChange={(event) => onChange(event.target.value)}>
            <option value="" disabled>
              Make a selection
            </option>
            {data?.data?.map((u: UserFragment) => {
              return (
                <option key={u.username} value={u.username}>
                  {u.fullname}
                </option>
              );
            })}
          </select>
        </div>
      </div>
    </div>
  );
};
