import React, { useEffect, useState } from "react";
import icons from "./icons-solid.json";
import { FieldProps } from "./field";
import { Image } from "../image";
import { ActionWrapper } from "../../wrapper/action";
import { Help } from "../help";

interface Icon {
  name: string;
  label: string;
  unicode: string;
  search: Array<string>;
}

function suggestIcon(search: string): Array<Icon> {
  return icons.filter((icon: Icon) => {
    return (
      icon.name.startsWith(search) ||
      icon.search
        .map((keyword: string) => {
          return keyword.startsWith(search);
        })
        .reduce((acc: boolean, value: boolean) => {
          return acc === true || value === true ? true : false;
        }, false)
    );
  });
}

function toBase64(file: Blob): Promise<any> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

function getTypeFromValue(value?: string): string {
  let type = "fa";
  if (value && value !== "") {
    if (value.includes("<svg") || value.startsWith("data:")) {
      type = "file";
    }
    if (value.startsWith("http")) {
      type = "url";
    }
  }
  return type;
}

export const FormImage: React.FC<FieldProps<string>> = (props: FieldProps<string>) => {
  const { field, onChange } = props;
  // icon suggestions
  const [suggests, setSuggests] = useState<Array<Icon> | null>(null);
  // fa, file, url
  const [type, setType] = useState<string>("fa");
  // field value
  const [value, setValue] = useState<string>("");
  // setting the field value
  useEffect(() => {
    if (props.value) {
      setType(getTypeFromValue(props.value));
      setValue(props.value);
    }
  }, [props]);

  return (
    <div className="field field-image">
      <div className="label">
        <label htmlFor={field.name}>{field.label || field.name}</label>
        <Help text={field.help} />
      </div>
      <div className="input">
        {value && value !== "" && (
          <ActionWrapper actions={[{ name: "Delete", icon: "times", action: () => setValue("") }]}>
            <div className="preview">
              <Image value={value} />
            </div>
          </ActionWrapper>
        )}

        {type === "fa" && (
          <>
            <input
              id={field.name}
              type={"text"}
              value={
                value && !value.startsWith("http") && !value.startsWith("data:") && !value.includes("<svg") ? value : ""
              }
              placeholder={"Search icon"}
              onChange={(event) => {
                setSuggests(suggestIcon(value || ""));
                onChange(event.target.value);
              }}
              onFocus={(e) => {
                setSuggests(suggestIcon(value || ""));
              }}
              onBlur={(e) => {
                setTimeout(() => {
                  setSuggests(null);
                }, 200); //magic number
              }}
            />

            {suggests ? (
              <ul className="autocomplete">
                {suggests.map((icon: Icon, index: number) => {
                  return (
                    <li
                      key={index}
                      value={icon.name}
                      onClick={(e) => {
                        onChange(icon.name);
                      }}
                    >
                      <i className={`fa fa-${icon.name}`}></i> - {icon.name}
                    </li>
                  );
                })}
              </ul>
            ) : null}
          </>
        )}

        {type === "file" && (
          <>
            <input
              id={field.name}
              type={"file"}
              onChange={async (event) => {
                if (event.target.files) {
                  const value = await toBase64(event?.target?.files[0]);
                  onChange(value);
                }
              }}
            />
          </>
        )}
        {type === "url" && (
          <>
            <input
              id={field.name}
              value={value && value.startsWith("http") ? value : ""}
              type={"url"}
              placeholder={"Url of your image"}
              onChange={(event) => {
                onChange(event.target.value);
              }}
            />
          </>
        )}
        <div className="type">
          <input
            type="radio"
            value="fa"
            id="fa"
            onChange={(e) => setType(e.target.value)}
            name="type"
            checked={type === "fa"}
          />
          <label htmlFor="fa">Icon</label>
          <input
            type="radio"
            value="file"
            id="file"
            onChange={(e) => setType(e.target.value)}
            name="type"
            checked={type === "file"}
          />
          <label htmlFor="file">File</label>
          <input
            type="radio"
            value="url"
            id="url"
            onChange={(e) => setType(e.target.value)}
            name="type"
            checked={type === "url"}
          />
          <label htmlFor="url">Url</label>
        </div>
      </div>
    </div>
  );
};
