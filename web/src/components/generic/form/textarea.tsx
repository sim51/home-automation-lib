import React, { useState, useEffect } from "react";
import { FieldProps } from "./field";
import { Help } from "../help";

export const FormTextArea: React.FC<FieldProps<string>> = (props: FieldProps<string>) => {
  const { field, onChange } = props;
  const [value, setValue] = useState<string>("");

  useEffect(() => {
    if (props.value) {
      setValue(props.value);
    }
  }, [props]);
  return (
    <div className="field">
      <div className="label">
        <label htmlFor={field.name}>{field.label || field.name}</label>
        <Help text={field.help} />
      </div>
      <div className="input">
        <textarea
          {...field}
          id={field.name}
          value={value}
          onChange={(event) => {
            onChange(event.target.value);
          }}
        />
      </div>
    </div>
  );
};
