import React from "react";
import { FormInput } from "./input";
import { FormTextArea } from "./textarea";
import { FormImage } from "./image";
import { FormLocation } from "./location";
import { FormComponent } from "./component";
import { FormUser } from "./user";
import { FieldFragment } from "../../../graphql/types";

export interface FieldProps<T> {
  field: FieldFragment;
  value?: T;
  onChange: (data: any) => void;
}

export const FormField: React.FC<FieldProps<any>> = (props: FieldProps<any>) => {
  const fieldProps = props;

  switch (props.field.type) {
    case "textarea":
      return <FormTextArea {...fieldProps} />;
    case "location":
      return <FormLocation {...fieldProps} />;
    case "component":
      return <FormComponent {...fieldProps} />;
    case "image":
      return <FormImage {...fieldProps} />;
    case "user":
      return <FormUser {...fieldProps} />;
    default:
      return <FormInput {...fieldProps} />;
  }
};
