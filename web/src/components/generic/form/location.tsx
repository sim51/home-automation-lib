import React, { useState, useEffect } from "react";
import L from "leaflet";
import { MapContainer, TileLayer, Marker, MapConsumer } from "react-leaflet";
import { FieldProps } from "./field";
import { Help } from "../help";

export const FormLocation: React.FC<FieldProps<{ longitude: number; latitude: number }>> = (
  props: FieldProps<{ longitude: number; latitude: number }>,
) => {
  const { field, onChange, value } = props;
  const icon = L.divIcon({
    iconSize: [25, 40],
    iconAnchor: [13, 40],
    className: "leaflet-default-icon-path",
  });
  const position: [number, number] = [value ? value.latitude || 0 : 0, value ? value.longitude || 0 : 0];
  const defaultZoom: number = value ? 15 : 0;
  const [zoom, setZoom] = useState<number>(defaultZoom);

  useEffect(() => {
    if (!value || (value.latitude === 0 && value.longitude === 0)) {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          onChange({ longitude: position.coords.longitude, latitude: position.coords.latitude });
          setZoom(15);
        });
      }
    }
  }, [value, onChange]);

  return (
    <div className="field">
      <div className="label">
        <label>{field.label || field.name}</label>
        <Help text={field.help} />
      </div>
      <div className="input">
        <MapContainer center={position} zoom={zoom || defaultZoom} scrollWheelZoom={true}>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={position} icon={icon} draggable={false} />
          <MapConsumer>
            {(map) => {
              map.on("zoomlevelschange", (e) => setZoom(e.target._zoom));
              map.on("moveend", (e) => {
                setZoom(e.target._zoom);
              });
              map.on("click", (e: any) => {
                map.setView(e.latlng, zoom);
                onChange({ longitude: e.latlng.lng, latitude: e.latlng.lat });
              });
              return null;
            }}
          </MapConsumer>
        </MapContainer>
      </div>
    </div>
  );
};
