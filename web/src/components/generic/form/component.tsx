import React, { useState, useEffect } from "react";
import { uniqBy, orderBy } from "lodash";
import { Help } from "../help";
import { FieldProps } from "./field";
import { ComponentFragment, useGetComponentsQuery } from "../../../graphql/types";

export const FormComponent: React.FC<FieldProps<string>> = (props: FieldProps<string>) => {
  const { field, onChange, value } = props;

  // Get all the components
  const { data } = useGetComponentsQuery({});
  // State
  const [typeFilter, setTypeFilter] = useState<string>("");
  const [types, setTypes] = useState<Array<{ id: string; name: string }>>([]);
  const [components, setComponents] = useState<Array<{ id: string; name: string; type: string }>>([]);

  useEffect(() => {
    if (data && data.data) {
      setTypes(
        orderBy(
          uniqBy(
            data.data.map((component: ComponentFragment) => {
              return { id: component.type.id, name: component.type.name };
            }),
            (e) => e.id,
          ),
          ["name"],
          ["asc"],
        ),
      );
      setComponents(
        orderBy(
          data.data.map((component: ComponentFragment) => {
            return { id: component.id, name: component.name, type: component.type.id };
          }),
          ["name"],
          ["asc"],
        ),
      );
    }
  }, [data]);

  function onTypeFilterChange(id: string) {
    setTypeFilter(id);
    const selectedComponent = components.filter((e) => e.type === id)[0];
    if (selectedComponent) {
      onChange(selectedComponent.id);
    }
  }

  return (
    <div className="field">
      <div className="label">
        <label htmlFor={field.name}>{field.label || field.name}</label>
        <Help text={field.help} />
      </div>
      <div className="input">
        <div className="select">
          <select className="filter" onChange={(e) => onTypeFilterChange(e.target.value)}>
            <option value="" selected={value === null || value === ""}>
              Filter by type ...
            </option>
            {types.map((e) => {
              return (
                <option key={e.id} value={e.id}>
                  {e.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="select">
          <select
            id={field.name}
            value={value}
            onChange={(event) => {
              onChange(event.target.value);
            }}
          >
            <option value="" selected={value === null || value === ""}>
              Select a component
            </option>
            {components
              .filter((e) => e.type.includes(typeFilter))
              .map((e) => {
                return (
                  <option key={e.id} value={e.id}>
                    {e.name}
                  </option>
                );
              })}
          </select>
        </div>
      </div>
    </div>
  );
};
