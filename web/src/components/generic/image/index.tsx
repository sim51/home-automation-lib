import React from "react";

interface Props {
  value?: string;
  alt?: string;
  onHoverStart?: (e: any) => void;
  onHoverEnd?: (e: any) => void;
}

export const Image: React.FC<Props> = (props: Props) => {
  const { value, alt, onHoverStart, onHoverEnd } = props;
  return (
    <>
      {value && value !== "" && value.includes("<svg ") && (
        <div className="image">
          <i
            dangerouslySetInnerHTML={{ __html: value }}
            onPointerEnter={onHoverStart}
            onPointerMove={onHoverStart}
            onPointerLeave={onHoverEnd}
          ></i>
        </div>
      )}
      {value && value !== "" && (value.startsWith("http") || value.startsWith("data:")) && (
        <div className="image">
          <img
            src={value}
            alt={alt}
            onPointerEnter={onHoverStart}
            onPointerMove={onHoverStart}
            onPointerLeave={onHoverEnd}
          />
        </div>
      )}
      {value && value !== "" && !value.includes("<svg ") && !value.startsWith("http") && !value.startsWith("data:") && (
        <div className="image">
          <i
            className={`fa fa-${value}`}
            onPointerEnter={onHoverStart}
            onPointerMove={onHoverStart}
            onPointerLeave={onHoverEnd}
          ></i>
        </div>
      )}
    </>
  );
};
