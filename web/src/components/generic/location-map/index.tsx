import React from "react";
import { MapContainer, TileLayer, Marker } from "react-leaflet";
import L from "leaflet";

interface Props {
  longitude?: number;
  latitude?: number;
  zoom?: number;
}

export const LocationMap: React.FC<Props> = ({ longitude = 0, latitude = 0, zoom = 13 }: Props) => {
  const position: [number, number] = [latitude, longitude];
  const icon = L.divIcon({
    iconSize: [25, 40],
    iconAnchor: [13, 40],
    className: "leaflet-default-icon-path",
  });
  return (
    <MapContainer center={position} zoom={zoom}>
      <TileLayer
        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={position} icon={icon} />
    </MapContainer>
  );
};
