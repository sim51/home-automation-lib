import React from "react";
import { HeatMapDataType } from "./index";
import { roundNumber } from "../../../utils";

export const DefaultTooltip: React.FC<{ data: HeatMapDataType; min: number; max: number }> = ({ data, min, max }) => {
  return (
    <>
      <h6>{data.label}</h6>
      <ul>
        <li>
          <span className="property">Value:</span>
          <span className="value">
            {data.value !== null && data.value !== undefined ? roundNumber(data.value) : "no data"}
          </span>
        </li>
        <li>
          <span className="property">Min:</span>
          <span className="value">{roundNumber(min)}</span>
        </li>
        <li>
          <span className="property">Max:</span>
          <span className="value">{roundNumber(max)}</span>
        </li>
      </ul>
    </>
  );
};
