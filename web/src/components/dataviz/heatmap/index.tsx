import React, { useEffect, useState, useRef } from "react";
import { minBy, maxBy } from "lodash";
import chroma, { Scale } from "chroma-js";

import { tooltipPosition } from "../../../utils";
import { DefaultTooltip } from "./tooltip";

// Data definition
export type HeatMapDataType = {
  label: string;
  value?: number | null;
};

// Props definition
interface Props {
  className?: string;
  // Color that is used in the heatmap (with opacity)
  colors: Array<string>;
  // Color that is used in the heatmap when there is no data (oterwise it's opacity 0)
  noDataColor?: string;
  // If this props is not set, we display square dynamically (so based on the width and the number of item)
  height?: string;
  // The min data value. If not set the value is computed from the data
  min?: number;
  // The max data value. If not set the value is computed from the data
  max?: number;
  // The data for the heatmap.
  // It's an ordered array of label/value where label is the x coordinate, and value the y
  data: Array<HeatMapDataType>;
  // What we do when we click (or double tap) on an item of the viz
  onClick?: (e: HeatMapDataType) => void;
  // What we do when we do when we ends the selection ?
  onSelectionEnd?: (from: HeatMapDataType, to: HeatMapDataType) => void;
  // Tooltip
  Tooltip?: React.FC<{ data: HeatMapDataType; min: number; max: number }>;
}

// Check if the index is in the range defined by from/to
function isInSelection(index: number, range: [number, number] | null): boolean {
  let result = false;
  if (range) {
    const from: number = range[0];
    const to: number = range[1];
    if (from <= to && from <= index && index <= to) result = true;
    if (from > to && to <= index && index <= from) result = true;
  }
  return result;
}

function colorScale(scale: Scale, e: number | null, noColor?: string): string {
  if (e === null) {
    return noColor || "#FFFFFF";
  }
  return scale(e).hex();
}

export const HeatMap: React.FC<Props> = (props: Props) => {
  // constant
  const { className, colors, noDataColor, onClick, onSelectionEnd, Tooltip } = props;
  const tooltipRef = useRef<HTMLDivElement | null>(null);

  // State
  const [data, setData] = useState<Array<HeatMapDataType>>([]);
  const [min, setMin] = useState<number>(-Infinity);
  const [max, setMax] = useState<number>(Infinity);
  const [height, setHeight] = useState<string>("0px");
  const [width, setWidth] = useState<string>("0px");
  const [selected, setSelected] = useState<number | null>(null);
  const [selection, setSelection] = useState<[number, number] | null>(null);
  const [chromaScale, setChromaSacle] = useState<Scale>(chroma.scale());

  // On new props we reset the state
  useEffect(() => {
    setSelected(null);
    setSelection(null);
    setData(props.data);
    setMin(props.min !== undefined && props.min !== null ? props.min : minBy(props.data, "value")?.value || -Infinity);
    setMax(props.max !== undefined && props.max !== null ? props.max : maxBy(props.data, "value")?.value || Infinity);

    const size = props.data.length > 0 ? `${100 / props.data.length}%` : "0px";
    setWidth(size);
    setHeight(props.height ? props.height : size);
  }, [props.data, props.min, props.max, props.height]);

  useEffect(() => {
    setChromaSacle((prev) => {
      if (min !== -Infinity && max !== Infinity) {
        return chroma.scale(colors.map((c) => c.replace("#", ""))).domain([min, max]);
      } else {
        return chroma.scale();
      }
    });
  }, [min, max, colors]);

  // Helper function to display the tooltip
  function displayToolTip(x: number, y: number): void {
    if (tooltipRef.current) {
      tooltipRef.current.style.visibility = "visible";
      tooltipPosition([x, y], tooltipRef.current);
    }
  }

  return (
    <div className={`dataviz heatmap ${className || ""}`}>
      <div
        className="data"
        onMouseEnter={(e) => {
          displayToolTip(e.nativeEvent.x, e.nativeEvent.y);
        }}
        onMouseLeave={() => {
          setSelected(null);
        }}
        onMouseOver={(e) => {
          displayToolTip(e.nativeEvent.x, e.nativeEvent.y);
        }}
        onTouchStart={(e) => {
          displayToolTip(e.nativeEvent.touches[0].clientX, e.nativeEvent.touches[0].clientY);
        }}
        onTouchEnd={(e) => {
          setSelected(null);
        }}
        onTouchMove={(e) => {
          const overElement: HTMLElement = document.elementFromPoint(
            e.nativeEvent.touches[0].clientX,
            e.nativeEvent.touches[0].clientY,
          ) as HTMLElement;
          if (overElement && overElement.attributes.getNamedItem("data-index")) {
            setSelected(parseInt(overElement.attributes!.getNamedItem("data-index")!.value));
            displayToolTip(e.nativeEvent.touches[0].clientX, e.nativeEvent.touches[0].clientY);
          } else {
            setSelected(null);
          }
        }}
      >
        {data.map((item, index) => (
          <div
            key={index}
            className={isInSelection(index, selection) ? "selected square" : "square"}
            data-index={index}
            style={{
              width: width,
              height: height,
            }}
            onMouseDown={(e) => {
              setSelected(null);
              // if selection monde is on
              if (onSelectionEnd) setSelection([index, index]);
            }}
            onMouseUp={(e) => {
              // if selection monde is on
              if (onSelectionEnd && selection) {
                const from = selection[0] < selection[1] ? selection[0] : selection[1];
                const to = selection[0] < selection[1] ? selection[1] : selection[0];
                const rangeInf = from - 1 >= 0 ? from - 1 : 0;
                const rangeSup = to + 1 <= data.length - 1 ? to + 1 : data.length - 1;
                onSelectionEnd(data[rangeInf], data[rangeSup]);
              }
              setSelection(null);
            }}
            onMouseOver={(e) => {
              if (selection !== null) {
                setSelection((selection) => {
                  if (selection) {
                    return [selection[0], index];
                  }
                  return null;
                });
              } else {
                setSelected(index);
              }
            }}
            onTouchStart={(e) => {
              setSelected(index);
            }}
            onClick={(e) => {
              if (onClick) onClick(data[index]);
            }}
          >
            <div
              style={{
                backgroundColor:
                  item.value === undefined || item.value === null
                    ? noDataColor || colors[0]
                    : colorScale(chromaScale, item.value, noDataColor),
              }}
            />
          </div>
        ))}
      </div>

      {selected !== null && (
        <div className="tooltip" ref={tooltipRef}>
          {Tooltip ? (
            <Tooltip data={data[selected]} min={min} max={max} />
          ) : (
            <DefaultTooltip data={data[selected]} min={min} max={max} />
          )}
        </div>
      )}
    </div>
  );
};
