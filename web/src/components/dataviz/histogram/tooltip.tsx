import React from "react";
import { HistogramDataType } from "./index";
import { roundNumber } from "../../../utils";

export const DefaultTooltip: React.FC<{ data: HistogramDataType }> = ({ data }) => {
  return (
    <>
      <h6>{data.label}</h6>
      <ul>
        {data.data.map((data) => (
          <li key={data.label}>
            <span className="property">{data.label}:</span>
            <span className="value">{roundNumber(data.value)}</span>
          </li>
        ))}
      </ul>
    </>
  );
};
