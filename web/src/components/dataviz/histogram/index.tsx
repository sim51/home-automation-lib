import React, { useEffect, useRef, useState } from "react";
import { getElementPosition, roundNumber, tooltipPosition } from "../../../utils";
import { DefaultTooltip } from "./tooltip";

/**
 * Check if the index is in the defined range.
 */
function isInSelection(index: number, range: [number, number] | null): boolean {
  let result = false;
  if (range) {
    const from: number = range[0];
    const to: number = range[1];
    if (from <= to && from <= index && index <= to) result = true;
    if (from > to && to <= index && index <= from) result = true;
  }
  return result;
}

/**
 * Helper function to display the tooltip
 */
function displayToolTip(element: { current: HTMLDivElement | null }, x: number, y: number): void {
  if (element.current) {
    element.current.style.visibility = "visible";
    tooltipPosition([x, y], element.current);
  }
}

// Data definition
export type HistogramDataType = {
  // Name of the column
  label: string;
  // The column values
  data: Array<{ label: string; value: number }>;
};

// Props definition
interface Props {
  className?: string;
  // The data of the histogram. It's an ordered the array of column
  data: Array<HistogramDataType>;
  // Hasmap of colros for the column's values
  colors: { [label: string]: string };
  // The height of the dataviz. If not set it takes 100% of its container's height.
  height?: string;
  // The y-axis min data value. If not set the value is computed from the data
  min?: number;
  // The y-axis max data value. If not set the value is computed from the data
  max?: number;
  // What we do when we click (or double tap) on an item of the viz
  onClick?: (e: HistogramDataType) => void;
  // What we do when we do when we ends the selection ?
  onSelectionEnd?: (from: HistogramDataType, to: HistogramDataType) => void;
  // Tooltip
  Tooltip?: React.FC<{ data: HistogramDataType }>;
}

export const Histogram: React.FC<Props> = (props: Props) => {
  // constant
  const { className, colors, height, onClick, Tooltip, onSelectionEnd } = props;
  const tooltipRef = useRef<HTMLDivElement | null>(null);
  const chartRef = useRef<HTMLDivElement | null>(null);

  // State
  const [data, setData] = useState<Array<HistogramDataType>>([]);
  const [min, setMin] = useState<number>(-Infinity);
  const [max, setMax] = useState<number>(Infinity);
  const [width, setWidth] = useState<string>("0px");
  const [selected, setSelected] = useState<number | null>(null);
  const [selection, setSelection] = useState<[number, number] | null>(null);

  // On new props
  //  => we reset the state
  useEffect(() => {
    setSelected(null);
    setSelection(null);
    setData(props.data);
    setWidth(props.data.length > 0 ? `${100 / props.data.length}%` : "0px");
    let max = -Infinity;
    let min = Infinity;
    if (props.min === undefined || props.max === undefined) {
      props.data.forEach((column) => {
        const sum = column.data.reduce((acc, curr) => acc + curr.value, 0);
        if (sum > max) max = sum;
        if (sum < min) min = sum;
      });
    }
    setMin(props.min !== undefined ? props.min : min);
    setMax(props.max !== undefined ? props.max : max);
  }, [props.data, props.min, props.max, props.height]);

  // When selection is in progress
  // => we register an on move on the document to track the mouse
  // => we register an mouseup to finalize the selection
  useEffect(() => {
    if (onSelectionEnd && chartRef.current && selection) {
      // function that handle the mouse move and track the closest item for the selection
      const mouseMoveHandler = (e: MouseEvent) => {
        const chartWidth = (chartRef.current as HTMLElement).clientWidth;
        const chartStartX = getElementPosition(chartRef.current as HTMLElement)[0];
        const chartEndX = chartStartX + chartWidth;
        const mouseX = e.clientX;

        // Mouse if before the element => select 0
        if (mouseX <= chartStartX) {
          setSelection((selection) => {
            if (selection) {
              return [selection[0], 0];
            }
            return null;
          });
        }

        // Mouse if after the element => take last item
        if (mouseX >= chartEndX) {
          setSelection((selection) => {
            if (selection) {
              return [selection[0], data.length - 1];
            }
            return null;
          });
        }

        // Mouse if in the element range => search the closest item
        if (chartStartX < mouseX && mouseX < chartEndX) {
          setSelection((selection) => {
            if (selection) {
              return [selection[0], Math.round((mouseX - chartStartX) / (chartWidth / data.length))];
            }
            return null;
          });
        }
      };

      // function to finalized the selection
      const mouseUpHandler = () => {
        // if selection monde is on
        if (onSelectionEnd && selection) {
          // Searching the data for the inf/sup border
          // Note : a selection can start by selecting the up border, so we need to reorder
          const from = selection[0] < selection[1] ? selection[0] : selection[1];
          const to = selection[0] < selection[1] ? selection[1] : selection[0];
          // Calling the function with the data that match  the borders
          onSelectionEnd(data[from], data[to]);
        }
        // reset the selection
        setSelection(null);
      };

      document.addEventListener("mousemove", mouseMoveHandler as EventListenerOrEventListenerObject);
      document.addEventListener("mouseup", mouseUpHandler as EventListenerOrEventListenerObject);
      // cleanup
      return () => {
        document.removeEventListener("mousemove", mouseMoveHandler as EventListenerOrEventListenerObject);
        document.removeEventListener("mouseup", mouseUpHandler as EventListenerOrEventListenerObject);
      };
    }
  }, [chartRef, data, selection, onSelectionEnd]);

  return (
    <div className={`dataviz histogram ${className || ""}`} ref={chartRef}>
      <div className="legend">
        <ul>
          {Object.entries(colors).map((color: [string, string]) => (
            <li key={color[0]}>
              <div style={{ backgroundColor: color[1] }}></div>
              {color[0]}
            </li>
          ))}
        </ul>
      </div>
      <div className={selection ? "selection data" : "data"} style={{ height: height ? height : "100%" }}>
        <div className="axis y">
          <span>{roundNumber(max)}</span>
          <span>{roundNumber(min)}</span>
        </div>
        {data.map((column, index) => (
          <div
            key={index}
            data-index={index}
            className={isInSelection(index, selection) ? "selected column" : "column"}
            style={{ width: width }}
            onClick={(e) => {
              if (onClick) onClick(column);
            }}
            onMouseDown={(e) => {
              // if selection mode is on
              if (onSelectionEnd) {
                setSelection([index, index]);
              }
            }}
            onMouseOver={(e) => {
              if (!selection) {
                displayToolTip(tooltipRef, e.nativeEvent.x, e.nativeEvent.y);
              }
            }}
            onTouchStart={(e) => {
              setSelected(index);
              displayToolTip(tooltipRef, e.nativeEvent.touches[0].clientX, e.nativeEvent.touches[0].clientY);
            }}
            onTouchEnd={(e) => {
              setSelected(null);
            }}
            onTouchMove={(e) => {
              const overElement: HTMLElement = document.elementFromPoint(
                e.nativeEvent.touches[0].clientX,
                e.nativeEvent.touches[0].clientY,
              ) as HTMLElement;
              if (overElement && overElement.attributes.getNamedItem("data-object")) {
                setSelected(parseInt(overElement.attributes!.getNamedItem("data-index")!.value));
                displayToolTip(tooltipRef, e.nativeEvent.touches[0].clientX, e.nativeEvent.touches[0].clientY);
              } else {
                setSelected(null);
              }
            }}
          >
            {column.data.map((value) => (
              <div
                key={value.label}
                style={{
                  height: `${((value.value - min) / max) * 100}%`,
                  backgroundColor: colors[value.label] || "grey",
                }}
                onMouseMove={(e) => {
                  displayToolTip(tooltipRef, e.nativeEvent.x, e.nativeEvent.y);
                }}
              ></div>
            ))}
          </div>
        ))}
      </div>
      {selected !== null && (
        <div className="tooltip" ref={tooltipRef}>
          {Tooltip ? <Tooltip data={data[selected]} /> : <DefaultTooltip data={data[selected]} />}
        </div>
      )}
    </div>
  );
};
