import React from "react";
import { ActionWrapper } from "./action";

interface Props {
  opened?: boolean;
  close: () => void;
}

export const PopinWrapper: React.FC<Props> = (props: React.PropsWithChildren<Props>) => {
  const { opened, close, children } = props;

  if (!opened) {
    return null;
  } else {
    return (
      <div className="popin-overlay" onClick={close}>
        <ActionWrapper actions={[{ name: "Close", icon: "times", action: close }]}>
          <div
            className="container popin-container"
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            {children}
          </div>
        </ActionWrapper>
      </div>
    );
  }
};
