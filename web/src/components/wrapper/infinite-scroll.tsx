import React, { useEffect } from "react";

interface Props {
  action: () => void;
  className: string;
  threshold?: number;
}

export const InfiniteScrollWrapper: React.FC<Props> = (props: React.PropsWithChildren<Props>) => {
  const { action, children, className } = props;
  const threshold = props.threshold || 100;

  // Register listerner on window for scroll
  useEffect(() => {
    const scrollHandler = () => {
      if (document.documentElement.scrollHeight - window.innerHeight - document.documentElement.scrollTop < threshold) {
        action();
      }
    };
    window.addEventListener("scroll", scrollHandler);
    // cleanup
    return () => {
      document.removeEventListener("scroll", scrollHandler);
    };
  }, [action, threshold]);
  return <div className={className}>{children}</div>;
};
