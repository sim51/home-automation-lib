import React, { useRef } from "react";
import { toPairs } from "lodash";
import { Image } from "../generic/image";
import { roundNumber, tooltipPosition } from "../../utils";

interface Props {
  icon?: string;
  className?: string;
  data?: { [key: string]: any };
}

export const BoxWrapper: React.FC<Props> = (props: React.PropsWithChildren<Props>) => {
  const { icon, children, className, data } = props;
  const tooltip = useRef<HTMLDivElement | null>(null);

  const dataAttributes: { [key: string]: any } = {};
  if (data) {
    toPairs(data).forEach((n: [string, any]) => {
      dataAttributes[`data-${n[0].toLowerCase()}`] = n[1];
    });
  }

  const onHoverStart = (e: PointerEvent) => {
    if (tooltip.current) {
      tooltip.current.style.visibility = "visible";
      tooltipPosition([e.clientX, e.clientY], tooltip.current);
    }
  };
  const onHoverEnd = (e: PointerEvent) => {
    if (tooltip.current) {
      tooltip.current.style.visibility = "hidden";
    }
  };

  return (
    <>
      <div {...dataAttributes} className={`box ${className || ""}`}>
        {icon && icon !== "" && <Image value={icon} onHoverStart={onHoverStart} onHoverEnd={onHoverEnd} />}
        <div className="content">{children}</div>
      </div>
      {data ? (
        <div className="tooltip" ref={tooltip}>
          <h6>Current Status</h6>
          <ul>
            {Object.keys(data)
              .filter((e) => e !== "time" && e !== "id")
              .map((key) => (
                <li key={key}>
                  <span className="property">{key}:</span>
                  <span className="value">{roundNumber(data[key])}</span>
                </li>
              ))}
          </ul>
        </div>
      ) : null}
    </>
  );
};
