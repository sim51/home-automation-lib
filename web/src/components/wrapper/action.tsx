import React from "react";
import { Link } from "react-router-dom";

type ActionFunction = () => void | Promise<void>;

export interface Action {
  name: string;
  icon: string;
  action: ActionFunction | string;
}

interface Props {
  actions?: Array<Action>;
  onClick?: ActionFunction | string;
}

export const ActionWrapper: React.FC<Props> = (props: React.PropsWithChildren<Props>) => {
  const { onClick, actions, children } = props;

  return (
    <div className="action-wrapper">
      {children}
      <div className="action-wrapper-menu">
        {actions && actions.length > 0 && (
          <div className="action-wrapper-menu-top-right">
            {actions.map((action: Action, index: number) => {
              if (typeof action.action === "string") {
                return (
                  <Link key={index} title={action.name} to={action.action}>
                    <i className={`fa fa-${action.icon}`} aria-hidden="true"></i>
                  </Link>
                );
              } else {
                return (
                  <button
                    key={index}
                    title={action.name}
                    onClick={(e) => {
                      if (typeof action.action === "function") {
                        action.action();
                      }
                    }}
                  >
                    <i className={`fa fa-${action.icon}`} aria-hidden="true"></i>
                  </button>
                );
              }
            })}
          </div>
        )}
        {onClick && (
          <div className="action-wrapper-menu-right">
            <Link
              title="View detail"
              to={typeof onClick === "string" ? onClick : "#"}
              onClick={
                typeof onClick === "function"
                  ? (e) => {
                      onClick();
                      e.stopPropagation();
                    }
                  : undefined
              }
            >
              <i className={`fa fa-chevron-right`} aria-hidden="true"></i>
            </Link>
          </div>
        )}
      </div>
    </div>
  );
};
