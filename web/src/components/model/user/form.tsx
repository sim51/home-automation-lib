import React from "react";
import { HalForm } from "../../generic/form";
import { UserFragment } from "../../../graphql/types";
import type { Field } from "hal";

export interface User extends UserFragment {
  password?: string;
  confirmation?: string;
}

export const fields: Field[] = [
  {
    name: "username",
    placeholder: "Username",
    required: true,
  },
  {
    name: "fullname",
    placeholder: "Full name",
  },
  {
    name: "avatar",
    type: "image",
  },
  {
    name: "password",
    type: "password",
  },
  {
    name: "confirmation",
    type: "password",
  },
];

interface Props {
  user?: User | null;
  onSubmit: (item: User) => Promise<void>;
  onCancel?: () => void;
}
export const UserForm: React.FC<Props> = (props: Props) => {
  const { user, onSubmit, onCancel } = props;
  return (
    <HalForm
      onSubmit={onSubmit}
      onCancel={onCancel}
      schema={fields}
      item={Object.assign(user ? user : {}, { password: null, confirmation: null })}
    />
  );
};
