import React, { useState, useEffect } from "react";
import { User, getAuthenticatedUser } from "../../../user";
import { Link } from "../../../router/link";

interface Props {
  user: User | null;
}

export const Avatar: React.FC<Props> = (props: Props) => {
  const { user } = props;
  const currentUser = getAuthenticatedUser();
  const [classname, setClassname] = useState<string>("");

  useEffect(() => {
    if (user && user?.isPresent !== null && user?.isPresent !== undefined) {
      setClassname(user.isPresent ? "present" : "absent");
    }
  }, [user]);

  if (!user) return null;
  return (
    <div className={`avatar ${classname}`}>
      {currentUser?.username === user?.username ? (
        <Link id="account">
          {user.avatar ? (
            <img src={user.avatar} alt={`${user.username}'s avatar`} />
          ) : (
            <i className={`fa fa-user`} aria-hidden="true"></i>
          )}
        </Link>
      ) : (
        <>
          {user.avatar ? (
            <img src={user.avatar} alt={`${user.username}'s avatar`} />
          ) : (
            <i className={`fa fa-user`} aria-hidden="true"></i>
          )}
        </>
      )}
    </div>
  );
};
