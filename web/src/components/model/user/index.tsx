import React from "react";
import { User } from "../../../user";
import { BoxWrapper } from "../../wrapper/box";

interface Props {
  user: User | null;
}

export const UserBox: React.FC<Props> = (props: Props) => {
  const { user } = props;

  return (
    <BoxWrapper className={"user"} icon={user?.avatar || "user"}>
      <div className="info">
        <h3>{user?.username}</h3>
        <ul className="metadata">
          {user?.fullname !== null && (
            <li>
              <span>Full name:</span>
              {user?.fullname}
            </li>
          )}
        </ul>
      </div>
    </BoxWrapper>
  );
};
