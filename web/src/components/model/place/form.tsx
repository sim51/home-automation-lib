import React from "react";
import { v4 as uuid } from "uuid";
import { HalForm } from "../../generic/form";
import { PlaceFragment } from "../../../graphql/types";
import type { Field } from "hal";

export const fields: Field[] = [
  {
    name: "name",
    placeholder: "Home",
    required: true,
  },
  {
    name: "icon",
    required: true,
    type: "image",
  },
  {
    name: "description",
    placeholder: "My principal home",
    type: "textarea",
  },
  {
    name: "address",
    placeholder: "21 Jump Street",
  },
  {
    name: "postal_code",
    placeholder: "75000",
  },
  {
    name: "city",
    placeholder: "Paris",
  },
  {
    name: "country",
    placeholder: "FRANCE",
  },
  {
    name: "location",
    type: "location",
  },
];

interface Props {
  place?: PlaceFragment;
  onSubmit: (item: PlaceFragment) => Promise<void>;
  onCancel?: () => void;
}
export const PlaceForm: React.FC<Props> = (props: Props) => {
  const { place, onSubmit, onCancel } = props;

  return (
    <HalForm
      onSubmit={onSubmit}
      onCancel={onCancel}
      schema={fields}
      item={
        place
          ? place
          : {
              id: uuid(),
              icon: "home",
              name: null,
              description: null,
              address: null,
              postal_code: null,
              city: null,
              country: null,
              location: null,
            }
      }
    />
  );
};
