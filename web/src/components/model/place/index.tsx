import React from "react";
import { PlaceFragment } from "../../../graphql/types";
import { LocationMap } from "../../generic/location-map";
import { BoxWrapper } from "../../wrapper/box";

interface Props {
  item: PlaceFragment;
}

export const PlaceBox: React.FC<Props> = (props: Props) => {
  const { item } = props;

  return (
    <BoxWrapper className="place" icon={item.icon}>
      <div className="info">
        <h3>{item.name}</h3>
        <p>{item.description}</p>
        <div className="address">
          {item.address} <br />
          {item.postal_code} {item.city} <br />
          {item.country}
        </div>
      </div>
      {item.location ? (
        <div
          className="map"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <LocationMap longitude={item.location.longitude || 0} latitude={item.location.latitude || 0} />
        </div>
      ) : null}
    </BoxWrapper>
  );
};
