import React from "react";
import { v4 as uuid } from "uuid";
import { HalForm } from "../../generic/form";
import { RoomFragment } from "../../../graphql/types";
import type { Field } from "hal";

export const fields: Field[] = [
  {
    name: "name",
    placeholder: "Home",
    required: true,
  },
  {
    name: "icon",
    required: true,
    type: "image",
  },
  {
    name: "description",
    placeholder: "My principal home",
    type: "textarea",
  },
];

interface Props {
  room?: RoomFragment;
  onSubmit: (item: RoomFragment) => Promise<void>;
  onCancel?: () => void;
}
export const RoomForm: React.FC<Props> = (props: Props) => {
  const { room, onSubmit, onCancel } = props;
  return (
    <HalForm
      onSubmit={onSubmit}
      onCancel={onCancel}
      schema={fields}
      item={room ? room : { id: uuid(), icon: "home", name: "", description: null }}
    />
  );
};
