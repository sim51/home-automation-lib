import React from "react";
import { BoxWrapper } from "../../wrapper/box";
import { RoomFragment } from "../../../graphql/types";

interface Props {
  item: RoomFragment;
}

export const RoomBox: React.FC<Props> = (props: Props) => {
  const { item } = props;
  return (
    <BoxWrapper className="room" icon={item.icon || ""}>
      <div className="info">
        <h3>{item.name}</h3>
        <p>{item.description}</p>
      </div>
    </BoxWrapper>
  );
};
