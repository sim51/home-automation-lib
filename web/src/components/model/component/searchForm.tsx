import React from "react";
import { HalForm } from "../../generic/form";
import type { Field } from "hal";

export const fields: Field[] = [
  {
    name: "component",
    placeholder: "Type name ...",
    required: true,
    type: "component",
  },
];

interface Props {
  componentId?: string;
  onSubmit: (id: string) => Promise<void>;
  onCancel?: () => void;
}

export const SearchComponentForm: React.FC<Props> = (props: Props) => {
  const { componentId, onSubmit, onCancel } = props;

  return (
    <HalForm
      onSubmit={(item) => onSubmit(item.component)}
      onCancel={onCancel}
      schema={fields}
      item={{ component: componentId }}
    />
  );
};
