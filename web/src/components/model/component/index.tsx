import React, { useState } from "react";
import { isObject, pick } from "lodash";
import { useGraphqlMutationParam } from "../../../hooks/graph-mutation-param";
import { BoxWrapper } from "../../wrapper/box";
import { PopinWrapper } from "../../../components/wrapper/popin";
import { ComponentFragment, useComponentActionMutation } from "../../../graphql/types";
import { HalForm } from "../../../components/generic/form";
import type { Field } from "hal";

interface Props {
  item: ComponentFragment;
  stopPolling?: () => void;
  startPolling?: () => void;
}

interface Action {
  name: string;
  description: string;
  params: Array<Field>;
}
export const ComponentBox: React.FC<Props> = (props: Props) => {
  // Component props
  const { item, stopPolling, startPolling } = props;

  // Class computation
  let componentClass = "component";
  if (item.started !== undefined && item.started !== null) {
    if (item.started) {
      componentClass += " component-started";
    } else {
      componentClass += " component-stopped";
    }
  }

  //
  // State
  // ---------------------------------------------------------------------------
  // Store the last action
  const [action, setAction] = useState<Action | null>(null);

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  const [execAction, { loading, error }] = useComponentActionMutation(mutationParam);

  //
  // Actions
  // ---------------------------------------------------------------------------
  function actionCall(act: Action) {
    if (act.params && act.params.length > 0) {
      setAction(act);
      if (stopPolling) stopPolling();
    } else {
      doAction(act, {});
    }
  }
  async function doAction(act: Action | null, params: any) {
    if (act !== null) {
      await execAction({ variables: { id: item.id, action: act?.name, params: params } });
      setAction(null);
      if (startPolling) startPolling();
    }
  }

  return (
    <>
      <BoxWrapper data={item.measure} className={componentClass} icon={item.icon}>
        <div className="info">
          <h3>{item.name}</h3>
          {item.description && <p className="description">{item.description}</p>}

          {item.type.actions && item.type.actions.length > 0 && (
            <div className="actions">
              {item.type.actions?.map((action: any) => {
                return (
                  <button
                    key={action.name}
                    className="badge badge-warning"
                    title={action.description}
                    onClick={() => {
                      actionCall(action);
                    }}
                  >
                    {action.name}
                    {action.name === action && loading && <i className="fas fa-circle-notch"></i>}
                    {action.name === action && error && (
                      <i
                        className="fas fa-exclamation"
                        title={error.graphQLErrors.map((error) => error.message).join(" - ")}
                      ></i>
                    )}
                  </button>
                );
              })}
            </div>
          )}

          <ul className="metadata">
            {item.type?.categories?.length > 0 && (
              <li>
                <span>Tags:</span>
                {item.type?.categories?.map((c) => c.name).join(", ")}
              </li>
            )}
            {item.config &&
              Object.keys(item.config).map((key) => {
                return (
                  <li key={key}>
                    <span>{key}:</span>
                    {isObject(item.config[key]) ? JSON.stringify(item.config[key]) : item.config[key]}
                  </li>
                );
              })}
          </ul>
        </div>
      </BoxWrapper>

      <PopinWrapper opened={action !== null} close={() => setAction(null)}>
        <div className="form">
          <h3>Action parameter</h3>
          <HalForm
            onSubmit={(data) => doAction(action, data)}
            schema={action?.params || []}
            item={pick(
              item.measure,
              (action?.params || []).map((p) => p.name),
            )}
            onCancel={() => setAction(null)}
          />
        </div>
      </PopinWrapper>
    </>
  );
};
