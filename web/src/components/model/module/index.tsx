import React from "react";
import { BoxWrapper } from "../../wrapper/box";
import { ModuleFragment } from "../../../graphql/types";

interface Props {
  item: ModuleFragment;
}

export const ModuleBox: React.FC<Props> = (props: Props) => {
  const { item } = props;

  let itemClass = "module";
  if (item.started !== undefined && item.started !== null) {
    if (item.started) {
      itemClass += " module-started";
    } else {
      itemClass += " module-stopped";
    }
  }

  return (
    <BoxWrapper className={itemClass} icon={item.icon}>
      <div className="info">
        <h3>{item.name}</h3>
        {item.description && <p className="description">{item.description}</p>}

        <ul className="metadata">
          {item.categories?.length > 0 && (
            <li>
              <span>Tags:</span>
              {item.categories?.map((c) => c.name).join(", ")}
            </li>
          )}
          {item.startPriority !== null && (
            <li>
              <span>Start priority:</span>
              {item.startPriority}
            </li>
          )}
          {item.version !== null && (
            <li>
              <span>version:</span>
              {item.version}
            </li>
          )}
          {item.license !== null && (
            <li>
              <span>license:</span>
              {item.license}
            </li>
          )}
          {item.author !== null && (
            <li>
              <span>author:</span>
              {item.author}
            </li>
          )}
          {item.url !== null && (
            <li>
              <span>url:</span>
              <a href={item?.url || "#"} title={`Website of ${item.name}`}>
                {item?.url}
              </a>
            </li>
          )}
        </ul>
      </div>
    </BoxWrapper>
  );
};
