import React from "react";
import { getAuthenticatedUser } from "../user";
import { Link } from "../router/link";
import { Avatar } from "../components/model/user/avatar";

/**
 * Application user.
 */
interface Props {}
export const User: React.FC<Props> = (props: Props) => {
  const user = getAuthenticatedUser();
  return (
    <div id="user">
      {user && (
        <>
          <Avatar user={user} />
          <span className="name">{user.fullname || user.username}</span>
          <Link id="logout" />
        </>
      )}
    </div>
  );
};
