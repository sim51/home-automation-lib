import React from "react";
import { merge } from "lodash";
import { RouteComponentProps } from "react-router";
import { useHistory } from "react-router-dom";
import { Route } from "../router/routes";
import { isRouteSecure, getUrlFromRouteId } from "../router/utils";
import { isAuthenticated, setAuthenticatedUser } from "../user";
import { stringGuessCast } from "../utils";

// Layout
import { Footer } from "./../layout/footer";
import { Header } from "./../layout/header";
import { SideBar } from "./sidebar";
import { TopBar } from "./topbar";
// Components
import { Spinner } from "../components/generic/spinner";
import { ApolloError } from "../components/generic/apollo-error";
// graphql
import { useWhoamiQuery } from "../graphql/types";

// Component props
interface Props extends RouteComponentProps {
  route: Route;
}

// Props that will propagated to layout component
export interface CommonLayoutProps extends Props {
  templateData: { [key: string]: string };
}

/**
 * Application page
 */
export const Page: React.FC<Props> = (props: Props) => {
  const history = useHistory();
  const { route, match, location } = props;

  // Check if the page is secure or not and if the user is auth
  if (!isAuthenticated() && isRouteSecure(route.id)) {
    const url = `${getUrlFromRouteId("login")}?redirect=${history.location.pathname}${history.location.search.replace(
      "?",
      "%3F",
    )}`;
    history.push(url);
  }

  // Retrieve query params and put them in the graphql variables
  const urlQueryParams = new URLSearchParams(location.search);
  let queryParams: { [key: string]: string } = {};
  urlQueryParams.forEach((value, key) => {
    queryParams[key] = stringGuessCast(value);
  });

  // Loading the user
  const { data: dataUser, loading: userLoading, error: userError } = useWhoamiQuery({});
  if (dataUser?.user) {
    setAuthenticatedUser(dataUser.user);
  }

  // Loading the data
  const { data, loading, error } = route.graphql
    ? route.graphql.hook(merge({ variables: match.params }, { variables: queryParams }, route.graphql.queryOption))
    : { data: null, loading: false, error: null };

  // Create props that have to be propagated to layout component
  const routeComponentProps: CommonLayoutProps = Object.assign({}, props, {
    templateData: Object.assign(
      route.graphql?.templateReducer ? route.graphql.templateReducer(data) : {},
      match.params,
    ),
  });

  return (
    <>
      <SideBar {...routeComponentProps} />
      <main>
        <TopBar {...routeComponentProps} />
        <div className="main-wrapper">
          <Header {...routeComponentProps} />
          <ApolloError error={error || userError} />
          <Spinner loading={loading || userLoading} />
          {route.graphql && data && (
            <route.component
              data={route.graphql.resultReducer ? route.graphql.resultReducer(data) : data["data"]}
              {...match.params}
            />
          )}
          {!route.graphql && <route.component {...match.params} />}
          <Footer />
        </div>
      </main>
    </>
  );
};
