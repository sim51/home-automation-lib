import React from "react";
import { CommonLayoutProps } from "./page";

/**
 * Application top bar
 */
export const TopBar: React.FC<CommonLayoutProps> = (props: CommonLayoutProps) => {
  return <nav id="top"></nav>;
};
