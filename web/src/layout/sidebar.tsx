import React from "react";
import { useStateLocalStorage } from "../hooks/state-local-storage";
import { CommonLayoutProps } from "./page";
import { Brand } from "./brand";
import { User } from "./user";
import { Menu } from "./menu";

/**
 * Application side bar.
 */
export const SideBar: React.FC<CommonLayoutProps> = (props: CommonLayoutProps) => {
  const [open, setOpen] = useStateLocalStorage<boolean>("menu", true);

  return (
    <nav id="sidebar" className={open === true ? "opened" : "closed"}>
      <div className="header">
        <Brand />
        <button
          id="burger-menu"
          onClick={() => {
            setOpen(!open);
          }}
        >
          <i className="fas fa-bars"></i>
        </button>
      </div>
      <div className="content">
        <User />
        <Menu {...props} />
      </div>
    </nav>
  );
};
