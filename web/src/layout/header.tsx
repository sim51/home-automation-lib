import React, { useEffect, useState } from "react";
import { template } from "../utils";
import { CommonLayoutProps } from "./page";
import { Breadcrumb } from "./breadcrumb";

/**
 * Application content header
 */
export const Header: React.FC<CommonLayoutProps> = (props: CommonLayoutProps) => {
  const { route, templateData } = props;
  const [title, setTitle] = useState<string>("");

  useEffect(() => {
    setTitle(template(route.title ? route.title : route.name, templateData));
    document.title = title;
  }, [route.title, route.name, templateData, title]);

  return (
    <header>
      <h1>{title}</h1>
      <Breadcrumb {...props} />
    </header>
  );
};
