import React from "react";
import { Link } from "../router/link";
import { Route, routes } from "../router/routes";
import { findRoutePath } from "../router/utils";
import { CommonLayoutProps } from "./page";
import { getAuthenticatedUser } from "../user";
/**
 * Application main menu.
 */
export const Menu: React.FC<CommonLayoutProps> = (props: CommonLayoutProps) => {
  const { route, templateData } = props;
  const routePath: Array<Route> | undefined = findRoutePath(route.id);
  const routesMenu = routes.find((route) => route.id === "home")?.routes || [];
  const user = getAuthenticatedUser();
  return (
    <ul className="menu">
      {routesMenu.map((item) => {
        return (
          <li key={item.id} className={routePath?.find((e) => e.id === item.id) ? "selected" : ""}>
            <Link id={item.id} params={templateData} />
          </li>
        );
      })}
      {user?.roles && user.roles.includes("admin") && (
        <li className={routePath?.find((e) => e.id === "user-list") ? "selected" : ""}>
          <Link id={"user-list"} />
        </li>
      )}
      <li>
        <a href={`/red/ui?access_token=${localStorage.getItem("id_token")}`} title="Nodered Admin">
          Node Red
        </a>
      </li>
    </ul>
  );
};
