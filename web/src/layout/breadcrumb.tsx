import React from "react";
import { Route } from "../router/routes";
import { Link } from "../router/link";
import { findRoutePath } from "../router/utils";
import { template } from "../utils";
import { CommonLayoutProps } from "./page";

export const Breadcrumb: React.FC<CommonLayoutProps> = (props: CommonLayoutProps) => {
  const { route, templateData } = props;
  const routePath: Array<Route> | undefined = findRoutePath(route.id);
  return (
    <nav className="breadcrumb" aria-label="breadcrumb">
      <ul>
        {routePath?.map((route, index) => {
          return (
            <li key={route.id}>
              {index === routePath.length - 1 ? (
                template(route.name, templateData)
              ) : (
                <Link id={route.id} params={templateData} />
              )}
            </li>
          );
        })}
      </ul>
    </nav>
  );
};
