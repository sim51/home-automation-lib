import React from "react";
import { Link } from "react-router-dom";

/**
 * Application brand.
 */
interface Props {}
export const Brand: React.FC<Props> = (props: Props) => {
  return (
    <div id="brand">
      <Link to="/" title="Home">
        HAL
      </Link>
    </div>
  );
};
