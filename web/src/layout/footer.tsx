import React from "react";

/**
 * Display the application footer.
 */
interface Props {}
export const Footer: React.FC<Props> = (props: Props) => {
  return (
    <footer>
      <p> &copy; 2020 - Sim51</p>
    </footer>
  );
};
