import * as Apollo from "@apollo/client";
// Location pages
import { PagePlaceList } from "../pages/place/list";
import { PagePlaceNew } from "../pages/place/new";
import { PagePlaceEdit } from "../pages/place/edit";
import { PagePlaceView } from "../pages/place/view";
import { PageRoomView } from "../pages/room/view";
import { PageRoomEdit } from "../pages/room/edit";
import { PageRoomNew } from "../pages/room/new";
// Module pages
import { PageModuleList } from "../pages/module/list";
import { PageModuleDiscovery } from "../pages/module/discovery";
import { PageModuleView } from "../pages/module/view";
import { PageComponentNew } from "../pages/component/new";
import { PageComponentView } from "../pages/component/view";
import { PageComponentEdit } from "../pages/component/edit";
import { PageLogs } from "../pages/logs";
// User pages
import { PageLogin } from "../pages/user/login";
import { PageLogout } from "../pages/user/logout";
import { PageUserAccount } from "../pages/user/account";
import { PageUserUpdateProfile } from "../pages/user/edit-myprofile";
import { PageUserEdit } from "../pages/user/edit";
import { PageUserList } from "../pages/user/list";
import { PageUserNew } from "../pages/user/new";

// Graphql
import {
  useGetPlacesQuery,
  useGetPlaceByIdQuery,
  useGetPlaceFullByIdQuery,
  useGetRoomByIdQuery,
  useGetModulesQuery,
  useGetModuleByIdQuery,
  useGetModuleWithComponentsByIdQuery,
  useGetComponentQuery,
  useGetComponentTypeQuery,
  useGetLogsQuery,
  useGetUsersQuery,
  useGetUserQuery,
} from "../graphql/types";

// Definition of a route
export interface Route {
  // ID of the route
  id: string;

  // Name of the page, can be used for menu & breadcrumb, so it depends of its ancestors
  name: string;

  // Title of the page, used in HTML h1 &  title
  // if not defined, the name is used
  title?: string;

  // Definition for the router
  // -------------------------
  path: string;
  redirect?: string;
  exact?: boolean;
  isSecure?: boolean;

  // GraphQl query for the page
  graphql?: {
    // The generated hook that will be used to retrieve the page data
    hook: (baseOption: Apollo.QueryHookOptions<any, any>) => Apollo.QueryResult<any, any>;
    // If you need to pass aditionnals params to the query than the URL params
    queryOption?: Apollo.QueryHookOptions;
    // Reducer to compute the data that should be passed to the component
    resultReducer?: (result: any) => unknown;
    // Reducer that is used for name and title. It should return a map
    // where keys are the url params value, and the value is the value to display
    templateReducer?: (result: any) => { [key: string]: string };
  };
  // The component that display the page
  component?: any;

  // Children routes
  routes?: Array<Route>;
}

export const routes: Array<Route> = [
  {
    id: "login",
    name: "Authentification",
    path: "/login",
    component: PageLogin,
  },
  {
    id: "logout",
    name: "Logout",
    path: "/logout",
    component: PageLogout,
  },
  {
    id: "user",
    name: "User",
    path: "/user",
    exact: true,
    isSecure: true,
    redirect: "/user/me",
    routes: [
      {
        id: "account",
        name: "My Account",
        path: "/me",
        component: PageUserAccount,
        routes: [
          {
            id: "account-edit",
            name: "Edition",
            title: "My account: Edition",
            path: "/edit",
            component: PageUserUpdateProfile,
          },
        ],
      },
      {
        id: "user-list",
        name: "Users",
        title: "List of users",
        path: "/list",
        component: PageUserList,
        graphql: { hook: useGetUsersQuery },
      },
      {
        id: "user-edit",
        name: "Edit :username",
        title: "User: edit :username",
        path: "/:username/edit",
        component: PageUserEdit,
        graphql: { hook: useGetUserQuery, resultReducer: (data) => data?.data[0] },
      },
      {
        id: "user-new",
        name: "New",
        title: "New user",
        path: "/new",
        component: PageUserNew,
      },
    ],
  },
  {
    id: "home",
    name: "Home",
    path: "",
    redirect: "/place",
    routes: [
      {
        id: "places",
        name: "Places",
        title: "Places",
        path: "/place",
        exact: true,
        isSecure: true,
        component: PagePlaceList,
        graphql: { hook: useGetPlacesQuery },
        routes: [
          {
            id: "place-new",
            name: "New",
            title: "Place : Creation",
            path: "/new",
            exact: true,
            component: PagePlaceNew,
          },
          {
            id: "place",
            name: "Place : :placeName",
            path: "/:placeId",
            exact: true,
            component: PagePlaceView,
            graphql: {
              hook: useGetPlaceFullByIdQuery,
              resultReducer: (data) => data.data[0],
              templateReducer: (data) => {
                return {
                  placeName: data?.data[0]?.name,
                };
              },
            },
            routes: [
              {
                id: "place-edit",
                name: "Edit",
                title: "Place : Edit :placeName",
                path: "/edit",
                exact: true,
                component: PagePlaceEdit,
                graphql: {
                  hook: useGetPlaceByIdQuery,
                  resultReducer: (data) => data.data[0],
                  templateReducer: (data) => {
                    return {
                      placeName: data?.data[0]?.name,
                    };
                  },
                },
              },
              {
                id: "room-new",
                name: "New",
                title: ":placeName : Room creation",
                path: "/room/new",
                exact: true,
                component: PageRoomNew,
                graphql: {
                  hook: useGetPlaceByIdQuery,
                  resultReducer: (data) => data.data[0],
                  templateReducer: (data) => {
                    return {
                      placeName: data?.data[0]?.name,
                    };
                  },
                },
              },
              {
                id: "room",
                name: ":roomName",
                title: ":placeName : :roomName",
                path: "/room/:roomId",
                exact: true,
                component: PageRoomView,
                graphql: {
                  hook: useGetRoomByIdQuery,
                  resultReducer: (data) => data.data[0],
                  templateReducer: (data) => {
                    return {
                      placeName: data?.data[0]?.place?.name,
                      roomName: data?.data[0]?.name,
                    };
                  },
                },
                routes: [
                  {
                    id: "room-edit",
                    name: "Edit",
                    title: ":placeName : Edit :roomName",
                    path: "/edit",
                    exact: true,
                    component: PageRoomEdit,
                    graphql: {
                      hook: useGetRoomByIdQuery,
                      resultReducer: (data) => data.data[0],
                      templateReducer: (data) => {
                        return {
                          placeName: data?.data[0]?.place?.name,
                          roomName: data?.data[0]?.name,
                        };
                      },
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        id: "modules",
        name: "Modules",
        title: "Modules",
        path: "/module",
        exact: true,
        isSecure: true,
        component: PageModuleList,
        graphql: { hook: useGetModulesQuery },
        routes: [
          {
            id: "module",
            name: ":moduleName",
            title: "Module : :moduleName",
            path: "/:moduleId",
            exact: true,
            component: PageModuleView,
            graphql: {
              hook: useGetModuleWithComponentsByIdQuery,
              resultReducer: (data) => data.data[0],
              templateReducer: (data) => {
                return {
                  moduleName: data?.data[0]?.name,
                };
              },
            },
            routes: [
              {
                id: "module-discovery",
                name: "Discovery :moduleName",
                title: "Module :moduleName - discovery",
                path: "/discovery",
                exact: true,
                component: PageModuleDiscovery,
                graphql: {
                  hook: useGetModuleByIdQuery,
                  resultReducer: (data) => data.data[0],
                  templateReducer: (data) => {
                    return {
                      moduleName: data?.data[0]?.name,
                    };
                  },
                },
              },
              {
                id: "component",
                name: "Component :componentName",
                title: ":moduleName : :componentName",
                path: "/:componentId",
                exact: true,
                component: PageComponentView,
                graphql: {
                  hook: useGetComponentQuery,
                  resultReducer: (data) => {
                    return {
                      component: data?.component[0],
                      measurements: data?.measurements || [],
                    };
                  },
                  templateReducer: (data) => {
                    return {
                      moduleName: data?.component[0]?.module?.name,
                      componentName: data?.component[0]?.name,
                    };
                  },
                },
                routes: [
                  {
                    id: "component-edit",
                    name: "Edit",
                    title: ":moduleName : Edit :componentName",
                    path: "/edit",
                    exact: true,
                    component: PageComponentEdit,
                    graphql: {
                      hook: useGetComponentQuery,
                      resultReducer: (data) => data.component[0],
                      templateReducer: (data) => {
                        return {
                          moduleName: data?.component[0]?.module?.name,
                          componentName: data?.component[0]?.name,
                        };
                      },
                    },
                  },
                ],
              },
              {
                id: "component-new",
                name: "New :typeName",
                title: ":moduleName : :typeName creation",
                path: "/new/:typeId",
                exact: true,
                component: PageComponentNew,
                graphql: {
                  hook: useGetComponentTypeQuery,
                  resultReducer: (data) => data.data[0],
                  templateReducer: (data) => {
                    return {
                      moduleName: data?.data[0]?.module?.name,
                      typeName: data?.data[0]?.name,
                    };
                  },
                },
              },
            ],
          },
        ],
      },
      {
        id: "logs",
        name: "Logs",
        title: "Logs",
        path: "/logs",
        exact: true,
        isSecure: true,
        component: PageLogs,
        graphql: {
          hook: useGetLogsQuery,
          resultReducer: (data) => {
            return { data: data.data, histogram: data.viz };
          },
        },
      },
    ],
  },
];
