import React from "react";
import { Link as LinkRouter } from "react-router-dom";
import { useStateLocalStorage } from "../hooks/state-local-storage";
import { Route } from "../router/routes";
import { findRoutePath } from "../router/utils";
import { template } from "../utils";

interface Props {
  id: string;
  params?: { [key: string]: string };
  className?: string;
}

export const Link: React.FC<Props> = (props: React.PropsWithChildren<Props>) => {
  const { id, params, className, children } = props;
  const routePath: Array<Route> | undefined = findRoutePath(id);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [sideMenuOpen, setSideMenuOpen] = useStateLocalStorage<boolean>("menu", true);
  const closeSideBarOnMobile = () => {
    if (window.screen.width <= 700) {
      setSideMenuOpen(false);
    }
  };

  if (routePath) {
    const path: string = routePath.map((route) => route.path).join("");
    const route: Route = routePath[routePath.length - 1];
    return (
      <LinkRouter
        className={className}
        to={template(path, params)}
        title={template(route.title ? route.title : route.name, params)}
        onClick={closeSideBarOnMobile}
      >
        {children ? children : template(route.name, params)}
      </LinkRouter>
    );
  } else {
    return null;
  }
};
