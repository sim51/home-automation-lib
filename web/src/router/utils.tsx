import { omit } from "lodash";
import { Route, routes as routesTree } from "./routes";
import { template } from "../utils";
/**
 * Retrieve a the route full path
 */
export function getUrlFromRouteId(id: string, params: { [key: string]: string } = {}): string {
  const path = "";
  const routePath = findRoutePath(id, routesTree);
  return template(path + routePath?.map((route) => route.path).join(""), params);
}

/**
 * Get a route by its id.
 */
export function getRouteById(id: string): Route | undefined {
  return flattenRoutes().find((route: Route) => route.id === id);
}

/**
 * Function that flatten tree routes recursively.
 */
export function flattenRoutes(path = "", routes: Array<Route> = routesTree): Array<Route> {
  // The result
  let routesList: Array<Route> = [];
  routes.forEach((route: Route) => {
    // construct the route definition
    const currentRoute: Route = Object.assign({ path: `${path}${route.path}` }, omit(route, ["path", "routes"]));
    // recursivity
    if (route.routes) {
      routesList = routesList.concat(flattenRoutes(currentRoute.path, route.routes));
    }
    routesList.push(currentRoute);
  });
  return routesList;
}

/**
 * Function that return the current route path.
 * It returns an array of route that match the path of the route in the tree routes.
 */
export function findRoutePath(
  id: string,
  routes: Array<Route> = routesTree,
  path: Array<Route> = [],
): Array<Route> | undefined {
  return routes
    .map((route) => {
      if (route.id === id) {
        return path.concat([omit(route, "routes")]);
      } else {
        if (route.routes) return findRoutePath(id, route.routes, path.concat([omit(route, "routes")]));
        else return undefined;
      }
    })
    .find((e) => e !== undefined);
}

/**
 * Function that return if the route is secure or not.
 * NB: A route is secure if one of its parents has the secure flag to true.
 */
export function isRouteSecure(id: string, routes: Array<Route> = routesTree): Boolean {
  const path = findRoutePath(id);
  return path ? path.some((route) => route.isSecure) : false;
}
