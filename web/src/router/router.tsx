import React from "react";
import { Redirect, Route as RouteComponent, Switch, useLocation } from "react-router-dom";
import { Route, routes as treeRoutes } from "./routes";
import { flattenRoutes } from "./utils";
import { Page } from "../layout/page";

const routes = flattenRoutes("", treeRoutes);

export const RouterWrapper: React.FC = () => {
  const location = useLocation();
  if (routes.length > 0)
    return (
      <Switch location={location}>
        {routes.map((route: Route) => {
          if (route.redirect) {
            return (
              <Redirect
                key={`${route.path}`}
                exact={route.exact ? true : false}
                path={`${route.path}`}
                to={`${route.redirect}`}
              />
            );
          } else {
            return (
              <RouteComponent
                key={`${route.path}`}
                path={`${route.path}`}
                exact={route.exact ? true : false}
                render={(props) => {
                  if (route.id === "login" || route.id === "logout") {
                    return <route.component />;
                  } else {
                    return <Page route={route} {...props} />;
                  }
                }}
              />
            );
          }
        })}
      </Switch>
    );
  else return null;
};
