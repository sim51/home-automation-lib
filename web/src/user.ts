import { UserFragment } from "./graphql/types";

export type User = UserFragment;

export const logout = (): void => {
  localStorage.removeItem("id_token");
};

export const authenticate = (token: string): void => {
  localStorage.setItem("id_token", token);
};

export const isAuthenticated = (): boolean => {
  return localStorage.getItem("id_token") ? true : false;
};

export const setAuthenticatedUser = (data: User | null): void => {
  if (data) {
    localStorage.setItem("profil", JSON.stringify(data));
  } else {
    localStorage.removeItem("profil");
  }
};

export const getAuthenticatedUser = (): User | null => {
  let user: User | null = null;
  if (localStorage.getItem("id_token") !== null && localStorage.getItem("profil") !== null) {
    user = JSON.parse(localStorage.getItem("profil") || "") as User;
  }
  return user;
};
