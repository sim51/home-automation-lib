import gql from "graphql-tag";
import { DocumentNode } from "graphql";

const fragments: { [name: string]: DocumentNode } = {};

fragments.field = gql`
  fragment Field on Field {
    name
    type
    label
    help
    placeholder
    required
    pattern
    min
    max
    maxLength
    step
  }
`;
fragments.componentType = gql`
  fragment ComponentType on ComponentType {
    id
    icon
    name
    description
    definition {
      ...Field
    }
    actions {
      name
      description
      params {
        ...Field
      }
    }
    measurements {
      name
      label
      help
      unit
      colors
      min
      max
    }
    categories {
      name
    }
  }
  ${fragments.field}
`;

fragments.component = gql`
  fragment Component on Component {
    id
    icon
    name
    description
    started
    config
    measure
    type {
      ...ComponentType
    }
  }
  ${fragments.componentType}
`;

fragments.measure = gql`
  fragment Measurement on Measurement {
    module
    type
    id
    time
    field
    value
  }
`;

fragments.componentMeasure = gql`
  fragment ComponentMeasure on ComponentMeasure {
    name
    label
    help
    unit
    colors
    min
    max
    data {
      ...Measurement
    }
  }
  ${fragments.measure}
`;

fragments.componentInfo = gql`
  fragment ComponentInfo on ComponentInfo {
    id
    icon
    name
    description
    categories
    config
    type
  }
`;

fragments.module = gql`
  fragment Module on Module {
    id
    icon
    name
    description
    url
    version
    author
    license
    started
    startPriority
    discovery
    isSystemModule
    categories {
      name
    }
  }
`;

fragments.place = gql`
  fragment Place on Place {
    id
    icon
    name
    description
    address
    postal_code
    city
    country
    location {
      longitude
      latitude
    }
  }
`;

fragments.room = gql`
  fragment Room on Room {
    id
    icon
    name
    description
  }
`;

fragments.logEntry = gql`
  fragment LogEntry on LogEntry {
    time
    level
    label
    message
  }
`;

fragments.user = gql`
  fragment User on User {
    username
    fullname
    avatar
    roles
    isPresent
  }
`;

export { fragments };
