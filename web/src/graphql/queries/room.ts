/* eslint @typescript-eslint/no-unused-vars: 0 */
import gql from "graphql-tag";
import { fragments } from "../fragments";

const getRoomById = gql`
  query GetRoomById($roomId: ID) {
    data: Room(id: $roomId) {
      ...Room
      components {
        ...Component
      }
      place {
        name
      }
    }
    components: Component {
      id
      name
      type {
        name
      }
    }
  }
  ${fragments.room}
  ${fragments.component}
`;

const roomCreate = gql`
  mutation RoomCreate($place: ID!, $id: ID!, $icon: String!, $name: String!, $description: String) {
    data: CreateRoomInPlace(place: $place, id: $id, icon: $icon, name: $name, description: $description) {
      ...Room
    }
  }
  ${fragments.room}
`;

const roomUpdate = gql`
  mutation RoomUpdate($id: ID!, $icon: String!, $name: String!, $description: String) {
    data: RoomUpdate(id: $id, icon: $icon, name: $name, description: $description) {
      ...Room
    }
  }
  ${fragments.room}
`;

const roomDelete = gql`
  mutation RoomDelete($id: ID!) {
    data: RoomDelete(id: $id) {
      _id
    }
  }
  ${fragments.room}
`;

const roomAddComponent = gql`
  mutation RoomMergeComponent($room: ID!, $component: ID!) {
    RoomAddComponent(id: $room, component: $component) {
      ...Room
    }
  }
  ${fragments.room}
`;

const roomRemoveComponent = gql`
  mutation RoomRemoveComponent($room: ID!, $component: ID!) {
    RoomDeleteComponent(id: $room, component: $component) {
      ...Room
    }
  }
  ${fragments.room}
`;
