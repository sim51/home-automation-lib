/* eslint @typescript-eslint/no-unused-vars: 0 */
import gql from "graphql-tag";
import { fragments } from "../fragments";

export const getComponentType = gql`
  query GetComponentType($typeId: ID!) {
    data: ComponentType(id: $typeId) {
      ...ComponentType
      module {
        ...Module
      }
    }
  }
  ${fragments.componentType}
  ${fragments.module}
`;

export const componentAction = gql`
  mutation ComponentAction($id: ID!, $action: String!, $params: JSON) {
    data: componentAction(component: $id, action: $action, params: $params)
  }
`;
