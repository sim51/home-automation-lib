/* eslint @typescript-eslint/no-unused-vars: 0 */
import gql from "graphql-tag";
import { fragments } from "../fragments";

const getComponents = gql`
  query GetComponents {
    data: Component {
      ...Component
      module {
        ...Module
      }
    }
  }
  ${fragments.component}
  ${fragments.module}
`;

const getComponent = gql`
  query GetComponent($componentId: ID!, $start: String, $stop: String, $field: String) {
    component: Component(id: $componentId) {
      ...Component
      module {
        ...Module
      }
    }
    measurements: getComponentMeasure(id: $componentId, start: $start, stop: $stop, field: $field) {
      ...ComponentMeasure
    }
  }
  ${fragments.component}
  ${fragments.module}
  ${fragments.componentMeasure}
`;

const componentStart = gql`
  mutation ComponentStart($id: ID!) {
    data: componentStart(component: $id)
  }
`;

const componentStop = gql`
  mutation ComponentStop($id: ID!) {
    data: componentStop(component: $id)
  }
`;

const componentDelete = gql`
  mutation ComponentDelete($id: ID!) {
    data: componentDelete(component: $id)
  }
  ${fragments.component}
`;

const componentCreate = gql`
  mutation ComponentCreate($component: ComponentInfoInput!) {
    data: componentCreate(component: $component)
  }
`;

const componentUpdate = gql`
  mutation ComponentUpdate($component: ComponentInfoInput!) {
    data: componentUpdate(component: $component)
  }
`;
