/* eslint @typescript-eslint/no-unused-vars: 0 */
import gql from "graphql-tag";
import { fragments } from "../fragments";

const getPlaces = gql`
  query GetPlaces {
    data: Place {
      ...Place
    }
  }
  ${fragments.place}
`;

const getPlaceById = gql`
  query GetPlaceById($placeId: ID!) {
    data: Place(id: $placeId) {
      ...Place
    }
  }
  ${fragments.place}
`;

const getPlaceWithRoomsById = gql`
  query GetPlaceFullById($placeId: ID!) {
    data: Place(id: $placeId) {
      ...Place
      rooms {
        ...Room
      }
      components {
        ...Component
      }
    }
  }
  ${fragments.place}
  ${fragments.place}
  ${fragments.room}
`;

const placeCreate = gql`
  mutation PlaceCreate(
    $id: ID!
    $icon: String!
    $name: String!
    $description: String
    $address: String
    $postal_code: String
    $city: String
    $country: String
    $location: _Neo4jPointInput
  ) {
    data: PlaceCreate(
      id: $id
      icon: $icon
      name: $name
      description: $description
      address: $address
      postal_code: $postal_code
      city: $city
      country: $country
      location: $location
    ) {
      ...Place
    }
  }
  ${fragments.place}
`;

const placeUpdate = gql`
  mutation PlaceUpdate(
    $id: ID!
    $icon: String!
    $name: String!
    $description: String
    $address: String
    $postal_code: String
    $city: String
    $country: String
    $location: _Neo4jPointInput
  ) {
    data: PlaceUpdate(
      id: $id
      icon: $icon
      name: $name
      description: $description
      address: $address
      postal_code: $postal_code
      city: $city
      country: $country
      location: $location
    ) {
      ...Place
    }
  }
  ${fragments.place}
`;

const placeDelete = gql`
  mutation PlaceDelete($id: ID!) {
    data: PlaceDelete(id: $id) {
      _id
    }
  }
`;

const placeAddComponent = gql`
  mutation PlaceAddComponent($place: ID!, $component: ID!) {
    PlaceAddComponent(id: $place, component: $component) {
      ...Component
    }
  }
  ${fragments.component}
`;
const placeRemoveComponent = gql`
  mutation PlaceRemoveComponent($place: ID!, $component: ID!) {
    PlaceDeleteComponent(id: $place, component: $component) {
      ...Place
    }
  }
  ${fragments.place}
`;
