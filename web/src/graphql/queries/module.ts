/* eslint @typescript-eslint/no-unused-vars: 0 */
import gql from "graphql-tag";
import { fragments } from "../fragments";

const getModules = gql`
  query GetModules {
    data: Module {
      ...Module
    }
  }
  ${fragments.module}
`;

const getModuleById = gql`
  query GetModuleById($moduleId: ID!) {
    data: Module(id: $moduleId) {
      ...Module
    }
  }
  ${fragments.module}
`;

const getModuleWithComponentsById = gql`
  query GetModuleWithComponentsById($moduleId: ID!) {
    data: Module(id: $moduleId) {
      ...Module
      components {
        ...Component
      }
      types {
        ...ComponentType
      }
    }
  }
  ${fragments.module}
  ${fragments.component}
  ${fragments.componentType}
`;

const moduleStart = gql`
  mutation ModuleStart($id: ID!) {
    data: moduleStart(module: $id)
  }
`;

const moduleStop = gql`
  mutation ModuleStop($id: ID!) {
    data: moduleStop(module: $id)
  }
`;

const moduleDiscovery = gql`
  mutation ModuleDiscovery($id: ID!) {
    data: moduleDiscoverComponents(module: $id) {
      ...Component
    }
  }
  ${fragments.component}
`;
