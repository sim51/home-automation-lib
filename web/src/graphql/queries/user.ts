/* eslint @typescript-eslint/no-unused-vars: 0 */
import gql from "graphql-tag";
import { fragments } from "../fragments";

const login = gql`
  mutation login($username: ID!, $password: String!) {
    data: login(username: $username, password: $password)
  }
`;

const whoami = gql`
  query whoami {
    user: whoami {
      ...User
    }
  }
  ${fragments.user}
`;

const refresh = gql`
  mutation refresh {
    data: refresh
  }
`;

const updateProfile = gql`
  mutation updateProfile($password: String, $fullname: String, $avatar: String) {
    data: updateProfile(fullname: $fullname, avatar: $avatar, password: $password)
  }
`;

const getUsers = gql`
  query getUsers {
    data: User(orderBy: username_asc) {
      ...User
    }
  }
  ${fragments.user}
`;

const getUser = gql`
  query getUser($username: ID!) {
    data: User(username: $username) {
      ...User
    }
  }
  ${fragments.user}
`;

const createUser = gql`
  mutation UserCreate($username: ID!, $password: String!, $fullname: String, $avatar: String, $roles: String) {
    data: UserCreate(username: $username, password: $password, fullname: $fullname, avatar: $avatar, roles: $roles) {
      ...User
    }
  }
  ${fragments.user}
`;

const updateUser = gql`
  mutation UserUpdate($username: ID!, $password: String, $fullname: String, $avatar: String, $roles: String) {
    data: UserUpdate(username: $username, password: $password, fullname: $fullname, avatar: $avatar, roles: $roles) {
      ...User
    }
  }
`;

const deleteUser = gql`
  mutation UserDelete($username: ID!) {
    data: UserDelete(username: $username)
  }
  ${fragments.user}
`;
