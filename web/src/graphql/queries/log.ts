/* eslint @typescript-eslint/no-unused-vars: 0 */
import gql from "graphql-tag";
import { fragments } from "../fragments";

const getLogs = gql`
  query GetLogs(
    $start: String = "-1h"
    $stop: String = "now()"
    $limit: Int = 25
    $skip: Int = 0
    $level: String = ""
    $label: String
    $desc: Boolean = true
  ) {
    data: logs(start: $start, stop: $stop, limit: $limit, skip: $skip, level: $level, label: $label, desc: $desc) {
      levels
      labels
      data {
        ...LogEntry
      }
    }
    viz: logsHistogram(start: $start, stop: $stop, level: $level, label: $label) {
      time
      data {
        label
        value
      }
    }
  }
  ${fragments.logEntry}
`;
