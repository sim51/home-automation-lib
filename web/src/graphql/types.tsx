import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
  /** The `JSONObject` scalar type represents JSON objects as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSONObject: any;
};



export type Role = 
  | 'user'
  | 'admin';

export type _ModelOrdering = 
  | 'id_asc'
  | 'id_desc'
  | 'icon_asc'
  | 'icon_desc'
  | 'name_asc'
  | 'name_desc'
  | 'description_asc'
  | 'description_desc';

export type _ModelFilter = {
  AND?: Maybe<Array<_ModelFilter>>;
  OR?: Maybe<Array<_ModelFilter>>;
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  icon_not?: Maybe<Scalars['String']>;
  icon_in?: Maybe<Array<Scalars['String']>>;
  icon_not_in?: Maybe<Array<Scalars['String']>>;
  icon_contains?: Maybe<Scalars['String']>;
  icon_not_contains?: Maybe<Scalars['String']>;
  icon_starts_with?: Maybe<Scalars['String']>;
  icon_not_starts_with?: Maybe<Scalars['String']>;
  icon_ends_with?: Maybe<Scalars['String']>;
  icon_not_ends_with?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
};

export type Model = {
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
};

export type _UserOrdering = 
  | 'username_asc'
  | 'username_desc'
  | 'fullname_asc'
  | 'fullname_desc'
  | 'avatar_asc'
  | 'avatar_desc'
  | 'isPresent_asc'
  | 'isPresent_desc'
  | '_id_asc'
  | '_id_desc';

export type _UserFilter = {
  AND?: Maybe<Array<_UserFilter>>;
  OR?: Maybe<Array<_UserFilter>>;
  username?: Maybe<Scalars['ID']>;
  username_not?: Maybe<Scalars['ID']>;
  username_in?: Maybe<Array<Scalars['ID']>>;
  username_not_in?: Maybe<Array<Scalars['ID']>>;
  username_contains?: Maybe<Scalars['ID']>;
  username_not_contains?: Maybe<Scalars['ID']>;
  username_starts_with?: Maybe<Scalars['ID']>;
  username_not_starts_with?: Maybe<Scalars['ID']>;
  username_ends_with?: Maybe<Scalars['ID']>;
  username_not_ends_with?: Maybe<Scalars['ID']>;
  fullname?: Maybe<Scalars['String']>;
  fullname_not?: Maybe<Scalars['String']>;
  fullname_in?: Maybe<Array<Scalars['String']>>;
  fullname_not_in?: Maybe<Array<Scalars['String']>>;
  fullname_contains?: Maybe<Scalars['String']>;
  fullname_not_contains?: Maybe<Scalars['String']>;
  fullname_starts_with?: Maybe<Scalars['String']>;
  fullname_not_starts_with?: Maybe<Scalars['String']>;
  fullname_ends_with?: Maybe<Scalars['String']>;
  fullname_not_ends_with?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  avatar_not?: Maybe<Scalars['String']>;
  avatar_in?: Maybe<Array<Scalars['String']>>;
  avatar_not_in?: Maybe<Array<Scalars['String']>>;
  avatar_contains?: Maybe<Scalars['String']>;
  avatar_not_contains?: Maybe<Scalars['String']>;
  avatar_starts_with?: Maybe<Scalars['String']>;
  avatar_not_starts_with?: Maybe<Scalars['String']>;
  avatar_ends_with?: Maybe<Scalars['String']>;
  avatar_not_ends_with?: Maybe<Scalars['String']>;
};

export type User = {
  __typename?: 'User';
  username: Scalars['ID'];
  fullname: Scalars['String'];
  avatar?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<Maybe<Scalars['String']>>>;
  isPresent?: Maybe<Scalars['Boolean']>;
  _id?: Maybe<Scalars['String']>;
};

export type _PlaceOrdering = 
  | 'id_asc'
  | 'id_desc'
  | 'icon_asc'
  | 'icon_desc'
  | 'name_asc'
  | 'name_desc'
  | 'description_asc'
  | 'description_desc'
  | 'address_asc'
  | 'address_desc'
  | 'postal_code_asc'
  | 'postal_code_desc'
  | 'city_asc'
  | 'city_desc'
  | 'country_asc'
  | 'country_desc'
  | '_id_asc'
  | '_id_desc';

export type _PlaceFilter = {
  AND?: Maybe<Array<_PlaceFilter>>;
  OR?: Maybe<Array<_PlaceFilter>>;
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  icon_not?: Maybe<Scalars['String']>;
  icon_in?: Maybe<Array<Scalars['String']>>;
  icon_not_in?: Maybe<Array<Scalars['String']>>;
  icon_contains?: Maybe<Scalars['String']>;
  icon_not_contains?: Maybe<Scalars['String']>;
  icon_starts_with?: Maybe<Scalars['String']>;
  icon_not_starts_with?: Maybe<Scalars['String']>;
  icon_ends_with?: Maybe<Scalars['String']>;
  icon_not_ends_with?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  address_not?: Maybe<Scalars['String']>;
  address_in?: Maybe<Array<Scalars['String']>>;
  address_not_in?: Maybe<Array<Scalars['String']>>;
  address_contains?: Maybe<Scalars['String']>;
  address_not_contains?: Maybe<Scalars['String']>;
  address_starts_with?: Maybe<Scalars['String']>;
  address_not_starts_with?: Maybe<Scalars['String']>;
  address_ends_with?: Maybe<Scalars['String']>;
  address_not_ends_with?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  postal_code_not?: Maybe<Scalars['String']>;
  postal_code_in?: Maybe<Array<Scalars['String']>>;
  postal_code_not_in?: Maybe<Array<Scalars['String']>>;
  postal_code_contains?: Maybe<Scalars['String']>;
  postal_code_not_contains?: Maybe<Scalars['String']>;
  postal_code_starts_with?: Maybe<Scalars['String']>;
  postal_code_not_starts_with?: Maybe<Scalars['String']>;
  postal_code_ends_with?: Maybe<Scalars['String']>;
  postal_code_not_ends_with?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  city_not?: Maybe<Scalars['String']>;
  city_in?: Maybe<Array<Scalars['String']>>;
  city_not_in?: Maybe<Array<Scalars['String']>>;
  city_contains?: Maybe<Scalars['String']>;
  city_not_contains?: Maybe<Scalars['String']>;
  city_starts_with?: Maybe<Scalars['String']>;
  city_not_starts_with?: Maybe<Scalars['String']>;
  city_ends_with?: Maybe<Scalars['String']>;
  city_not_ends_with?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  country_not?: Maybe<Scalars['String']>;
  country_in?: Maybe<Array<Scalars['String']>>;
  country_not_in?: Maybe<Array<Scalars['String']>>;
  country_contains?: Maybe<Scalars['String']>;
  country_not_contains?: Maybe<Scalars['String']>;
  country_starts_with?: Maybe<Scalars['String']>;
  country_not_starts_with?: Maybe<Scalars['String']>;
  country_ends_with?: Maybe<Scalars['String']>;
  country_not_ends_with?: Maybe<Scalars['String']>;
  location?: Maybe<_Neo4jPointInput>;
  location_not?: Maybe<_Neo4jPointInput>;
  location_distance?: Maybe<_Neo4jPointDistanceFilter>;
  location_distance_lt?: Maybe<_Neo4jPointDistanceFilter>;
  location_distance_lte?: Maybe<_Neo4jPointDistanceFilter>;
  location_distance_gt?: Maybe<_Neo4jPointDistanceFilter>;
  location_distance_gte?: Maybe<_Neo4jPointDistanceFilter>;
  rooms?: Maybe<_RoomFilter>;
  rooms_not?: Maybe<_RoomFilter>;
  rooms_in?: Maybe<Array<_RoomFilter>>;
  rooms_not_in?: Maybe<Array<_RoomFilter>>;
  rooms_some?: Maybe<_RoomFilter>;
  rooms_none?: Maybe<_RoomFilter>;
  rooms_single?: Maybe<_RoomFilter>;
  rooms_every?: Maybe<_RoomFilter>;
  components?: Maybe<_ComponentFilter>;
  components_not?: Maybe<_ComponentFilter>;
  components_in?: Maybe<Array<_ComponentFilter>>;
  components_not_in?: Maybe<Array<_ComponentFilter>>;
  components_some?: Maybe<_ComponentFilter>;
  components_none?: Maybe<_ComponentFilter>;
  components_single?: Maybe<_ComponentFilter>;
  components_every?: Maybe<_ComponentFilter>;
};

export type Place = Model & {
  __typename?: 'Place';
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  location?: Maybe<_Neo4jPoint>;
  rooms: Array<Maybe<Room>>;
  components?: Maybe<Array<Maybe<Component>>>;
  _id?: Maybe<Scalars['String']>;
};


export type PlaceRoomsArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_RoomOrdering>>>;
  filter?: Maybe<_RoomFilter>;
};


export type PlaceComponentsArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentOrdering>>>;
  filter?: Maybe<_ComponentFilter>;
};

export type _RoomOrdering = 
  | 'id_asc'
  | 'id_desc'
  | 'icon_asc'
  | 'icon_desc'
  | 'name_asc'
  | 'name_desc'
  | 'description_asc'
  | 'description_desc'
  | '_id_asc'
  | '_id_desc';

export type _RoomFilter = {
  AND?: Maybe<Array<_RoomFilter>>;
  OR?: Maybe<Array<_RoomFilter>>;
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  icon_not?: Maybe<Scalars['String']>;
  icon_in?: Maybe<Array<Scalars['String']>>;
  icon_not_in?: Maybe<Array<Scalars['String']>>;
  icon_contains?: Maybe<Scalars['String']>;
  icon_not_contains?: Maybe<Scalars['String']>;
  icon_starts_with?: Maybe<Scalars['String']>;
  icon_not_starts_with?: Maybe<Scalars['String']>;
  icon_ends_with?: Maybe<Scalars['String']>;
  icon_not_ends_with?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
  place?: Maybe<_PlaceFilter>;
  place_not?: Maybe<_PlaceFilter>;
  place_in?: Maybe<Array<_PlaceFilter>>;
  place_not_in?: Maybe<Array<_PlaceFilter>>;
  components?: Maybe<_ComponentFilter>;
  components_not?: Maybe<_ComponentFilter>;
  components_in?: Maybe<Array<_ComponentFilter>>;
  components_not_in?: Maybe<Array<_ComponentFilter>>;
  components_some?: Maybe<_ComponentFilter>;
  components_none?: Maybe<_ComponentFilter>;
  components_single?: Maybe<_ComponentFilter>;
  components_every?: Maybe<_ComponentFilter>;
};

export type Room = Model & {
  __typename?: 'Room';
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  place: Place;
  components?: Maybe<Array<Maybe<Component>>>;
  _id?: Maybe<Scalars['String']>;
};


export type RoomPlaceArgs = {
  filter?: Maybe<_PlaceFilter>;
};


export type RoomComponentsArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentOrdering>>>;
  filter?: Maybe<_ComponentFilter>;
};

export type Category = {
  __typename?: 'Category';
  name: Scalars['ID'];
};

export type _ModuleOrdering = 
  | 'id_asc'
  | 'id_desc'
  | 'icon_asc'
  | 'icon_desc'
  | 'name_asc'
  | 'name_desc'
  | 'description_asc'
  | 'description_desc'
  | 'url_asc'
  | 'url_desc'
  | 'version_asc'
  | 'version_desc'
  | 'author_asc'
  | 'author_desc'
  | 'license_asc'
  | 'license_desc'
  | 'started_asc'
  | 'started_desc'
  | 'startPriority_asc'
  | 'startPriority_desc'
  | 'isSystemModule_asc'
  | 'isSystemModule_desc'
  | 'discovery_asc'
  | 'discovery_desc'
  | '_id_asc'
  | '_id_desc';

export type _ModuleFilter = {
  AND?: Maybe<Array<_ModuleFilter>>;
  OR?: Maybe<Array<_ModuleFilter>>;
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  icon_not?: Maybe<Scalars['String']>;
  icon_in?: Maybe<Array<Scalars['String']>>;
  icon_not_in?: Maybe<Array<Scalars['String']>>;
  icon_contains?: Maybe<Scalars['String']>;
  icon_not_contains?: Maybe<Scalars['String']>;
  icon_starts_with?: Maybe<Scalars['String']>;
  icon_not_starts_with?: Maybe<Scalars['String']>;
  icon_ends_with?: Maybe<Scalars['String']>;
  icon_not_ends_with?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  url_not?: Maybe<Scalars['String']>;
  url_in?: Maybe<Array<Scalars['String']>>;
  url_not_in?: Maybe<Array<Scalars['String']>>;
  url_contains?: Maybe<Scalars['String']>;
  url_not_contains?: Maybe<Scalars['String']>;
  url_starts_with?: Maybe<Scalars['String']>;
  url_not_starts_with?: Maybe<Scalars['String']>;
  url_ends_with?: Maybe<Scalars['String']>;
  url_not_ends_with?: Maybe<Scalars['String']>;
  version?: Maybe<Scalars['String']>;
  version_not?: Maybe<Scalars['String']>;
  version_in?: Maybe<Array<Scalars['String']>>;
  version_not_in?: Maybe<Array<Scalars['String']>>;
  version_contains?: Maybe<Scalars['String']>;
  version_not_contains?: Maybe<Scalars['String']>;
  version_starts_with?: Maybe<Scalars['String']>;
  version_not_starts_with?: Maybe<Scalars['String']>;
  version_ends_with?: Maybe<Scalars['String']>;
  version_not_ends_with?: Maybe<Scalars['String']>;
  author?: Maybe<Scalars['String']>;
  author_not?: Maybe<Scalars['String']>;
  author_in?: Maybe<Array<Scalars['String']>>;
  author_not_in?: Maybe<Array<Scalars['String']>>;
  author_contains?: Maybe<Scalars['String']>;
  author_not_contains?: Maybe<Scalars['String']>;
  author_starts_with?: Maybe<Scalars['String']>;
  author_not_starts_with?: Maybe<Scalars['String']>;
  author_ends_with?: Maybe<Scalars['String']>;
  author_not_ends_with?: Maybe<Scalars['String']>;
  license?: Maybe<Scalars['String']>;
  license_not?: Maybe<Scalars['String']>;
  license_in?: Maybe<Array<Scalars['String']>>;
  license_not_in?: Maybe<Array<Scalars['String']>>;
  license_contains?: Maybe<Scalars['String']>;
  license_not_contains?: Maybe<Scalars['String']>;
  license_starts_with?: Maybe<Scalars['String']>;
  license_not_starts_with?: Maybe<Scalars['String']>;
  license_ends_with?: Maybe<Scalars['String']>;
  license_not_ends_with?: Maybe<Scalars['String']>;
  started?: Maybe<Scalars['Boolean']>;
  started_not?: Maybe<Scalars['Boolean']>;
  startPriority?: Maybe<Scalars['Int']>;
  startPriority_not?: Maybe<Scalars['Int']>;
  startPriority_in?: Maybe<Array<Scalars['Int']>>;
  startPriority_not_in?: Maybe<Array<Scalars['Int']>>;
  startPriority_lt?: Maybe<Scalars['Int']>;
  startPriority_lte?: Maybe<Scalars['Int']>;
  startPriority_gt?: Maybe<Scalars['Int']>;
  startPriority_gte?: Maybe<Scalars['Int']>;
  isSystemModule?: Maybe<Scalars['Boolean']>;
  isSystemModule_not?: Maybe<Scalars['Boolean']>;
  discovery?: Maybe<Scalars['Boolean']>;
  discovery_not?: Maybe<Scalars['Boolean']>;
  types?: Maybe<_ComponentTypeFilter>;
  types_not?: Maybe<_ComponentTypeFilter>;
  types_in?: Maybe<Array<_ComponentTypeFilter>>;
  types_not_in?: Maybe<Array<_ComponentTypeFilter>>;
  types_some?: Maybe<_ComponentTypeFilter>;
  types_none?: Maybe<_ComponentTypeFilter>;
  types_single?: Maybe<_ComponentTypeFilter>;
  types_every?: Maybe<_ComponentTypeFilter>;
};

export type Module = Model & {
  __typename?: 'Module';
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  version: Scalars['String'];
  author?: Maybe<Scalars['String']>;
  license: Scalars['String'];
  started: Scalars['Boolean'];
  startPriority?: Maybe<Scalars['Int']>;
  isSystemModule: Scalars['Boolean'];
  discovery: Scalars['Boolean'];
  categories: Array<Maybe<Category>>;
  types: Array<Maybe<ComponentType>>;
  components: Array<Maybe<Component>>;
  _id?: Maybe<Scalars['String']>;
};


export type ModuleTypesArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentTypeOrdering>>>;
  filter?: Maybe<_ComponentTypeFilter>;
};


export type ModuleComponentsArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentOrdering>>>;
};

export type Field = {
  __typename?: 'Field';
  name: Scalars['String'];
  type?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  help?: Maybe<Scalars['String']>;
  placeholder?: Maybe<Scalars['String']>;
  required?: Maybe<Scalars['Boolean']>;
  pattern?: Maybe<Scalars['String']>;
  min?: Maybe<Scalars['Float']>;
  max?: Maybe<Scalars['Float']>;
  maxLength?: Maybe<Scalars['Float']>;
  step?: Maybe<Scalars['Float']>;
};

export type Action = {
  __typename?: 'Action';
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  params: Array<Maybe<Field>>;
};

export type MeasureDefinition = {
  __typename?: 'MeasureDefinition';
  name: Scalars['String'];
  label?: Maybe<Scalars['String']>;
  help?: Maybe<Scalars['String']>;
  unit?: Maybe<Scalars['String']>;
  min?: Maybe<Scalars['Float']>;
  max?: Maybe<Scalars['Float']>;
  colors?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type _ComponentTypeOrdering = 
  | 'id_asc'
  | 'id_desc'
  | 'icon_asc'
  | 'icon_desc'
  | 'name_asc'
  | 'name_desc'
  | 'description_asc'
  | 'description_desc'
  | '_id_asc'
  | '_id_desc';

export type _ComponentTypeFilter = {
  AND?: Maybe<Array<_ComponentTypeFilter>>;
  OR?: Maybe<Array<_ComponentTypeFilter>>;
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  icon_not?: Maybe<Scalars['String']>;
  icon_in?: Maybe<Array<Scalars['String']>>;
  icon_not_in?: Maybe<Array<Scalars['String']>>;
  icon_contains?: Maybe<Scalars['String']>;
  icon_not_contains?: Maybe<Scalars['String']>;
  icon_starts_with?: Maybe<Scalars['String']>;
  icon_not_starts_with?: Maybe<Scalars['String']>;
  icon_ends_with?: Maybe<Scalars['String']>;
  icon_not_ends_with?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
  components?: Maybe<_ComponentFilter>;
  components_not?: Maybe<_ComponentFilter>;
  components_in?: Maybe<Array<_ComponentFilter>>;
  components_not_in?: Maybe<Array<_ComponentFilter>>;
  components_some?: Maybe<_ComponentFilter>;
  components_none?: Maybe<_ComponentFilter>;
  components_single?: Maybe<_ComponentFilter>;
  components_every?: Maybe<_ComponentFilter>;
  module?: Maybe<_ModuleFilter>;
  module_not?: Maybe<_ModuleFilter>;
  module_in?: Maybe<Array<_ModuleFilter>>;
  module_not_in?: Maybe<Array<_ModuleFilter>>;
};

export type ComponentType = Model & {
  __typename?: 'ComponentType';
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  definition: Array<Maybe<Field>>;
  actions?: Maybe<Array<Maybe<Action>>>;
  measurements?: Maybe<Array<Maybe<MeasureDefinition>>>;
  categories: Array<Maybe<Category>>;
  components: Array<Maybe<Component>>;
  module: Module;
  _id?: Maybe<Scalars['String']>;
};


export type ComponentTypeComponentsArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentOrdering>>>;
  filter?: Maybe<_ComponentFilter>;
};


export type ComponentTypeModuleArgs = {
  filter?: Maybe<_ModuleFilter>;
};

export type _ComponentOrdering = 
  | 'id_asc'
  | 'id_desc'
  | 'icon_asc'
  | 'icon_desc'
  | 'name_asc'
  | 'name_desc'
  | 'description_asc'
  | 'description_desc'
  | 'started_asc'
  | 'started_desc'
  | 'config_asc'
  | 'config_desc'
  | 'measure_asc'
  | 'measure_desc'
  | '_id_asc'
  | '_id_desc';

export type _ComponentFilter = {
  AND?: Maybe<Array<_ComponentFilter>>;
  OR?: Maybe<Array<_ComponentFilter>>;
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  icon_not?: Maybe<Scalars['String']>;
  icon_in?: Maybe<Array<Scalars['String']>>;
  icon_not_in?: Maybe<Array<Scalars['String']>>;
  icon_contains?: Maybe<Scalars['String']>;
  icon_not_contains?: Maybe<Scalars['String']>;
  icon_starts_with?: Maybe<Scalars['String']>;
  icon_not_starts_with?: Maybe<Scalars['String']>;
  icon_ends_with?: Maybe<Scalars['String']>;
  icon_not_ends_with?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description_not?: Maybe<Scalars['String']>;
  description_in?: Maybe<Array<Scalars['String']>>;
  description_not_in?: Maybe<Array<Scalars['String']>>;
  description_contains?: Maybe<Scalars['String']>;
  description_not_contains?: Maybe<Scalars['String']>;
  description_starts_with?: Maybe<Scalars['String']>;
  description_not_starts_with?: Maybe<Scalars['String']>;
  description_ends_with?: Maybe<Scalars['String']>;
  description_not_ends_with?: Maybe<Scalars['String']>;
  started?: Maybe<Scalars['Boolean']>;
  started_not?: Maybe<Scalars['Boolean']>;
  type?: Maybe<_ComponentTypeFilter>;
  type_not?: Maybe<_ComponentTypeFilter>;
  type_in?: Maybe<Array<_ComponentTypeFilter>>;
  type_not_in?: Maybe<Array<_ComponentTypeFilter>>;
  room?: Maybe<_ComponentFilter>;
  room_not?: Maybe<_ComponentFilter>;
  room_in?: Maybe<Array<_ComponentFilter>>;
  room_not_in?: Maybe<Array<_ComponentFilter>>;
  room_some?: Maybe<_ComponentFilter>;
  room_none?: Maybe<_ComponentFilter>;
  room_single?: Maybe<_ComponentFilter>;
  room_every?: Maybe<_ComponentFilter>;
};

export type Component = Model & {
  __typename?: 'Component';
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  started: Scalars['Boolean'];
  module: Module;
  type: ComponentType;
  config?: Maybe<Scalars['JSON']>;
  measure?: Maybe<Scalars['JSON']>;
  room?: Maybe<Array<Maybe<Component>>>;
  _id?: Maybe<Scalars['String']>;
};


export type ComponentTypeArgs = {
  filter?: Maybe<_ComponentTypeFilter>;
};


export type ComponentRoomArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentOrdering>>>;
  filter?: Maybe<_ComponentFilter>;
};

export type _MeasureOrdering = 
  | 'id_asc'
  | 'id_desc'
  | '_id_asc'
  | '_id_desc';

export type _MeasureFilter = {
  AND?: Maybe<Array<_MeasureFilter>>;
  OR?: Maybe<Array<_MeasureFilter>>;
  id?: Maybe<Scalars['ID']>;
  id_not?: Maybe<Scalars['ID']>;
  id_in?: Maybe<Array<Scalars['ID']>>;
  id_not_in?: Maybe<Array<Scalars['ID']>>;
  id_contains?: Maybe<Scalars['ID']>;
  id_not_contains?: Maybe<Scalars['ID']>;
  id_starts_with?: Maybe<Scalars['ID']>;
  id_not_starts_with?: Maybe<Scalars['ID']>;
  id_ends_with?: Maybe<Scalars['ID']>;
  id_not_ends_with?: Maybe<Scalars['ID']>;
};

export type Measure = {
  __typename?: 'Measure';
  id: Scalars['ID'];
  _id?: Maybe<Scalars['String']>;
};

export type ComponentInfo = Model & {
  __typename?: 'ComponentInfo';
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  categories: Array<Maybe<Scalars['String']>>;
  config?: Maybe<Scalars['JSON']>;
  module: Scalars['String'];
  type: Scalars['String'];
};

export type ComponentInfoInput = {
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  categories: Array<Maybe<Scalars['String']>>;
  config?: Maybe<Scalars['JSON']>;
  module: Scalars['String'];
  type: Scalars['String'];
};

export type Event = {
  __typename?: 'Event';
  datetime: Scalars['String'];
  type: Scalars['String'];
  label?: Maybe<Scalars['String']>;
  data?: Maybe<Scalars['JSON']>;
};

export type _MeasurementOrdering = 
  | 'module_asc'
  | 'module_desc'
  | 'type_asc'
  | 'type_desc'
  | 'id_asc'
  | 'id_desc'
  | 'time_asc'
  | 'time_desc'
  | 'field_asc'
  | 'field_desc'
  | 'value_asc'
  | 'value_desc'
  | '_id_asc'
  | '_id_desc';

export type _MeasurementFilter = {
  AND?: Maybe<Array<_MeasurementFilter>>;
  OR?: Maybe<Array<_MeasurementFilter>>;
  module?: Maybe<Scalars['String']>;
  module_not?: Maybe<Scalars['String']>;
  module_in?: Maybe<Array<Scalars['String']>>;
  module_not_in?: Maybe<Array<Scalars['String']>>;
  module_contains?: Maybe<Scalars['String']>;
  module_not_contains?: Maybe<Scalars['String']>;
  module_starts_with?: Maybe<Scalars['String']>;
  module_not_starts_with?: Maybe<Scalars['String']>;
  module_ends_with?: Maybe<Scalars['String']>;
  module_not_ends_with?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  type_not?: Maybe<Scalars['String']>;
  type_in?: Maybe<Array<Scalars['String']>>;
  type_not_in?: Maybe<Array<Scalars['String']>>;
  type_contains?: Maybe<Scalars['String']>;
  type_not_contains?: Maybe<Scalars['String']>;
  type_starts_with?: Maybe<Scalars['String']>;
  type_not_starts_with?: Maybe<Scalars['String']>;
  type_ends_with?: Maybe<Scalars['String']>;
  type_not_ends_with?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  id_not?: Maybe<Scalars['String']>;
  id_in?: Maybe<Array<Scalars['String']>>;
  id_not_in?: Maybe<Array<Scalars['String']>>;
  id_contains?: Maybe<Scalars['String']>;
  id_not_contains?: Maybe<Scalars['String']>;
  id_starts_with?: Maybe<Scalars['String']>;
  id_not_starts_with?: Maybe<Scalars['String']>;
  id_ends_with?: Maybe<Scalars['String']>;
  id_not_ends_with?: Maybe<Scalars['String']>;
  time?: Maybe<Scalars['String']>;
  time_not?: Maybe<Scalars['String']>;
  time_in?: Maybe<Array<Scalars['String']>>;
  time_not_in?: Maybe<Array<Scalars['String']>>;
  time_contains?: Maybe<Scalars['String']>;
  time_not_contains?: Maybe<Scalars['String']>;
  time_starts_with?: Maybe<Scalars['String']>;
  time_not_starts_with?: Maybe<Scalars['String']>;
  time_ends_with?: Maybe<Scalars['String']>;
  time_not_ends_with?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  field_not?: Maybe<Scalars['String']>;
  field_in?: Maybe<Array<Scalars['String']>>;
  field_not_in?: Maybe<Array<Scalars['String']>>;
  field_contains?: Maybe<Scalars['String']>;
  field_not_contains?: Maybe<Scalars['String']>;
  field_starts_with?: Maybe<Scalars['String']>;
  field_not_starts_with?: Maybe<Scalars['String']>;
  field_ends_with?: Maybe<Scalars['String']>;
  field_not_ends_with?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Float']>;
  value_not?: Maybe<Scalars['Float']>;
  value_in?: Maybe<Array<Scalars['Float']>>;
  value_not_in?: Maybe<Array<Scalars['Float']>>;
  value_lt?: Maybe<Scalars['Float']>;
  value_lte?: Maybe<Scalars['Float']>;
  value_gt?: Maybe<Scalars['Float']>;
  value_gte?: Maybe<Scalars['Float']>;
};

export type Measurement = {
  __typename?: 'Measurement';
  module: Scalars['String'];
  type: Scalars['String'];
  id: Scalars['String'];
  time: Scalars['String'];
  field: Scalars['String'];
  value?: Maybe<Scalars['Float']>;
  _id?: Maybe<Scalars['String']>;
};

export type _ComponentMeasureOrdering = 
  | 'name_asc'
  | 'name_desc'
  | 'label_asc'
  | 'label_desc'
  | 'help_asc'
  | 'help_desc'
  | 'unit_asc'
  | 'unit_desc'
  | 'min_asc'
  | 'min_desc'
  | 'max_asc'
  | 'max_desc'
  | '_id_asc'
  | '_id_desc';

export type _ComponentMeasureFilter = {
  AND?: Maybe<Array<_ComponentMeasureFilter>>;
  OR?: Maybe<Array<_ComponentMeasureFilter>>;
  name?: Maybe<Scalars['String']>;
  name_not?: Maybe<Scalars['String']>;
  name_in?: Maybe<Array<Scalars['String']>>;
  name_not_in?: Maybe<Array<Scalars['String']>>;
  name_contains?: Maybe<Scalars['String']>;
  name_not_contains?: Maybe<Scalars['String']>;
  name_starts_with?: Maybe<Scalars['String']>;
  name_not_starts_with?: Maybe<Scalars['String']>;
  name_ends_with?: Maybe<Scalars['String']>;
  name_not_ends_with?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  label_not?: Maybe<Scalars['String']>;
  label_in?: Maybe<Array<Scalars['String']>>;
  label_not_in?: Maybe<Array<Scalars['String']>>;
  label_contains?: Maybe<Scalars['String']>;
  label_not_contains?: Maybe<Scalars['String']>;
  label_starts_with?: Maybe<Scalars['String']>;
  label_not_starts_with?: Maybe<Scalars['String']>;
  label_ends_with?: Maybe<Scalars['String']>;
  label_not_ends_with?: Maybe<Scalars['String']>;
  help?: Maybe<Scalars['String']>;
  help_not?: Maybe<Scalars['String']>;
  help_in?: Maybe<Array<Scalars['String']>>;
  help_not_in?: Maybe<Array<Scalars['String']>>;
  help_contains?: Maybe<Scalars['String']>;
  help_not_contains?: Maybe<Scalars['String']>;
  help_starts_with?: Maybe<Scalars['String']>;
  help_not_starts_with?: Maybe<Scalars['String']>;
  help_ends_with?: Maybe<Scalars['String']>;
  help_not_ends_with?: Maybe<Scalars['String']>;
  unit?: Maybe<Scalars['String']>;
  unit_not?: Maybe<Scalars['String']>;
  unit_in?: Maybe<Array<Scalars['String']>>;
  unit_not_in?: Maybe<Array<Scalars['String']>>;
  unit_contains?: Maybe<Scalars['String']>;
  unit_not_contains?: Maybe<Scalars['String']>;
  unit_starts_with?: Maybe<Scalars['String']>;
  unit_not_starts_with?: Maybe<Scalars['String']>;
  unit_ends_with?: Maybe<Scalars['String']>;
  unit_not_ends_with?: Maybe<Scalars['String']>;
  min?: Maybe<Scalars['Float']>;
  min_not?: Maybe<Scalars['Float']>;
  min_in?: Maybe<Array<Scalars['Float']>>;
  min_not_in?: Maybe<Array<Scalars['Float']>>;
  min_lt?: Maybe<Scalars['Float']>;
  min_lte?: Maybe<Scalars['Float']>;
  min_gt?: Maybe<Scalars['Float']>;
  min_gte?: Maybe<Scalars['Float']>;
  max?: Maybe<Scalars['Float']>;
  max_not?: Maybe<Scalars['Float']>;
  max_in?: Maybe<Array<Scalars['Float']>>;
  max_not_in?: Maybe<Array<Scalars['Float']>>;
  max_lt?: Maybe<Scalars['Float']>;
  max_lte?: Maybe<Scalars['Float']>;
  max_gt?: Maybe<Scalars['Float']>;
  max_gte?: Maybe<Scalars['Float']>;
};

export type ComponentMeasure = {
  __typename?: 'ComponentMeasure';
  name: Scalars['String'];
  label?: Maybe<Scalars['String']>;
  help?: Maybe<Scalars['String']>;
  unit?: Maybe<Scalars['String']>;
  colors?: Maybe<Array<Maybe<Scalars['String']>>>;
  min?: Maybe<Scalars['Float']>;
  max?: Maybe<Scalars['Float']>;
  data: Array<Maybe<Measurement>>;
  _id?: Maybe<Scalars['String']>;
};


export type ComponentMeasureDataArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_MeasurementOrdering>>>;
  filter?: Maybe<_MeasurementFilter>;
};

export type LogEntry = {
  __typename?: 'LogEntry';
  time: Scalars['String'];
  label?: Maybe<Scalars['String']>;
  level: Scalars['String'];
  message: Scalars['String'];
};

export type _LogResultOrdering = 
  | '_id_asc'
  | '_id_desc';

export type _LogResultFilter = {
  AND?: Maybe<Array<_LogResultFilter>>;
  OR?: Maybe<Array<_LogResultFilter>>;
};

export type LogResult = {
  __typename?: 'LogResult';
  data?: Maybe<Array<Maybe<LogEntry>>>;
  levels: Array<Maybe<Scalars['String']>>;
  labels: Array<Maybe<Scalars['String']>>;
  modules: Array<Maybe<Scalars['String']>>;
  components: Array<Maybe<Scalars['String']>>;
  _id?: Maybe<Scalars['String']>;
};

export type _LogHistogramItemOrdering = 
  | 'time_asc'
  | 'time_desc'
  | '_id_asc'
  | '_id_desc';

export type _LogHistogramItemFilter = {
  AND?: Maybe<Array<_LogHistogramItemFilter>>;
  OR?: Maybe<Array<_LogHistogramItemFilter>>;
  time?: Maybe<Scalars['String']>;
  time_not?: Maybe<Scalars['String']>;
  time_in?: Maybe<Array<Scalars['String']>>;
  time_not_in?: Maybe<Array<Scalars['String']>>;
  time_contains?: Maybe<Scalars['String']>;
  time_not_contains?: Maybe<Scalars['String']>;
  time_starts_with?: Maybe<Scalars['String']>;
  time_not_starts_with?: Maybe<Scalars['String']>;
  time_ends_with?: Maybe<Scalars['String']>;
  time_not_ends_with?: Maybe<Scalars['String']>;
};

export type LogHistogramItem = {
  __typename?: 'LogHistogramItem';
  time: Scalars['String'];
  data?: Maybe<Array<Maybe<Facet>>>;
  _id?: Maybe<Scalars['String']>;
};


export type LogHistogramItemDataArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_FacetOrdering>>>;
  filter?: Maybe<_FacetFilter>;
};

export type _FacetOrdering = 
  | 'label_asc'
  | 'label_desc'
  | 'value_asc'
  | 'value_desc'
  | '_id_asc'
  | '_id_desc';

export type _FacetFilter = {
  AND?: Maybe<Array<_FacetFilter>>;
  OR?: Maybe<Array<_FacetFilter>>;
  label?: Maybe<Scalars['String']>;
  label_not?: Maybe<Scalars['String']>;
  label_in?: Maybe<Array<Scalars['String']>>;
  label_not_in?: Maybe<Array<Scalars['String']>>;
  label_contains?: Maybe<Scalars['String']>;
  label_not_contains?: Maybe<Scalars['String']>;
  label_starts_with?: Maybe<Scalars['String']>;
  label_not_starts_with?: Maybe<Scalars['String']>;
  label_ends_with?: Maybe<Scalars['String']>;
  label_not_ends_with?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Float']>;
  value_not?: Maybe<Scalars['Float']>;
  value_in?: Maybe<Array<Scalars['Float']>>;
  value_not_in?: Maybe<Array<Scalars['Float']>>;
  value_lt?: Maybe<Scalars['Float']>;
  value_lte?: Maybe<Scalars['Float']>;
  value_gt?: Maybe<Scalars['Float']>;
  value_gte?: Maybe<Scalars['Float']>;
};

export type Facet = {
  __typename?: 'Facet';
  label: Scalars['String'];
  value: Scalars['Float'];
  _id?: Maybe<Scalars['String']>;
};

export type _Neo4jTime = {
  __typename?: '_Neo4jTime';
  hour?: Maybe<Scalars['Int']>;
  minute?: Maybe<Scalars['Int']>;
  second?: Maybe<Scalars['Int']>;
  millisecond?: Maybe<Scalars['Int']>;
  microsecond?: Maybe<Scalars['Int']>;
  nanosecond?: Maybe<Scalars['Int']>;
  timezone?: Maybe<Scalars['String']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jTimeInput = {
  hour?: Maybe<Scalars['Int']>;
  minute?: Maybe<Scalars['Int']>;
  second?: Maybe<Scalars['Int']>;
  millisecond?: Maybe<Scalars['Int']>;
  microsecond?: Maybe<Scalars['Int']>;
  nanosecond?: Maybe<Scalars['Int']>;
  timezone?: Maybe<Scalars['String']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jDate = {
  __typename?: '_Neo4jDate';
  year?: Maybe<Scalars['Int']>;
  month?: Maybe<Scalars['Int']>;
  day?: Maybe<Scalars['Int']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jDateInput = {
  year?: Maybe<Scalars['Int']>;
  month?: Maybe<Scalars['Int']>;
  day?: Maybe<Scalars['Int']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jDateTime = {
  __typename?: '_Neo4jDateTime';
  year?: Maybe<Scalars['Int']>;
  month?: Maybe<Scalars['Int']>;
  day?: Maybe<Scalars['Int']>;
  hour?: Maybe<Scalars['Int']>;
  minute?: Maybe<Scalars['Int']>;
  second?: Maybe<Scalars['Int']>;
  millisecond?: Maybe<Scalars['Int']>;
  microsecond?: Maybe<Scalars['Int']>;
  nanosecond?: Maybe<Scalars['Int']>;
  timezone?: Maybe<Scalars['String']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jDateTimeInput = {
  year?: Maybe<Scalars['Int']>;
  month?: Maybe<Scalars['Int']>;
  day?: Maybe<Scalars['Int']>;
  hour?: Maybe<Scalars['Int']>;
  minute?: Maybe<Scalars['Int']>;
  second?: Maybe<Scalars['Int']>;
  millisecond?: Maybe<Scalars['Int']>;
  microsecond?: Maybe<Scalars['Int']>;
  nanosecond?: Maybe<Scalars['Int']>;
  timezone?: Maybe<Scalars['String']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jLocalTime = {
  __typename?: '_Neo4jLocalTime';
  hour?: Maybe<Scalars['Int']>;
  minute?: Maybe<Scalars['Int']>;
  second?: Maybe<Scalars['Int']>;
  millisecond?: Maybe<Scalars['Int']>;
  microsecond?: Maybe<Scalars['Int']>;
  nanosecond?: Maybe<Scalars['Int']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jLocalTimeInput = {
  hour?: Maybe<Scalars['Int']>;
  minute?: Maybe<Scalars['Int']>;
  second?: Maybe<Scalars['Int']>;
  millisecond?: Maybe<Scalars['Int']>;
  microsecond?: Maybe<Scalars['Int']>;
  nanosecond?: Maybe<Scalars['Int']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jLocalDateTime = {
  __typename?: '_Neo4jLocalDateTime';
  year?: Maybe<Scalars['Int']>;
  month?: Maybe<Scalars['Int']>;
  day?: Maybe<Scalars['Int']>;
  hour?: Maybe<Scalars['Int']>;
  minute?: Maybe<Scalars['Int']>;
  second?: Maybe<Scalars['Int']>;
  millisecond?: Maybe<Scalars['Int']>;
  microsecond?: Maybe<Scalars['Int']>;
  nanosecond?: Maybe<Scalars['Int']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jLocalDateTimeInput = {
  year?: Maybe<Scalars['Int']>;
  month?: Maybe<Scalars['Int']>;
  day?: Maybe<Scalars['Int']>;
  hour?: Maybe<Scalars['Int']>;
  minute?: Maybe<Scalars['Int']>;
  second?: Maybe<Scalars['Int']>;
  millisecond?: Maybe<Scalars['Int']>;
  microsecond?: Maybe<Scalars['Int']>;
  nanosecond?: Maybe<Scalars['Int']>;
  formatted?: Maybe<Scalars['String']>;
};

export type _Neo4jPointDistanceFilter = {
  point: _Neo4jPointInput;
  distance: Scalars['Float'];
};

export type _Neo4jPoint = {
  __typename?: '_Neo4jPoint';
  x?: Maybe<Scalars['Float']>;
  y?: Maybe<Scalars['Float']>;
  z?: Maybe<Scalars['Float']>;
  longitude?: Maybe<Scalars['Float']>;
  latitude?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  crs?: Maybe<Scalars['String']>;
  srid?: Maybe<Scalars['Int']>;
};

export type _Neo4jPointInput = {
  x?: Maybe<Scalars['Float']>;
  y?: Maybe<Scalars['Float']>;
  z?: Maybe<Scalars['Float']>;
  longitude?: Maybe<Scalars['Float']>;
  latitude?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  crs?: Maybe<Scalars['String']>;
  srid?: Maybe<Scalars['Int']>;
};

export type _RelationDirections = 
  | 'IN'
  | 'OUT';

export type Query = {
  __typename?: 'Query';
  whoami?: Maybe<User>;
  getComponentMeasure?: Maybe<Array<Maybe<ComponentMeasure>>>;
  logs?: Maybe<LogResult>;
  logsHistogram?: Maybe<Array<Maybe<LogHistogramItem>>>;
  Model?: Maybe<Array<Maybe<Model>>>;
  User?: Maybe<Array<Maybe<User>>>;
  Place?: Maybe<Array<Maybe<Place>>>;
  Room?: Maybe<Array<Maybe<Room>>>;
  Module?: Maybe<Array<Maybe<Module>>>;
  ComponentType?: Maybe<Array<Maybe<ComponentType>>>;
  Component?: Maybe<Array<Maybe<Component>>>;
  Measure?: Maybe<Array<Maybe<Measure>>>;
  Measurement?: Maybe<Array<Maybe<Measurement>>>;
  ComponentMeasure?: Maybe<Array<Maybe<ComponentMeasure>>>;
  LogResult?: Maybe<Array<Maybe<LogResult>>>;
  LogHistogramItem?: Maybe<Array<Maybe<LogHistogramItem>>>;
  Facet?: Maybe<Array<Maybe<Facet>>>;
};


export type QueryWhoamiArgs = {
  filter?: Maybe<_UserFilter>;
};


export type QueryGetComponentMeasureArgs = {
  id: Scalars['ID'];
  start?: Maybe<Scalars['String']>;
  stop?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentMeasureOrdering>>>;
  filter?: Maybe<_ComponentMeasureFilter>;
};


export type QueryLogsArgs = {
  start?: Maybe<Scalars['String']>;
  stop?: Maybe<Scalars['String']>;
  skip?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
  level?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  desc?: Maybe<Scalars['Boolean']>;
  filter?: Maybe<_LogResultFilter>;
};


export type QueryLogsHistogramArgs = {
  start?: Maybe<Scalars['String']>;
  stop?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_LogHistogramItemOrdering>>>;
  filter?: Maybe<_LogHistogramItemFilter>;
};


export type QueryModelArgs = {
  id?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ModelOrdering>>>;
  filter?: Maybe<_ModelFilter>;
};


export type QueryUserArgs = {
  username?: Maybe<Scalars['ID']>;
  fullname?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  roles?: Maybe<Scalars['String']>;
  isPresent?: Maybe<Scalars['Boolean']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_UserOrdering>>>;
  filter?: Maybe<_UserFilter>;
};


export type QueryPlaceArgs = {
  id?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  location?: Maybe<_Neo4jPointInput>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_PlaceOrdering>>>;
  filter?: Maybe<_PlaceFilter>;
};


export type QueryRoomArgs = {
  id?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_RoomOrdering>>>;
  filter?: Maybe<_RoomFilter>;
};


export type QueryModuleArgs = {
  id?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
  version?: Maybe<Scalars['String']>;
  author?: Maybe<Scalars['String']>;
  license?: Maybe<Scalars['String']>;
  started?: Maybe<Scalars['Boolean']>;
  startPriority?: Maybe<Scalars['Int']>;
  isSystemModule?: Maybe<Scalars['Boolean']>;
  discovery?: Maybe<Scalars['Boolean']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ModuleOrdering>>>;
  filter?: Maybe<_ModuleFilter>;
};


export type QueryComponentTypeArgs = {
  id?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentTypeOrdering>>>;
  filter?: Maybe<_ComponentTypeFilter>;
};


export type QueryComponentArgs = {
  id?: Maybe<Scalars['ID']>;
  icon?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  started?: Maybe<Scalars['Boolean']>;
  config?: Maybe<Scalars['JSON']>;
  measure?: Maybe<Scalars['JSON']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentOrdering>>>;
  filter?: Maybe<_ComponentFilter>;
};


export type QueryMeasureArgs = {
  id?: Maybe<Scalars['ID']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_MeasureOrdering>>>;
  filter?: Maybe<_MeasureFilter>;
};


export type QueryMeasurementArgs = {
  module?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  time?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Float']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_MeasurementOrdering>>>;
  filter?: Maybe<_MeasurementFilter>;
};


export type QueryComponentMeasureArgs = {
  name?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  help?: Maybe<Scalars['String']>;
  unit?: Maybe<Scalars['String']>;
  colors?: Maybe<Scalars['String']>;
  min?: Maybe<Scalars['Float']>;
  max?: Maybe<Scalars['Float']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_ComponentMeasureOrdering>>>;
  filter?: Maybe<_ComponentMeasureFilter>;
};


export type QueryLogResultArgs = {
  levels?: Maybe<Scalars['String']>;
  labels?: Maybe<Scalars['String']>;
  modules?: Maybe<Scalars['String']>;
  components?: Maybe<Scalars['String']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_LogResultOrdering>>>;
  filter?: Maybe<_LogResultFilter>;
};


export type QueryLogHistogramItemArgs = {
  time?: Maybe<Scalars['String']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_LogHistogramItemOrdering>>>;
  filter?: Maybe<_LogHistogramItemFilter>;
};


export type QueryFacetArgs = {
  label?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Float']>;
  _id?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Array<Maybe<_FacetOrdering>>>;
  filter?: Maybe<_FacetFilter>;
};

export type Mutation = {
  __typename?: 'Mutation';
  PlaceCreate?: Maybe<Place>;
  PlaceUpdate?: Maybe<Place>;
  PlaceDelete?: Maybe<Place>;
  PlaceAddComponent?: Maybe<Component>;
  PlaceDeleteComponent?: Maybe<Place>;
  CreateRoomInPlace?: Maybe<Room>;
  RoomUpdate?: Maybe<Room>;
  RoomDelete?: Maybe<Room>;
  RoomAddComponent?: Maybe<Room>;
  RoomDeleteComponent?: Maybe<Room>;
  login?: Maybe<Scalars['String']>;
  refresh?: Maybe<Scalars['String']>;
  updateProfile?: Maybe<Scalars['Boolean']>;
  UserCreate?: Maybe<User>;
  UserUpdate?: Maybe<User>;
  UserDelete?: Maybe<Scalars['Boolean']>;
  moduleStart?: Maybe<Scalars['Boolean']>;
  moduleStop?: Maybe<Scalars['Boolean']>;
  moduleDiscoverComponents?: Maybe<Array<Maybe<Component>>>;
  componentStart?: Maybe<Scalars['Boolean']>;
  componentStop?: Maybe<Scalars['Boolean']>;
  componentDelete?: Maybe<Scalars['Boolean']>;
  componentCreate?: Maybe<Scalars['Boolean']>;
  componentUpdate?: Maybe<Scalars['Boolean']>;
  componentAction?: Maybe<Scalars['Boolean']>;
};


export type MutationPlaceCreateArgs = {
  id: Scalars['ID'];
  icon?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  location?: Maybe<_Neo4jPointInput>;
};


export type MutationPlaceUpdateArgs = {
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  location?: Maybe<_Neo4jPointInput>;
};


export type MutationPlaceDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationPlaceAddComponentArgs = {
  id: Scalars['ID'];
  component: Scalars['ID'];
};


export type MutationPlaceDeleteComponentArgs = {
  id: Scalars['ID'];
  component: Scalars['ID'];
};


export type MutationCreateRoomInPlaceArgs = {
  place: Scalars['ID'];
  id: Scalars['ID'];
  icon?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
};


export type MutationRoomUpdateArgs = {
  id: Scalars['ID'];
  icon?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
};


export type MutationRoomDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationRoomAddComponentArgs = {
  id: Scalars['ID'];
  component: Scalars['ID'];
};


export type MutationRoomDeleteComponentArgs = {
  id: Scalars['ID'];
  component: Scalars['ID'];
};


export type MutationLoginArgs = {
  username: Scalars['ID'];
  password: Scalars['String'];
};


export type MutationUpdateProfileArgs = {
  password?: Maybe<Scalars['String']>;
  fullname?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
};


export type MutationUserCreateArgs = {
  username: Scalars['ID'];
  password: Scalars['String'];
  fullname?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  roles?: Maybe<Scalars['String']>;
};


export type MutationUserUpdateArgs = {
  username: Scalars['ID'];
  password?: Maybe<Scalars['String']>;
  fullname?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  roles?: Maybe<Scalars['String']>;
};


export type MutationUserDeleteArgs = {
  username: Scalars['ID'];
};


export type MutationModuleStartArgs = {
  module: Scalars['ID'];
};


export type MutationModuleStopArgs = {
  module: Scalars['ID'];
};


export type MutationModuleDiscoverComponentsArgs = {
  module: Scalars['ID'];
};


export type MutationComponentStartArgs = {
  component: Scalars['ID'];
};


export type MutationComponentStopArgs = {
  component: Scalars['ID'];
};


export type MutationComponentDeleteArgs = {
  component: Scalars['ID'];
};


export type MutationComponentCreateArgs = {
  component: ComponentInfoInput;
};


export type MutationComponentUpdateArgs = {
  component: ComponentInfoInput;
};


export type MutationComponentActionArgs = {
  component: Scalars['ID'];
  action: Scalars['String'];
  params?: Maybe<Scalars['JSON']>;
};

export type GetComponentsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetComponentsQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'Component' }
    & { module: (
      { __typename?: 'Module' }
      & ModuleFragment
    ) }
    & ComponentFragment
  )>>> }
);

export type GetComponentQueryVariables = Exact<{
  componentId: Scalars['ID'];
  start?: Maybe<Scalars['String']>;
  stop?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
}>;


export type GetComponentQuery = (
  { __typename?: 'Query' }
  & { component?: Maybe<Array<Maybe<(
    { __typename?: 'Component' }
    & { module: (
      { __typename?: 'Module' }
      & ModuleFragment
    ) }
    & ComponentFragment
  )>>>, measurements?: Maybe<Array<Maybe<(
    { __typename?: 'ComponentMeasure' }
    & ComponentMeasureFragment
  )>>> }
);

export type ComponentStartMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ComponentStartMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['componentStart'] }
);

export type ComponentStopMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ComponentStopMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['componentStop'] }
);

export type ComponentDeleteMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ComponentDeleteMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['componentDelete'] }
);

export type ComponentCreateMutationVariables = Exact<{
  component: ComponentInfoInput;
}>;


export type ComponentCreateMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['componentCreate'] }
);

export type ComponentUpdateMutationVariables = Exact<{
  component: ComponentInfoInput;
}>;


export type ComponentUpdateMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['componentUpdate'] }
);

export type GetComponentTypeQueryVariables = Exact<{
  typeId: Scalars['ID'];
}>;


export type GetComponentTypeQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'ComponentType' }
    & { module: (
      { __typename?: 'Module' }
      & ModuleFragment
    ) }
    & ComponentTypeFragment
  )>>> }
);

export type ComponentActionMutationVariables = Exact<{
  id: Scalars['ID'];
  action: Scalars['String'];
  params?: Maybe<Scalars['JSON']>;
}>;


export type ComponentActionMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['componentAction'] }
);

export type GetLogsQueryVariables = Exact<{
  start?: Maybe<Scalars['String']>;
  stop?: Maybe<Scalars['String']>;
  limit?: Maybe<Scalars['Int']>;
  skip?: Maybe<Scalars['Int']>;
  level?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  desc?: Maybe<Scalars['Boolean']>;
}>;


export type GetLogsQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<(
    { __typename?: 'LogResult' }
    & Pick<LogResult, 'levels' | 'labels'>
    & { data?: Maybe<Array<Maybe<(
      { __typename?: 'LogEntry' }
      & LogEntryFragment
    )>>> }
  )>, viz?: Maybe<Array<Maybe<(
    { __typename?: 'LogHistogramItem' }
    & Pick<LogHistogramItem, 'time'>
    & { data?: Maybe<Array<Maybe<(
      { __typename?: 'Facet' }
      & Pick<Facet, 'label' | 'value'>
    )>>> }
  )>>> }
);

export type GetModulesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetModulesQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'Module' }
    & ModuleFragment
  )>>> }
);

export type GetModuleByIdQueryVariables = Exact<{
  moduleId: Scalars['ID'];
}>;


export type GetModuleByIdQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'Module' }
    & ModuleFragment
  )>>> }
);

export type GetModuleWithComponentsByIdQueryVariables = Exact<{
  moduleId: Scalars['ID'];
}>;


export type GetModuleWithComponentsByIdQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'Module' }
    & { components: Array<Maybe<(
      { __typename?: 'Component' }
      & ComponentFragment
    )>>, types: Array<Maybe<(
      { __typename?: 'ComponentType' }
      & ComponentTypeFragment
    )>> }
    & ModuleFragment
  )>>> }
);

export type ModuleStartMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ModuleStartMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['moduleStart'] }
);

export type ModuleStopMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ModuleStopMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['moduleStop'] }
);

export type ModuleDiscoveryMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ModuleDiscoveryMutation = (
  { __typename?: 'Mutation' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'Component' }
    & ComponentFragment
  )>>> }
);

export type GetPlacesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetPlacesQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'Place' }
    & PlaceFragment
  )>>> }
);

export type GetPlaceByIdQueryVariables = Exact<{
  placeId: Scalars['ID'];
}>;


export type GetPlaceByIdQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'Place' }
    & PlaceFragment
  )>>> }
);

export type GetPlaceFullByIdQueryVariables = Exact<{
  placeId: Scalars['ID'];
}>;


export type GetPlaceFullByIdQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'Place' }
    & { rooms: Array<Maybe<(
      { __typename?: 'Room' }
      & RoomFragment
    )>>, components?: Maybe<Array<Maybe<(
      { __typename?: 'Component' }
      & ComponentFragment
    )>>> }
    & PlaceFragment
  )>>> }
);

export type PlaceCreateMutationVariables = Exact<{
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  location?: Maybe<_Neo4jPointInput>;
}>;


export type PlaceCreateMutation = (
  { __typename?: 'Mutation' }
  & { data?: Maybe<(
    { __typename?: 'Place' }
    & PlaceFragment
  )> }
);

export type PlaceUpdateMutationVariables = Exact<{
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  location?: Maybe<_Neo4jPointInput>;
}>;


export type PlaceUpdateMutation = (
  { __typename?: 'Mutation' }
  & { data?: Maybe<(
    { __typename?: 'Place' }
    & PlaceFragment
  )> }
);

export type PlaceDeleteMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PlaceDeleteMutation = (
  { __typename?: 'Mutation' }
  & { data?: Maybe<(
    { __typename?: 'Place' }
    & Pick<Place, '_id'>
  )> }
);

export type PlaceAddComponentMutationVariables = Exact<{
  place: Scalars['ID'];
  component: Scalars['ID'];
}>;


export type PlaceAddComponentMutation = (
  { __typename?: 'Mutation' }
  & { PlaceAddComponent?: Maybe<(
    { __typename?: 'Component' }
    & ComponentFragment
  )> }
);

export type PlaceRemoveComponentMutationVariables = Exact<{
  place: Scalars['ID'];
  component: Scalars['ID'];
}>;


export type PlaceRemoveComponentMutation = (
  { __typename?: 'Mutation' }
  & { PlaceDeleteComponent?: Maybe<(
    { __typename?: 'Place' }
    & PlaceFragment
  )> }
);

export type GetRoomByIdQueryVariables = Exact<{
  roomId?: Maybe<Scalars['ID']>;
}>;


export type GetRoomByIdQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'Room' }
    & { components?: Maybe<Array<Maybe<(
      { __typename?: 'Component' }
      & ComponentFragment
    )>>>, place: (
      { __typename?: 'Place' }
      & Pick<Place, 'name'>
    ) }
    & RoomFragment
  )>>>, components?: Maybe<Array<Maybe<(
    { __typename?: 'Component' }
    & Pick<Component, 'id' | 'name'>
    & { type: (
      { __typename?: 'ComponentType' }
      & Pick<ComponentType, 'name'>
    ) }
  )>>> }
);

export type RoomCreateMutationVariables = Exact<{
  place: Scalars['ID'];
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
}>;


export type RoomCreateMutation = (
  { __typename?: 'Mutation' }
  & { data?: Maybe<(
    { __typename?: 'Room' }
    & RoomFragment
  )> }
);

export type RoomUpdateMutationVariables = Exact<{
  id: Scalars['ID'];
  icon: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
}>;


export type RoomUpdateMutation = (
  { __typename?: 'Mutation' }
  & { data?: Maybe<(
    { __typename?: 'Room' }
    & RoomFragment
  )> }
);

export type RoomDeleteMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type RoomDeleteMutation = (
  { __typename?: 'Mutation' }
  & { data?: Maybe<(
    { __typename?: 'Room' }
    & Pick<Room, '_id'>
  )> }
);

export type RoomMergeComponentMutationVariables = Exact<{
  room: Scalars['ID'];
  component: Scalars['ID'];
}>;


export type RoomMergeComponentMutation = (
  { __typename?: 'Mutation' }
  & { RoomAddComponent?: Maybe<(
    { __typename?: 'Room' }
    & RoomFragment
  )> }
);

export type RoomRemoveComponentMutationVariables = Exact<{
  room: Scalars['ID'];
  component: Scalars['ID'];
}>;


export type RoomRemoveComponentMutation = (
  { __typename?: 'Mutation' }
  & { RoomDeleteComponent?: Maybe<(
    { __typename?: 'Room' }
    & RoomFragment
  )> }
);

export type LoginMutationVariables = Exact<{
  username: Scalars['ID'];
  password: Scalars['String'];
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['login'] }
);

export type WhoamiQueryVariables = Exact<{ [key: string]: never; }>;


export type WhoamiQuery = (
  { __typename?: 'Query' }
  & { user?: Maybe<(
    { __typename?: 'User' }
    & UserFragment
  )> }
);

export type RefreshMutationVariables = Exact<{ [key: string]: never; }>;


export type RefreshMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['refresh'] }
);

export type UpdateProfileMutationVariables = Exact<{
  password?: Maybe<Scalars['String']>;
  fullname?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
}>;


export type UpdateProfileMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['updateProfile'] }
);

export type GetUsersQueryVariables = Exact<{ [key: string]: never; }>;


export type GetUsersQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'User' }
    & UserFragment
  )>>> }
);

export type GetUserQueryVariables = Exact<{
  username: Scalars['ID'];
}>;


export type GetUserQuery = (
  { __typename?: 'Query' }
  & { data?: Maybe<Array<Maybe<(
    { __typename?: 'User' }
    & UserFragment
  )>>> }
);

export type UserCreateMutationVariables = Exact<{
  username: Scalars['ID'];
  password: Scalars['String'];
  fullname?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  roles?: Maybe<Scalars['String']>;
}>;


export type UserCreateMutation = (
  { __typename?: 'Mutation' }
  & { data?: Maybe<(
    { __typename?: 'User' }
    & UserFragment
  )> }
);

export type UserUpdateMutationVariables = Exact<{
  username: Scalars['ID'];
  password?: Maybe<Scalars['String']>;
  fullname?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  roles?: Maybe<Scalars['String']>;
}>;


export type UserUpdateMutation = (
  { __typename?: 'Mutation' }
  & { data?: Maybe<(
    { __typename?: 'User' }
    & UserFragment
  )> }
);

export type UserDeleteMutationVariables = Exact<{
  username: Scalars['ID'];
}>;


export type UserDeleteMutation = (
  { __typename?: 'Mutation' }
  & { data: Mutation['UserDelete'] }
);

export type FieldFragment = (
  { __typename?: 'Field' }
  & Pick<Field, 'name' | 'type' | 'label' | 'help' | 'placeholder' | 'required' | 'pattern' | 'min' | 'max' | 'maxLength' | 'step'>
);

export type ComponentTypeFragment = (
  { __typename?: 'ComponentType' }
  & Pick<ComponentType, 'id' | 'icon' | 'name' | 'description'>
  & { definition: Array<Maybe<(
    { __typename?: 'Field' }
    & FieldFragment
  )>>, actions?: Maybe<Array<Maybe<(
    { __typename?: 'Action' }
    & Pick<Action, 'name' | 'description'>
    & { params: Array<Maybe<(
      { __typename?: 'Field' }
      & FieldFragment
    )>> }
  )>>>, measurements?: Maybe<Array<Maybe<(
    { __typename?: 'MeasureDefinition' }
    & Pick<MeasureDefinition, 'name' | 'label' | 'help' | 'unit' | 'colors' | 'min' | 'max'>
  )>>>, categories: Array<Maybe<(
    { __typename?: 'Category' }
    & Pick<Category, 'name'>
  )>> }
);

export type ComponentFragment = (
  { __typename?: 'Component' }
  & Pick<Component, 'id' | 'icon' | 'name' | 'description' | 'started' | 'config' | 'measure'>
  & { type: (
    { __typename?: 'ComponentType' }
    & ComponentTypeFragment
  ) }
);

export type MeasurementFragment = (
  { __typename?: 'Measurement' }
  & Pick<Measurement, 'module' | 'type' | 'id' | 'time' | 'field' | 'value'>
);

export type ComponentMeasureFragment = (
  { __typename?: 'ComponentMeasure' }
  & Pick<ComponentMeasure, 'name' | 'label' | 'help' | 'unit' | 'colors' | 'min' | 'max'>
  & { data: Array<Maybe<(
    { __typename?: 'Measurement' }
    & MeasurementFragment
  )>> }
);

export type ComponentInfoFragment = (
  { __typename?: 'ComponentInfo' }
  & Pick<ComponentInfo, 'id' | 'icon' | 'name' | 'description' | 'categories' | 'config' | 'type'>
);

export type ModuleFragment = (
  { __typename?: 'Module' }
  & Pick<Module, 'id' | 'icon' | 'name' | 'description' | 'url' | 'version' | 'author' | 'license' | 'started' | 'startPriority' | 'discovery' | 'isSystemModule'>
  & { categories: Array<Maybe<(
    { __typename?: 'Category' }
    & Pick<Category, 'name'>
  )>> }
);

export type PlaceFragment = (
  { __typename?: 'Place' }
  & Pick<Place, 'id' | 'icon' | 'name' | 'description' | 'address' | 'postal_code' | 'city' | 'country'>
  & { location?: Maybe<(
    { __typename?: '_Neo4jPoint' }
    & Pick<_Neo4jPoint, 'longitude' | 'latitude'>
  )> }
);

export type RoomFragment = (
  { __typename?: 'Room' }
  & Pick<Room, 'id' | 'icon' | 'name' | 'description'>
);

export type LogEntryFragment = (
  { __typename?: 'LogEntry' }
  & Pick<LogEntry, 'time' | 'level' | 'label' | 'message'>
);

export type UserFragment = (
  { __typename?: 'User' }
  & Pick<User, 'username' | 'fullname' | 'avatar' | 'roles' | 'isPresent'>
);

export const FieldFragmentDoc = gql`
    fragment Field on Field {
  name
  type
  label
  help
  placeholder
  required
  pattern
  min
  max
  maxLength
  step
}
    `;
export const ComponentTypeFragmentDoc = gql`
    fragment ComponentType on ComponentType {
  id
  icon
  name
  description
  definition {
    ...Field
  }
  actions {
    name
    description
    params {
      ...Field
    }
  }
  measurements {
    name
    label
    help
    unit
    colors
    min
    max
  }
  categories {
    name
  }
}
    ${FieldFragmentDoc}`;
export const ComponentFragmentDoc = gql`
    fragment Component on Component {
  id
  icon
  name
  description
  started
  config
  measure
  type {
    ...ComponentType
  }
}
    ${ComponentTypeFragmentDoc}`;
export const MeasurementFragmentDoc = gql`
    fragment Measurement on Measurement {
  module
  type
  id
  time
  field
  value
}
    `;
export const ComponentMeasureFragmentDoc = gql`
    fragment ComponentMeasure on ComponentMeasure {
  name
  label
  help
  unit
  colors
  min
  max
  data {
    ...Measurement
  }
}
    ${MeasurementFragmentDoc}`;
export const ComponentInfoFragmentDoc = gql`
    fragment ComponentInfo on ComponentInfo {
  id
  icon
  name
  description
  categories
  config
  type
}
    `;
export const ModuleFragmentDoc = gql`
    fragment Module on Module {
  id
  icon
  name
  description
  url
  version
  author
  license
  started
  startPriority
  discovery
  isSystemModule
  categories {
    name
  }
}
    `;
export const PlaceFragmentDoc = gql`
    fragment Place on Place {
  id
  icon
  name
  description
  address
  postal_code
  city
  country
  location {
    longitude
    latitude
  }
}
    `;
export const RoomFragmentDoc = gql`
    fragment Room on Room {
  id
  icon
  name
  description
}
    `;
export const LogEntryFragmentDoc = gql`
    fragment LogEntry on LogEntry {
  time
  level
  label
  message
}
    `;
export const UserFragmentDoc = gql`
    fragment User on User {
  username
  fullname
  avatar
  roles
  isPresent
}
    `;
export const GetComponentsDocument = gql`
    query GetComponents {
  data: Component {
    ...Component
    module {
      ...Module
    }
  }
}
    ${ComponentFragmentDoc}
${ModuleFragmentDoc}`;

/**
 * __useGetComponentsQuery__
 *
 * To run a query within a React component, call `useGetComponentsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetComponentsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetComponentsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetComponentsQuery(baseOptions?: Apollo.QueryHookOptions<GetComponentsQuery, GetComponentsQueryVariables>) {
        return Apollo.useQuery<GetComponentsQuery, GetComponentsQueryVariables>(GetComponentsDocument, baseOptions);
      }
export function useGetComponentsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetComponentsQuery, GetComponentsQueryVariables>) {
          return Apollo.useLazyQuery<GetComponentsQuery, GetComponentsQueryVariables>(GetComponentsDocument, baseOptions);
        }
export type GetComponentsQueryHookResult = ReturnType<typeof useGetComponentsQuery>;
export type GetComponentsLazyQueryHookResult = ReturnType<typeof useGetComponentsLazyQuery>;
export type GetComponentsQueryResult = Apollo.QueryResult<GetComponentsQuery, GetComponentsQueryVariables>;
export const GetComponentDocument = gql`
    query GetComponent($componentId: ID!, $start: String, $stop: String, $field: String) {
  component: Component(id: $componentId) {
    ...Component
    module {
      ...Module
    }
  }
  measurements: getComponentMeasure(
    id: $componentId
    start: $start
    stop: $stop
    field: $field
  ) {
    ...ComponentMeasure
  }
}
    ${ComponentFragmentDoc}
${ModuleFragmentDoc}
${ComponentMeasureFragmentDoc}`;

/**
 * __useGetComponentQuery__
 *
 * To run a query within a React component, call `useGetComponentQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetComponentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetComponentQuery({
 *   variables: {
 *      componentId: // value for 'componentId'
 *      start: // value for 'start'
 *      stop: // value for 'stop'
 *      field: // value for 'field'
 *   },
 * });
 */
export function useGetComponentQuery(baseOptions: Apollo.QueryHookOptions<GetComponentQuery, GetComponentQueryVariables>) {
        return Apollo.useQuery<GetComponentQuery, GetComponentQueryVariables>(GetComponentDocument, baseOptions);
      }
export function useGetComponentLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetComponentQuery, GetComponentQueryVariables>) {
          return Apollo.useLazyQuery<GetComponentQuery, GetComponentQueryVariables>(GetComponentDocument, baseOptions);
        }
export type GetComponentQueryHookResult = ReturnType<typeof useGetComponentQuery>;
export type GetComponentLazyQueryHookResult = ReturnType<typeof useGetComponentLazyQuery>;
export type GetComponentQueryResult = Apollo.QueryResult<GetComponentQuery, GetComponentQueryVariables>;
export const ComponentStartDocument = gql`
    mutation ComponentStart($id: ID!) {
  data: componentStart(component: $id)
}
    `;
export type ComponentStartMutationFn = Apollo.MutationFunction<ComponentStartMutation, ComponentStartMutationVariables>;

/**
 * __useComponentStartMutation__
 *
 * To run a mutation, you first call `useComponentStartMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useComponentStartMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [componentStartMutation, { data, loading, error }] = useComponentStartMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useComponentStartMutation(baseOptions?: Apollo.MutationHookOptions<ComponentStartMutation, ComponentStartMutationVariables>) {
        return Apollo.useMutation<ComponentStartMutation, ComponentStartMutationVariables>(ComponentStartDocument, baseOptions);
      }
export type ComponentStartMutationHookResult = ReturnType<typeof useComponentStartMutation>;
export type ComponentStartMutationResult = Apollo.MutationResult<ComponentStartMutation>;
export type ComponentStartMutationOptions = Apollo.BaseMutationOptions<ComponentStartMutation, ComponentStartMutationVariables>;
export const ComponentStopDocument = gql`
    mutation ComponentStop($id: ID!) {
  data: componentStop(component: $id)
}
    `;
export type ComponentStopMutationFn = Apollo.MutationFunction<ComponentStopMutation, ComponentStopMutationVariables>;

/**
 * __useComponentStopMutation__
 *
 * To run a mutation, you first call `useComponentStopMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useComponentStopMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [componentStopMutation, { data, loading, error }] = useComponentStopMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useComponentStopMutation(baseOptions?: Apollo.MutationHookOptions<ComponentStopMutation, ComponentStopMutationVariables>) {
        return Apollo.useMutation<ComponentStopMutation, ComponentStopMutationVariables>(ComponentStopDocument, baseOptions);
      }
export type ComponentStopMutationHookResult = ReturnType<typeof useComponentStopMutation>;
export type ComponentStopMutationResult = Apollo.MutationResult<ComponentStopMutation>;
export type ComponentStopMutationOptions = Apollo.BaseMutationOptions<ComponentStopMutation, ComponentStopMutationVariables>;
export const ComponentDeleteDocument = gql`
    mutation ComponentDelete($id: ID!) {
  data: componentDelete(component: $id)
}
    `;
export type ComponentDeleteMutationFn = Apollo.MutationFunction<ComponentDeleteMutation, ComponentDeleteMutationVariables>;

/**
 * __useComponentDeleteMutation__
 *
 * To run a mutation, you first call `useComponentDeleteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useComponentDeleteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [componentDeleteMutation, { data, loading, error }] = useComponentDeleteMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useComponentDeleteMutation(baseOptions?: Apollo.MutationHookOptions<ComponentDeleteMutation, ComponentDeleteMutationVariables>) {
        return Apollo.useMutation<ComponentDeleteMutation, ComponentDeleteMutationVariables>(ComponentDeleteDocument, baseOptions);
      }
export type ComponentDeleteMutationHookResult = ReturnType<typeof useComponentDeleteMutation>;
export type ComponentDeleteMutationResult = Apollo.MutationResult<ComponentDeleteMutation>;
export type ComponentDeleteMutationOptions = Apollo.BaseMutationOptions<ComponentDeleteMutation, ComponentDeleteMutationVariables>;
export const ComponentCreateDocument = gql`
    mutation ComponentCreate($component: ComponentInfoInput!) {
  data: componentCreate(component: $component)
}
    `;
export type ComponentCreateMutationFn = Apollo.MutationFunction<ComponentCreateMutation, ComponentCreateMutationVariables>;

/**
 * __useComponentCreateMutation__
 *
 * To run a mutation, you first call `useComponentCreateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useComponentCreateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [componentCreateMutation, { data, loading, error }] = useComponentCreateMutation({
 *   variables: {
 *      component: // value for 'component'
 *   },
 * });
 */
export function useComponentCreateMutation(baseOptions?: Apollo.MutationHookOptions<ComponentCreateMutation, ComponentCreateMutationVariables>) {
        return Apollo.useMutation<ComponentCreateMutation, ComponentCreateMutationVariables>(ComponentCreateDocument, baseOptions);
      }
export type ComponentCreateMutationHookResult = ReturnType<typeof useComponentCreateMutation>;
export type ComponentCreateMutationResult = Apollo.MutationResult<ComponentCreateMutation>;
export type ComponentCreateMutationOptions = Apollo.BaseMutationOptions<ComponentCreateMutation, ComponentCreateMutationVariables>;
export const ComponentUpdateDocument = gql`
    mutation ComponentUpdate($component: ComponentInfoInput!) {
  data: componentUpdate(component: $component)
}
    `;
export type ComponentUpdateMutationFn = Apollo.MutationFunction<ComponentUpdateMutation, ComponentUpdateMutationVariables>;

/**
 * __useComponentUpdateMutation__
 *
 * To run a mutation, you first call `useComponentUpdateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useComponentUpdateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [componentUpdateMutation, { data, loading, error }] = useComponentUpdateMutation({
 *   variables: {
 *      component: // value for 'component'
 *   },
 * });
 */
export function useComponentUpdateMutation(baseOptions?: Apollo.MutationHookOptions<ComponentUpdateMutation, ComponentUpdateMutationVariables>) {
        return Apollo.useMutation<ComponentUpdateMutation, ComponentUpdateMutationVariables>(ComponentUpdateDocument, baseOptions);
      }
export type ComponentUpdateMutationHookResult = ReturnType<typeof useComponentUpdateMutation>;
export type ComponentUpdateMutationResult = Apollo.MutationResult<ComponentUpdateMutation>;
export type ComponentUpdateMutationOptions = Apollo.BaseMutationOptions<ComponentUpdateMutation, ComponentUpdateMutationVariables>;
export const GetComponentTypeDocument = gql`
    query GetComponentType($typeId: ID!) {
  data: ComponentType(id: $typeId) {
    ...ComponentType
    module {
      ...Module
    }
  }
}
    ${ComponentTypeFragmentDoc}
${ModuleFragmentDoc}`;

/**
 * __useGetComponentTypeQuery__
 *
 * To run a query within a React component, call `useGetComponentTypeQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetComponentTypeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetComponentTypeQuery({
 *   variables: {
 *      typeId: // value for 'typeId'
 *   },
 * });
 */
export function useGetComponentTypeQuery(baseOptions: Apollo.QueryHookOptions<GetComponentTypeQuery, GetComponentTypeQueryVariables>) {
        return Apollo.useQuery<GetComponentTypeQuery, GetComponentTypeQueryVariables>(GetComponentTypeDocument, baseOptions);
      }
export function useGetComponentTypeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetComponentTypeQuery, GetComponentTypeQueryVariables>) {
          return Apollo.useLazyQuery<GetComponentTypeQuery, GetComponentTypeQueryVariables>(GetComponentTypeDocument, baseOptions);
        }
export type GetComponentTypeQueryHookResult = ReturnType<typeof useGetComponentTypeQuery>;
export type GetComponentTypeLazyQueryHookResult = ReturnType<typeof useGetComponentTypeLazyQuery>;
export type GetComponentTypeQueryResult = Apollo.QueryResult<GetComponentTypeQuery, GetComponentTypeQueryVariables>;
export const ComponentActionDocument = gql`
    mutation ComponentAction($id: ID!, $action: String!, $params: JSON) {
  data: componentAction(component: $id, action: $action, params: $params)
}
    `;
export type ComponentActionMutationFn = Apollo.MutationFunction<ComponentActionMutation, ComponentActionMutationVariables>;

/**
 * __useComponentActionMutation__
 *
 * To run a mutation, you first call `useComponentActionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useComponentActionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [componentActionMutation, { data, loading, error }] = useComponentActionMutation({
 *   variables: {
 *      id: // value for 'id'
 *      action: // value for 'action'
 *      params: // value for 'params'
 *   },
 * });
 */
export function useComponentActionMutation(baseOptions?: Apollo.MutationHookOptions<ComponentActionMutation, ComponentActionMutationVariables>) {
        return Apollo.useMutation<ComponentActionMutation, ComponentActionMutationVariables>(ComponentActionDocument, baseOptions);
      }
export type ComponentActionMutationHookResult = ReturnType<typeof useComponentActionMutation>;
export type ComponentActionMutationResult = Apollo.MutationResult<ComponentActionMutation>;
export type ComponentActionMutationOptions = Apollo.BaseMutationOptions<ComponentActionMutation, ComponentActionMutationVariables>;
export const GetLogsDocument = gql`
    query GetLogs($start: String = "-1h", $stop: String = "now()", $limit: Int = 25, $skip: Int = 0, $level: String = "", $label: String, $desc: Boolean = true) {
  data: logs(
    start: $start
    stop: $stop
    limit: $limit
    skip: $skip
    level: $level
    label: $label
    desc: $desc
  ) {
    levels
    labels
    data {
      ...LogEntry
    }
  }
  viz: logsHistogram(start: $start, stop: $stop, level: $level, label: $label) {
    time
    data {
      label
      value
    }
  }
}
    ${LogEntryFragmentDoc}`;

/**
 * __useGetLogsQuery__
 *
 * To run a query within a React component, call `useGetLogsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetLogsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetLogsQuery({
 *   variables: {
 *      start: // value for 'start'
 *      stop: // value for 'stop'
 *      limit: // value for 'limit'
 *      skip: // value for 'skip'
 *      level: // value for 'level'
 *      label: // value for 'label'
 *      desc: // value for 'desc'
 *   },
 * });
 */
export function useGetLogsQuery(baseOptions?: Apollo.QueryHookOptions<GetLogsQuery, GetLogsQueryVariables>) {
        return Apollo.useQuery<GetLogsQuery, GetLogsQueryVariables>(GetLogsDocument, baseOptions);
      }
export function useGetLogsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetLogsQuery, GetLogsQueryVariables>) {
          return Apollo.useLazyQuery<GetLogsQuery, GetLogsQueryVariables>(GetLogsDocument, baseOptions);
        }
export type GetLogsQueryHookResult = ReturnType<typeof useGetLogsQuery>;
export type GetLogsLazyQueryHookResult = ReturnType<typeof useGetLogsLazyQuery>;
export type GetLogsQueryResult = Apollo.QueryResult<GetLogsQuery, GetLogsQueryVariables>;
export const GetModulesDocument = gql`
    query GetModules {
  data: Module {
    ...Module
  }
}
    ${ModuleFragmentDoc}`;

/**
 * __useGetModulesQuery__
 *
 * To run a query within a React component, call `useGetModulesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetModulesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetModulesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetModulesQuery(baseOptions?: Apollo.QueryHookOptions<GetModulesQuery, GetModulesQueryVariables>) {
        return Apollo.useQuery<GetModulesQuery, GetModulesQueryVariables>(GetModulesDocument, baseOptions);
      }
export function useGetModulesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetModulesQuery, GetModulesQueryVariables>) {
          return Apollo.useLazyQuery<GetModulesQuery, GetModulesQueryVariables>(GetModulesDocument, baseOptions);
        }
export type GetModulesQueryHookResult = ReturnType<typeof useGetModulesQuery>;
export type GetModulesLazyQueryHookResult = ReturnType<typeof useGetModulesLazyQuery>;
export type GetModulesQueryResult = Apollo.QueryResult<GetModulesQuery, GetModulesQueryVariables>;
export const GetModuleByIdDocument = gql`
    query GetModuleById($moduleId: ID!) {
  data: Module(id: $moduleId) {
    ...Module
  }
}
    ${ModuleFragmentDoc}`;

/**
 * __useGetModuleByIdQuery__
 *
 * To run a query within a React component, call `useGetModuleByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetModuleByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetModuleByIdQuery({
 *   variables: {
 *      moduleId: // value for 'moduleId'
 *   },
 * });
 */
export function useGetModuleByIdQuery(baseOptions: Apollo.QueryHookOptions<GetModuleByIdQuery, GetModuleByIdQueryVariables>) {
        return Apollo.useQuery<GetModuleByIdQuery, GetModuleByIdQueryVariables>(GetModuleByIdDocument, baseOptions);
      }
export function useGetModuleByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetModuleByIdQuery, GetModuleByIdQueryVariables>) {
          return Apollo.useLazyQuery<GetModuleByIdQuery, GetModuleByIdQueryVariables>(GetModuleByIdDocument, baseOptions);
        }
export type GetModuleByIdQueryHookResult = ReturnType<typeof useGetModuleByIdQuery>;
export type GetModuleByIdLazyQueryHookResult = ReturnType<typeof useGetModuleByIdLazyQuery>;
export type GetModuleByIdQueryResult = Apollo.QueryResult<GetModuleByIdQuery, GetModuleByIdQueryVariables>;
export const GetModuleWithComponentsByIdDocument = gql`
    query GetModuleWithComponentsById($moduleId: ID!) {
  data: Module(id: $moduleId) {
    ...Module
    components {
      ...Component
    }
    types {
      ...ComponentType
    }
  }
}
    ${ModuleFragmentDoc}
${ComponentFragmentDoc}
${ComponentTypeFragmentDoc}`;

/**
 * __useGetModuleWithComponentsByIdQuery__
 *
 * To run a query within a React component, call `useGetModuleWithComponentsByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetModuleWithComponentsByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetModuleWithComponentsByIdQuery({
 *   variables: {
 *      moduleId: // value for 'moduleId'
 *   },
 * });
 */
export function useGetModuleWithComponentsByIdQuery(baseOptions: Apollo.QueryHookOptions<GetModuleWithComponentsByIdQuery, GetModuleWithComponentsByIdQueryVariables>) {
        return Apollo.useQuery<GetModuleWithComponentsByIdQuery, GetModuleWithComponentsByIdQueryVariables>(GetModuleWithComponentsByIdDocument, baseOptions);
      }
export function useGetModuleWithComponentsByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetModuleWithComponentsByIdQuery, GetModuleWithComponentsByIdQueryVariables>) {
          return Apollo.useLazyQuery<GetModuleWithComponentsByIdQuery, GetModuleWithComponentsByIdQueryVariables>(GetModuleWithComponentsByIdDocument, baseOptions);
        }
export type GetModuleWithComponentsByIdQueryHookResult = ReturnType<typeof useGetModuleWithComponentsByIdQuery>;
export type GetModuleWithComponentsByIdLazyQueryHookResult = ReturnType<typeof useGetModuleWithComponentsByIdLazyQuery>;
export type GetModuleWithComponentsByIdQueryResult = Apollo.QueryResult<GetModuleWithComponentsByIdQuery, GetModuleWithComponentsByIdQueryVariables>;
export const ModuleStartDocument = gql`
    mutation ModuleStart($id: ID!) {
  data: moduleStart(module: $id)
}
    `;
export type ModuleStartMutationFn = Apollo.MutationFunction<ModuleStartMutation, ModuleStartMutationVariables>;

/**
 * __useModuleStartMutation__
 *
 * To run a mutation, you first call `useModuleStartMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useModuleStartMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [moduleStartMutation, { data, loading, error }] = useModuleStartMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useModuleStartMutation(baseOptions?: Apollo.MutationHookOptions<ModuleStartMutation, ModuleStartMutationVariables>) {
        return Apollo.useMutation<ModuleStartMutation, ModuleStartMutationVariables>(ModuleStartDocument, baseOptions);
      }
export type ModuleStartMutationHookResult = ReturnType<typeof useModuleStartMutation>;
export type ModuleStartMutationResult = Apollo.MutationResult<ModuleStartMutation>;
export type ModuleStartMutationOptions = Apollo.BaseMutationOptions<ModuleStartMutation, ModuleStartMutationVariables>;
export const ModuleStopDocument = gql`
    mutation ModuleStop($id: ID!) {
  data: moduleStop(module: $id)
}
    `;
export type ModuleStopMutationFn = Apollo.MutationFunction<ModuleStopMutation, ModuleStopMutationVariables>;

/**
 * __useModuleStopMutation__
 *
 * To run a mutation, you first call `useModuleStopMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useModuleStopMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [moduleStopMutation, { data, loading, error }] = useModuleStopMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useModuleStopMutation(baseOptions?: Apollo.MutationHookOptions<ModuleStopMutation, ModuleStopMutationVariables>) {
        return Apollo.useMutation<ModuleStopMutation, ModuleStopMutationVariables>(ModuleStopDocument, baseOptions);
      }
export type ModuleStopMutationHookResult = ReturnType<typeof useModuleStopMutation>;
export type ModuleStopMutationResult = Apollo.MutationResult<ModuleStopMutation>;
export type ModuleStopMutationOptions = Apollo.BaseMutationOptions<ModuleStopMutation, ModuleStopMutationVariables>;
export const ModuleDiscoveryDocument = gql`
    mutation ModuleDiscovery($id: ID!) {
  data: moduleDiscoverComponents(module: $id) {
    ...Component
  }
}
    ${ComponentFragmentDoc}`;
export type ModuleDiscoveryMutationFn = Apollo.MutationFunction<ModuleDiscoveryMutation, ModuleDiscoveryMutationVariables>;

/**
 * __useModuleDiscoveryMutation__
 *
 * To run a mutation, you first call `useModuleDiscoveryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useModuleDiscoveryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [moduleDiscoveryMutation, { data, loading, error }] = useModuleDiscoveryMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useModuleDiscoveryMutation(baseOptions?: Apollo.MutationHookOptions<ModuleDiscoveryMutation, ModuleDiscoveryMutationVariables>) {
        return Apollo.useMutation<ModuleDiscoveryMutation, ModuleDiscoveryMutationVariables>(ModuleDiscoveryDocument, baseOptions);
      }
export type ModuleDiscoveryMutationHookResult = ReturnType<typeof useModuleDiscoveryMutation>;
export type ModuleDiscoveryMutationResult = Apollo.MutationResult<ModuleDiscoveryMutation>;
export type ModuleDiscoveryMutationOptions = Apollo.BaseMutationOptions<ModuleDiscoveryMutation, ModuleDiscoveryMutationVariables>;
export const GetPlacesDocument = gql`
    query GetPlaces {
  data: Place {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;

/**
 * __useGetPlacesQuery__
 *
 * To run a query within a React component, call `useGetPlacesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPlacesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPlacesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetPlacesQuery(baseOptions?: Apollo.QueryHookOptions<GetPlacesQuery, GetPlacesQueryVariables>) {
        return Apollo.useQuery<GetPlacesQuery, GetPlacesQueryVariables>(GetPlacesDocument, baseOptions);
      }
export function useGetPlacesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPlacesQuery, GetPlacesQueryVariables>) {
          return Apollo.useLazyQuery<GetPlacesQuery, GetPlacesQueryVariables>(GetPlacesDocument, baseOptions);
        }
export type GetPlacesQueryHookResult = ReturnType<typeof useGetPlacesQuery>;
export type GetPlacesLazyQueryHookResult = ReturnType<typeof useGetPlacesLazyQuery>;
export type GetPlacesQueryResult = Apollo.QueryResult<GetPlacesQuery, GetPlacesQueryVariables>;
export const GetPlaceByIdDocument = gql`
    query GetPlaceById($placeId: ID!) {
  data: Place(id: $placeId) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;

/**
 * __useGetPlaceByIdQuery__
 *
 * To run a query within a React component, call `useGetPlaceByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPlaceByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPlaceByIdQuery({
 *   variables: {
 *      placeId: // value for 'placeId'
 *   },
 * });
 */
export function useGetPlaceByIdQuery(baseOptions: Apollo.QueryHookOptions<GetPlaceByIdQuery, GetPlaceByIdQueryVariables>) {
        return Apollo.useQuery<GetPlaceByIdQuery, GetPlaceByIdQueryVariables>(GetPlaceByIdDocument, baseOptions);
      }
export function useGetPlaceByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPlaceByIdQuery, GetPlaceByIdQueryVariables>) {
          return Apollo.useLazyQuery<GetPlaceByIdQuery, GetPlaceByIdQueryVariables>(GetPlaceByIdDocument, baseOptions);
        }
export type GetPlaceByIdQueryHookResult = ReturnType<typeof useGetPlaceByIdQuery>;
export type GetPlaceByIdLazyQueryHookResult = ReturnType<typeof useGetPlaceByIdLazyQuery>;
export type GetPlaceByIdQueryResult = Apollo.QueryResult<GetPlaceByIdQuery, GetPlaceByIdQueryVariables>;
export const GetPlaceFullByIdDocument = gql`
    query GetPlaceFullById($placeId: ID!) {
  data: Place(id: $placeId) {
    ...Place
    rooms {
      ...Room
    }
    components {
      ...Component
    }
  }
}
    ${PlaceFragmentDoc}
${RoomFragmentDoc}
${ComponentFragmentDoc}`;

/**
 * __useGetPlaceFullByIdQuery__
 *
 * To run a query within a React component, call `useGetPlaceFullByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPlaceFullByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPlaceFullByIdQuery({
 *   variables: {
 *      placeId: // value for 'placeId'
 *   },
 * });
 */
export function useGetPlaceFullByIdQuery(baseOptions: Apollo.QueryHookOptions<GetPlaceFullByIdQuery, GetPlaceFullByIdQueryVariables>) {
        return Apollo.useQuery<GetPlaceFullByIdQuery, GetPlaceFullByIdQueryVariables>(GetPlaceFullByIdDocument, baseOptions);
      }
export function useGetPlaceFullByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPlaceFullByIdQuery, GetPlaceFullByIdQueryVariables>) {
          return Apollo.useLazyQuery<GetPlaceFullByIdQuery, GetPlaceFullByIdQueryVariables>(GetPlaceFullByIdDocument, baseOptions);
        }
export type GetPlaceFullByIdQueryHookResult = ReturnType<typeof useGetPlaceFullByIdQuery>;
export type GetPlaceFullByIdLazyQueryHookResult = ReturnType<typeof useGetPlaceFullByIdLazyQuery>;
export type GetPlaceFullByIdQueryResult = Apollo.QueryResult<GetPlaceFullByIdQuery, GetPlaceFullByIdQueryVariables>;
export const PlaceCreateDocument = gql`
    mutation PlaceCreate($id: ID!, $icon: String!, $name: String!, $description: String, $address: String, $postal_code: String, $city: String, $country: String, $location: _Neo4jPointInput) {
  data: PlaceCreate(
    id: $id
    icon: $icon
    name: $name
    description: $description
    address: $address
    postal_code: $postal_code
    city: $city
    country: $country
    location: $location
  ) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;
export type PlaceCreateMutationFn = Apollo.MutationFunction<PlaceCreateMutation, PlaceCreateMutationVariables>;

/**
 * __usePlaceCreateMutation__
 *
 * To run a mutation, you first call `usePlaceCreateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePlaceCreateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [placeCreateMutation, { data, loading, error }] = usePlaceCreateMutation({
 *   variables: {
 *      id: // value for 'id'
 *      icon: // value for 'icon'
 *      name: // value for 'name'
 *      description: // value for 'description'
 *      address: // value for 'address'
 *      postal_code: // value for 'postal_code'
 *      city: // value for 'city'
 *      country: // value for 'country'
 *      location: // value for 'location'
 *   },
 * });
 */
export function usePlaceCreateMutation(baseOptions?: Apollo.MutationHookOptions<PlaceCreateMutation, PlaceCreateMutationVariables>) {
        return Apollo.useMutation<PlaceCreateMutation, PlaceCreateMutationVariables>(PlaceCreateDocument, baseOptions);
      }
export type PlaceCreateMutationHookResult = ReturnType<typeof usePlaceCreateMutation>;
export type PlaceCreateMutationResult = Apollo.MutationResult<PlaceCreateMutation>;
export type PlaceCreateMutationOptions = Apollo.BaseMutationOptions<PlaceCreateMutation, PlaceCreateMutationVariables>;
export const PlaceUpdateDocument = gql`
    mutation PlaceUpdate($id: ID!, $icon: String!, $name: String!, $description: String, $address: String, $postal_code: String, $city: String, $country: String, $location: _Neo4jPointInput) {
  data: PlaceUpdate(
    id: $id
    icon: $icon
    name: $name
    description: $description
    address: $address
    postal_code: $postal_code
    city: $city
    country: $country
    location: $location
  ) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;
export type PlaceUpdateMutationFn = Apollo.MutationFunction<PlaceUpdateMutation, PlaceUpdateMutationVariables>;

/**
 * __usePlaceUpdateMutation__
 *
 * To run a mutation, you first call `usePlaceUpdateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePlaceUpdateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [placeUpdateMutation, { data, loading, error }] = usePlaceUpdateMutation({
 *   variables: {
 *      id: // value for 'id'
 *      icon: // value for 'icon'
 *      name: // value for 'name'
 *      description: // value for 'description'
 *      address: // value for 'address'
 *      postal_code: // value for 'postal_code'
 *      city: // value for 'city'
 *      country: // value for 'country'
 *      location: // value for 'location'
 *   },
 * });
 */
export function usePlaceUpdateMutation(baseOptions?: Apollo.MutationHookOptions<PlaceUpdateMutation, PlaceUpdateMutationVariables>) {
        return Apollo.useMutation<PlaceUpdateMutation, PlaceUpdateMutationVariables>(PlaceUpdateDocument, baseOptions);
      }
export type PlaceUpdateMutationHookResult = ReturnType<typeof usePlaceUpdateMutation>;
export type PlaceUpdateMutationResult = Apollo.MutationResult<PlaceUpdateMutation>;
export type PlaceUpdateMutationOptions = Apollo.BaseMutationOptions<PlaceUpdateMutation, PlaceUpdateMutationVariables>;
export const PlaceDeleteDocument = gql`
    mutation PlaceDelete($id: ID!) {
  data: PlaceDelete(id: $id) {
    _id
  }
}
    `;
export type PlaceDeleteMutationFn = Apollo.MutationFunction<PlaceDeleteMutation, PlaceDeleteMutationVariables>;

/**
 * __usePlaceDeleteMutation__
 *
 * To run a mutation, you first call `usePlaceDeleteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePlaceDeleteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [placeDeleteMutation, { data, loading, error }] = usePlaceDeleteMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function usePlaceDeleteMutation(baseOptions?: Apollo.MutationHookOptions<PlaceDeleteMutation, PlaceDeleteMutationVariables>) {
        return Apollo.useMutation<PlaceDeleteMutation, PlaceDeleteMutationVariables>(PlaceDeleteDocument, baseOptions);
      }
export type PlaceDeleteMutationHookResult = ReturnType<typeof usePlaceDeleteMutation>;
export type PlaceDeleteMutationResult = Apollo.MutationResult<PlaceDeleteMutation>;
export type PlaceDeleteMutationOptions = Apollo.BaseMutationOptions<PlaceDeleteMutation, PlaceDeleteMutationVariables>;
export const PlaceAddComponentDocument = gql`
    mutation PlaceAddComponent($place: ID!, $component: ID!) {
  PlaceAddComponent(id: $place, component: $component) {
    ...Component
  }
}
    ${ComponentFragmentDoc}`;
export type PlaceAddComponentMutationFn = Apollo.MutationFunction<PlaceAddComponentMutation, PlaceAddComponentMutationVariables>;

/**
 * __usePlaceAddComponentMutation__
 *
 * To run a mutation, you first call `usePlaceAddComponentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePlaceAddComponentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [placeAddComponentMutation, { data, loading, error }] = usePlaceAddComponentMutation({
 *   variables: {
 *      place: // value for 'place'
 *      component: // value for 'component'
 *   },
 * });
 */
export function usePlaceAddComponentMutation(baseOptions?: Apollo.MutationHookOptions<PlaceAddComponentMutation, PlaceAddComponentMutationVariables>) {
        return Apollo.useMutation<PlaceAddComponentMutation, PlaceAddComponentMutationVariables>(PlaceAddComponentDocument, baseOptions);
      }
export type PlaceAddComponentMutationHookResult = ReturnType<typeof usePlaceAddComponentMutation>;
export type PlaceAddComponentMutationResult = Apollo.MutationResult<PlaceAddComponentMutation>;
export type PlaceAddComponentMutationOptions = Apollo.BaseMutationOptions<PlaceAddComponentMutation, PlaceAddComponentMutationVariables>;
export const PlaceRemoveComponentDocument = gql`
    mutation PlaceRemoveComponent($place: ID!, $component: ID!) {
  PlaceDeleteComponent(id: $place, component: $component) {
    ...Place
  }
}
    ${PlaceFragmentDoc}`;
export type PlaceRemoveComponentMutationFn = Apollo.MutationFunction<PlaceRemoveComponentMutation, PlaceRemoveComponentMutationVariables>;

/**
 * __usePlaceRemoveComponentMutation__
 *
 * To run a mutation, you first call `usePlaceRemoveComponentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePlaceRemoveComponentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [placeRemoveComponentMutation, { data, loading, error }] = usePlaceRemoveComponentMutation({
 *   variables: {
 *      place: // value for 'place'
 *      component: // value for 'component'
 *   },
 * });
 */
export function usePlaceRemoveComponentMutation(baseOptions?: Apollo.MutationHookOptions<PlaceRemoveComponentMutation, PlaceRemoveComponentMutationVariables>) {
        return Apollo.useMutation<PlaceRemoveComponentMutation, PlaceRemoveComponentMutationVariables>(PlaceRemoveComponentDocument, baseOptions);
      }
export type PlaceRemoveComponentMutationHookResult = ReturnType<typeof usePlaceRemoveComponentMutation>;
export type PlaceRemoveComponentMutationResult = Apollo.MutationResult<PlaceRemoveComponentMutation>;
export type PlaceRemoveComponentMutationOptions = Apollo.BaseMutationOptions<PlaceRemoveComponentMutation, PlaceRemoveComponentMutationVariables>;
export const GetRoomByIdDocument = gql`
    query GetRoomById($roomId: ID) {
  data: Room(id: $roomId) {
    ...Room
    components {
      ...Component
    }
    place {
      name
    }
  }
  components: Component {
    id
    name
    type {
      name
    }
  }
}
    ${RoomFragmentDoc}
${ComponentFragmentDoc}`;

/**
 * __useGetRoomByIdQuery__
 *
 * To run a query within a React component, call `useGetRoomByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetRoomByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetRoomByIdQuery({
 *   variables: {
 *      roomId: // value for 'roomId'
 *   },
 * });
 */
export function useGetRoomByIdQuery(baseOptions?: Apollo.QueryHookOptions<GetRoomByIdQuery, GetRoomByIdQueryVariables>) {
        return Apollo.useQuery<GetRoomByIdQuery, GetRoomByIdQueryVariables>(GetRoomByIdDocument, baseOptions);
      }
export function useGetRoomByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetRoomByIdQuery, GetRoomByIdQueryVariables>) {
          return Apollo.useLazyQuery<GetRoomByIdQuery, GetRoomByIdQueryVariables>(GetRoomByIdDocument, baseOptions);
        }
export type GetRoomByIdQueryHookResult = ReturnType<typeof useGetRoomByIdQuery>;
export type GetRoomByIdLazyQueryHookResult = ReturnType<typeof useGetRoomByIdLazyQuery>;
export type GetRoomByIdQueryResult = Apollo.QueryResult<GetRoomByIdQuery, GetRoomByIdQueryVariables>;
export const RoomCreateDocument = gql`
    mutation RoomCreate($place: ID!, $id: ID!, $icon: String!, $name: String!, $description: String) {
  data: CreateRoomInPlace(
    place: $place
    id: $id
    icon: $icon
    name: $name
    description: $description
  ) {
    ...Room
  }
}
    ${RoomFragmentDoc}`;
export type RoomCreateMutationFn = Apollo.MutationFunction<RoomCreateMutation, RoomCreateMutationVariables>;

/**
 * __useRoomCreateMutation__
 *
 * To run a mutation, you first call `useRoomCreateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRoomCreateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [roomCreateMutation, { data, loading, error }] = useRoomCreateMutation({
 *   variables: {
 *      place: // value for 'place'
 *      id: // value for 'id'
 *      icon: // value for 'icon'
 *      name: // value for 'name'
 *      description: // value for 'description'
 *   },
 * });
 */
export function useRoomCreateMutation(baseOptions?: Apollo.MutationHookOptions<RoomCreateMutation, RoomCreateMutationVariables>) {
        return Apollo.useMutation<RoomCreateMutation, RoomCreateMutationVariables>(RoomCreateDocument, baseOptions);
      }
export type RoomCreateMutationHookResult = ReturnType<typeof useRoomCreateMutation>;
export type RoomCreateMutationResult = Apollo.MutationResult<RoomCreateMutation>;
export type RoomCreateMutationOptions = Apollo.BaseMutationOptions<RoomCreateMutation, RoomCreateMutationVariables>;
export const RoomUpdateDocument = gql`
    mutation RoomUpdate($id: ID!, $icon: String!, $name: String!, $description: String) {
  data: RoomUpdate(id: $id, icon: $icon, name: $name, description: $description) {
    ...Room
  }
}
    ${RoomFragmentDoc}`;
export type RoomUpdateMutationFn = Apollo.MutationFunction<RoomUpdateMutation, RoomUpdateMutationVariables>;

/**
 * __useRoomUpdateMutation__
 *
 * To run a mutation, you first call `useRoomUpdateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRoomUpdateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [roomUpdateMutation, { data, loading, error }] = useRoomUpdateMutation({
 *   variables: {
 *      id: // value for 'id'
 *      icon: // value for 'icon'
 *      name: // value for 'name'
 *      description: // value for 'description'
 *   },
 * });
 */
export function useRoomUpdateMutation(baseOptions?: Apollo.MutationHookOptions<RoomUpdateMutation, RoomUpdateMutationVariables>) {
        return Apollo.useMutation<RoomUpdateMutation, RoomUpdateMutationVariables>(RoomUpdateDocument, baseOptions);
      }
export type RoomUpdateMutationHookResult = ReturnType<typeof useRoomUpdateMutation>;
export type RoomUpdateMutationResult = Apollo.MutationResult<RoomUpdateMutation>;
export type RoomUpdateMutationOptions = Apollo.BaseMutationOptions<RoomUpdateMutation, RoomUpdateMutationVariables>;
export const RoomDeleteDocument = gql`
    mutation RoomDelete($id: ID!) {
  data: RoomDelete(id: $id) {
    _id
  }
}
    `;
export type RoomDeleteMutationFn = Apollo.MutationFunction<RoomDeleteMutation, RoomDeleteMutationVariables>;

/**
 * __useRoomDeleteMutation__
 *
 * To run a mutation, you first call `useRoomDeleteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRoomDeleteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [roomDeleteMutation, { data, loading, error }] = useRoomDeleteMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRoomDeleteMutation(baseOptions?: Apollo.MutationHookOptions<RoomDeleteMutation, RoomDeleteMutationVariables>) {
        return Apollo.useMutation<RoomDeleteMutation, RoomDeleteMutationVariables>(RoomDeleteDocument, baseOptions);
      }
export type RoomDeleteMutationHookResult = ReturnType<typeof useRoomDeleteMutation>;
export type RoomDeleteMutationResult = Apollo.MutationResult<RoomDeleteMutation>;
export type RoomDeleteMutationOptions = Apollo.BaseMutationOptions<RoomDeleteMutation, RoomDeleteMutationVariables>;
export const RoomMergeComponentDocument = gql`
    mutation RoomMergeComponent($room: ID!, $component: ID!) {
  RoomAddComponent(id: $room, component: $component) {
    ...Room
  }
}
    ${RoomFragmentDoc}`;
export type RoomMergeComponentMutationFn = Apollo.MutationFunction<RoomMergeComponentMutation, RoomMergeComponentMutationVariables>;

/**
 * __useRoomMergeComponentMutation__
 *
 * To run a mutation, you first call `useRoomMergeComponentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRoomMergeComponentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [roomMergeComponentMutation, { data, loading, error }] = useRoomMergeComponentMutation({
 *   variables: {
 *      room: // value for 'room'
 *      component: // value for 'component'
 *   },
 * });
 */
export function useRoomMergeComponentMutation(baseOptions?: Apollo.MutationHookOptions<RoomMergeComponentMutation, RoomMergeComponentMutationVariables>) {
        return Apollo.useMutation<RoomMergeComponentMutation, RoomMergeComponentMutationVariables>(RoomMergeComponentDocument, baseOptions);
      }
export type RoomMergeComponentMutationHookResult = ReturnType<typeof useRoomMergeComponentMutation>;
export type RoomMergeComponentMutationResult = Apollo.MutationResult<RoomMergeComponentMutation>;
export type RoomMergeComponentMutationOptions = Apollo.BaseMutationOptions<RoomMergeComponentMutation, RoomMergeComponentMutationVariables>;
export const RoomRemoveComponentDocument = gql`
    mutation RoomRemoveComponent($room: ID!, $component: ID!) {
  RoomDeleteComponent(id: $room, component: $component) {
    ...Room
  }
}
    ${RoomFragmentDoc}`;
export type RoomRemoveComponentMutationFn = Apollo.MutationFunction<RoomRemoveComponentMutation, RoomRemoveComponentMutationVariables>;

/**
 * __useRoomRemoveComponentMutation__
 *
 * To run a mutation, you first call `useRoomRemoveComponentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRoomRemoveComponentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [roomRemoveComponentMutation, { data, loading, error }] = useRoomRemoveComponentMutation({
 *   variables: {
 *      room: // value for 'room'
 *      component: // value for 'component'
 *   },
 * });
 */
export function useRoomRemoveComponentMutation(baseOptions?: Apollo.MutationHookOptions<RoomRemoveComponentMutation, RoomRemoveComponentMutationVariables>) {
        return Apollo.useMutation<RoomRemoveComponentMutation, RoomRemoveComponentMutationVariables>(RoomRemoveComponentDocument, baseOptions);
      }
export type RoomRemoveComponentMutationHookResult = ReturnType<typeof useRoomRemoveComponentMutation>;
export type RoomRemoveComponentMutationResult = Apollo.MutationResult<RoomRemoveComponentMutation>;
export type RoomRemoveComponentMutationOptions = Apollo.BaseMutationOptions<RoomRemoveComponentMutation, RoomRemoveComponentMutationVariables>;
export const LoginDocument = gql`
    mutation login($username: ID!, $password: String!) {
  data: login(username: $username, password: $password)
}
    `;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, baseOptions);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const WhoamiDocument = gql`
    query whoami {
  user: whoami {
    ...User
  }
}
    ${UserFragmentDoc}`;

/**
 * __useWhoamiQuery__
 *
 * To run a query within a React component, call `useWhoamiQuery` and pass it any options that fit your needs.
 * When your component renders, `useWhoamiQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useWhoamiQuery({
 *   variables: {
 *   },
 * });
 */
export function useWhoamiQuery(baseOptions?: Apollo.QueryHookOptions<WhoamiQuery, WhoamiQueryVariables>) {
        return Apollo.useQuery<WhoamiQuery, WhoamiQueryVariables>(WhoamiDocument, baseOptions);
      }
export function useWhoamiLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<WhoamiQuery, WhoamiQueryVariables>) {
          return Apollo.useLazyQuery<WhoamiQuery, WhoamiQueryVariables>(WhoamiDocument, baseOptions);
        }
export type WhoamiQueryHookResult = ReturnType<typeof useWhoamiQuery>;
export type WhoamiLazyQueryHookResult = ReturnType<typeof useWhoamiLazyQuery>;
export type WhoamiQueryResult = Apollo.QueryResult<WhoamiQuery, WhoamiQueryVariables>;
export const RefreshDocument = gql`
    mutation refresh {
  data: refresh
}
    `;
export type RefreshMutationFn = Apollo.MutationFunction<RefreshMutation, RefreshMutationVariables>;

/**
 * __useRefreshMutation__
 *
 * To run a mutation, you first call `useRefreshMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRefreshMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [refreshMutation, { data, loading, error }] = useRefreshMutation({
 *   variables: {
 *   },
 * });
 */
export function useRefreshMutation(baseOptions?: Apollo.MutationHookOptions<RefreshMutation, RefreshMutationVariables>) {
        return Apollo.useMutation<RefreshMutation, RefreshMutationVariables>(RefreshDocument, baseOptions);
      }
export type RefreshMutationHookResult = ReturnType<typeof useRefreshMutation>;
export type RefreshMutationResult = Apollo.MutationResult<RefreshMutation>;
export type RefreshMutationOptions = Apollo.BaseMutationOptions<RefreshMutation, RefreshMutationVariables>;
export const UpdateProfileDocument = gql`
    mutation updateProfile($password: String, $fullname: String, $avatar: String) {
  data: updateProfile(fullname: $fullname, avatar: $avatar, password: $password)
}
    `;
export type UpdateProfileMutationFn = Apollo.MutationFunction<UpdateProfileMutation, UpdateProfileMutationVariables>;

/**
 * __useUpdateProfileMutation__
 *
 * To run a mutation, you first call `useUpdateProfileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateProfileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateProfileMutation, { data, loading, error }] = useUpdateProfileMutation({
 *   variables: {
 *      password: // value for 'password'
 *      fullname: // value for 'fullname'
 *      avatar: // value for 'avatar'
 *   },
 * });
 */
export function useUpdateProfileMutation(baseOptions?: Apollo.MutationHookOptions<UpdateProfileMutation, UpdateProfileMutationVariables>) {
        return Apollo.useMutation<UpdateProfileMutation, UpdateProfileMutationVariables>(UpdateProfileDocument, baseOptions);
      }
export type UpdateProfileMutationHookResult = ReturnType<typeof useUpdateProfileMutation>;
export type UpdateProfileMutationResult = Apollo.MutationResult<UpdateProfileMutation>;
export type UpdateProfileMutationOptions = Apollo.BaseMutationOptions<UpdateProfileMutation, UpdateProfileMutationVariables>;
export const GetUsersDocument = gql`
    query getUsers {
  data: User(orderBy: username_asc) {
    ...User
  }
}
    ${UserFragmentDoc}`;

/**
 * __useGetUsersQuery__
 *
 * To run a query within a React component, call `useGetUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUsersQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetUsersQuery(baseOptions?: Apollo.QueryHookOptions<GetUsersQuery, GetUsersQueryVariables>) {
        return Apollo.useQuery<GetUsersQuery, GetUsersQueryVariables>(GetUsersDocument, baseOptions);
      }
export function useGetUsersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUsersQuery, GetUsersQueryVariables>) {
          return Apollo.useLazyQuery<GetUsersQuery, GetUsersQueryVariables>(GetUsersDocument, baseOptions);
        }
export type GetUsersQueryHookResult = ReturnType<typeof useGetUsersQuery>;
export type GetUsersLazyQueryHookResult = ReturnType<typeof useGetUsersLazyQuery>;
export type GetUsersQueryResult = Apollo.QueryResult<GetUsersQuery, GetUsersQueryVariables>;
export const GetUserDocument = gql`
    query getUser($username: ID!) {
  data: User(username: $username) {
    ...User
  }
}
    ${UserFragmentDoc}`;

/**
 * __useGetUserQuery__
 *
 * To run a query within a React component, call `useGetUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserQuery({
 *   variables: {
 *      username: // value for 'username'
 *   },
 * });
 */
export function useGetUserQuery(baseOptions: Apollo.QueryHookOptions<GetUserQuery, GetUserQueryVariables>) {
        return Apollo.useQuery<GetUserQuery, GetUserQueryVariables>(GetUserDocument, baseOptions);
      }
export function useGetUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUserQuery, GetUserQueryVariables>) {
          return Apollo.useLazyQuery<GetUserQuery, GetUserQueryVariables>(GetUserDocument, baseOptions);
        }
export type GetUserQueryHookResult = ReturnType<typeof useGetUserQuery>;
export type GetUserLazyQueryHookResult = ReturnType<typeof useGetUserLazyQuery>;
export type GetUserQueryResult = Apollo.QueryResult<GetUserQuery, GetUserQueryVariables>;
export const UserCreateDocument = gql`
    mutation UserCreate($username: ID!, $password: String!, $fullname: String, $avatar: String, $roles: String) {
  data: UserCreate(
    username: $username
    password: $password
    fullname: $fullname
    avatar: $avatar
    roles: $roles
  ) {
    ...User
  }
}
    ${UserFragmentDoc}`;
export type UserCreateMutationFn = Apollo.MutationFunction<UserCreateMutation, UserCreateMutationVariables>;

/**
 * __useUserCreateMutation__
 *
 * To run a mutation, you first call `useUserCreateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUserCreateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [userCreateMutation, { data, loading, error }] = useUserCreateMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *      fullname: // value for 'fullname'
 *      avatar: // value for 'avatar'
 *      roles: // value for 'roles'
 *   },
 * });
 */
export function useUserCreateMutation(baseOptions?: Apollo.MutationHookOptions<UserCreateMutation, UserCreateMutationVariables>) {
        return Apollo.useMutation<UserCreateMutation, UserCreateMutationVariables>(UserCreateDocument, baseOptions);
      }
export type UserCreateMutationHookResult = ReturnType<typeof useUserCreateMutation>;
export type UserCreateMutationResult = Apollo.MutationResult<UserCreateMutation>;
export type UserCreateMutationOptions = Apollo.BaseMutationOptions<UserCreateMutation, UserCreateMutationVariables>;
export const UserUpdateDocument = gql`
    mutation UserUpdate($username: ID!, $password: String, $fullname: String, $avatar: String, $roles: String) {
  data: UserUpdate(
    username: $username
    password: $password
    fullname: $fullname
    avatar: $avatar
    roles: $roles
  ) {
    ...User
  }
}
    ${UserFragmentDoc}`;
export type UserUpdateMutationFn = Apollo.MutationFunction<UserUpdateMutation, UserUpdateMutationVariables>;

/**
 * __useUserUpdateMutation__
 *
 * To run a mutation, you first call `useUserUpdateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUserUpdateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [userUpdateMutation, { data, loading, error }] = useUserUpdateMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *      fullname: // value for 'fullname'
 *      avatar: // value for 'avatar'
 *      roles: // value for 'roles'
 *   },
 * });
 */
export function useUserUpdateMutation(baseOptions?: Apollo.MutationHookOptions<UserUpdateMutation, UserUpdateMutationVariables>) {
        return Apollo.useMutation<UserUpdateMutation, UserUpdateMutationVariables>(UserUpdateDocument, baseOptions);
      }
export type UserUpdateMutationHookResult = ReturnType<typeof useUserUpdateMutation>;
export type UserUpdateMutationResult = Apollo.MutationResult<UserUpdateMutation>;
export type UserUpdateMutationOptions = Apollo.BaseMutationOptions<UserUpdateMutation, UserUpdateMutationVariables>;
export const UserDeleteDocument = gql`
    mutation UserDelete($username: ID!) {
  data: UserDelete(username: $username)
}
    `;
export type UserDeleteMutationFn = Apollo.MutationFunction<UserDeleteMutation, UserDeleteMutationVariables>;

/**
 * __useUserDeleteMutation__
 *
 * To run a mutation, you first call `useUserDeleteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUserDeleteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [userDeleteMutation, { data, loading, error }] = useUserDeleteMutation({
 *   variables: {
 *      username: // value for 'username'
 *   },
 * });
 */
export function useUserDeleteMutation(baseOptions?: Apollo.MutationHookOptions<UserDeleteMutation, UserDeleteMutationVariables>) {
        return Apollo.useMutation<UserDeleteMutation, UserDeleteMutationVariables>(UserDeleteDocument, baseOptions);
      }
export type UserDeleteMutationHookResult = ReturnType<typeof useUserDeleteMutation>;
export type UserDeleteMutationResult = Apollo.MutationResult<UserDeleteMutation>;
export type UserDeleteMutationOptions = Apollo.BaseMutationOptions<UserDeleteMutation, UserDeleteMutationVariables>;