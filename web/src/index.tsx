import "./scss/index.scss";
// React
import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
// graphQl
import { ApolloProvider } from "@apollo/client";
import { client } from "./graphql/client";
// Routing system
import { BrowserRouter as Router } from "react-router-dom";
import { RouterWrapper } from "./router/router";

ReactDOM.render(
  <ApolloProvider client={client}>
    <Router>
      <div id="app-wrapper">
        <RouterWrapper />
      </div>
    </Router>
  </ApolloProvider>,
  document.getElementById("root"),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
