import type { FieldFragment } from "../graphql/types";

export interface Config {
  graphql: {
    http: string;
    ws: string;
  };
  formSchemaBaseFields: Array<FieldFragment>;
}

export const config: Config = {
  graphql: {
    http: window.location.protocol + "//" + window.location.hostname + "/graphql",
    ws: (window.location.protocol === "https:" ? "wss://" : "ws://") + window.location.hostname + "/subscriptions",
  },
  formSchemaBaseFields: [
    {
      name: "name",
      placeholder: "Put the description of the component here",
      required: true,
    },
    {
      name: "icon",
      required: true,
      type: "image",
    },
    {
      name: "description",
      placeholder: "Put the description of the component here",
      type: "textarea",
    },
  ],
};
