import React from "react";
import { uniq, filter, find } from "lodash";
import { useStateUrl } from "../../hooks/state-url";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { Action, ActionWrapper } from "../../components/wrapper/action";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { ModuleBox } from "../../components/model/module";
import { ModuleFragment, useModuleStartMutation, useModuleStopMutation } from "../../graphql/types";
import { getUrlFromRouteId } from "../../router/utils";

interface Props {
  data: Array<ModuleFragment>;
}

export const PageModuleList: React.FC<Props> = (props: Props) => {
  const { data: modules } = props;

  // for types filtering
  const [type, setType] = useStateUrl<string>("type", "");

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  // Mutations
  const mutationParam = useGraphqlMutationParam();
  const [moduleStart, { loading: startLoading, error: startError }] = useModuleStartMutation(mutationParam);
  const [moduleStop, { loading: stopLoading, error: stopError }] = useModuleStopMutation(mutationParam);

  //
  // Module actions
  // ---------------------------------------------------------------------------
  const onModuleStart = async (module: ModuleFragment): Promise<void> => {
    await moduleStart({ variables: { id: module.id } });
  };
  const onModuleStop = async (module: ModuleFragment): Promise<void> => {
    await moduleStop({ variables: { id: module.id } });
  };
  function actionsFor(module: ModuleFragment): Array<Action> {
    const actions = [];
    if (module.discovery) {
      actions.push({
        name: "Discover",
        icon: "magic",
        action: getUrlFromRouteId("module-discovery", { moduleId: module.id }),
      });
    }
    if (!module.isSystemModule) {
      if (module.started) {
        actions.push({ name: "Stop", icon: "stop", action: () => onModuleStop(module) });
      } else {
        actions.push({ name: "Start", icon: "play", action: () => onModuleStart(module) });
      }
    }

    return actions;
  }

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <Spinner loading={startLoading || stopLoading} />
      <ApolloError error={startError || stopError} />

      <div className="container fields horizontal">
        <button className={type === "" ? "selected" : ""} onClick={() => setType("")}>
          All ({modules.length})
        </button>
        {uniq(modules.flatMap((m) => m.categories.map((c) => c.name).filter((e) => e !== "module")))
          .sort()
          .map((tag) => {
            return (
              <button key={tag} className={type === tag ? "selected" : ""} onClick={() => setType(tag)}>
                {tag} ( {filter(modules, (m) => find(m.categories, { name: tag })).length})
              </button>
            );
          })}
      </div>

      {modules
        .filter((m) => (type !== "" ? find(m.categories, { name: type }) : true))
        .map((module: ModuleFragment, index: number) => {
          return (
            <ActionWrapper
              key={index}
              onClick={getUrlFromRouteId("module", { moduleId: module.id })}
              actions={actionsFor(module)}
            >
              <ModuleBox item={module} />
            </ActionWrapper>
          );
        })}
    </>
  );
};
