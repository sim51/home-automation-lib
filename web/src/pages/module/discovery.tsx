import React, { useEffect, useState } from "react";
import { Action, ActionWrapper } from "../../components/wrapper/action";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { ComponentBox } from "../../components/model/component";
import { getUrlFromRouteId } from "../../router/utils";
import {
  ModuleFragment,
  ModuleDiscoveryMutation,
  ComponentFragment,
  useModuleDiscoveryMutation,
  useComponentStartMutation,
  useComponentStopMutation,
} from "../../graphql/types";

interface Props {
  data: ModuleFragment;
}

export const PageModuleDiscovery: React.FC<Props> = (props: Props) => {
  // Component props
  const { data: module } = props;
  const [components, setComponents] = useState<Array<ComponentFragment>>([]);

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  // Mutations
  const [componentStart, { loading: startLoading, error: startError }] = useComponentStartMutation();
  const [componentStop, { loading: stopLoading, error: stopError }] = useComponentStopMutation();
  const [discovery, { loading: discoveryLoading, error: discoveryError }] = useModuleDiscoveryMutation({
    onCompleted: (data: ModuleDiscoveryMutation) => {
      setComponents(data?.data || []);
    },
  });

  //
  // Effect
  // ---------------------------------------------------------------------------
  useEffect(() => {
    discovery({ variables: { id: module.id } });
  }, [module, discovery]);

  // Actions
  // -----------------------------------------------
  const onComponentStart = async (component: ComponentFragment): Promise<void> => {
    await componentStart({ variables: { id: component.id } });
  };
  const onComponentStop = async (component: ComponentFragment): Promise<void> => {
    await componentStop({ variables: { id: component.id } });
  };
  const actionsFor = (component: ComponentFragment): Array<Action> => {
    const actions: Array<Action> = [];
    if (component.started) {
      actions.push({ name: "Stop", icon: "stop", action: () => onComponentStop(component) });
    } else {
      actions.push({ name: "Start", icon: "play", action: () => onComponentStart(component) });
    }
    return actions;
  };

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <Spinner loading={startLoading || stopLoading || discoveryLoading} />
      <ApolloError error={startError || stopError || discoveryError} />

      {!discoveryLoading && components.length === 0 && (
        <div className="container">
          <div className="no-data">
            <p>No new component has been found !!!</p>
          </div>
        </div>
      )}

      {!discoveryLoading &&
        components.map((component, index) => {
          return (
            <ActionWrapper
              key={index}
              onClick={getUrlFromRouteId("component", { componentId: component.id })}
              actions={actionsFor(component)}
            >
              <ComponentBox item={component} />
            </ActionWrapper>
          );
        })}
    </>
  );
};
