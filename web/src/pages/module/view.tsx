import React from "react";
import { useHistory } from "react-router-dom";
import { useStateUrl } from "../../hooks/state-url";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { Action, ActionWrapper } from "../../components/wrapper/action";
import { PopinWrapper } from "../../components/wrapper/popin";
import { BigButtonNew } from "../../components/generic/big-button-new";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { ComponentBox } from "../../components/model/component";
import { getUrlFromRouteId } from "../../router/utils";
import { Link } from "../../router/link";
import {
  ComponentFragment,
  ComponentTypeFragment,
  ModuleFragment,
  useComponentDeleteMutation,
  useComponentStartMutation,
  useComponentStopMutation,
} from "../../graphql/types";

interface Props {
  data: ModuleFragment & { components: Array<ComponentFragment> } & { types: Array<ComponentTypeFragment> };
}

export const PageModuleView: React.FC<Props> = (props: Props) => {
  const history = useHistory();

  // Component props
  const { data: module } = props;

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  // Mutations
  const [componentStart, { loading: startLoading, error: startError }] = useComponentStartMutation(mutationParam);
  const [componentStop, { loading: stopLoading, error: stopError }] = useComponentStopMutation(mutationParam);
  const [componentDelete, { loading: deleteLoading, error: deleteError }] = useComponentDeleteMutation(mutationParam);

  //
  // State
  // ---------------------------------------------------------------------------
  // For deletion
  const [deletion, setDeletion] = useStateUrl<string | null>("delete", null);
  // For the poping with types
  const [displayTypes, setDisplayTypes] = useStateUrl<boolean>("types", false);
  // for types filtering
  const [type, setType] = useStateUrl<string>("type", "");

  //
  // Actions
  // ---------------------------------------------------------------------------
  const onComponentStart = async (component: ComponentFragment): Promise<void> => {
    await componentStart({ variables: { id: component.id } });
  };
  const onComponentStop = async (component: ComponentFragment): Promise<void> => {
    await componentStop({ variables: { id: component.id } });
  };
  function deleteConfirmation(component: ComponentFragment): void {
    setDeletion(component.id);
  }
  async function deleteExecution(): Promise<void> {
    await componentDelete({ variables: { id: deletion || "-1" } });
    setDeletion(null);
  }
  const actionsFor = (component: ComponentFragment): Array<Action> => {
    const actions: Array<Action> = [];
    actions.push({
      name: "Edit",
      icon: "pencil-alt",
      action: getUrlFromRouteId("component-edit", { moduleId: module.id, componentId: component?.id }),
    });
    if (module && !module.isSystemModule) {
      actions.push({
        name: "Delete",
        icon: "times",
        action: () => deleteConfirmation(component),
      });
      if (component.started) {
        actions.push({ name: "Stop", icon: "stop", action: () => onComponentStop(component) });
      } else {
        actions.push({ name: "Start", icon: "play", action: () => onComponentStart(component) });
      }
    }
    return actions;
  };

  function addNewComponent() {
    // redirect to the new page of the only type
    if (module?.types.filter((t) => t.definition != null).length === 1) {
      history.push(
        getUrlFromRouteId("component-new", {
          moduleId: module.id,
          typeId: module.types.filter((t) => t.definition != null)[0].id,
        }),
      );
    }
    // display the popin with the type list
    else {
      setDisplayTypes(true);
    }
  }

  type TypeFacet = { [id: string]: { count: number; name: string; id: string } };
  const types: TypeFacet = module?.components.reduce((acc, curr) => {
    if (curr) {
      if (acc[curr.type.id] !== undefined) {
        acc[curr.type.id].count++;
      } else {
        acc[curr.type.id] = { count: 1, name: curr.type.name, id: curr.type.id };
      }
    }
    return acc;
  }, {} as TypeFacet);

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <Spinner loading={startLoading || stopLoading || deleteLoading} />
      <ApolloError error={startError || stopError || deleteError} />

      {Object.keys(types).length > 1 && (
        <div className="container fields horizontal">
          <button className={type === "" ? "selected" : ""} onClick={() => setType("")}>
            All ({module?.components.length})
          </button>
          {Object.keys(types).map((typeId) => (
            <button
              key={typeId}
              className={type === types[typeId].id ? "selected" : ""}
              onClick={() => setType(types[typeId].id)}
            >
              {types[typeId].name} ({types[typeId].count})
            </button>
          ))}
        </div>
      )}

      {module?.components
        ?.filter((c) => c.type.id.startsWith(type))
        .map((component: ComponentFragment) => {
          return (
            <ActionWrapper
              key={component.id}
              onClick={getUrlFromRouteId("component", { moduleId: module.id, componentId: component.id })}
              actions={actionsFor(component)}
            >
              <ComponentBox item={component} />
            </ActionWrapper>
          );
        })}

      {(!module?.components || module?.components.length === 0) && (
        <div className="container">
          <div className="no-data">
            <h3>There is no component here ...</h3>
            <p>
              You can add a component by clicking <button onClick={addNewComponent}>here</button>
            </p>
            {module.discovery && (
              <p>
                Or you can try to auto discover components by clicking{" "}
                <Link className="button" id="module-discovery" params={{ moduleId: module.id }}>
                  here
                </Link>
              </p>
            )}
          </div>
        </div>
      )}

      {!module?.isSystemModule && (
        <>
          {/* Button for discovery */}
          {module.discovery && (
            <Link className="big-button-new discovery" id="module-discovery" params={{ moduleId: module.id }}>
              <i className="fa fa-magic" aria-hidden="true"></i>
            </Link>
          )}
          {/* Button for create a new component */}
          <BigButtonNew title="Add a component" action={() => addNewComponent()} />
          {/* Popin to choose the component type */}
          <PopinWrapper opened={displayTypes} close={() => setDisplayTypes(false)}>
            <div>
              <h3>Select a component type</h3>
              <div className="fields">
                <div className="field">
                  <div className="label">
                    <label>Type</label>
                  </div>
                  <div className="input">
                    <select
                      onChange={(e) =>
                        history.push(
                          getUrlFromRouteId("component-new", { moduleId: module.id, typeId: e.target.value }),
                        )
                      }
                    >
                      <option value="">Select a type</option>
                      {module.types
                        .filter((t) => t.definition !== null)
                        .map((type) => {
                          //TODO: display the icon
                          return (
                            <option key={type.id} value={type.id}>
                              {type.name}
                            </option>
                          );
                        })}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </PopinWrapper>

          {/* Popin remove */}
          <PopinWrapper opened={deletion ? true : false} close={() => setDeletion(null)}>
            <div className="form">
              <h3>Are you sure you want to delete this component ?</h3>
              <div className="actions">
                <button title="Yes" className="submit" onClick={deleteExecution}>
                  Yes
                </button>
                <button title="No" className="cancel" onClick={() => setDeletion(null)}>
                  No
                </button>
              </div>
            </div>
          </PopinWrapper>
        </>
      )}
    </>
  );
};
