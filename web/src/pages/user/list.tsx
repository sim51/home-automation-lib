import React from "react";
import { Action, ActionWrapper } from "../../components/wrapper/action";
import { useStateUrl } from "../../hooks/state-url";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { PopinWrapper } from "../../components/wrapper/popin";
import { BigButtonNew } from "../../components/generic/big-button-new";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { UserBox } from "../../components/model/user";
import { getUrlFromRouteId } from "../../router/utils";
import { UserFragment, useUserDeleteMutation } from "../../graphql/types";

/**
 * List users
 */
interface Props {
  data: Array<UserFragment>;
}
export const PageUserList: React.FC<Props> = (props: Props) => {
  const { data } = props;

  // State
  // -----------------------------------------------
  const [deletion, setDeletion] = useStateUrl<string | null>("delete", null);

  // GraphQL
  // -----------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  const [deleteUser, { loading, error }] = useUserDeleteMutation(mutationParam);

  // Actions
  // -----------------------------------------------
  function deleteConfirmation(user: UserFragment): void {
    setDeletion(user.username);
  }
  async function deleteExecution(): Promise<void> {
    await deleteUser({ variables: { username: deletion || "-1" } });
    setDeletion(null);
  }
  const actionsFor = (user: UserFragment): Array<Action> => {
    return [
      {
        name: "Edit",
        icon: "pencil-alt",
        action: getUrlFromRouteId("user-edit", { username: user.username }),
      },
      {
        name: "Delete",
        icon: "times",
        action: () => deleteConfirmation(user),
      },
    ];
  };

  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />

      {data.map((user) => (
        <ActionWrapper key={user.username} actions={actionsFor(user)}>
          <UserBox user={user} />
        </ActionWrapper>
      ))}

      {/* Button for create a new item */}
      <BigButtonNew title="Add a user" action={getUrlFromRouteId("user-new")} />

      {/* Popin remove */}
      <PopinWrapper opened={deletion ? true : false} close={() => setDeletion(null)}>
        <div className="form">
          <h3>Are you sure you want to delete this user ?</h3>
          <div className="actions">
            <button title="Yes" className="submit" onClick={deleteExecution}>
              Yes
            </button>
            <button title="No" className="cancel" onClick={() => setDeletion(null)}>
              No
            </button>
          </div>
        </div>
      </PopinWrapper>
    </>
  );
};
