import React from "react";
import { ActionWrapper } from "../../components/wrapper/action";
import { UserBox } from "../../components/model/user";
import { getAuthenticatedUser } from "../../user";
import { getUrlFromRouteId } from "../../router/utils";

/**
 * Account page.
 */
interface Props {}
export const PageUserAccount: React.FC<Props> = (props: Props) => {
  const user = getAuthenticatedUser();
  const actions = [
    {
      name: "Edit",
      icon: "pencil-alt",
      action: getUrlFromRouteId("account-edit"),
    },
  ];

  return (
    <>
      <ActionWrapper actions={actions}>
        <UserBox user={user} />
      </ActionWrapper>
    </>
  );
};
