import React from "react";
import { logout } from "../../user";
import { getRouteById } from "../../router/utils";

/**
 * Logout page.
 */
interface Props {}
export const PageLogout: React.FC<Props> = (props: Props) => {
  logout();

  const route = getRouteById("logout");
  const title = (route?.title ? route?.title : route?.name) || "";
  document.title = title;

  return (
    <div id="page-full">
      <img src="/favicon.svg" alt="Application logo" />
      <div id="auth">
        <h1>{title}</h1>
        <div>
          <p>You have been logged out !</p>
        </div>
      </div>
    </div>
  );
};
