import React, { useState } from "react";
import type { Field } from "hal";
import { useHistory } from "react-router-dom";
import { HalForm } from "../../components/generic/form";
import { authenticate, logout } from "../../user";
import { useLoginMutation } from "../../graphql/types";
import { ApolloError } from "../../components/generic/apollo-error";
import { Spinner } from "../../components/generic/spinner";
import { useStateUrl } from "../../hooks/state-url";
import { getRouteById } from "../../router/utils";

export const fields: Field[] = [
  {
    name: "username",
    placeholder: "Username",
    required: true,
  },
  {
    name: "password",
    placeholder: "Password",
    required: true,
    type: "password",
  },
];

/**
 * Login page.
 */
interface Props {}
export const PageLogin: React.FC<Props> = (props: Props) => {
  const history = useHistory();

  const route = getRouteById("login");
  const title = (route?.title ? route?.title : route?.name) || "";
  document.title = title;

  // clear the
  logout();
  // login redirect url
  const [redirect] = useStateUrl<string>("redirect", "/");
  // Graphql query for login
  const [auth, { loading, error }] = useLoginMutation();
  // we store the login in case of bad password (avoid to retype username)
  const [username, setUsername] = useState<string | null>(null);

  // What we do when we validate the form
  const onSubmit = async (user: { username: string; password: string }): Promise<void> => {
    setUsername(user.username);
    const result = await auth({ variables: user });
    if (result.data?.data) {
      authenticate(result.data.data);
      history.replace(redirect);
    }
  };

  return (
    <div id="page-full">
      <img src="/favicon.svg" alt="Application logo" />
      <div id="auth">
        <h1>{title}</h1>
        {error && <ApolloError error={error} />}
        <Spinner loading={loading} />
        {!loading && <HalForm onSubmit={onSubmit} schema={fields} item={{ username }} />}
      </div>
    </div>
  );
};
