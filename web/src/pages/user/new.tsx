import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { useUserCreateMutation, UserCreateMutationVariables } from "../../graphql/types";
import { UserForm, User } from "../../components/model/user/form";

/**
 * Account page.
 */
interface Props {}
export const PageUserNew: React.FC<Props> = (props: Props) => {
  const history = useHistory();
  //
  // Create user
  // ---------------------------------------------------------------------------
  const [user, setUser] = useState<User | null>(null);
  const [save, { loading, error, called }] = useUserCreateMutation();
  async function onSubmit(data: User): Promise<void> {
    setUser(data);
    if (!data.password || data.password !== data.confirmation) {
      throw new Error("Passwords are not the same");
    }
    await save({ variables: data as UserCreateMutationVariables });
  }

  if (called && !error && !loading) {
    history.goBack();
  }

  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />

      <div className="container">
        <UserForm user={user} onSubmit={onSubmit} onCancel={() => history.goBack()} />
      </div>
    </>
  );
};
