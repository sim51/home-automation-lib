import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { BoxWrapper } from "../../components/wrapper/box";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { useUserUpdateMutation } from "../../graphql/types";
import { UserForm, User } from "../../components/model/user/form";

/**
 * Edit a user profile
 */
interface Props {
  data: User;
}
export const PageUserEdit: React.FC<Props> = (props: Props) => {
  const history = useHistory();

  //
  // Update user's info
  // ---------------------------------------------------------------------------
  const [user, setUser] = useState<User | null>(props.data);
  const [save, { loading, error, called }] = useUserUpdateMutation();
  async function onSubmit(data: User): Promise<void> {
    setUser(data);
    if ((data.password || data.confirmation) && data.password === data.confirmation) {
      throw new Error("Passwords are identical");
    } else {
      data = Object.assign(data, { password: null, confirmation: null });
    }
    await save({ variables: Object.assign(data, { roles: data?.roles?.pop() }) });
  }

  if (called && !error && !loading) {
    history.goBack();
  }

  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />
      <BoxWrapper>
        <div className="info">
          <UserForm user={user || undefined} onSubmit={onSubmit} onCancel={() => history.goBack()} />
        </div>
      </BoxWrapper>
    </>
  );
};
