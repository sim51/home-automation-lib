import React from "react";
import { useHistory } from "react-router-dom";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { PlaceForm } from "../../components/model/place/form";
import { PlaceFragment, usePlaceCreateMutation } from "../../graphql/types";

export const PagePlaceNew: React.FC = () => {
  const history = useHistory();

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const [placeCreate, { loading, error, called }] = usePlaceCreateMutation();
  if (called && !error && !loading) {
    history.goBack();
  }

  //
  // Actions
  // ---------------------------------------------------------------------------
  async function save(place: PlaceFragment): Promise<void> {
    await placeCreate({ variables: Object.assign({}, place) });
  }

  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />

      <div className="container">
        <PlaceForm onSubmit={save} onCancel={() => history.goBack()} />
      </div>
    </>
  );
};
