import React from "react";
import { useHistory } from "react-router-dom";
import { Spinner } from "../../components/generic/spinner";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { ApolloError } from "../../components/generic/apollo-error";
import { PlaceForm } from "../../components/model/place/form";
import { PlaceFragment, usePlaceUpdateMutation } from "../../graphql/types";

interface Props {
  data: PlaceFragment;
}

export const PagePlaceEdit: React.FC<Props> = (props: Props) => {
  const history = useHistory();

  // Component props
  const { data: place } = props;

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  const [placeUpdate, { loading, error, called }] = usePlaceUpdateMutation(mutationParam);
  if (called && !error && !loading) {
    history.goBack();
  }
  //
  // Actions
  // ---------------------------------------------------------------------------
  async function save(place: PlaceFragment): Promise<void> {
    await placeUpdate({ variables: Object.assign({}, place) });
  }

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />

      <div className="container">
        <PlaceForm place={place} onSubmit={save} onCancel={() => history.goBack()} />
      </div>
    </>
  );
};
