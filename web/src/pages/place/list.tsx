import React from "react";
import { useStateUrl } from "../../hooks/state-url";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { Action, ActionWrapper } from "../../components/wrapper/action";
import { PopinWrapper } from "../../components/wrapper/popin";
import { BigButtonNew } from "../../components/generic/big-button-new";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { PlaceBox } from "../../components/model/place";
import { PlaceFragment, usePlaceDeleteMutation } from "../../graphql/types";
import { getUrlFromRouteId } from "../../router/utils";
import { Link } from "../../router/link";

interface Props {
  data: Array<PlaceFragment>;
}

export const PagePlaceList: React.FC<Props> = (props: Props) => {
  const { data: places } = props;

  // GraphQL
  // -----------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  const [placeDelete, { loading, error }] = usePlaceDeleteMutation(mutationParam);

  // State
  // -----------------------------------------------
  const [deletion, setDeletion] = useStateUrl<string | null>("delete", null);

  // Actions
  // -----------------------------------------------
  function deleteConfirmation(place: PlaceFragment): void {
    setDeletion(place.id);
  }
  async function deleteExecution(): Promise<void> {
    await placeDelete({ variables: { id: deletion || "-1" } });
    setDeletion(null);
  }
  const actionsFor = (place: PlaceFragment): Array<Action> => {
    return [
      {
        name: "Edit",
        icon: "pencil-alt",
        action: getUrlFromRouteId("place-edit", { placeId: place.id }),
      },
      {
        name: "Delete",
        icon: "times",
        action: () => deleteConfirmation(place),
      },
    ];
  };

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />

      {(!places || places.length === 0) && (
        <div className="container">
          <div className="no-data">
            <h3>There is no place here ...</h3>
            <p>
              You can add a place by clicking{" "}
              <Link className="button" id="place-new">
                here
              </Link>
            </p>
          </div>
        </div>
      )}

      {places.map((place: PlaceFragment, index: number) => {
        return (
          <ActionWrapper
            key={index}
            onClick={getUrlFromRouteId("place", { placeId: place.id })}
            actions={actionsFor(place)}
          >
            <PlaceBox item={place} />
          </ActionWrapper>
        );
      })}

      {/* Button for create a new item */}
      <BigButtonNew title="Add a place" action={getUrlFromRouteId("place-new")} />

      {/* Popin remove */}
      <PopinWrapper opened={deletion ? true : false} close={() => setDeletion(null)}>
        <div className="form">
          <h3>Are you sure you want to delete this place ?</h3>
          <div className="actions">
            <button title="Yes" className="submit" onClick={deleteExecution}>
              Yes
            </button>
            <button title="No" className="cancel" onClick={() => setDeletion(null)}>
              No
            </button>
          </div>
        </div>
      </PopinWrapper>
    </>
  );
};
