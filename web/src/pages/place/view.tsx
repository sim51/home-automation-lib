import React from "react";
import { useStateUrl } from "../../hooks/state-url";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { Action, ActionWrapper } from "../../components/wrapper/action";
import { PopinWrapper } from "../../components/wrapper/popin";
import { BigButtonNew } from "../../components/generic/big-button-new";
import { ApolloError } from "../../components/generic/apollo-error";
import { Spinner } from "../../components/generic/spinner";
import { RoomBox } from "../../components/model/room";
import { ComponentBox } from "../../components/model/component";
import { SearchComponentForm } from "../../components/model/component/searchForm";
import {
  ComponentFragment,
  PlaceFragment,
  RoomFragment,
  useRoomDeleteMutation,
  usePlaceRemoveComponentMutation,
  usePlaceAddComponentMutation,
} from "../../graphql/types";
import { getUrlFromRouteId } from "../../router/utils";
import { Link } from "../../router/link";

interface Props {
  data: PlaceFragment & { rooms: Array<RoomFragment>; components: Array<ComponentFragment> };
}

export const PagePlaceView: React.FC<Props> = (props: Props) => {
  // Component props
  const { data: place } = props;

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  // Mutations
  const mutationParam = useGraphqlMutationParam();
  const [roomDelete, { loading: deleteRoomLoading, error: deleteRoomError }] = useRoomDeleteMutation(mutationParam);
  const [
    componentDelete,
    { loading: deleteComponentLoading, error: deleteComponentError },
  ] = usePlaceRemoveComponentMutation(mutationParam);
  const [componentAdd, { loading: addComponentLoading, error: addComponentError }] = usePlaceAddComponentMutation(
    mutationParam,
  );

  //
  // State
  // ---------------------------------------------------------------------------
  // For deletion
  const [roomDeletion, setRoomDeletion] = useStateUrl<string | null>("rdelete", null);
  const [componentDeletion, setComponentDeletion] = useStateUrl<string | null>("cdelete", null);
  const [addNew, setAddNew] = useStateUrl<string | null>("new", null);

  // Actions
  // -----------------------------------------------
  // Room
  function deleteRoomConfirmation(room: RoomFragment): void {
    setRoomDeletion(room.id);
  }
  async function roomDeleteExecution(): Promise<void> {
    await roomDelete({ variables: { id: roomDeletion || "-1" } });
    setRoomDeletion(null);
  }
  const actionsForRoom = (room: RoomFragment): Array<Action> => {
    return [
      {
        name: "Edit",
        icon: "pencil-alt",
        action: getUrlFromRouteId("room-edit", { placeId: place.id, roomId: room.id }),
      },
      {
        name: "Delete",
        icon: "times",
        action: () => deleteRoomConfirmation(room),
      },
    ];
  };

  // Component
  function deleteComponentConfirmation(component: ComponentFragment): void {
    setComponentDeletion(component.id);
  }
  async function componentDeleteExecution(): Promise<void> {
    await componentDelete({ variables: { place: place.id, component: componentDeletion || "-1" } });
    setComponentDeletion(null);
  }
  async function componentAddExecution(id: string) {
    await componentAdd({ variables: { place: place.id, component: id } });
    setAddNew(null);
  }
  const actionsForComponent = (component: ComponentFragment): Array<Action> => {
    return [
      {
        name: "Delete",
        icon: "times",
        action: () => deleteComponentConfirmation(component),
      },
    ];
  };

  //
  // Render phase
  // ---------------------------------------------------------------------------

  return (
    <>
      <Spinner loading={deleteRoomLoading || deleteComponentLoading || addComponentLoading} />
      <ApolloError error={deleteRoomError || deleteComponentError || addComponentError} />

      {(!place?.rooms || place?.rooms.length === 0) && (
        <div className="container">
          <div className="no-data">
            <h3>There is no room here ...</h3>
            <p>
              You can add a room by clicking{" "}
              <Link className="button" id="room-new" params={{ placeId: place.id }}>
                here
              </Link>
            </p>
          </div>
        </div>
      )}

      {place?.rooms?.map((room: RoomFragment) => {
        return (
          <ActionWrapper
            key={room.id}
            onClick={getUrlFromRouteId("room", { placeId: place.id, roomId: room.id })}
            actions={actionsForRoom(room)}
          >
            <RoomBox item={room} />
          </ActionWrapper>
        );
      })}

      {(!place?.components || place?.components.length === 0) && (
        <div className="container">
          <div className="no-data">
            <h3>There is no component here ...</h3>
            <p>
              You can add a component by clicking{" "}
              <button title="Add a component to the place" onClick={() => setAddNew("component")}>
                here
              </button>
            </p>
          </div>
        </div>
      )}

      {place?.components?.map((component: ComponentFragment) => {
        return (
          <ActionWrapper key={component.id} actions={actionsForComponent(component)}>
            <ComponentBox item={component} />
          </ActionWrapper>
        );
      })}

      {/* Button for create a new item */}
      <BigButtonNew title="Add a room / component" action={() => setAddNew("new")} />

      {/* Popin remove room*/}
      <PopinWrapper opened={roomDeletion ? true : false} close={() => setRoomDeletion(null)}>
        <div className="form">
          <h3>Are you sure you want to delete this room ?</h3>
          <div className="actions">
            <button title="Yes" className="submit" onClick={roomDeleteExecution}>
              Yes
            </button>
            <button title="No" className="cancel" onClick={() => setRoomDeletion(null)}>
              No
            </button>
          </div>
        </div>
      </PopinWrapper>

      {/* Popin remove component */}
      <PopinWrapper opened={componentDeletion ? true : false} close={() => setComponentDeletion(null)}>
        <div className="form">
          <h3>Are you sure you want to remove this component ?</h3>
          <div className="actions">
            <button title="Yes" className="submit" onClick={componentDeleteExecution}>
              Yes
            </button>
            <button title="No" className="cancel" onClick={() => setComponentDeletion(null)}>
              No
            </button>
          </div>
        </div>
      </PopinWrapper>

      {/* Popin to choose the component type */}
      <PopinWrapper opened={addNew === "component"} close={() => setAddNew(null)}>
        <div className="form">
          <h3>Which component do you want to add ?</h3>
          <SearchComponentForm onSubmit={(id) => componentAddExecution(id)} onCancel={() => setAddNew(null)} />
        </div>
      </PopinWrapper>

      {/* Popin to choose between room creation and add a component */}
      <PopinWrapper opened={addNew === "new"} close={() => setAddNew(null)}>
        <div className="form">
          <h3>What do you want to do?</h3>
          <div className="actions">
            <Link className="button" id="room-new" params={{ placeId: place.id, placeName: place.name }}>
              Create a room
            </Link>
            <button
              title="Link a component to this place"
              onClick={() => {
                setAddNew("component");
              }}
            >
              Add a component
            </button>
          </div>
        </div>
      </PopinWrapper>
    </>
  );
};
