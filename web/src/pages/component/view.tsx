import React, { useEffect, useState } from "react";
import { useStateUrl } from "../../hooks/state-url";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { getUrlFromRouteId } from "../../router/utils";
import { Action, ActionWrapper } from "../../components/wrapper/action";
import { PopinWrapper } from "../../components/wrapper/popin";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { Help } from "../../components/generic/help";
import { ComponentBox } from "../../components/model/component/index";
import { HeatMap } from "../../components/dataviz/heatmap";
import { TimeSelector } from "../../components/generic/time-selector";
import { componentToLogLabel, displayDateTime, displayDateTimeToDate, roundNumber } from "../../utils";
import { Column, DataTable } from "../../components/generic/data-table";
import { Histogram, HistogramDataType } from "../../components/dataviz/histogram";
import {
  ComponentFragment,
  ComponentMeasureFragment,
  ModuleFragment,
  useComponentDeleteMutation,
  useComponentStartMutation,
  useComponentStopMutation,
  LogEntry,
  useGetLogsLazyQuery,
} from "../../graphql/types";

interface Props {
  data: {
    component: ComponentFragment & { module: ModuleFragment };
    measurements: Array<ComponentMeasureFragment>;
  };
}

export const PageComponentView: React.FC<Props> = (props: Props) => {
  const { component, measurements } = props.data;

  //
  // State
  // ---------------------------------------------------------------------------
  // For deletion
  const [deletion, setDeletion] = useStateUrl<boolean>("delete", false);
  const [start, setStart] = useStateUrl<string>("start", "-1d");
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [stop, setStop] = useStateUrl<string>("stop", "now()");
  const [desc, setDesc] = useStateUrl<boolean>("desc", true);
  const [logs, setLogs] = useState<Array<LogEntry>>([]);
  const [logsHisto, setLogsHisto] = useState<Array<HistogramDataType>>([]);

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  // Mutations
  const [componentStart, { loading: startLoading, error: startError }] = useComponentStartMutation(mutationParam);
  const [componentStop, { loading: stopLoading, error: stopError }] = useComponentStopMutation(mutationParam);
  const [componentDelete, { loading: deleteLoading, error: deleteError }] = useComponentDeleteMutation(mutationParam);
  // Gql query for inifite scroll
  const [getMore, { loading: logsLoading, error: logsError, data: more }] = useGetLogsLazyQuery();
  window.onscroll = async () => {
    if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.scrollHeight) {
      if (logs?.length > 0) {
        getMore({
          variables: {
            start,
            stop: logs[0]?.time || "now()",
            label: componentToLogLabel(component),
            skip: logs?.length || 0,
            limit: 10,
            desc: desc,
          },
        });
      }
    }
  };

  //
  // Column definition
  // ---------------------------------------------------------------------------
  const columns: Array<Column> = [
    { prop: "time", title: "Date / Time", size: 15, center: true, format: (e) => displayDateTime(e) },
    { prop: "level", title: "Level", size: 10, center: true },
    { prop: "message", title: "message", size: 75 },
  ];

  // If the stop date is not now, we order the logs by ASC. Otherwise it's DESC
  useEffect(() => {
    setDesc(stop === "now()" ? true : false);
  }, [stop, setDesc]);

  // on mount, we check the component and logs the correspondig logs
  useEffect(() => {
    getMore({ variables: { start, label: componentToLogLabel(component), stop: "now()", limit: 10 } });
  }, [component, getMore, start]);

  // if more changed, we add its contents to logs
  useEffect(() => {
    if (more) {
      setLogs((logs) => logs.concat(more?.data?.data || []));
      if (more && more.viz) {
        setLogsHisto(
          more.viz.map((e) => {
            return { label: displayDateTime(e.time), data: e.data } as HistogramDataType;
          }),
        );
      }
    }
  }, [more]);

  //
  // Actions
  // ---------------------------------------------------------------------------
  const onComponentStart = async (): Promise<void> => {
    await componentStart({ variables: { id: component.id } });
  };
  const onComponentStop = async (): Promise<void> => {
    await componentStop({ variables: { id: component.id } });
  };
  async function deleteExecution(): Promise<void> {
    await componentDelete({ variables: { id: component.id } });
    setDeletion(false);
  }
  const actions: Array<Action> = [];
  actions.push({
    name: "Edit",
    icon: "pencil-alt",
    action: getUrlFromRouteId("component-edit", { moduleId: component.module.id, componentId: component.id }),
  });
  if (!component.module.isSystemModule) {
    actions.push({
      name: "Delete",
      icon: "times",
      action: () => setDeletion(true),
    });
  }
  if (component.started) {
    actions.push({ name: "Stop", icon: "stop", action: onComponentStop });
  } else {
    actions.push({ name: "Start", icon: "play", action: onComponentStart });
  }
  return (
    <>
      <Spinner loading={startLoading || stopLoading || deleteLoading} />
      <ApolloError error={startError || stopError || deleteError || logsError} />

      <ActionWrapper actions={actions}>
        <ComponentBox item={component} />
      </ActionWrapper>

      <div className="fields container horizontal">
        <TimeSelector
          name="start"
          label="Period"
          value={start}
          onChange={(e) => {
            setStart(e.target.value);
            setStop("now()");
          }}
        />
      </div>

      <div className="container measurements">
        {measurements?.map((measurement) => {
          return (
            <div key={measurement.name} className="measurement">
              <h5>
                {measurement.label || measurement.name} <Help text={measurement.help} />
              </h5>
              <HeatMap
                data={measurement.data.map((e) => {
                  return { label: displayDateTime(e.time), value: e.value };
                })}
                colors={measurement.colors || ["#FFFFFF", "#008000"]}
                noDataColor="grey"
                min={measurement.min}
                max={measurement.max}
                height="20px"
                onSelectionEnd={(a, b) => {
                  if (a.label !== b.label) {
                    setStart(displayDateTimeToDate(a.label).toISOString());
                    setStop(displayDateTimeToDate(b.label).toISOString());
                  }
                }}
                Tooltip={({ data, min, max }) => {
                  return (
                    <>
                      <h6>{measurement.label || measurement.name}</h6>
                      <ul>
                        <li>
                          <span className="property">Time:</span>
                          <span className="value">{data.label}</span>
                        </li>
                        <li>
                          <span className="property">Value:</span>
                          <span className="value">
                            {data.value !== undefined && data.value !== null ? roundNumber(data?.value) : "No data"}{" "}
                            {measurement?.unit}
                          </span>
                        </li>
                        <li>
                          <span className="property">Min:</span>
                          <span className="value">
                            {roundNumber(min)} {measurement?.unit}
                          </span>
                        </li>
                        <li>
                          <span className="property">Max:</span>
                          <span className="value">
                            {roundNumber(max)} {measurement?.unit}
                          </span>
                        </li>
                      </ul>
                    </>
                  );
                }}
              />
              <div className="current-value">
                {roundNumber(component.measure[measurement.name])} {measurement.unit}
              </div>
            </div>
          );
        })}
      </div>

      <div className="container">
        <Histogram
          className="spaced"
          data={logsHisto}
          height="100px"
          min={0}
          colors={{ ERROR: "#dc3545", WARN: "#fd7e14", INFO: "#9A00FF", DEBUG: "#2164a3" }}
          onSelectionEnd={(a, b) => {
            if (a.label !== b.label) {
              setStart(displayDateTimeToDate(a.label).toISOString());
              setStop(displayDateTimeToDate(b.label).toISOString());
            }
          }}
        />
      </div>

      <div className="container logs">
        {logs.length === 0 && logsLoading ? <p>Loading logs ...</p> : <DataTable data={logs} columns={columns} />}
      </div>

      {/* Popin remove */}
      <PopinWrapper opened={deletion === true} close={() => setDeletion(false)}>
        <div className="form">
          <h3>Are you sure you want to delete this component ?</h3>
          <div className="actions">
            <button title="Yes" className="submit" onClick={deleteExecution}>
              Yes
            </button>
            <button title="No" className="cancel" onClick={() => setDeletion(false)}>
              No
            </button>
          </div>
        </div>
      </PopinWrapper>
    </>
  );
};
