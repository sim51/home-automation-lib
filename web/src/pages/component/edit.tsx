import React from "react";
import { useHistory } from "react-router-dom";
import { omit } from "lodash";
import { config } from "../../config";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { HalForm } from "../../components/generic/form";
import { useComponentUpdateMutation, ComponentFragment, ModuleFragment, ComponentInfoInput } from "../../graphql/types";

interface Props {
  data: ComponentFragment & { module: ModuleFragment };
}

export const PageComponentEdit: React.FC<Props> = (props: Props) => {
  const history = useHistory();
  const { data: component } = props;

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  const [componentUpdate, { loading, error, called }] = useComponentUpdateMutation(mutationParam);
  if (called && !error && !loading) {
    history.goBack();
  }

  //
  // Actions
  // ---------------------------------------------------------------------------
  async function save(formData: any): Promise<void> {
    const info: ComponentInfoInput = {
      id: component?.id || "-1",
      icon: formData.icon,
      name: formData.name || "-1",
      description: formData.description,
      categories: component?.type?.categories.map((e) => e.name) || [],
      module: component?.module.id || "-1",
      type: component?.type?.id || "-1",
      config: omit(formData, ["id"].concat(config.formSchemaBaseFields.map((e) => e.name))),
    };
    await componentUpdate({ variables: { component: info } });
  }

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />

      <div className="container">
        <HalForm
          onSubmit={save}
          onCancel={() => history.goBack()}
          schema={config.formSchemaBaseFields.concat(component?.type?.definition || [])}
          item={Object.assign(
            {
              icon: component?.icon,
              name: component?.name,
              description: component?.description,
            },
            component?.config,
          )}
        />
      </div>
    </>
  );
};
