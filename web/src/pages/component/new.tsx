import React from "react";
import { useHistory } from "react-router-dom";
import { v4 as uuid } from "uuid";
import { omit } from "lodash";
import { config } from "../../config";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { HalForm } from "../../components/generic/form";
import {
  useComponentCreateMutation,
  ComponentTypeFragment,
  ModuleFragment,
  ComponentInfoInput,
} from "../../graphql/types";

interface Props {
  data: ComponentTypeFragment & { module: ModuleFragment };
}

export const PageComponentNew: React.FC<Props> = (props: Props) => {
  const history = useHistory();
  const { data: componentType } = props;

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  const [componentCreate, { loading, error, called }] = useComponentCreateMutation(mutationParam);
  if (called && !error && !loading) {
    history.goBack();
  }

  //
  // Actions
  // ---------------------------------------------------------------------------
  async function save(formData: any): Promise<void> {
    const info: ComponentInfoInput = {
      id: uuid(),
      icon: formData.icon,
      name: formData.name,
      description: formData.description,
      categories: componentType?.categories.map((e) => e.name) || [],
      module: componentType?.module.id || "-1",
      type: componentType?.id || "-1",
      config: omit(
        formData,
        config.formSchemaBaseFields.map((e) => e.name),
      ),
    };
    await componentCreate({ variables: { component: info } });
  }

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />

      <div className="container">
        <HalForm
          onSubmit={save}
          onCancel={() => history.goBack()}
          schema={config.formSchemaBaseFields.concat(componentType?.definition || [])}
          item={{
            icon: componentType?.icon,
            name: componentType?.name,
          }}
        />
      </div>
    </>
  );
};
