import React from "react";
import { useHistory } from "react-router-dom";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { Spinner } from "../../components/generic/spinner";
import { RoomForm } from "../../components/model/room/form";
import { ApolloError } from "../../components/generic/apollo-error";
import { RoomFragment, useRoomUpdateMutation } from "../../graphql/types";

interface Props {
  data: RoomFragment;
}

export const PageRoomEdit: React.FC<Props> = (props: Props) => {
  const history = useHistory();
  // Component props
  const { data: room } = props;

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  const [roomUpdate, { loading, error, called }] = useRoomUpdateMutation(mutationParam);
  if (called && !error && !loading) {
    history.goBack();
  }

  //
  // Actions
  // ---------------------------------------------------------------------------
  async function save(room: RoomFragment): Promise<void> {
    await roomUpdate({ variables: Object.assign({}, room) });
  }

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />

      <div className="container">
        <RoomForm room={room} onSubmit={save} onCancel={() => history.goBack()} />
      </div>
    </>
  );
};
