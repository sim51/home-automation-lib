import React from "react";
import { useStateUrl } from "../../hooks/state-url";
import { useGraphqlMutationParam } from "../../hooks/graph-mutation-param";
import { Action, ActionWrapper } from "../../components/wrapper/action";
import { PopinWrapper } from "../../components/wrapper/popin";
import { BigButtonNew } from "../../components/generic/big-button-new";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { ComponentBox } from "../../components/model/component";
import { SearchComponentForm } from "../../components/model/component/searchForm";
import {
  ComponentFragment,
  RoomFragment,
  useRoomMergeComponentMutation,
  useRoomRemoveComponentMutation,
} from "../../graphql/types";

interface Props {
  data: RoomFragment & { components?: Array<ComponentFragment> };
}

export const PageRoomView: React.FC<Props> = (props: Props) => {
  const { data: room } = props;

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const mutationParam = useGraphqlMutationParam();
  const [componentAdd, { loading: addLoading, error: addError }] = useRoomMergeComponentMutation(mutationParam);
  const [componentRemove, { loading: removeLoading, error: removeError }] = useRoomRemoveComponentMutation(
    mutationParam,
  );

  //
  // State
  // ---------------------------------------------------------------------------
  // For deletion
  const [remove, setRemove] = useStateUrl<string | null>("delete", null);
  // For the poping with types
  const [displayComponents, setDisplayComponents] = useStateUrl<boolean>("components", false);

  //
  // Actions
  // ---------------------------------------------------------------------------
  const actionsFor = (component: ComponentFragment): Array<Action> => {
    return [
      {
        name: "Delete",
        icon: "times",
        action: () => removeConfirmation(component),
      },
    ];
  };
  async function addComponentExecution(id: string) {
    await componentAdd({ variables: { room: room.id, component: id } });
    setDisplayComponents(false);
  }
  function removeConfirmation(component: ComponentFragment): void {
    setRemove(component.id);
  }
  async function removeExecution(): Promise<void> {
    await componentRemove({ variables: { room: room.id, component: remove || "-1" } });
    setRemove(null);
  }

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <Spinner loading={addLoading || removeLoading} />
      <ApolloError error={addError || removeError} />

      {(!room?.components || room?.components.length === 0) && (
        <div className="container">
          <div className="no-data">
            <h3>There is no component here ...</h3>
            <p>
              You can add a component by clicking <button onClick={() => setDisplayComponents(true)}>here</button>
            </p>
          </div>
        </div>
      )}

      {room?.components?.map((component: ComponentFragment) => {
        return (
          <ActionWrapper key={component.id} actions={actionsFor(component)}>
            <ComponentBox item={component} />
          </ActionWrapper>
        );
      })}

      {/* Button to add a component */}
      <BigButtonNew title="Add a component" action={() => setDisplayComponents(true)} />

      {/* Popin to choose the component type */}
      <PopinWrapper opened={displayComponents} close={() => setDisplayComponents(false)}>
        <div className="form">
          <h3>Which component do you want to add ?</h3>
          <SearchComponentForm
            onSubmit={(id) => addComponentExecution(id)}
            onCancel={() => setDisplayComponents(false)}
          />
        </div>
      </PopinWrapper>

      {/* Popin remove */}
      <PopinWrapper opened={remove ? true : false} close={() => setRemove(null)}>
        <div className="form">
          <h3>Are you sure you want to remove this component of the room?</h3>
          <div className="actions">
            <button title="Yes" className="submit" onClick={removeExecution}>
              Yes
            </button>
            <button title="No" className="cancel" onClick={() => setRemove(null)}>
              No
            </button>
          </div>
        </div>
      </PopinWrapper>
    </>
  );
};
