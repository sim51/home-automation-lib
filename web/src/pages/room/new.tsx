import React from "react";
import { useHistory } from "react-router-dom";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { RoomForm } from "../../components/model/room/form";
import { RoomFragment, useRoomCreateMutation } from "../../graphql/types";

interface Props {
  placeId: string;
}

export const PageRoomNew: React.FC<Props> = (props: Props) => {
  const { placeId } = props;
  const history = useHistory();

  //
  // GraphQL
  // ---------------------------------------------------------------------------
  const [roomCreate, { loading, error, called }] = useRoomCreateMutation();
  if (called && !error && !loading) {
    history.goBack();
  }

  //
  // Actions
  // ---------------------------------------------------------------------------
  async function save(room: RoomFragment): Promise<void> {
    await roomCreate({ variables: Object.assign({ place: placeId }, room) });
  }

  return (
    <>
      <Spinner loading={loading} />
      <ApolloError error={error} />

      <div className="container">
        <RoomForm onSubmit={save} onCancel={() => history.goBack()} />
      </div>
    </>
  );
};
