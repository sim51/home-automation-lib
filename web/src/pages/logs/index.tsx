import React, { useState, useEffect } from "react";
import { useStateUrl } from "../../hooks/state-url";
import { Spinner } from "../../components/generic/spinner";
import { ApolloError } from "../../components/generic/apollo-error";
import { Column, DataTable } from "../../components/generic/data-table";
import { LogResult, LogEntry, useGetLogsLazyQuery } from "../../graphql/types";
import { displayDateTime, displayDateTimeToDate } from "../../utils";
import { TimeSelector } from "../../components/generic/time-selector";
import { Histogram } from "../../components/dataviz/histogram";
import { InfiniteScrollWrapper } from "../../components/wrapper/infinite-scroll";

interface Props {
  data: { data: LogResult; histogram: Array<any> };
}

export const PageLogs: React.FC<Props> = (props: Props) => {
  const { data } = props;
  const [logs, setLogs] = useState<Array<LogEntry>>(data.data?.data || []);
  //
  // Url params
  // ---------------------------------------------------------------------------
  const [level, setLevel] = useStateUrl<string>("level", "");
  const [label, setLabel] = useStateUrl<string>("label", "");
  const [start, setStart] = useStateUrl<string>("start", "-1h");
  const [stop, setStop] = useStateUrl<string>("stop", "now()");
  const [desc, setDesc] = useStateUrl<boolean>("desc", true);

  // Gql query for inifite scroll
  const [getMore, { loading, error, data: more }] = useGetLogsLazyQuery();

  // If the stop date is not now, we order the logs by ASC. Otherwise it's DESC
  useEffect(() => {
    setDesc(stop === "now()" ? true : false);
  }, [stop, setDesc]);

  useEffect(() => {
    setLogs(data.data?.data || []);
  }, [data]);

  useEffect(() => {
    if (more) {
      setLogs((logs) => logs.concat(more?.data?.data || []));
    }
  }, [more]);

  //
  // Column definition
  // ---------------------------------------------------------------------------
  const columns: Array<Column> = [
    { prop: "time", title: "Date / Time", size: 15, center: true, format: (e) => displayDateTime(e) },
    { prop: "level", title: "Level", size: 5, center: true },
    { prop: "label", title: "Label", size: 25, center: true },
    { prop: "message", title: "message", size: 55 },
  ];

  //
  // Render phase
  // ---------------------------------------------------------------------------
  return (
    <>
      <ApolloError error={error} />
      <div className="fields container horizontal">
        <TimeSelector
          name="start"
          label="Period"
          value={start}
          onChange={(e) => {
            setStart(e.target.value);
            setStop("now()");
          }}
        />
        <div className="field">
          <div className="label">
            <label htmlFor="level">Level</label>
          </div>
          <div className="input">
            <div className="select">
              <select id="level" onChange={(e) => setLevel(e.target.value)} value={level}>
                <option value="">All</option>
                {data?.data?.levels?.map((value) => (
                  <option key={value} value={value}>
                    {value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
        <div className="field">
          <div className="label">
            <label htmlFor="label">Label</label>
          </div>
          <div className="input">
            <div className="select">
              <select id="label" onChange={(e) => setLabel(e.target.value)}>
                <option value="">All</option>
                {data?.data?.labels?.map((value) => (
                  <option key={value} value={`${value}.*`}>
                    {value}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>

      {data.histogram && (
        <div className="container">
          <Histogram
            className="spaced"
            data={data.histogram.map((e) => {
              return { label: displayDateTime(e.time), data: e.data };
            })}
            height="100px"
            min={0}
            colors={{ ERROR: "#dc3545", WARN: "#fd7e14", INFO: "#9A00FF", DEBUG: "#2164a3" }}
            onSelectionEnd={(a, b) => {
              if (a.label !== b.label) {
                setStart(displayDateTimeToDate(a.label).toISOString());
                setStop(displayDateTimeToDate(b.label).toISOString());
              }
            }}
          />
        </div>
      )}

      {logs && (
        <InfiniteScrollWrapper
          className="container"
          action={() => {
            if (logs?.length > 0) {
              getMore({
                variables: {
                  start,
                  stop: logs[0].time,
                  level,
                  label,
                  skip: logs?.length,
                  desc: desc,
                },
              });
            }
          }}
        >
          <DataTable data={logs} columns={columns} />
        </InfiniteScrollWrapper>
      )}
      <Spinner loading={loading} />
    </>
  );
};
