import events from "events";
events.EventEmitter["captureRejections"] = true;

const emit: events.EventEmitter = new events.EventEmitter();
emit.setMaxListeners(0);

export { emit };
