export function resolveService(serviceId: string): string {
  return serviceId.includes(":") ? serviceId : `urn:upnp-org:serviceId:${serviceId}`;
}
