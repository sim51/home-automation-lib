export interface Icon {
  mimetype: string;
  width: number;
  height: number;
  url: string;
}

export interface Service {
  serviceType: string;
  serviceId: string;
  SCPDURL: string;
  controlURL: string;
  eventSubURL: string;
}

export interface ServiceDescription {
  actions?: Actions;
  stateVariables?: { [key: string]: StateVariable };
}

export interface ActionInOut {
  inputs: Array<ActionArgument>;
  outputs: Array<ActionArgument>;
}

export interface Actions {
  [name: string]: {
    inputs: Array<ActionArgument>;
    outputs: Array<ActionArgument>;
  };
}
export interface ActionArgument {
  name: string;
  relatedStateVariable: string;
}

export interface StateVariable {
  name: string;
  sendEvents: boolean;
  dataType: string;
  defaultValue: string;
  allowedValues: Array<string>;
}

export interface Device {
  deviceType: string;
  friendlyName: string;
  manufacturer: string;
  manufacturerURL: string;
  modelName: string;
  modelNumber: string;
  modelDescription: string;
  UDN: string;
  icons: Array<Icon>;
  services: { [servcieId: string]: Service };
}
