import { parseTime } from "./time";

function toArray(value: string): Array<string> {
  return value.split(",");
}
export const CurrentMediaDuration = parseTime;
export const CurrentTrackDuration = parseTime;
export const CurrentTransportActions = toArray;
export const PossiblePlaybackStorageMedia = toArray;
