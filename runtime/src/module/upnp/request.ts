import Parser from "fast-xml-parser";

const parser = new Parser.j2xParser({
  attributeNamePrefix: "@",
  ignoreAttributes: false,
});

function getData(data: null | undefined | Record<string, unknown>): { [key: string]: string } {
  if (!data) {
    return {};
  }

  return Object.keys(data).reduce((a, name) => {
    const value = data[name];
    if (value !== undefined) {
      a[name] = value === null ? "" : value.toString();
    }

    return a;
  }, {});
}

export function createSOAPAction(
  serviceType: string,
  actionName: string,
  data: null | undefined | Record<string, unknown>,
): string {
  const envelope = {
    "s:Envelope": {
      "@xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/",
      "@s:encodingStyle": "http://schemas.xmlsoap.org/soap/encoding/",
      "s:Body": {
        [`u:${actionName}`]: {
          "@xmlns:u": serviceType,
          ...getData(data),
        },
      },
    },
  };

  return parser.parse(envelope);
}
