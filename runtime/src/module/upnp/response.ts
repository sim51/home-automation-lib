import { URL } from "url";
import parser from "fast-xml-parser";
import {
  Actions,
  ActionArgument,
  ActionInOut,
  Device,
  Icon,
  Service,
  ServiceDescription,
  StateVariable,
} from "./types";

/**
 * Given a relative URL and the base url, this method return the absolute url.
 * @param {string} baseurl
 * @param {string}  url the relative url
 * @returns {string}
 */
function absoluteUrl(baseUrl: string, url: string): string {
  const completeUrl = new URL(url, baseUrl);
  return completeUrl.toString();
}

/**
 * Transform a list of Icon with relative urls, to a list with absolute url.
 */
function getIcons(iconList: { icon: Array<Icon> }, url: string): Array<Icon> {
  if (!iconList) {
    return [];
  }
  return Array.from(iconList.icon).map((icon: Icon) => ({
    ...icon,
    url: absoluteUrl(url, icon.url),
  }));
}

function getServices(serviceList: { service: Array<Service> }, url: string): { [key: string]: Service } {
  if (!serviceList) {
    return {};
  }
  return Array.from(serviceList.service).reduce((a, { serviceType, serviceId, SCPDURL, controlURL, eventSubURL }) => {
    a[serviceId] = {
      serviceType,
      SCPDURL: absoluteUrl(url, SCPDURL),
      controlURL: absoluteUrl(url, controlURL),
      eventSubURL: absoluteUrl(url, eventSubURL),
    };
    return a;
  }, {});
}

function getActionArguments(
  argumentList:
    | { argument: Array<ActionArgument & { direction: string }> }
    | Array<ActionArgument & { direction: string }>,
): ActionInOut {
  if (!argumentList) {
    return {
      inputs: [],
      outputs: [],
    };
  }
  const arg: Array<ActionArgument & { direction: string }> = argumentList["argument"]
    ? argumentList["argument"]
    : argumentList;
  return Array.from(arg).reduce(
    (a: ActionInOut, { direction, name, relatedStateVariable }) => {
      if (direction === "in") {
        a.inputs.push({
          name,
          relatedStateVariable,
        });
      } else {
        a.outputs.push({
          name,
          relatedStateVariable,
        });
      }
      return a;
    },
    {
      inputs: [],
      outputs: [],
    },
  );
}

function getActions(actionList: {
  action: Array<{
    name: string;
    argumentList: {
      argument: Array<ActionArgument & { direction: string }>;
    };
  }>;
}): Actions {
  if (!actionList) {
    return {};
  }
  return Array.from(actionList.action).reduce((a, { name, argumentList }) => {
    a[name] = getActionArguments(argumentList);
    return a;
  }, {});
}

function getAllowedValues(allowedValueList: { allowedValue: Array<string> }): Array<string> {
  if (!allowedValueList) {
    return [];
  }
  if (Array.isArray(allowedValueList.allowedValue)) {
    return allowedValueList.allowedValue;
  }
  return [allowedValueList.allowedValue];
}

function getStateVariables(serviceStateTable: {
  stateVariable: Array<StateVariable & { __sendEvents: string; allowedValueList: { allowedValue: Array<string> } }>;
}): { [key: string]: StateVariable } {
  if (!serviceStateTable) {
    return {};
  }
  return Array.from(serviceStateTable.stateVariable).reduce(
    (a, { name, __sendEvents, allowedValueList, ...fields }) => {
      a[name] = {
        ...fields,
        sendEvents: __sendEvents !== "no",
        allowedValues: getAllowedValues(allowedValueList),
      };
      return a;
    },
    {},
  );
}

export function parseDeviceDescription(xmlString: string, baseurl: string): Device {
  const url = new URL(baseurl);
  const obj = parser.parse(xmlString, { parseTrueNumberOnly: true });
  const {
    deviceType,
    friendlyName,
    manufacturer,
    manufacturerURL,
    modelName,
    modelNumber,
    modelDescription,
    UDN,
    iconList,
    serviceList,
  } = obj.root.device;
  return {
    deviceType,
    friendlyName,
    manufacturer,
    manufacturerURL,
    modelName,
    modelNumber,
    modelDescription,
    UDN,
    icons: getIcons(iconList, url.toString()),
    services: getServices(serviceList, url.toString()),
  };
}

export function parseServiceDescription(xmlString: string): ServiceDescription {
  const obj = parser.parse(xmlString, {
    attributeNamePrefix: "__",
    ignoreAttributes: false,
  });

  const { actionList, serviceStateTable } = obj.scpd;

  return {
    actions: getActions(actionList),
    stateVariables: getStateVariables(serviceStateTable),
  };
}

export function parseSOAPResponse(
  xmlString: string,
  actionName: string,
  outputs: Array<{ name: string }>,
): { [key: string]: string } {
  const envelope = parser.parse(xmlString);
  const res = envelope["s:Envelope"]["s:Body"][`u:${actionName}Response`];
  return outputs.reduce((a, { name }) => {
    a[name] = res[name];
    return a;
  }, {});
}

export function parseEvents(data: string): { [key: string]: string } {
  const body = parser.parse(data);
  const props = body["e:propertyset"]["e:property"];
  return props;
}

export function parseTimeout(header: string): number {
  return Number(header.split("-")[1]);
}
