import http, { Server, IncomingMessage, ServerResponse } from "http";
import { AddressInfo } from "net";
import os from "os";
import { URL } from "url";
import events from "events";
import * as ipUtils from "ip-utils";
import got from "got";
import { Device, ServiceDescription } from "./types";
import {
  parseDeviceDescription,
  parseServiceDescription,
  parseSOAPResponse,
  parseEvents,
  parseTimeout,
} from "./response";

import { createSOAPAction } from "./request";
import { resolveService } from "./util";
import * as error from "./error";

const DEFAULT_USER_AGENT = `${os.platform()}/${os.release()} UPnP/1.1`;
const SUBSCRIPTION_TIMEOUT = 2000;
const SUBSCRIPTION_TIMEOUT_MIN = 30;

export class UPnPClient {
  url: URL;
  deviceDescription: Device | null;
  serviceDescriptions: { [key: string]: ServiceDescription } | null;
  serviceIp: string;
  eventsServer: Server;
  subscriptions: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [serviceId: string]: { sid?: string; url?: URL; timer?: NodeJS.Timeout; listeners: Array<(e: any) => void> };
  };
  eventEmitter: events.EventEmitter;
  // cant type it to Got due to Method that has nt SUBSCRIBE & UNSUBSCRIBE
  client: any; // eslint-disable-line @typescript-eslint/no-explicit-any

  constructor(url: string, userAgent = DEFAULT_USER_AGENT) {
    this.url = new URL(url);
    this.deviceDescription = null;
    this.serviceDescriptions = {};
    this.serviceIp = null;
    this.eventsServer = null;
    this.subscriptions = {};
    this.eventEmitter = new events.EventEmitter();
    this.handleStateUpdate = this.handleStateUpdate.bind(this);

    this.client = got.extend({
      headers: {
        "user-agent": userAgent,
      },
    });
  }

  async getDeviceDescription(): Promise<Device | null> {
    if (!this.deviceDescription) {
      const response = await this.client(this.url);
      this.deviceDescription = parseDeviceDescription(response.body, this.url.toString());
      this.serviceIp = response.ip;
    }

    return this.deviceDescription;
  }

  async hasService(serviceId: string): Promise<boolean> {
    serviceId = resolveService(serviceId);
    const description = await this.getDeviceDescription();
    return Boolean(description.services[serviceId]);
  }

  async getServiceDescription(serviceId: string): Promise<ServiceDescription> {
    if (!(await this.hasService(serviceId))) {
      throw error.NoService(serviceId);
    }

    const service = this.deviceDescription.services[serviceId];
    if (!this.serviceDescriptions[serviceId]) {
      const response = await this.client(service.SCPDURL);
      this.serviceDescriptions[serviceId] = parseServiceDescription(response.body);
    }

    return this.serviceDescriptions[serviceId];
  }

  async getVariableServiceId(variable: string, force: boolean): Promise<string> {
    const { services } = await this.getDeviceDescription();

    for (const serviceId of Object.keys(services)) {
      const { stateVariables } = await this.getServiceDescription(serviceId);
      if (!stateVariables) {
        continue;
      }

      for (const v in stateVariables) {
        if (v === variable && (stateVariables[v].sendEvents || force === true)) {
          return serviceId;
        }
      }
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  async call(serviceId: string, actionName: string, data: any): Promise<{ [key: string]: string }> {
    serviceId = resolveService(serviceId);
    const description = await this.getServiceDescription(serviceId);
    const action = description.actions[actionName];
    if (!action) {
      throw error.NoAction(actionName);
    }

    const service = this.deviceDescription.services[serviceId];
    const SOAPAction = createSOAPAction(service.serviceType, actionName, data);

    const res = await this.client({
      throwHttpErrors: false,
      url: service.controlURL,
      method: "POST",
      body: SOAPAction,
      headers: {
        "Content-Type": 'text/xml; charset="utf-8"',
        "Content-Length": `${SOAPAction.length}`,
        Connection: "close",
        SOAPACTION: `"${service.serviceType}#${actionName}"`,
      },
    });

    if (res.statusCode !== 200) {
      throw error.UPnPError(res.statusCode, res.body);
    }

    const result = parseSOAPResponse(res.body, actionName, action.outputs);
    return result;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async on(variable: string, listener: (e: any) => void, options: { force?: boolean } = {}): Promise<void> {
    const serviceId = await this.getVariableServiceId(variable, options.force);
    if (!serviceId) {
      throw error.NoEvents(variable);
    }

    this.eventEmitter.on(variable, listener);
    await this.subscribe(serviceId, this.handleStateUpdate);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async off(variable: string, listener: (e: any) => void): Promise<void> {
    this.eventEmitter.off(variable, listener);
    const serviceId = await this.getVariableServiceId(variable, true);
    await this.unsubscribe(serviceId, this.handleStateUpdate);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  handleStateUpdate(e: { name: string; value: any }): void {
    this.eventEmitter.emit(e.name, e.value);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async subscribe(serviceId: string, listener: (e: any) => void): Promise<void> {
    serviceId = resolveService(serviceId);

    const subs = this.subscriptions[serviceId];
    if (subs) {
      if (!subs.listeners.includes(listener)) {
        this.subscriptions[serviceId].listeners.push(listener);
      }
      return;
    } else {
      this.subscriptions[serviceId] = { listeners: [listener] };
    }

    if (!(await this.hasService(serviceId))) {
      throw error.NoService(serviceId);
    }

    const service = this.deviceDescription.services[serviceId];
    const server = await this.getEventsServer();
    const url = new URL(service.eventSubURL);

    // searching the ip address that is on the same subnet of the device
    let localServerIp = Object.keys(os.networkInterfaces())
      .flatMap((i) => os.networkInterfaces()[i])
      .find((i) => i.family === "IPv4" && i.internal === false && ipUtils.subnet(i.cidr).contains(this.serviceIp));
    if (!localServerIp) {
      localServerIp = Object.keys(os.networkInterfaces())
        .flatMap((i) => os.networkInterfaces()[i])
        .find((i) => i.family === "IPv4" && i.internal === false);
    }

    try {
      const res = await this.client({
        url,
        throwHttpErrors: true,
        method: "SUBSCRIBE",
        headers: {
          CALLBACK: `<http://${localServerIp.address}:${
            typeof server.address() === "string" ? 80 : (server.address() as AddressInfo).port
          }/>`,
          NT: "upnp:event",
          TIMEOUT: `Second-${SUBSCRIPTION_TIMEOUT}`,
        },
      });
      if (!res.headers.timeout || !res.headers.sid) {
        throw new Error(`Subscription error: timeout or sid is missing, result was ${JSON.stringify(res.headers)}`);
      }
      const { sid, timeout } = res.headers;
      const renewTimeout = Math.max(parseTimeout(timeout) - SUBSCRIPTION_TIMEOUT_MIN, SUBSCRIPTION_TIMEOUT_MIN);

      const timer = setTimeout(() => this.renewSubscription(url.toString(), sid, serviceId), renewTimeout * 1000);

      this.subscriptions[serviceId] = {
        sid,
        url,
        timer,
        listeners: [listener],
      };
    } catch (e) {
      this.close();
      throw error.Subscribe(e);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async unsubscribe(serviceIdString: string, listener: (e: any) => void): Promise<void> {
    const serviceId = resolveService(serviceIdString);

    const subscription = this.subscriptions[serviceId];
    if (!subscription) {
      return;
    }

    const index = subscription.listeners.indexOf(listener);
    if (index === -1) {
      return;
    }
    subscription.listeners.splice(index, 1);

    if (subscription.listeners.length !== 0) {
      return;
    }

    clearTimeout(subscription.timer);

    try {
      const res = await this.client({
        url: subscription.url,
        throwHttpErrors: true,
        method: "UNSUBSCRIBE",
        headers: {
          HOST: subscription.url.host,
          SID: subscription.sid,
        },
      });
    } catch (e) {
      throw error.Unsubscribe(e);
    }

    // remove the subscription, and if there no more subscription we stop the server
    delete this.subscriptions[serviceId];
    await this.close();
  }

  async renewSubscription(url: string, sid: string, serviceId: string): Promise<void> {
    try {
      const res = await this.client({
        url,
        throwHttpErrors: true,
        method: "SUBSCRIBE",
        headers: {
          SID: sid,
          TIMEOUT: `Second-${SUBSCRIPTION_TIMEOUT}`,
        },
      });
      if (!res.headers.timeout || !res.headers.sid) {
        throw new Error(`Subscription error: timeout or sid is missing, result was ${JSON.stringify(res.headers)}`);
      }
      const timeout = parseTimeout(res.headers.timeout);
      const renewTimeout = Math.max(timeout - SUBSCRIPTION_TIMEOUT_MIN, SUBSCRIPTION_TIMEOUT_MIN); // renew 30 seconds before expiration
      const timer = setTimeout(() => {
        try {
          this.renewSubscription(url, sid, serviceId);
        } catch (e) {
          // do nothing, the log is already done in the renewSubscription funciton
        }
      }, renewTimeout * 1000);
      this.subscriptions[serviceId].timer = timer;
    } catch (e) {
      delete this.subscriptions[serviceId];
      await this.close();
      throw error.SubscriptionRenewal(e);
    }
  }

  async getEventsServer(): Promise<Server> {
    if (!this.eventsServer) {
      this.eventsServer = await this.createEventsServer();
    }

    if (!this.eventsServer.listening) {
      await new Promise((resolve, reject) => {
        // listen on all interfaces
        this.eventsServer.listen({ port: 0, host: "0.0.0.0" });
        this.eventsServer.on("error", (err) => {
          reject(err);
        });
        this.eventsServer.on("listening", () => {
          resolve();
        });
      });
    }

    return this.eventsServer;
  }

  createEventsServer(): Server {
    return http.createServer(this.eventsServerRequestHandler());
  }

  eventsServerRequestHandler(): (req: IncomingMessage, res: ServerResponse) => void {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this;
    return (req: IncomingMessage, res: ServerResponse): void => {
      req.setEncoding("utf8");
      let data = "";
      req.on("data", function (chunk) {
        data += chunk;
        const { sid } = req.headers;
        const events = parseEvents(data);

        if (!self.subscriptions) {
          // silently ignore unknown SIDs
          return;
        }
        const keys = Object.keys(self.subscriptions);
        const serviceId = keys.find((key) => self.subscriptions[key].sid === sid);
        if (!serviceId) {
          // silently ignore unknown SIDs
          return;
        }
        const listeners = self.subscriptions[serviceId].listeners;
        listeners.forEach((listener) => {
          listener(events);
        });
      });
      req.on("error", function (err) {
        throw new Error(`Error on CALLBACK subscribe request event : ${err}`);
      });
      res.writeHead(200);
      res.end();
      res.on("error", function (err) {
        throw new Error(`Error on CALLBACK subscribe responseevent : ${err}`);
      });
    };
  }

  /**
   * Close everything : remove all listener and stop the server if there is no more subscriptions
   */
  async close(force = false): Promise<void> {
    return new Promise((resolve, reject) => {
      if (Object.keys(this.subscriptions).length === 0 || force) {
        this.subscriptions = {};
        this.eventEmitter.removeAllListeners();
        this.eventsServer.close((err) => {
          if (err) reject(err);
          this.eventsServer = null;
          resolve();
        });
      }
      resolve();
    });
  }
}
