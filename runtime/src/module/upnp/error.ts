import parser from "fast-xml-parser";
import * as util from "util";

const code = {
  NO_SERVICE: "NO_SERVICE",
  NO_ACTION: "NO_ACTION",
  NO_EVENTS: "NO_EVENTS",
  UPNP: "UPNP",
  SUBSCRIBE: "SUBSCRIBE",
  SUBSCRIBE_RENEW: "SUBSCRIBE_RENEW",
  UNSUBSCRIBE: "UNSUBSCRIBE",
};

interface MyError extends Error {
  code?: string;
  statusCode?: string | number;
  errorCode?: string | number;
  source?: string;
}

export function NoService(serviceId: string): MyError {
  const err: MyError = new Error(`Service ${serviceId} not provided by device`);
  err.code = code.NO_SERVICE;
  return err;
}

export function NoAction(serviceId: string): MyError {
  const err: MyError = new Error(`Action ${serviceId} not implemented by service`);
  err.code = code.NO_ACTION;
  return err;
}

export function UPnPError(statusCode: number, xmlString: string): MyError {
  const envelope = parser.parse(xmlString);
  const error = envelope["s:Envelope"]["s:Body"]["s:Fault"].detail.UPnPError;
  const { errorCode, errorDescription } = error;
  const err: MyError = new Error(`(${errorCode}) ${errorDescription}`);
  err.code = code.UPNP;
  err.statusCode = statusCode;
  err.errorCode = errorCode;
  return err;
}

export function NoEvents(variable: string): MyError {
  const err: MyError = new Error(`Variable ${variable} does not generate event messages`);
  err.code = code.NO_EVENTS;
  return err;
}

export function Subscribe(source: Error): MyError {
  const err: MyError = new Error("Subscription error");
  err.code = code.SUBSCRIBE;
  err.source = util.inspect(source, true, null);
  return err;
}

export function SubscriptionRenewal(source: Error): MyError {
  const err: MyError = new Error("Subscription renewal error");
  err.code = code.SUBSCRIBE_RENEW;
  err.source = util.inspect(source, true, null);
  return err;
}

export function Unsubscribe(source: Error): MyError {
  const err: MyError = new Error("Unsubscription error");
  err.code = code.UNSUBSCRIBE;
  err.source = util.inspect(source, true, null);
  return err;
}
