import Request from "request-promise-native";
import { Client } from "node-ssdp";
import { parseString } from "xml2js";
import { promisify } from "util";

const xml2json = promisify(parseString);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getServiceInfo = async (location: string): Promise<Record<string, any>> => {
  const result = await Request(location);
  const json = await xml2json(result, { explicitRoot: false, ignoreAttrs: true, explicitArray: false });
  json.location = location;
  return json;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const ssdpSearch = async (target: string, timeout: number): Promise<Array<Record<string, any>>> => {
  const client = new Client({
    explicitSocketBind: true,
    customLogger: console.log,
    ssdpTtl: 1,
  });
  const found: { [location: string]: Record<string, unknown> } = {};
  client.on("response", async (headers, code) => {
    if (code === 200) {
      const serviceInfo = await getServiceInfo(headers.LOCATION);
      found[headers.LOCATION] = serviceInfo;
    }
  });

  client.search(target);

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      try {
        client.stop();
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const result: Array<Record<string, any>> = [];
        Object.keys(found).forEach(key => {
          result.push(found[key]);
        });
        resolve(result);
      } catch (e) {
        reject(e);
      }
    }, timeout);
  });
};
