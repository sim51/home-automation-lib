import os from "os";
import { readFile } from "../utils/file";
import evilscan from "evilscan";

export interface ModuleInfo {
  id: string;
  name: string;
  version: string;
  description: string;
  license: string;
  author: string;
  url: string;
  categories: Array<string>;
}

export async function moduleParsePackageJson(path: string): Promise<ModuleInfo> {
  const info = JSON.parse(await readFile(path, { encoding: "utf8" }));
  return {
    id: `${info.name}`,
    name: info.name.replace(/hal\-module\-/, "").replace(/\-/gi, " "),
    version: info.version,
    description: info.description,
    license: info.license,
    author: info.author,
    url: info.homepage,
    categories: info.keywords,
  };
}

export async function discoveringIpThatListenOnPort(
  ports: string | number,
): Promise<Array<{ ip: string; hostname: string; port: number }>> {
  return (
    await Promise.all(
      Object.keys(os.networkInterfaces())
        .flatMap((i) => os.networkInterfaces()[i])
        .filter((i) => i.family === "IPv4" && i.internal === false)
        .map((i) => i.cidr)
        .map((subnet) => {
          return new Promise<Array<{ ip: string; hostname: string; port: number }>>((resolve, reject) => {
            const result: Array<{ ip: string; hostname: string; port: number }> = [];
            const options = {
              target: subnet,
              port: `${ports}`,
              status: "O",
              reverse: true,
              timeout: 1000,
            };
            const scanner = new evilscan(options);
            scanner.on("result", (data) => result.push({ ip: data.ip, hostname: data.reverse, port: data.port }));
            scanner.on("error", (e) => reject(e));
            scanner.on("done", () => resolve(result));
            scanner.run();
          });
        }),
    )
  ).flat();
}
