import * as fs from "fs";
import { promisify } from "util";
import glob from "glob";

export const readFile = promisify(fs.readFile);
export const writeFile = promisify(fs.writeFile);
export const globAsync = promisify(glob);
