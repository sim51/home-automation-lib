// safely handles circular references
export function jsonStringify(obj: unknown, indent = 2): string {
  let cache = [];
  const retVal = JSON.stringify(
    obj,
    (key, value) =>
      typeof value === "object" && value !== null
        ? cache.includes(value)
          ? undefined // Duplicate reference found, discard key
          : cache.push(value) && value // Store value in our collection
        : key === "icon"
        ? undefined
        : value,
    indent,
  );
  cache = null;
  return retVal;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function promisesInSeries<T>(tasks: Array<() => Promise<T>>): Promise<any> {
  return tasks.reduce((acc, current) => acc.then(current), Promise.resolve([]));
}
