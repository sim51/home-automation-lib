import path from "path";
import { parseString } from "xml2js";
import { promisify } from "util";
import Request from "request-promise-native";
import { v4 as uuid } from "uuid";
import { promisesInSeries } from "./utils/index";
import { emit } from "./events";
import {
  Event,
  EventHal,
  EventComponent,
  EventComponentAction,
  EventComponentConsumedEvent,
  EventModule,
  EventType,
  Field,
  FieldType,
  IHal,
  Component,
  ComponentInfo,
  ComponentType,
  LogLevel,
  Logger,
  MeasurementDefinition,
  Module,
  Settings,
} from "./types";
import { UPnPClient } from "./module/upnp/upnp";
import { ssdpSearch } from "./module/upnp/ssdp";
import { moduleParsePackageJson, discoveringIpThatListenOnPort, ModuleInfo } from "./module/helper";
import { readFile, writeFile, globAsync as glob } from "./utils/file";

const HAL: IHal = {
  // List of modules
  modules: [],
  // List of components
  components: [],
  // Events
  events: emit,
  // Global configuration
  config: {},
  // keep track of component functions listerner for remove
  componentListeners: {},

  /**
   * Initialize HAL runtime : search modules, components and create listeners.
   *
   * @param config - HAL configuration
   * @throw {Error} When something goes wrong
   */
  async init(config: Settings): Promise<void> {
    // Setting the configuration
    this.config = config;
    this.module_path = config.module_path || ".";
    // Log configuration
    // if a logger is specified we use it, otherwise it's just a console
    this.logLevel = config.log_level || LogLevel.info;
    if (this.config.logger) {
      this.logger = this.config.logger;
    }

    // Splash message
    this.log(["HAL"], LogLevel.info, `##     ##    ###    ##      `);
    this.log(["HAL"], LogLevel.info, `##     ##   ## ##   ##      `);
    this.log(["HAL"], LogLevel.info, `##     ##  ##   ##  ##      `);
    this.log(["HAL"], LogLevel.info, `######### ##     ## ##      `);
    this.log(["HAL"], LogLevel.info, `##     ## ######### ##      `);
    this.log(["HAL"], LogLevel.info, `##     ## ##     ## ##      `);
    this.log(["HAL"], LogLevel.info, `##     ## ##     ## ########`);

    // Listen and log all events in debug mode
    // ---------------------------------------
    for (const event in EventType) {
      if (typeof EventType[event] === "string") {
        this.events.on(EventType[event], async (data: Record<string, unknown>) => {
          this.log(["HAL", "EVENT"], LogLevel.debug, `${EventType[event]} ->`, data);
        });
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    this.events.on("error", (error: Error) => {
      this.log(["HAL", "EVENT"], LogLevel.error, error.message);
    });

    // Search modules
    // --------------
    try {
      // Find all the package.json file in the module directory
      const files = await glob("**/package.json", { cwd: this.module_path, ignore: ["**/node_modules/**"] });

      await Promise.all(
        files.map(async (file: string) => {
          this.log(["HAL"], LogLevel.info, `Loading module from ${file}`);
          // Read the package.json and parse it
          const info = JSON.parse(await readFile(`${this.module_path}/${file}`, { encoding: "utf8" }));
          // A module name should start with "hal-module", so we check it
          if (info.name.startsWith("hal-module")) {
            const module_path = path.dirname(path.resolve(`${this.module_path}/${file}`));
            try {
              // Loading the main file of the module
              /* eslint-disable @typescript-eslint/no-var-requires */
              const moduleDef = require(`${module_path}/${info.main}`);
              const module = await moduleDef.default();
              // Register the module
              this.modules.push(module);
              this.publishEvent(EventType.ModuleRegister, module);
            } catch (e) {
              throw this.error(["HAL"], `Failed to load module from ${file}`, e.message);
            }
          }
        }),
      );
    } catch (e) {
      throw this.error(["HAL"], `Failed to load modules`, e.message);
    }

    // Load components if there are some
    // ---------------------------------
    if (config.components) {
      try {
        await promisesInSeries(
          config.components.map((component: ComponentInfo) => {
            return async () => {
              return await this.componentCreate(component);
            };
          }),
        );
      } catch (e) {
        throw this.error(["HAL"], `Failed to create components`, e.message);
      }
    }

    return;
  },

  /**
   * Start HAL, ie. start all the enabled modules.
   *
   * @return Rejected when something goes wrong.
   */
  async start(): Promise<void> {
    this.log(["HAL"], LogLevel.info, `Starting ...`);
    try {
      await promisesInSeries(
        this.modules
          .sort((a, b) => {
            const aPriority = a.startPriority === undefined ? 99 : a.startPriority;
            const bPriority = b.startPriority === undefined ? 99 : b.startPriority;
            return aPriority - bPriority;
          })
          .map((module: Module) => {
            return async () => {
              return await this.moduleStart(module.id);
            };
          }),
      );
      this.publishEvent(EventType.HalStart);
    } catch (e) {
      throw this.error(["HAL"], `Failed to start HAL`, e.message);
    }
    return;
  },

  /**
   * Stop HAL, ie. stop all the modules.
   *
   * @return {Promise<void>} Rejected when something goes wrong.
   */
  async stop(): Promise<void> {
    this.log(["HAL"], LogLevel.info, `Stopping ...`);
    try {
      await promisesInSeries(
        this.modules
          .sort((a, b) => {
            const aPriority = a.startPriority === undefined ? 99 : a.startPriority;
            const bPriority = b.startPriority === undefined ? 99 : b.startPriority;
            return bPriority - aPriority;
          })
          .map((module: Module) => {
            return async () => {
              return await this.moduleStop(module.id, true);
            };
          }),
      );
      this.publishEvent(EventType.HalStop);
    } catch (e) {
      throw this.error(["HAL"], `Failed to stop HAL`, e.message);
    }
    return;
  },

  /**
   * Start a module.
   *
   * @param id - id of the module to start
   * @return  Rejected when something goes wrong.
   */
  async moduleStart(id: string): Promise<void> {
    // Searching the module by its id
    const module: Module = this.modules.find((module: Module) => {
      return module.id === id;
    });
    if (!module) {
      throw this.error(["HAL"], `Module ${id} not found`);
    }

    // Start the module
    this.log(["HAL"], LogLevel.info, `Starting module ${module.name}`);
    try {
      // Start all its components
      await Promise.all(
        this.components
          .filter((component: Component) => {
            return component.module === module.id;
          })
          .map(async (component) => {
            return await this.componentStart(component.id);
          }),
      );
      // Make the module as started
      this.publishEvent(EventType.ModuleStart, module);
    } catch (e) {
      throw this.error(["HAL"], `Failed to start module ${module.name}`, e.message);
    }
  },

  /**
   * Stop a module.
   *
   * @param id - id of the module to stop
   * @param force - Flag to force the stop of module (used for system module)
   * @return  Rejected when something goes wrong.
   */
  async moduleStop(id: string, force = false): Promise<void> {
    // Searching the module by its id
    const module: Module = this.modules.find((module: Module) => {
      return module.id === id;
    });

    // Make some checks
    if (!module) {
      throw this.error(["HAL"], `Module ${id} not found`);
    }
    if (module.isSystemModule === true && force === false) {
      throw this.error(["HAL"], `Can't stop system module ${module.name} without force flag`);
    }

    // Stop the module
    this.log(["HAL"], LogLevel.info, `Stopping module ${module.name}`);
    try {
      // Stop all its components
      await Promise.all(
        this.components
          .filter((component: Component) => {
            return component.module === module.id;
          })
          .map(async (component) => {
            return await this.componentStop(component.id, force);
          }),
      );
      // Make the module as stopped
      this.publishEvent(EventType.ModuleStop, module);
    } catch (e) {
      throw this.error(["HAL"], `Failed to stop module ${module.name}`, e.message);
    }
  },

  /**
   * Run the component discovery process of a module and start them.
   *
   * @param id - Id of the module
   * @return {Promise<Array<Component>>} The list of the discovered components
   */
  async moduleDiscoverComponents(id: string): Promise<Array<Component>> {
    // Searching the module by its id
    const module: Module = this.modules.find((module: Module) => {
      return module.id === id;
    });

    // Make some check
    if (!module) {
      throw this.error(["HAL"], `Module ${module.name} not found`);
    }
    if (!module.discover) {
      throw this.error(["HAL"], `Module ${id} has no discovering feature`);
    }

    this.log(["HAL"], LogLevel.info, `Discovering components for module ${module.name}`);
    // Exec the discover process
    const components: Array<Component> = await module.discover();
    // Start the components
    return Promise.all(
      components.map(async (component) => {
        await this.componentStart(component.id);
        return component;
      }),
    );
  },

  /**
   * Generate an error and log it.
   *
   * @param message - Message of the error
   * @return {Error}
   */
  moduleError(id: string, message: string, param: unknown = null): Error {
    const moduleName = this.findModule(id).name;
    return this.error(["HAL", moduleName], LogLevel.error, message, param);
  },

  /**
   * Generate a log forthe module.
   *
   * @param message - Message of the error
   * @return {Error}
   */
  moduleLog(id: string, level: LogLevel, message: string, param: unknown = null): Error {
    const moduleName = this.findModule(id).name;
    return this.log(["HAL", moduleName], level, message, param);
  },

  /**
   * Start a component.
   *
   * @param id - The id of the component
   * @return {Promise<void>} Rejected when something goes wrong.
   */
  async componentStart(id: string): Promise<void> {
    // Searching the component by its id
    const component: Component = this.components.find((component: Component) => {
      return component.id === id;
    });

    // Some checks
    if (!component) {
      throw this.error(["HAL"], `Component ${id} not found`);
    }

    // Start the component
    this.log(["HAL"], LogLevel.info, `Starting component ${component.name}`);
    try {
      await component.start();
      this.publishEvent(EventType.ComponentStart, component);
    } catch (e) {
      throw this.error(["HAL"], `Component ${component.name} can't be started `, e);
    }
  },

  /**
   * Stop a component.
   *
   * @param id - The id of the component
   * @param force - Flag to force the stop of component (used for system module)
   * @return - Rejected when something goes wrong.
   */
  async componentStop(id: string, force = false): Promise<void> {
    // Searching the component
    const component: Component = this.components.find((component: Component) => {
      return component.id === id;
    });
    if (!component) {
      throw this.error(["HAL"], `Component ${id} not found`);
    }

    this.log(["HAL"], LogLevel.info, `Stopping component ${component.name}`);

    // searching the module
    const module: Module = this.modules.find((module: Module) => {
      return module.id === component.module;
    });
    if (!module) {
      throw this.error(["HAL"], `Module for Component ${component.name} not found`);
    }

    // check for system module
    if (module.isSystemModule === true && force === false) {
      throw this.error(
        ["HAL"],
        `Can't stop component ${component.name} of system module ${module.name} without force flag`,
      );
    }

    try {
      // Remove all the listerners managed bu HAL for the component.
      this.componentListenersRemoveAll(component.id);
      // stopping the component
      await component.stop();

      this.publishEvent(EventType.ComponentStop, component);
    } catch (e) {
      throw this.error(["HAL"], `Component ${component.name} can't be stopped :`, e.message);
    }
  },

  /**
   * Create a component from its info.
   *
   * @param component - Info of the component to create
   * @return {Promise<Component>} The created component
   */
  async componentCreate(component: ComponentInfo): Promise<Component> {
    this.log(["HAL"], LogLevel.info, `Creating component ${component.name}`);

    // searching the module
    const module: Module = this.modules.find((module: Module) => {
      return module.id === component.module;
    });
    if (!module) {
      throw this.error(["HAL"], `Module ${component.module} not found for component ${component.name}`);
    }

    // Searching device type
    const componentType: ComponentType = module.components.find((componentType: ComponentType) => {
      return componentType.id === component.type;
    });
    if (!componentType) {
      throw this.error(
        ["HAL"],
        `Type ${component.type} not found in module ${module.name} for component ${component.name}`,
      );
    }

    // Search if there is already a component with the same id
    if (this.components.findIndex((c) => c.id === component.id) > -1) {
      throw this.error(["HAL"], `Component ${component.name} already exist`);
    }

    // Create the component
    try {
      const dev: Component = await componentType.constructor(component);
      this.components.push(dev);
      this.publishEvent(EventType.ComponentRegister, dev);
      await this.componentStart(dev.id);
      return dev;
    } catch (e) {
      throw this.error(["HAL"], `Failed to add component ${component.name}`, e.message);
    }
  },

  /**
   * Delete a component by its id.
   *
   * @param id - Id of the component to delete
   * @param force - Flag to force the deletion of module (used for system module)
   * @return {Promise<void>} Rejected when something goes wrong.
   */
  async componentDelete(id: string, force = false): Promise<void> {
    // search the component to delete
    const component: Component = this.components.find((component: Component) => {
      return component.id === id;
    });
    if (!component) {
      throw this.error(["HAL"], `Component ${id} not found`);
    }

    this.log(["HAL"], LogLevel.info, `Deleting component ${component.name}`);

    // searching the module
    const module: Module = this.modules.find((module: Module) => {
      return module.id === component.module;
    });
    if (!module) {
      throw this.error(["HAL"], `Module for component ${component.name} not found`);
    }
    if (module.isSystemModule === true && force === false) {
      throw this.error(
        "HAL",
        `Can't delete component ${component.name} of system module ${module.name} without force flag`,
      );
    }

    // stop & remove it
    try {
      await this.componentStop(id);
      // Remove it from the component list
      this.components = this.components.filter((component: Component) => {
        return component.id !== id;
      });
      // Make the component as unregistered
      this.publishEvent(EventType.ComponentUnRegister, component);
    } catch (e) {
      throw this.error(["HAL"], `Failed to remove component ${component.name}`, e.message);
    }
  },

  /**
   * Update a component from its info.
   * It's just a delete and create of the component.
   *
   * @param component - Info of the component to create
   * @return {Promise<void>}
   */
  async componentUpdate(component: ComponentInfo): Promise<Component> {
    this.componentLog(component.id, LogLevel.info, `Updating component ${component.name}`);
    await this.componentDelete(component.id, true);
    return await this.componentCreate(component);
  },

  /**
   * Send a component measure to the runtime.
   *
   * @param id - Id of the component
   * @param data - A map of measurements.
   */
  componentSendMeasure(id: string, data: Record<string, unknown>): void {
    // search the component to delete
    const component: Component = this.components.find((component: Component) => {
      return component.id === id;
    });
    if (!component) {
      throw this.componentLog(id, LogLevel.error, `Component ${id} not found on "componentSendMeasure"`);
    } else {
      this.componentLog(id, LogLevel.debug, "Sending measure", data);
      this.publishEvent(EventType.ComponentMeasure, component, data);
    }
  },

  /**
   * Send a component action to the runtime.
   *
   * @param id - Id of the component
   * @param name - Name of the action
   * @param params - A map of parameter for the action
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  componentSendAction(id: string, name: string, params: { [key: string]: any }): void {
    // search the component to delete
    const component: Component = this.components.find((component: Component) => {
      return component.id === id;
    });
    if (!component) {
      throw this.componentLog(id, LogLevel.error, `Component ${id} not found on "componentSendAction"`);
    } else {
      this.componentLog(id, LogLevel.debug, `Sending action ${name}`, params);
      this.publishEvent(EventType.ComponentAction, component, Object.assign({}, { action: name }, params));
    }
  },

  /**
   * Listen a specific event for a component.
   */
  componentListenForEventType(id: string, type: EventType, fn: (e: EventHal) => Promise<void>): void {
    // search the component by its id
    const component: Component = this.components.find((component: Component) => {
      return component.id === id;
    });
    if (!component) {
      throw this.error(["HAL"], `Component ${id} not found`);
    }

    // create the callback function
    const callback = async (event: EventComponentAction) => {
      await fn(event);
      // when the event is executed, we send the EventConsume event into the queue
      this.publishEvent(EventType.ComponentConsumedEvent, component, event);
    };

    if (this.componentListeners[id]) {
      this.componentListeners[id].push({ type: type, fn: callback });
    } else {
      this.componentListeners = [{ type: type, fn: callback }];
    }

    // Registert the listener
    this.events.on(type, callback);
  },

  /**
   * Listen a specific action for a component.
   *
   * @param id - Id of the component
   * @param name - Name of the action
   * @param fn - the callback function
   */
  componentListenAction(id: string, name: string, fn: (event: EventComponentAction) => Promise<void>): void {
    this.componentListenForEventType(id, EventType.ComponentAction, async (event: EventHal) => {
      if ((event as EventComponentAction).component.id === id && (event as EventComponentAction).data.action === name) {
        try {
          await fn(event as EventComponentAction);
        } catch (e) {
          throw this.componentError(id, `Failed to execute action ${name}`, e.message);
        }
      }
    });
  },

  /**
   * Remove all the Listener of a component.
   *
   * @param id - Id of the component
   */
  componentListenersRemoveAll(id: string): void {
    if (this.componentListeners[id]) {
      this.componentListeners[id].forEach((element) => {
        this.events.removeListener(element.type, element.fn);
      });
      this.componentListeners[id] = [];
    }
  },

  /**
   * Generate an error and log it.
   *
   * @param message - Message of the error
   * @return {Error}
   */
  componentError(id: string, message: string, param: unknown = null): Error {
    const component = this.findComponent(id);
    const componentTypeName = this.findComponentType(component.type).name;
    const moduleName = this.findModule(component.module).name;
    return this.error(["HAL", moduleName, componentTypeName, component.name], message, param);
  },

  /**
   * Generate an a log for the component.
   *
   * @param message - Message of the error
   */
  componentLog(id: string, level: LogLevel, message: string, param: unknown = null): void {
    const component = this.findComponent(id);
    const componentTypeName = this.findComponentType(component.type).name;
    const moduleName = this.findModule(component.module).name;
    return this.log(["HAL", moduleName, componentTypeName, component.name], level, message, param);
  },

  /**
   * Publish an event in the event emitter and return the generated Event.
   */
  publishEvent(type: EventType, item: Module | Component = null, data: unknown = null): Event {
    const event: Event = {
      uuid: uuid(),
      datetime: new Date(),
      type: type,
      data: data,
    };

    // make specific mapping, specially for the component/module attribut on the event
    switch (type) {
      case EventType.HalStart:
      case EventType.HalStop:
        break;
      case EventType.ModuleRegister:
      case EventType.ModuleStart:
      case EventType.ModuleStop:
        (event as EventModule).module = item as Module;
        break;
      case EventType.ComponentConsumedEvent:
      case EventType.ComponentUnRegister:
      case EventType.ComponentRegister:
      case EventType.ComponentStart:
      case EventType.ComponentStop:
      case EventType.ComponentMeasure:
      case EventType.ComponentAction:
        (event as EventComponent).component = item as Component;
        break;
      default:
        throw this.error(["HAL"], `Can't puhblished an unknown event : ${type} | ${data}`);
        break;
    }
    this.events.emit(type, event);
    return event;
  },

  /**
   * Logger function.
   *
   * @param labels - Labels of the message
   * @param logLevel - Level of the message
   * @param message - Message to log
   * @param param - Param of the message
   */
  log(labels: string[], logLevel: LogLevel, message: string, param: unknown = null): void {
    if (this.logger) {
      switch (logLevel) {
        case LogLevel.error:
          this.logger.error(labels, message, param);
          break;
        case LogLevel.warn:
          this.logger.warn(labels, message, param);
          break;
        case LogLevel.info:
          this.logger.info(labels, message, param);
          break;
        case LogLevel.debug:
          this.logger.debug(labels, message, param);
          break;
      }
    } else {
      if (this.logLevel <= logLevel) {
        console.log(`[${labels.join(" > ")}]: ${message}`, param);
      }
    }
  },

  /**
   * Generate an error and log it.
   *
   * @param message - Message of the error
   * @return {Error}
   */
  error(labels: string[], message: string, param: unknown = null): Error {
    this.log(labels, LogLevel.error, `Error: ${message}`, param);
    return new Error(message);
  },

  /**
   * Find a component by its id.
   */
  findComponent(id: string): Component | null {
    return this.components.find((c) => c.id === id) || null;
  },

  /**
   * Find a component type by its id.
   */
  findComponentType(id: string): ComponentType | null {
    return this.modules.flatMap((m) => m.components).find((ct) => ct.id === id) || null;
  },
  /**
   * Find a module by its id.
   */
  findModule(id: string): Module | null {
    return this.modules.find((m) => m.id === id) || null;
  },
};

// Making some exports for modules that depends on this runtime
const Xml2json = promisify(parseString);
export {
  Component,
  ComponentInfo,
  ComponentType,
  Event,
  EventComponentAction,
  EventHal,
  EventComponent,
  EventModule,
  EventType,
  Field,
  FieldType,
  glob,
  IHal,
  HAL,
  Logger,
  LogLevel,
  MeasurementDefinition,
  Module,
  ModuleInfo,
  moduleParsePackageJson,
  discoveringIpThatListenOnPort,
  readFile,
  Settings,
  Request,
  UPnPClient,
  ssdpSearch,
  writeFile,
  Xml2json,
};
