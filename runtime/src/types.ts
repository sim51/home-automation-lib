import events from "events";

export enum EventType {
  HalStart = "HalStarted",
  HalStop = "HalStopped",
  ModuleRegister = "ModuleRegistered",
  ModuleStart = "ModuleStarted",
  ModuleStop = "ModuleStopped",
  ComponentUnRegister = "ComponentUnRegistered",
  ComponentRegister = "ComponentRegistered",
  ComponentStart = "ComponentStarted",
  ComponentStop = "ComponentStopped",
  ComponentMeasure = "ComponentMeasure",
  ComponentAction = "ComponentAction",
  ComponentConsumedEvent = "ComponentConsumedEvent",
}

export type EventTypeString = keyof typeof EventType;

export interface EventHal {
  // unique identifier of the event
  uuid: string;
  // datetime in ISO
  datetime: Date;
  // type of the event
  type: EventType | EventTypeString;
  // Data of the ventEventConsumed
  data?: any; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export interface EventModule extends EventHal {
  module: Module;
}

export interface EventComponent extends EventHal {
  component: Component;
}

export interface EventComponentAction extends EventComponent {
  data: { action: string } & { [key: string]: any }; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export interface EventComponentConsumedEvent extends EventComponent {
  data: EventHal;
}

export type Event = EventHal | EventModule | EventComponent | EventComponentAction | EventComponentConsumedEvent;

export enum LogLevel {
  debug = 1,
  info = 2,
  warn = 3,
  error = 4,
}

export interface Logger {
  error: (labels: string[], message: string, params: unknown) => void;
  warn: (labels: string[], message: string, params: unknown) => void;
  info: (labels: string[], message: string, params: unknown) => void;
  debug: (labels: string[], message: string, params: unknown) => void;
}

export interface Field {
  name: string;
  label?: string;
  help?: string;
  placeholder?: string;
  required?: boolean;
  pattern?: string;
  type?: FieldType | FieldTypeString;
  min?: number;
  max?: number;
  maxlength?: number;
  step?: number;
}

export enum FieldType {
  text = "text",
  user = "user",
  password = "password",
  textarea = "textarea",
  number = "number",
  location = "location",
  boolean = "boolean",
  image = "image",
  component = "component",
  range = "range",
  color = "color",
}

export type FieldTypeString = keyof typeof FieldType;

export interface IHal {
  // Global configuration
  config: Settings;
  // List of modules
  modules: Array<Module>;
  // Events
  events: events.EventEmitter;
  // List of created devices
  components: Array<Component>;
  // Logger
  logger?: Logger;
  // keep track of component functions listerner for remove
  componentListeners: { [id: string]: Array<{ type: EventType; fn: (e: EventHal) => Promise<void> }> };

  // Logger
  log: (labels: string[], level: LogLevel, message: string, param?: unknown) => void;
  // Manage Error
  error: (labels: string[], message: string, param?: unknown) => Error;
  // Create event
  publishEvent: (type: EventType, item?: Module | Component, data?: any) => Event; // eslint-disable-line @typescript-eslint/no-explicit-any

  // Init HAL (search modules)
  init: (config: Settings) => Promise<void>;
  // start HAL, ie. start all the enabled modules
  start: () => Promise<void>;
  // stop HAL, ie. stop all the modules
  stop: () => Promise<void>;

  // start the module
  moduleStart: (id: string) => Promise<void>;
  // stop the module
  moduleStop: (id: string, force?: boolean) => Promise<void>;
  // try to discover component for the specified module
  moduleDiscoverComponents: (id: string) => Promise<Array<Component>>;
  // Logger for module
  moduleLog: (id: string, level: LogLevel, message: string, param?: unknown) => void;
  // Error for module
  moduleError: (id: string, message: string, param?: unknown) => Error;

  // start the component
  componentStart: (id: string) => Promise<void>;
  // stop the component
  componentStop: (id: string, force?: boolean) => Promise<void>;

  // add a component
  componentCreate: (component: ComponentInfo) => Promise<Component>;
  // update a component
  componentUpdate: (component: ComponentInfo) => Promise<Component>;
  // remove a component
  componentDelete: (id: string, force?: boolean) => Promise<void>;

  // Send a measure
  componentSendMeasure: (id: string, data: Record<string, number>) => void;
  // Send an action
  componentSendAction: (id: string, name: string, params: any) => void; // eslint-disable-line @typescript-eslint/no-explicit-any

  // Listerner for component
  componentListenForEventType: (
    id: string,
    type: EventType | EventTypeString,
    fn: (e: EventHal) => Promise<void>,
  ) => void;
  // Listener for component's action
  componentListenAction: (id: string, name: string, fn: (e: EventComponentAction) => Promise<void>) => void;
  // Remove all listener for the component
  componentListenersRemoveAll: (id: string) => void;
  // Logger for component
  componentLog: (id: string, level: LogLevel, message: string, param?: unknown) => void;
  // Error for component
  componentError: (id: string, message: string, param?: unknown) => Error;

  // Finders
  findComponent: (id: string) => Component | null;
  findComponentType: (id: string) => ComponentType | null;
  findModule: (id: string) => Module | null;
}

export interface Settings {
  log_level?: LogLevel;
  logger?: Logger;
  module_path?: string;
  components?: Array<ComponentInfo>;
}

export interface Base {
  id: string;
  name: string;
  description?: string;
  icon?: string;
  categories?: Array<string>;
}

export interface Module extends Base {
  // Version of the module
  version: string;
  // Author of the module
  author?: string;
  // License of the module
  license?: string;
  // Home page of the module
  url?: string;
  // List all the ccomponent types of the module
  components: Array<ComponentType>;
  // graphql definition for the server (if needed)
  graphql?: { typeDefs: any; resolvers?: any }; // eslint-disable-line @typescript-eslint/no-explicit-any
  // Lower number will sart first
  startPriority?: number;
  // Is is a module used by the system ?
  isSystemModule?: boolean;
  // Disover devices for the module
  // wemo / hue / kodi / ...
  // Must create the component with the function `HAL.componentCreate`
  discover?: () => Promise<Array<Component>>;
}

export interface MeasurementDefinition {
  name: string;
  label?: string;
  help?: string;
  unit?: string;
  min?: number;
  max?: number;
  colors?: Array<string>;
}
export interface ComponentType extends Base {
  // module
  module: string;
  // definition of the configuration for the device
  definition?: Array<Field>;
  // defintion of measurements
  measurements?: Array<MeasurementDefinition>;
  // actions
  actions?: Array<ComponentAction>;
  // constructor of the component
  constructor?: (info: ComponentInfo) => Promise<Component>;
}

export interface ComponentAction {
  name: string;
  description?: string;
  params?: Array<Field>;
}

export interface ComponentInfo extends Base {
  // module
  module: string;
  // type of the device (match the id of the type)
  type: string;
  // config
  config?: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
}

// Interface for a device
export interface Component extends ComponentInfo {
  // start the device
  start: () => Promise<void>;
  // stop the device
  stop: () => Promise<void>;
}

export let HAL: IHal;
