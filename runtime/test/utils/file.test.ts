import * as assert from "assert";
import { readFile, writeFile, globAsync } from "../../src/utils/file";

describe("Testing `readFile`", () => {
  it("Should failed on none existing file", async () => {
    await assert.rejects(readFile("file-that-doesnt-exist.json"));
  });
});
