import * as assert from "assert";
import { discoveringIpThatListenOnPort } from "../../src/module/helper";

describe("Testing `discoveringIpThatListenOnPort`", function () {
  this.timeout(600000);
  it("Should not failed", async () => {
    await assert.doesNotReject(discoveringIpThatListenOnPort(80));
  });
});
