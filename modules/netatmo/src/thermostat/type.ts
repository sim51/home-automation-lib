import { ComponentType, FieldType, ModuleInfo } from "hal";
import * as fs from "fs";
import path from "path";
import { getConstructor } from "./component";

export const thermostat = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-thermostat`,
    name: "Netatmo Smart Thermostat",
    module: moduleInfo.id,
    description: moduleInfo.description,
    icon: fs.readFileSync(path.resolve(__dirname, "../../icons/thermostat.svg"), "utf8"),
    categories: moduleInfo.categories,
    definition: [
      {
        name: "login",
        required: true,
        type: FieldType.text,
        placeholder: "My Netatmo username",
      },
      {
        name: "password",
        required: true,
        type: FieldType.password,
        placeholder: "My Netatmo Password",
      },
      {
        name: "client_id",
        required: true,
        type: FieldType.text,
      },
      {
        name: "client_secret",
        required: true,
        type: FieldType.password,
      },
      {
        name: "frequency",
        help: "Frequency in second for data retrieving",
        type: FieldType.number,
      },
    ],
    measurements: [
      {
        name: "away",
        label: "Away mode",
        help: "Does the away mode is enabled ?",
        min: 0,
        max: 1,
      },
      {
        name: "boiler_status",
        label: "Boiler status",
        help: "Does the boiler is working ?",
        min: 0,
        max: 1,
      },
      {
        name: "temperature",
        min: 0,
        max: 50,
      },
      {
        name: "thermostat",
        min: 5,
        max: 30,
      },
    ],
    actions: [
      { name: "SCHEDULE", description: "Set the boiler mode to 'schedule'" },
      { name: "AWAY", description: "Set the boiler mode to 'away'" },
      {
        name: "TEMPERATURE",
        description: "Set the temperature",
        params: [
          {
            name: "temperature",
            required: true,
            type: FieldType.number,
            min: 5,
            max: 30,
            step: 0.5,
          },
        ],
      },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
