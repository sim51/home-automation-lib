import { HAL, Component, ComponentInfo, ComponentType, EventComponentAction, LogLevel } from "hal";
import { v4 as uuid } from "uuid";
import axios from "axios";

const formUrlEncoded = (x: { [key: string]: string }): string => {
  return Object.keys(x).reduce((p, c) => p + `&${c}=${encodeURIComponent(x[c])}`, "");
};

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the ComponentInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the Component (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    const api_url = "https://api.netatmo.com/";
    let accessToken = "";
    let refreshToken = "";
    let homeId = "";
    let roomId = "";
    let timeoutRefreshToken;
    let timeoutMeasurement;

    /**
     * Netatmo Init process:
     *  - retieve tokens
     *  - retieve home & room ids
     */
    async function init(): Promise<void> {
      try {
        // Retrieve tokens
        const response = await axios({
          method: "POST",
          url: `${api_url}/oauth2/token`,
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          responseType: "json",
          data: formUrlEncoded({
            grant_type: "password",
            username: info.config.login,
            password: info.config.password,
            client_id: info.config.client_id,
            client_secret: info.config.client_secret,
            scope: "read_thermostat write_thermostat",
          }),
        });
        HAL.componentLog(component.id, LogLevel.debug, `API respond with code ${response.status}`);
        accessToken = response.data.access_token;
        refreshToken = response.data.refresh_token;
        timeoutRefreshToken = setTimeout(refreshAccessToken, response.data.expires_in - 500);

        // Get home & room info
        await getHome();
      } catch (e) {
        throw HAL.componentError(component.id, e.message);
      }
    }

    /**
     * Refresh  access token.
     */
    async function refreshAccessToken(): Promise<void> {
      try {
        // Retrieve tokens
        const response = await axios({
          method: "POST",
          url: `${api_url}/oauth2/token`,
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          responseType: "json",
          data: formUrlEncoded({
            grant_type: "refresh_token",
            refresh_token: refreshToken,
            client_id: info.config.client_id,
            client_secret: info.config.client_secret,
          }),
        });
        HAL.componentLog(component.id, LogLevel.debug, `API respond with code ${response.status}`);
        accessToken = response.data.access_token;
        refreshToken = response.data.refresh_token;
        timeoutRefreshToken = setTimeout(refreshAccessToken, response.data.expires_in - 500);
      } catch (e) {
        throw HAL.componentError(component.id, e.message);
      }
    }

    /**
     * Get home & room ids
     */
    async function getHome(): Promise<void> {
      try {
        const response = await axios({
          method: "POST",
          url: `${api_url}/api/homesdata`,
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          responseType: "json",
          data: formUrlEncoded({ access_token: accessToken }),
        });
        HAL.componentLog(component.id, LogLevel.debug, `API respond with code ${response.status}`);
        // Get home & room info
        homeId = response.data.body.homes[0].id;
        roomId = response.data.body.homes[0].rooms[0].id;
      } catch (e) {
        throw HAL.componentError(component.id, e.message);
      }
    }

    async function proceedMeasurements(): Promise<void> {
      try {
        const response = await axios({
          method: "POST",
          url: `${api_url}/api/homestatus`,
          responseType: "json",
          data: {
            access_token: accessToken,
            home_id: homeId,
          },
        });
        HAL.componentLog(component.id, LogLevel.debug, `API respond with code ${response.status}`);
        HAL.componentSendMeasure(component.id, {
          status: response.data.body.home.rooms[0].reachable ? 1 : 0,
          away: response.data.body.home.rooms[0].therm_setpoint_mode == "away" ? 1 : 0,
          temperature: response.data.body.home.rooms[0].therm_measured_temperature,
          thermostat: response.data.body.home.rooms[0].therm_setpoint_temperature,
          boiler_status: response.data.body.home.modules[1].boiler_status ? 1 : 0,
        });
      } catch (e) {
        throw HAL.componentError(component.id, e.message);
      }
    }

    /**
     * Set the boiler mode : schedule, away or hg (frost guard mode)
     */
    async function setMode(mode: string): Promise<void> {
      try {
        const response = await axios({
          method: "POST",
          url: `${api_url}/api/setthermmode`,
          responseType: "json",
          data: {
            access_token: accessToken,
            home_id: homeId,
            mode: mode,
          },
        });
        HAL.componentLog(component.id, LogLevel.debug, `API respond with code ${response.status}`);
      } catch (e) {
        throw HAL.componentError(component.id, e.message);
      }
    }

    async function setTemperature(temperature: number): Promise<void> {
      try {
        const response = await axios({
          method: "POST",
          url: `${api_url}/api/setroomthermpoint`,
          responseType: "json",
          data: {
            access_token: accessToken,
            home_id: homeId,
            room_id: roomId,
            mode: "manual",
            temp: temperature,
          },
        });
        HAL.componentLog(component.id, LogLevel.debug, `API respond with code ${response.status}`);
      } catch (e) {
        throw HAL.componentError(component.id, e.message);
      }
    }

    /**
     * HAL Start component.
     */
    async function start() {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
      await init();

      // measure
      timeoutMeasurement = setInterval(
        proceedMeasurements,
        component.config.frequency ? component.config.frequency * 1000 : 10000,
      );

      //actions
      HAL.componentListenAction(component.id, "SCHEDULE", () => setMode("schedule"));
      HAL.componentListenAction(component.id, "AWAY", () => setMode("away"));
      HAL.componentListenAction(component.id, "FROST", () => setMode("hg"));
      HAL.componentListenAction(component.id, "TEMPERATURE", (event: EventComponentAction) =>
        setTemperature(event.data.temperature),
      );
    }

    /**
     * HAL Stop component.
     */
    async function stop() {
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
      clearInterval(timeoutMeasurement);
      clearTimeout(timeoutRefreshToken);
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
