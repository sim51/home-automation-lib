import * as fs from "fs";
import path from "path";
import { Module, moduleParsePackageJson, ModuleInfo } from "hal";

// Construct the module.
// in params HAL + the package.json
export default async (): Promise<Module> => {
  // load module information
  const moduleInfo: ModuleInfo = await moduleParsePackageJson(path.resolve(__dirname, "../package.json"));
  // search components and load them
  const components = [];
  return {
    ...moduleInfo,
    icon: fs.readFileSync(path.resolve(__dirname, "../icons/logo.svg"), "utf8"),
    components: components,
  };
};
