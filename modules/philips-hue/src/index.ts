import * as fs from "fs";
import path from "path";
import { Component, ComponentInfo, LogLevel, Module, moduleParsePackageJson, ModuleInfo, HAL } from "hal";
import * as huejay from "huejay";
import { bridge } from "./bridge/type";
import { group } from "./group/type";
import { light } from "./light/type";
import { sensor } from "./sensor/type";

// Construct the module.
// in params HAL + the package.json
export default async (): Promise<Module> => {
  // load module information
  const moduleInfo: ModuleInfo = await moduleParsePackageJson(path.resolve(__dirname, "../package.json"));

  // search components and load them
  const components = [
    await bridge(moduleInfo),
    await light(moduleInfo),
    await group(moduleInfo),
    await sensor(moduleInfo),
  ];

  // Method for discovering components
  // it discovers the bridge and its underlying components
  // ---------------------------------
  const discover = async (): Promise<Array<Component>> => {
    try {
      const bridges = await huejay.discover();
      const discoveredComponentsInfo: Array<ComponentInfo> = [];
      await Promise.all(
        bridges.map(async (bridge) => {
          // The bridges
          // ----------------------------------------------------------------------------
          let hue = null;
          let bridgeComponent: ComponentInfo = HAL.components.find(
            (c) => c.type === "hal-module-philips-hue-bridge" && c.config.ip === bridge.ip,
          );
          if (bridgeComponent) {
            hue = new huejay.Client({
              host: bridgeComponent.config.ip,
              port: bridgeComponent.config.port,
              username: bridgeComponent.config.username,
            });
          } else {
            // Creating the user
            hue = new huejay.Client({ host: bridge.ip, port: 80 });
            let user = new hue.users.User();
            try {
              user = await hue.users.create(user);
            } catch (e) {
              if (e instanceof huejay.Error && e.type === 101) {
                throw HAL.moduleError(moduleInfo.id, "Link button not pressed", e);
              }
              throw HAL.moduleError(moduleInfo.id, "Failure to create user in discovery process", e);
            }
            hue = new huejay.Client({ host: bridge.ip, port: 80, username: user.username });

            // Building component
            bridgeComponent = {
              id: `${components[0].id}-${bridge.id}`,
              name: "Hue Bridge",
              module: moduleInfo.id,
              type: components[0].id,
              config: {
                ip: bridge.ip,
                port: 80,
                username: user.username,
              },
            };
            discoveredComponentsInfo.push(bridgeComponent);
          }

          // Lights
          // ----------------------------------------------------------------------------
          const lights = await hue.lights.getAll();
          lights.forEach((light) => {
            // Test if the component is already created
            if (HAL.components.findIndex((c) => c.id === `${components[1].id}-${light.id}`) === -1) {
              const componentInfo: ComponentInfo = {
                id: `${components[1].id}-${light.id}`,
                name: light.name,
                module: moduleInfo.id,
                type: components[1].id,
                config: {
                  bridge: bridgeComponent.id,
                  hueId: parseInt(light.id),
                },
              };
              discoveredComponentsInfo.push(componentInfo);
            }
          });

          // Groups
          // ----------------------------------------------------------------------------
          const groups = await hue.groups.getAll();
          groups.forEach((group) => {
            // Test if the component is already created
            if (HAL.components.findIndex((c) => c.id === `${components[2].id}-${group.id}`) === -1) {
              const componentInfo: ComponentInfo = {
                id: `${components[2].id}-${group.id}`,
                name: group.name,
                module: moduleInfo.id,
                type: components[2].id,
                config: {
                  bridge: bridgeComponent.id,
                  hueId: parseInt(group.id),
                },
              };
              discoveredComponentsInfo.push(componentInfo);
            }
          });

          // Sensors
          // ----------------------------------------------------------------------------
          const sensors = await hue.sensors.getAll();
          sensors.forEach((sensor) => {
            console.log(
              sensor,
              HAL.components.find((c) => c.id === `${components[3].id}-${sensor.id}`),
            );
            // Test if the component is already created
            if (HAL.components.findIndex((c) => c.id === `${components[3].id}-${sensor.id}`) === -1) {
              const componentInfo: ComponentInfo = {
                id: `${components[3].id}-${sensor.id}`,
                name: sensor.name,
                module: moduleInfo.id,
                type: components[3].id,
                config: {
                  bridge: bridgeComponent.id,
                  hueId: parseInt(sensor.id),
                },
              };
              discoveredComponentsInfo.push(componentInfo);
            }
          });

          return;
        }),
      );
      const discoveredComponents = await Promise.all(
        discoveredComponentsInfo.map(async (componentInfo) => {
          HAL.moduleLog(moduleInfo.id, LogLevel.info, `Discover component`, componentInfo);
          const component = await HAL.componentCreate(componentInfo);
          return component;
        }),
      );

      return discoveredComponents;
    } catch (e) {
      throw HAL.moduleError(moduleInfo.id, e);
    }
  };

  return {
    ...moduleInfo,
    icon: fs.readFileSync(path.resolve(__dirname, "../icons/logo.svg"), "utf8"),
    components: components,
    discover: discover,
  };
};
