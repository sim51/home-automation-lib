import { ComponentType, FieldType, ModuleInfo } from "hal";
import { getConstructor } from "./component";

export const sensor = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-sensor`,
    name: "Philips Hue - Sensor",
    module: moduleInfo.id,
    description: "",
    icon: "wave-square",
    categories: moduleInfo.categories,
    constructor: undefined,
    measurements: [
      {
        name: "lightlevel",
        min: 0,
        max: 254,
      },
      {
        name: "temperature",
      },
      {
        name: "dark",
        min: 0,
        max: 1,
      },
      {
        name: "presence",
        min: 0,
        max: 1,
      },
      {
        name: "daylight",
        min: 0,
        max: 1,
      },
    ],
    definition: [
      {
        name: "bridge",
        required: true,
        type: FieldType.component,
        placeholder: "Bridge component",
        pattern: `${moduleInfo.id}-bridge`,
      },
      {
        name: "hueId",
        required: true,
        type: FieldType.number,
        placeholder: "Hue Sensor ID",
      },
    ],
  };
  type.constructor = getConstructor(type);
  return type;
};
