import { HAL, Component, ComponentInfo, ComponentType, LogLevel } from "hal";
import { v4 as uuid } from "uuid";

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the ComponentInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the Component (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    /**
     * HAL Start component.
     */
    async function start() {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
    }

    /**
     * HAL Stop component.
     */
    async function stop() {
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
