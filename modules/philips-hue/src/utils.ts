export interface RGB {
  r: number;
  g: number;
  b: number;
}
export type HEX = string;
export type XY = [number, number];

/**
 * Convert RGB -> XY.
 */
export function rgbToXy(rgb: RGB): XY {
  // Apply a gamma correction to the RGB values, which makes the color
  // more vivid and more the like the color displayed on the screen of your device
  const red: number = rgb.r > 0.04045 ? Math.pow((rgb.r + 0.055) / (1.0 + 0.055), 2.4) : rgb.r / 12.92;
  const green: number = rgb.g > 0.04045 ? Math.pow((rgb.g + 0.055) / (1.0 + 0.055), 2.4) : rgb.g / 12.92;
  const blue: number = rgb.b > 0.04045 ? Math.pow((rgb.b + 0.055) / (1.0 + 0.055), 2.4) : rgb.b / 12.92;

  // RGB values to XYZ using the Wide RGB D65 conversion formula
  const X: number = red * 0.664511 + green * 0.154324 + blue * 0.162028;
  const Y: number = red * 0.283881 + green * 0.668433 + blue * 0.047685;
  const Z: number = red * 0.000088 + green * 0.07231 + blue * 0.986039;

  // Calculate the xy values from the XYZ values
  const x: string = (X / (X + Y + Z)).toFixed(4);
  const y: string = (Y / (X + Y + Z)).toFixed(4);

  // if (isNaN(x)) {
  //   x = "0";
  // }
  //
  // if (isNaN(y)) {
  //   y = "0";
  // }

  return [Number.parseFloat(x), Number.parseFloat(y)];
}

/**
 * Convert XY -> RGB
 */
export function xyToRgb(xy: XY, bri = 255): RGB {
  // default value
  if (xy === undefined) {
    return { r: 254, g: 254, b: 254 };
  }

  const x = xy[0];
  const y = xy[1];

  let r = 0,
    g = 0,
    b = 0;

  const z = 1.0 - x - y;
  const Y = bri / 255.0; // Brightness of lamp
  const X = (Y / y) * x;
  const Z = (Y / y) * z;
  r = X * 1.612 - Y * 0.203 - Z * 0.302;
  g = -X * 0.509 + Y * 1.412 + Z * 0.066;
  b = X * 0.026 - Y * 0.072 + Z * 0.962;

  r = r <= 0.0031308 ? 12.92 * r : (1.0 + 0.055) * Math.pow(r, 1.0 / 2.4) - 0.055;
  g = g <= 0.0031308 ? 12.92 * g : (1.0 + 0.055) * Math.pow(g, 1.0 / 2.4) - 0.055;
  b = b <= 0.0031308 ? 12.92 * b : (1.0 + 0.055) * Math.pow(b, 1.0 / 2.4) - 0.055;

  const maxValue = Math.max(r, g, b);

  r /= maxValue;
  g /= maxValue;
  b /= maxValue;

  r = r * 255;
  if (r < 0) {
    r = 255;
  }

  g = g * 255;
  if (g < 0) {
    g = 255;
  }

  b = b * 255;
  if (b < 0) {
    b = 255;
  }

  return {
    r: Math.round(r),
    g: Math.round(g),
    b: Math.round(b),
  };
}

export function toHex(c: number): string {
  const hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

export function rgbToHex(rgb: RGB): HEX {
  if (rgb === undefined) return "#FFFFFF";
  return "#" + toHex(rgb.r) + toHex(rgb.g) + toHex(rgb.b);
}

/**
 * Convert HEX -> RGB.
 */
export function hexToRgb(hex: HEX): RGB {
  // default value
  if (hex === undefined) {
    return { r: 254, g: 254, b: 254 };
  }
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b;
  });

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  if (!result) {
    throw new Error("Failed to convert HEX -> RBG");
  }
  return {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
  };
}
