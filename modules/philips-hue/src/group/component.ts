import { HAL, Component, ComponentInfo, ComponentType, EventComponentAction, LogLevel } from "hal";
import { v4 as uuid } from "uuid";

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the ComponentInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the Component (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    /**
     * HAL Start component.
     */
    async function start() {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
      HAL.componentListenAction(
        component.id,
        "ON",
        (event: EventComponentAction): Promise<void> => {
          return new Promise((resolve, reject) => {
            try {
              HAL.componentSendAction(component.config.bridge, "ON", { type: "group", id: component.config.hueId });
              resolve();
            } catch (e) {
              reject(e);
            }
          });
        },
      );
      HAL.componentListenAction(
        component.id,
        "OFF",
        (event: EventComponentAction): Promise<void> => {
          return new Promise((resolve, reject) => {
            try {
              HAL.componentSendAction(component.config.bridge, "OFF", { type: "group", id: component.config.hueId });
              resolve();
            } catch (e) {
              reject(e);
            }
          });
        },
      );
      HAL.componentListenAction(
        component.id,
        "CONFIG",
        (event: EventComponentAction): Promise<void> => {
          return new Promise((resolve, reject) => {
            try {
              HAL.componentSendAction(
                component.config.bridge,
                "CONFIG",
                Object.assign({}, event.data, {
                  type: "group",
                  id: component.config.hueId,
                }),
              );
              resolve();
            } catch (e) {
              reject(e);
            }
          });
        },
      );
    }

    /**
     * HAL Stop component.
     */
    async function stop() {
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
