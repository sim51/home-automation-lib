import { ComponentType, FieldType, ModuleInfo } from "hal";
import { getConstructor } from "./component";

export const group = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-group`,
    name: "Philips Hue - Group Of lights",
    module: moduleInfo.id,
    description: "",
    icon: "traffic-light",
    categories: moduleInfo.categories,
    definition: [
      {
        name: "bridge",
        required: true,
        type: FieldType.component,
        placeholder: "Bridge component",
        pattern: `${moduleInfo.id}-bridge`,
      },
      {
        name: "hueId",
        required: true,
        type: FieldType.number,
        placeholder: "Hue Group ID",
      },
    ],
    measurements: [
      {
        name: "hue",
        min: 0,
        max: 65535,
      },
      {
        name: "saturation",
        min: 0,
        max: 254,
      },
      {
        name: "brightness",
        min: 0,
        max: 254,
      },
      {
        name: "on",
        min: 0,
        max: 1,
      },
    ],
    // actions
    actions: [
      { name: "ON", description: "Turns on the group lights" },
      { name: "OFF", description: "Turns off the group lights" },
      {
        name: "CONFIG",
        description: "Set group color & brightness",
        params: [
          {
            name: "color",
            required: true,
            type: FieldType.color,
            placeholder: "Light color",
          },
          {
            name: "brightness",
            required: true,
            type: FieldType.range,
            placeholder: "Bridge component",
            min: 0,
            max: 254,
          },
        ],
      },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
