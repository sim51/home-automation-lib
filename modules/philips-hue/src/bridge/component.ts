import { HAL, Component, ComponentInfo, ComponentType, EventComponentAction, LogLevel } from "hal";
import { v4 as uuid } from "uuid";
import * as huejay from "huejay";
import { hexToRgb, xyToRgb, rgbToHex, rgbToXy, RGB, XY } from "../utils";

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the ComponentInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the Component (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    let hue = null;
    let timeout = null;

    async function proceedMeasurements(): Promise<void> {
      try {
        // For all lights
        const lights = await hue.lights.getAll();
        lights.forEach((light) => {
          const measure = {
            status: light.on && light.reachable ? 1 : 0,
            brightness: light.brightness ? parseInt(light.brightness) : undefined,
            saturation: light.saturation ? parseInt(light.saturation) : undefined,
            reachable: light.reachable ? 1 : 0,
            on: light.on ? 1 : 0,
            hue: light.hue,
          };
          const lightComponent = HAL.components.find(
            (c) => c.type === `${component.module}-light` && c.config.hueId === parseInt(light.id),
          );
          if (lightComponent) {
            HAL.componentSendMeasure(lightComponent.id, measure);
          }
        });

        // For groups
        const groups = await hue.groups.getAll();
        groups.forEach((group) => {
          // compute if all light are really on (reachable)
          const status = lights
            //filter lights of the group
            .filter((light) => {
              if (group.lightIds.indexOf(light.id) > -1) return true;
              else return false;
            })
            .reduce((status, element) => {
              if (status == 0 || !element.reachable || !element.on) {
                status = 0;
              }
              return status;
            }, 1);
          const measure = {
            status: status,
            brightness: group.brightness ? parseInt(group.brightness) : undefined,
            saturation: group.saturation ? parseInt(group.saturation) : undefined,
            hue: group.hue,
            on: group.on ? 1 : 0,
          };
          const groupComponent = HAL.components.find(
            (c) => c.type === `${component.module}-group` && c.config.hueId === parseInt(group.id),
          );
          if (groupComponent) {
            HAL.componentSendMeasure(groupComponent.id, measure);
          }
        });

        // For sensors
        const sensors = await hue.sensors.getAll();
        sensors.forEach((sensor) => {
          let data: { [key: string]: any } = {};
          switch (sensor.type) {
            case "ZLLLightLevel":
              data = {
                lightlevel: sensor.state.attributes.lightlevel
                  ? parseInt(sensor.state.attributes.lightlevel)
                  : undefined,
                dark: sensor.state.attributes.lightlevel ? 1 : 0,
                daylight: sensor.state.attributes.daylight ? 1 : 0,
              };
              break;
            case "CLIPPresence":
            case "ZLLPresence":
              data = {
                presence: sensor.state.attributes.presence ? 1 : 0,
              };
              break;
            case "CLIPTemperature":
            case "ZLLTemperature":
              data = {
                temperature: sensor.state.attributes.temperature
                  ? parseInt(sensor.state.attributes.temperature) / 100
                  : undefined,
              };
              break;
          }

          // Sending value if needed
          if (Object.keys(data).filter((key) => data[key] !== undefined).length > 0) {
            const sensorComponent = HAL.components.find(
              (c) => c.type === `${component.module}-sensor` && c.config.hueId === parseInt(sensor.id),
            );
            if (sensorComponent) {
              HAL.componentSendMeasure(sensorComponent.id, data);
            }
          }
        });
      } catch (e) {
        throw HAL.componentError(component.id, "Error on retrieving measures", e);
      }
    }

    /**
     * HAL Start component.
     */
    async function start() {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
      // Ceate the huejay client
      hue = new huejay.Client({
        host: component.config.ip,
        port: component.config.port,
        username: component.config.username,
      });

      // Start the measurements process
      timeout = setInterval(
        () => {
          proceedMeasurements();
        },
        component.config.frequency ? component.config.frequency * 1000 : 10000,
      );

      // Register for module actions
      HAL.componentListenAction(
        component.id,
        "ON",
        (event: EventComponentAction): Promise<void> => {
          return new Promise((resolve, reject) => {
            switch (event.data.type) {
              case "light":
                hue.lights
                  .getById(event.data.id)
                  .then((light) => {
                    light.on = true;
                    hue.lights.save(light);
                    resolve();
                  })
                  .catch((error) => {
                    reject(error);
                  });
                break;
              case "group":
                hue.groups
                  .getById(event.data.id)
                  .then((group) => {
                    group.on = true;
                    hue.groups.save(group);
                    resolve();
                  })
                  .catch((error) => {
                    reject(error);
                  });
                break;
            }
          });
        },
      );

      HAL.componentListenAction(
        component.id,
        "OFF",
        (event: EventComponentAction): Promise<void> => {
          return new Promise((resolve, reject) => {
            switch (event.data.type) {
              case "light":
                hue.lights
                  .getById(event.data.id)
                  .then((light) => {
                    light.on = false;
                    hue.lights.save(light);
                    resolve();
                  })
                  .catch((error) => {
                    reject(error);
                  });
                break;
              case "group":
                hue.groups
                  .getById(event.data.id)
                  .then((group) => {
                    group.on = false;
                    hue.groups.save(group);
                    resolve();
                  })
                  .catch((error) => {
                    reject(error);
                  });
                break;
            }
          });
        },
      );

      HAL.componentListenAction(component.id, "CONFIG", (event: EventComponentAction) => {
        return new Promise((resolve, reject) => {
          if (event.data.color) {
            const rgb: RGB = hexToRgb(event.data.color);
            const xy: XY = rgbToXy(rgb);
            switch (event.data.type) {
              case "light":
                hue.lights
                  .getById(event.data.id)
                  .then((light) => {
                    light.on = true;
                    light.brightness = event.data.brightness;
                    light.xy = xy;
                    hue.lights.save(light);
                    resolve();
                  })
                  .catch((error) => {
                    reject(error);
                  });
                break;
              case "group":
                hue.groups
                  .getById(event.data.id)
                  .then((group) => {
                    group.on = true;
                    group.brightness = event.data.brightness;
                    group.xy = xy;
                    hue.groups.save(group);
                    resolve();
                  })
                  .catch((error) => {
                    reject(error);
                  });
                break;
            }
          } else {
            reject("no color defined");
          }
        });
      });
    }

    /**
     * HAL Stop component.
     */
    async function stop() {
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
      clearInterval(timeout);
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
