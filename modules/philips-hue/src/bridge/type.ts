import { ComponentType, FieldType, ModuleInfo } from "hal";
import { getConstructor } from "./component";

export const bridge = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-bridge`,
    name: "Philips Hue - Bridge",
    module: moduleInfo.id,
    description: "",
    icon: "server",
    categories: moduleInfo.categories,
    definition: [
      {
        name: "ip",
        required: true,
        type: FieldType.text,
        placeholder: "123.0.12.34",
        pattern: "([0-9]{1,3}.){3}[0-9]{1,3}$",
      },
      {
        name: "port",
        required: true,
        type: FieldType.number,
        placeholder: "80",
      },
      {
        name: "username",
        required: true,
        type: FieldType.text,
      },
      {
        name: "frequency",
        help: "Frequency in second for data retrieving",
        type: FieldType.number,
      },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
