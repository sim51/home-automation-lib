import Neo4j from "neo4j-driver";
import { HAL, Component, ComponentInfo, ComponentType, LogLevel, EventType, EventComponent, EventModule } from "hal";
import { v4 as uuid } from "uuid";
import * as query from "./queries";

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the deviceInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the device (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    // instance of the neo4j driver
    let driver;
    async function runQuery(query: string, data: { [key: string]: any }, retry = 0): Promise<void> {
      const session = driver.session();
      try {
        await session.writeTransaction(async (txc) => {
          await txc.run(query, JSON.parse(JSON.stringify(data)));
        });
      } catch (e) {
        if (retry < 3) runQuery(query, data, retry++);
        else throw HAL.componentError(component.id, `Query error: ${e.message}`);
      } finally {
        session.close();
      }
    }

    // When there is a new module, we create/update it
    const onModuleRegister = async (event: EventModule): Promise<void> => {
      HAL.componentLog(component.id, LogLevel.debug, `Processing onModuleRegister event`, event);
      return await runQuery(query.registerModule, {
        module: event.module,
        started: false,
        discovery: event.module.discover ? true : false,
      });
    };
    // Set the status of the module
    const onModuleStart = async (event: EventModule): Promise<void> => {
      HAL.componentLog(component.id, LogLevel.debug, `Processing onModuleStart event`, event);
      return await runQuery(query.registerModule, {
        module: event.module,
        started: true,
        discovery: event.module.discover ? true : false,
      });
    };
    const onModuleStop = async (event: EventModule): Promise<void> => {
      HAL.componentLog(component.id, LogLevel.debug, `Processing onModuleStop event`, event);
      return await runQuery(query.stopStartModule, { module: event.module, started: false });
    };
    const onComponentUnRegister = async (event: EventComponent): Promise<void> => {
      HAL.componentLog(component.id, LogLevel.debug, `Processing onComponentUnRegister event`, event);
      return await runQuery(query.unregisterComponent, { component: event.component });
    };
    const onComponentRegister = async (event: EventComponent): Promise<void> => {
      HAL.componentLog(component.id, LogLevel.debug, `Processing onComponentRegister event`, event);
      return await runQuery(query.registerComponent, { component: event.component, started: true });
    };
    const onComponentStart = async (event: EventComponent): Promise<void> => {
      HAL.componentLog(component.id, LogLevel.debug, `Processing onComponentStart event`, event);
      if (event.component.module === "hal-module-presence") {
        return await runQuery(query.stopStartPresenceComponent, { component: event.component, started: true });
      } else {
        return await runQuery(query.stopStartComponent, { component: event.component, started: true });
      }
    };

    const onComponentStop = async (event: EventComponent): Promise<void> => {
      HAL.componentLog(component.id, LogLevel.debug, `Processing onComponentStop event`, event);
      return await runQuery(query.stopStartComponent, { component: event.component, started: false });
    };

    const onComponentMeasure = async (event: EventComponent): Promise<void> => {
      HAL.componentLog(component.id, LogLevel.debug, `Processing onComponentMeasure event`, event);
      return await runQuery(query.measureComponent, {
        component: event.component,
        data: event.data,
        time: event.datetime,
      });
    };

    const initDatabase = async (): Promise<void> => {
      const session = driver.session();
      try {
        const result = await session.run("MATCH (n:User) RETURN count(*) as count");
        const record = result.records.shift();
        if (record.get("count") === 0) {
          HAL.componentLog(component.id, LogLevel.info, "Create default admin user");
          await session.run(
            "CREATE (u:User { username: $username, password: apoc.util.md5([$password]), fullname: $fullname, roles:['admin']})",
            { username: "admin", password: "admin", fullname: "Administrateur" },
          );
        }
      } catch (e) {
        throw HAL.componentError(component.id, `Failed to init the database`, e);
      } finally {
        session.close();
      }
    };

    async function start(): Promise<void> {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
      // Initiate the Neo4j driver
      if (driver) {
        try {
          await driver.close();
        } catch (e) {
          throw HAL.componentError(component.id, `Failed to closed the driver`, e);
        }
      }
      try {
        driver = Neo4j.driver(
          component.config.url,
          Neo4j.auth.basic(component.config.login, component.config.password),
          {
            disableLosslessIntegers: true,
          },
        );
      } catch (e) {
        throw HAL.componentError(component.id, `Failed to create driver`, e);
      }

      try {
        await initDatabase();
      } catch (e) {
        throw HAL.componentError(component.id, "NEO4J", `Failed to init database`);
      }

      // Register event handler
      HAL.componentListenForEventType(component.id, EventType.ModuleRegister, onModuleRegister);
      HAL.componentListenForEventType(component.id, EventType.ModuleStart, onModuleStart);
      HAL.componentListenForEventType(component.id, EventType.ModuleStop, onModuleStop);
      HAL.componentListenForEventType(component.id, EventType.ComponentUnRegister, onComponentUnRegister);
      HAL.componentListenForEventType(component.id, EventType.ComponentRegister, onComponentRegister);
      HAL.componentListenForEventType(component.id, EventType.ComponentStart, onComponentStart);
      HAL.componentListenForEventType(component.id, EventType.ComponentStop, onComponentStop);
      HAL.componentListenForEventType(component.id, EventType.ComponentMeasure, onComponentMeasure);
    }

    async function stop() {
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
      // Stop the driver
      if (driver) {
        try {
          await driver.close();
        } catch (e) {
          throw HAL.componentError(component.id, `Failed to closed the driver`, e);
        }
      }
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
