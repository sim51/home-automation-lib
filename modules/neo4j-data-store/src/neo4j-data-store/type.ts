import { ComponentType, FieldType, ModuleInfo } from "hal";
import * as fs from "fs";
import path from "path";
import { getConstructor } from "./component";

export const neo4jStorage = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-component`,
    name: "Neo4j Data Storage",
    module: moduleInfo.id,
    description: moduleInfo.description,
    icon: fs.readFileSync(path.resolve(__dirname, "../../icons/neo4j.svg"), "utf8"),
    categories: ["storage", "neo4j"],
    definition: [
      {
        name: "url",
        required: true,
        type: FieldType.text,
        placeholder: "bolt://127.0.0.1:7687",
        pattern: "^bolt://([0-9]{1,3}.){3}[0-9]{1,3}(:[0-9]{1,5})?$",
      },
      {
        name: "login",
        required: true,
        type: FieldType.text,
        placeholder: "Username",
      },
      {
        name: "password",
        required: true,
        type: FieldType.text,
        placeholder: "Password",
      },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
