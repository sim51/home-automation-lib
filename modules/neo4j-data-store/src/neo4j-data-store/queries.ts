export const registerModule = `
  MERGE (m:Module {id: $module.id})
  SET m.started = $started,
      m.discovery = $discovery,
      m.name = $module.name,
      m.description = $module.description,
      m.icon = $module.icon,
      m.version = $module.version,
      m.author = $module.author,
      m.license = $module.license,
      m.url = $module.url,
      m.startPriority = $module.startPriority,
      m.isSystemModule = coalesce($module.isSystemModule, false)
  FOREACH(cat IN  $module.categories |
    MERGE (c:Category {name:cat})
    MERGE (m)-[:IN_CATEGORY]->(c)
  )

  WITH m UNWIND $module.components AS component
    MERGE (comp:ComponentType {id: component.id})
    SET comp.name = component.name,
        comp.description = component.description,
        comp.icon = component.icon
    MERGE (m)-[:HAS_COMPONENT_TYPE]->(comp)

    // Save actions
    FOREACH(action IN component.actions |
      MERGE (comp)-[:HAS_ACTION]->(a:Action {name:action.name})
      SET a.description = action.description

      FOREACH(field IN action.params |
        MERGE (a)-[:HAS_PARAM]->(f:Field {name:field.name})
        SET f += field
      )
    )

    // Save definition fields
    FOREACH(field IN  component.definition |
      MERGE (comp)-[:HAS_CONFIG]->(f:Field {name:field.name})
      SET f += field
    )

    // Save measurement fields
    FOREACH(ms IN  component.measurements |
      MERGE (comp)-[:HAS_MEASUREMENT]->(measure:MeasureDefinition {name: ms.name})
      SET measure += ms
    )

    // Save categories
    FOREACH(cat IN  component.categories |
      MERGE (c:Category {name:cat})
      MERGE (comp)-[:IN_CATEGORY]->(c)
    )`;

export const stopStartModule = `MATCH (m:Module {id: $module.id}) SET m.started=$started`;

export const registerComponent = `
  MATCH (type:ComponentType {id:$component.type})
  MERGE (c:Component {id: $component.id})
  SET c.name = $component.name,
      c.started = coalesce($started, c.started),
      c.description = $component.description,
      c.icon = $component.icon,
      c.config = apoc.convert.toJson($component.config)
  MERGE (type)-[:HAS_COMPONENT]->(c)
  `;

export const unregisterComponent = `MATCH (c:Component {id:$component.id}) DETACH DELETE c`;

export const stopStartComponent = `MATCH (c:Component {id:$component.id}) SET c.started = $started`;

export const stopStartPresenceComponent = `
    MATCH (u:User {username:$component.config.username})
    MERGE (type:ComponentType {id:$component.type})
    MERGE (c:Component {id: $component.id})
    SET c.name = $component.name,
        c.started = coalesce($started, c.started),
        c.description = $component.description,
        c.icon = $component.icon,
        c.config = apoc.convert.toJson($component.config)
    MERGE (type)-[:HAS_COMPONENT]->(c)
    WITH c, u
      OPTIONAL MATCH (:User)-[r:HAS_PRESENCE_COMPONENT]->(c)
      DELETE r
      MERGE (u)-[:HAS_PRESENCE_COMPONENT]->(c)`;

export const measureComponent = `
  MATCH (c:Component {id:$component.id})
  MERGE (m:Measure {id:$component.id})
  SET m+= $data
  SET m.time = $time
  MERGE (c)-[:LAST_MEASURE]->(m)`;
