= Neo4j data storage module

== Description

This module allows you to connect HAL with a Neo4j database.

== List of components

=== `neo4j-data-store`

It consumes all the module & component events and generate a graph state in Neo4j.