import path from "path";
import { Module, moduleParsePackageJson, ModuleInfo } from "hal";
import { openweather } from "./openweather/type";

// Construct the module.
// in params HAL + the package.json
export default async (): Promise<Module> => {
  // load module information
  const moduleInfo: ModuleInfo = await moduleParsePackageJson(path.resolve(__dirname, "../package.json"));
  // search components and load them
  const components = [await openweather(moduleInfo)];
  return {
    ...moduleInfo,
    icon: "cloud-sun",
    components: components,
  };
};
