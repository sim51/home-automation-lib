import { HAL, Component, ComponentInfo, ComponentType, LogLevel } from "hal";
import { v4 as uuid } from "uuid";
import axios from "axios";

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the deviceInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the device (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    let timeout = null;
    async function proceedMeasurements(): Promise<void> {
      try {
        const response = await axios({
          method: "GET",
          params: {
            lat: component.config.location.latitude,
            lon: component.config.location.longitude,
            appid: component.config.token,
            units: "metric",
          },
          url: "https://api.openweathermap.org/data/2.5/weather",
          responseType: "json",
        });
        HAL.componentLog(component.id, LogLevel.debug, `API response code`, response.status);
        HAL.componentSendMeasure(component.id, {
          status: 1,
          weather_id: response.data.weather[0].id,
          temperature: response.data.main.temp,
          feels_like: response.data.main.feels_like,
          pressure: response.data.main.pressure,
          humidity: response.data.main.humidity,
          cloudiness: response.data.clouds ? response.data.clouds.all : 0,
          snow: response.data.snow ? response.data.snow["1h"] : 0,
          visibility: response.data.visibility || 0,
          wind_speed: response.data.wind ? response.data.wind.speed : 0,
          wind_direction: response.data.wind ? response.data.wind.deg : 0,
          precipitation: response.data.rain ? response.data.rain["1h"] : 0,
        });
      } catch (e) {
        // Don't need a throw because it's wrapped inside a timeout function.
        // So it avoids a UnhandledPromiseRejectionWarning error
        HAL.componentError(component.id, e.message);
      }
    }

    async function start() {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
      timeout = setInterval(
        proceedMeasurements,
        component.config.frequency ? component.config.frequency * 1000 : 10000,
      );
    }

    async function stop() {
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
      clearInterval(timeout);
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
