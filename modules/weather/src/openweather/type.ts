import { ComponentType, FieldType, ModuleInfo } from "hal";
import { getConstructor } from "./component";

export const openweather = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-openweather`,
    name: "OpenWeather",
    module: moduleInfo.id,
    description: "OpenWeather component",
    icon: "sun",
    categories: moduleInfo.categories,
    definition: [
      {
        name: "location",
        required: true,
        type: FieldType.location,
      },
      {
        name: "token",
        required: true,
        type: FieldType.text,
      },
      {
        name: "frequency",
        help: "Frequency in second for data retrieving",
        type: FieldType.number,
      },
    ],
    measurements: [
      {
        name: "weather_id",
      },
      {
        name: "temperature",
        unit: "°C",
        min: -10,
        max: 50,
        // colors: ["#2164a3", "#dc3545"],
      },
      {
        name: "feels_like",
        unit: "°C",
        min: -10,
        max: 50,
      },
      {
        name: "pressure",
        unit: "hPa",
        min: 870,
        max: 1085,
      },
      {
        name: "humidity",
        unit: "%",
        min: 0,
        max: 100,
      },
      {
        name: "cloudiness",
        unit: "%",
        min: 0,
        max: 100,
      },
      {
        name: "snow",
        unit: "mm/h",
        min: 0,
      },
      {
        name: "visibility",
        unit: "m",
        min: 0,
        max: 10000,
      },
      {
        name: "wind_speed",
        unit: "m/s",
        min: 0,
      },
      {
        name: "wind_direction",
        unit: "°",
        min: 0,
        max: 360,
      },
      {
        name: "precipitation",
        unit: "mm",
        min: 0,
      },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
