import { HAL, Component, ComponentInfo, ComponentType, LogLevel, EventType, EventComponent } from "hal";
import { v4 as uuid } from "uuid";
import * as ping from "ping";

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the deviceInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the device (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    let timeout = null;
    function proceedMeasurements(): void {
      ping.sys.probe(component.config.ip, (isAlive) => {
        HAL.componentLog(component.id, LogLevel.debug, `Checking presence of ip ${component.config.ip} => ${isAlive}`);
        HAL.componentSendMeasure(component.id, { status: isAlive ? 1 : 0 });
      });
    }

    async function start() {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
      timeout = setInterval(
        proceedMeasurements,
        component.config.frequency ? component.config.frequency * 1000 : 10000,
      );
    }

    async function stop() {
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
      clearInterval(timeout);
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
