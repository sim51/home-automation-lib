import { ComponentType, FieldType, ModuleInfo } from "hal";
import { getConstructor } from "./component";

export const ip = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-ip`,
    name: "IP Check",
    module: moduleInfo.id,
    description: "Check presence by a phone IP on network",
    icon: "network-wired",
    categories: moduleInfo.categories,
    definition: [
      {
        name: "username",
        required: true,
        type: FieldType.user,
      },
      {
        name: "ip",
        required: true,
        type: FieldType.text,
        placeholder: "123.0.12.34",
        pattern: "([0-9]{1,3}.){3}[0-9]{1,3}$",
      },
      {
        name: "frequency",
        help: "Frequency in second for data retrieving",
        type: FieldType.number,
      },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
