import axios from "axios";
import { escape } from "./escape";
import { HAL, Component, ComponentInfo, ComponentType, LogLevel, EventType, EventComponent } from "hal";
import { v4 as uuid } from "uuid";

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the deviceInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the device (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    let sendingMeasuresJob = null;
    // store for measures
    let measures: Array<string> = [];

    const onComponentMeasure = async (event: EventComponent): Promise<void> => {
      HAL.componentLog(component.id, LogLevel.debug, `onComponentMeasure`, event);
      if (!event.component) {
        return;
      }
      // measurement
      let measure = `${escape.measurement(event.component.type)},`;
      // tags
      measure += ["id", "name", "module", "type"]
        .map((key: string) => {
          return `${escape.tag(key)}=${escape.tag(event.component[key])}`;
        })
        .join(",");

      // fields
      measure +=
        " " +
        Object.keys(event.data)
          .filter((key: string) => event.data[key] !== undefined)
          .map((key: string) => {
            const xfix: string = typeof event.data[key] === "string" ? '"' : "";
            return `${escape.tag(key)}=${xfix}${escape.measurement(event.data[key])}${xfix}`;
          })
          .join(",");

      // time
      measure += " " + event.datetime.getTime();

      measures.push(measure);

      return;
    };

    async function start() {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
      // Listen all measurement events
      HAL.componentListenForEventType(component.id, EventType.ComponentMeasure, onComponentMeasure);

      // Register the process to send the data to influxdb (by batch)
      sendingMeasuresJob = setInterval(
        async () => {
          if (measures.length > 0) {
            HAL.componentLog(component.id, LogLevel.info, `Saving ${measures.length} measures`);
            try {
              await axios.post(
                `${component.config.url}/api/v2/write?org=${component.config.organization}&bucket=${component.config.bucketHal}&precision=ms`,
                measures.join("\n"),
                {
                  headers: { Authorization: `Token ${component.config.token}` },
                },
              );
              measures = [];
            } catch (e) {
              HAL.componentError(component.id, `Failed to inject point ${e.message}`);
            }
          }
        },
        component.config.frequency ? component.config.frequency * 1000 : 10000,
      );
    }

    async function stop() {
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
      // Clear the interval that send data to influx
      clearInterval(sendingMeasuresJob);
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
