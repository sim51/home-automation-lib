import { ComponentType, FieldType, ModuleInfo } from "hal";
import * as fs from "fs";
import path from "path";
import { getConstructor } from "./component";

export const influxdbStorage = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-component`,
    name: "InfluxDb Data Storage",
    module: moduleInfo.id,
    description: moduleInfo.description,
    icon: fs.readFileSync(path.resolve(__dirname, "../../icons/influxdb.svg"), "utf8"),
    categories: ["storage", "influxdb"],
    definition: [
      {
        name: "url",
        required: true,
        type: FieldType.text,
        placeholder: "http://127.0.0.1:9999",
        pattern: "^http://([0-9]{1,3}.){3}[0-9]{1,3}:([0-9]{1,4})$",
      },
      {
        name: "token",
        required: true,
        type: FieldType.text,
        placeholder: "token",
      },
      {
        name: "organization",
        required: true,
        type: FieldType.text,
        placeholder: "Organization",
      },
      {
        name: "bucketHal",
        required: true,
        type: FieldType.text,
        placeholder: "BucketID for HAL",
      },
      {
        name: "bucketMonitoring",
        required: true,
        type: FieldType.text,
        placeholder: "BucketID for monitoring",
      },
      {
        name: "frequency",
        help: "Frequency in second for data sending",
        type: FieldType.number,
      },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
