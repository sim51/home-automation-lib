import * as fs from "fs";
import path from "path";
import {
  HAL,
  LogLevel,
  Component,
  ComponentInfo,
  Module,
  moduleParsePackageJson,
  ModuleInfo,
  ssdpSearch,
  Xml2json,
  Request,
} from "hal";
import { wemoInsightPlug } from "./wemo-insight-plug/type";

// Construct the module.
// in params HAL + the package.json
export default async (): Promise<Module> => {
  // load module information
  const moduleInfo: ModuleInfo = await moduleParsePackageJson(path.resolve(__dirname, "../package.json"));

  // search components and load them
  const components = [await wemoInsightPlug(moduleInfo)];

  // Method for discovering components
  // ---------------------------------
  const discover = async (): Promise<Array<Component>> => {
    // search device with ssdp
    const wemos: Array<Record<string, unknown>> = (await ssdpSearch("urn:Belkin:device:insight", 5000)).filter(
      (n) => n.device.deviceType === "urn:Belkin:device:insight:1",
    );
    HAL.moduleLog(moduleInfo.id, LogLevel.info, `Response of the discovery`, wemos);

    const discovered = await Promise.all(
      wemos.map(async (wemo: Record<string, unknown>) => {
        // Make a request to the wemo location for retrieving the data
        const response = await Request(wemo.location as string);
        const result = await Xml2json(response, { explicitRoot: false, ignoreAttrs: true, explicitArray: false });

        // Building component
        const componentInfo: ComponentInfo = {
          id: result.device.UDN.replace("uuid:", ""),
          name: result.device.friendlyName,
          module: moduleInfo.id,
          type: components[0].id,
          config: {
            url: wemo.location,
          },
        };

        // Test if the component is already created
        if (HAL.components.findIndex((c) => c.id === componentInfo.id) >= 0) {
          return null;
        }

        // Create the component
        HAL.moduleLog(moduleInfo.id, LogLevel.info, `Discover component`, componentInfo);
        return HAL.componentCreate(componentInfo);
      }),
    );
    return discovered.filter((e) => e !== null);
  };

  return {
    ...moduleInfo,
    icon: fs.readFileSync(path.resolve(__dirname, "../icons/wemo.svg"), "utf8"),
    components: components,
    discover: discover,
  };
};
