import { ComponentType, FieldType, ModuleInfo } from "hal";
import { getConstructor } from "./component";

export const wemoInsightPlug = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-plug`,
    name: "Wemo Insight Plug",
    module: moduleInfo.id,
    description: "",
    icon: "bolt",
    categories: ["device", "wemo", "plug"],
    definition: [
      {
        name: "url",
        required: true,
        type: FieldType.text,
        placeholder: "http://10.0.1.24:49153/setup.xml",
        pattern: "^http://([0-9]{1,3}.){3}[0-9]{1,3}:[0-9]{1,5}/setup.xml$",
      },
    ],
    measurements: [
      {
        name: "on_for",
        help: "Number of second since the plug is on",
        unit: "second",
        min: 0,
      },
      {
        name: "consumption",
        help: "Live consumption in mW",
        unit: "mW",
        min: 0,
      },
    ],
    // actions
    actions: [
      { name: "ON", description: "Turns ON the plug" },
      { name: "OFF", description: "Turns OFF the plug" },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
