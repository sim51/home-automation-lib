import * as util from "util";
import { HAL, Component, ComponentInfo, ComponentType, EventComponentAction, LogLevel, UPnPClient } from "hal";
import { v4 as uuid } from "uuid";

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the ComponentInfo
    const wemo: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the Component (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    /**
     * Parse an upnp event, and send the measure to hal
     */
    function manageUpnpEvent(data: { InsightParams: string; BinaryState: string }): void {
      HAL.componentLog(wemo.id, LogLevel.debug, `Receiving upnp event for device ${wemo.id}`, data);
      let value: string[] = null;
      if (data.InsightParams) {
        value = data.InsightParams.split("|");
      }
      if (data.BinaryState) {
        value = data.BinaryState.split("|");
      }
      if (value) {
        const data: { [key: string]: number } = {};
        data["status"] = Number.parseInt(value[0]) === 0 ? 0 : 1; //0:OFF, 1:ON, 8:standby
        data["on_for"] = Number.parseInt(value[2]); // second
        data["consumption"] = Number.parseInt(value[0]) === 0 ? 0 : Number.parseInt(value[7]); //mW
        HAL.componentLog(wemo.id, LogLevel.debug, `Sending measure for device ${wemo.id}`, data);
        HAL.componentSendMeasure(wemo.id, data);
      }
    }

    // Create the upnp client
    const upnpClient = new UPnPClient(wemo.config.url);

    // Send a upnp event for wemo.
    async function callSetBinaryState(status: number) {
      const result = await upnpClient.call("urn:Belkin:serviceId:basicevent1", "SetBinaryState", {
        BinaryState: status,
      });
      return util.inspect(result, false, null);
    }

    async function turnOn(event: EventComponentAction): Promise<void> {
      HAL.componentLog(wemo.id, LogLevel.info, `Turning on device ${wemo.name}`);
      await callSetBinaryState(1);
      HAL.componentSendMeasure(wemo.id, { status: 1, on_for: 0, consumption: 0 });
      return;
    }

    async function turnOff(event: EventComponentAction): Promise<void> {
      HAL.componentLog(wemo.id, LogLevel.info, `Turning off device ${wemo.name}`);
      await callSetBinaryState(0);
      HAL.componentSendMeasure(wemo.id, { status: 0, on_for: 0, consumption: 0 });
      return;
    }

    /**
     * HAL Start component.
     */
    async function start() {
      HAL.componentLog(wemo.id, LogLevel.info, `Starting ${wemo.name}`);
      // Start the UPNP listener.
      try {
        await upnpClient.subscribe("urn:Belkin:serviceId:insight1", manageUpnpEvent);
        HAL.componentLog(wemo.id, LogLevel.info, `Subscribe to upnp event for device ${wemo.id}`);
      } catch (e) {
        HAL.componentLog(wemo.id, LogLevel.error, `Failed to subscribe to upnp events`, e);
      }

      HAL.componentListenAction(wemo.id, "ON", turnOn);
      HAL.componentListenAction(wemo.id, "OFF", turnOff);
    }

    /**
     * HAL Stop component.
     */
    async function stop() {
      HAL.componentLog(wemo.id, LogLevel.info, `Stopping ${wemo.name}`);
      upnpClient.close(true);
    }

    return new Promise((resolve) => {
      const component: Component = {
        ...wemo,
        start: start,
        stop: stop,
      };
      resolve(component);
    });
  };
};
