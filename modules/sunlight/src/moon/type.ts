import { ComponentType, FieldType, ModuleInfo } from "hal";
import { getConstructor } from "./component";

export const moon = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-moon`,
    name: "Moon position",
    module: moduleInfo.id,
    description: "Give the position of the moon",
    icon: "moon",
    categories: moduleInfo.categories,
    definition: [
      {
        name: "location",
        required: true,
        type: FieldType.location,
      },
      {
        name: "frequency",
        help: "Frequency in second for data retrieving",
        type: FieldType.number,
      },
    ],
    measurements: [
      {
        name: "altitude",
        unit: "Radian",
        help: "Moon altitude above the horizon in radians. 0 at the horizon and PI/2 at the zenith",
        min: -Math.PI / 2,
        max: Math.PI / 2,
      },
      {
        name: "azimuth",
        unit: "Radian",
        help:
          "Moon azimuth in radians (direction along the horizon, measured from south to west), e.g. 0 is south and Math.PI * 3/4 is northwest",
        min: -Math.PI,
        max: Math.PI,
      },
      {
        name: "fraction",
        unit: "%",
        help: "illuminated fraction of the moon; varies from 0.0 (new moon) to 1.0 (full moon)",
        min: 0,
        max: 1,
      },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
