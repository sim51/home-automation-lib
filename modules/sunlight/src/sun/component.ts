import { HAL, Component, ComponentInfo, ComponentType, LogLevel, EventType, EventComponent } from "hal";
import { v4 as uuid } from "uuid";
import * as SunCalc from "suncalc";

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the deviceInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the device (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    let timeout = null;
    function proceedMeasurements(): void {
      const sunPosition = SunCalc.getPosition(
        Date.now(),
        component.config.location.latitude,
        component.config.location.longitude,
      );
      HAL.componentSendMeasure(component.id, {
        altitude: sunPosition.altitude,
        azimuth: sunPosition.azimuth,
        status: 1,
      });
    }

    async function start() {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
      timeout = setInterval(
        proceedMeasurements,
        component.config.frequency ? component.config.frequency * 1000 : 10000,
      );
    }

    async function stop() {
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
      clearInterval(timeout);
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
