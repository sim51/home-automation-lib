import { ComponentType, FieldType, ModuleInfo } from "hal";
import { getConstructor } from "./component";

export const sun = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-sun`,
    name: "Sun position",
    module: moduleInfo.id,
    description: "Give the position of the sun",
    icon: "sun",
    categories: moduleInfo.categories,
    definition: [
      {
        name: "location",
        required: true,
        type: FieldType.location,
      },
      {
        name: "frequency",
        help: "Frequency in second for data retrieving",
        type: FieldType.number,
      },
    ],
    measurements: [
      {
        name: "altitude",
        help: "Sun altitude above the horizon in radians. 0 at the horizon and PI/2 at the zenith",
        unit: "Radian",
        min: -Math.PI / 2,
        max: Math.PI / 2,
      },
      {
        name: "azimuth",
        unit: "Radian",
        help:
          "Sun azimuth in radians (direction along the horizon, measured from south to west), e.g. 0 is south and Math.PI * 3/4 is northwest",
        min: -Math.PI,
        max: Math.PI,
      },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
