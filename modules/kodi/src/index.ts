import * as fs from "fs";
import path from "path";
import KodiClient from "kodi-ws";

import {
  Component,
  ComponentInfo,
  HAL,
  LogLevel,
  Module,
  moduleParsePackageJson,
  discoveringIpThatListenOnPort,
  ModuleInfo,
} from "hal";
import { kodi } from "./kodi/type";
import { v4 as uuid } from "uuid";

// Construct the module.
// in params HAL + the package.json
export default async (): Promise<Module> => {
  // load module information
  const moduleInfo: ModuleInfo = await moduleParsePackageJson(path.resolve(__dirname, "../package.json"));

  // search components and load them
  const components = [await kodi(moduleInfo)];

  // Method for discovering components
  // ---------------------------------
  const discover = async (): Promise<Array<Component>> => {
    const discoverdIpOnGoodPort = await discoveringIpThatListenOnPort(9090);
    const discoveredComponents = await Promise.all(
      discoverdIpOnGoodPort.map(async (ip) => {
        // Testing if found component has JSON-RPC on default port 9090
        try {
          await KodiClient(ip.ip, 9090);
        } catch (e) {
          return null;
        }

        // Test if the component is already created
        if (HAL.components.findIndex((c) => c.config.ip === ip.ip) >= 0) return null;

        // Building component
        const componentInfo: ComponentInfo = {
          id: uuid(),
          name: ip.hostname || "Kodi",
          module: moduleInfo.id,
          type: components[0].id,
          config: {
            ip: ip.ip,
            port: 9090,
          },
        };

        // Create the component
        HAL.moduleLog(moduleInfo.id, LogLevel.info, `Discover component`, componentInfo);
        return HAL.componentCreate(componentInfo);
      }),
    );
    return discoveredComponents.filter((e) => e !== null);
  };

  return {
    ...moduleInfo,
    icon: fs.readFileSync(path.resolve(__dirname, "../icons/logo.svg"), "utf8"),
    components: components,
    discover: discover,
  };
};
