import { ComponentType, FieldType, ModuleInfo } from "hal";
import { getConstructor } from "./component";

export const kodi = async (moduleInfo: ModuleInfo): Promise<ComponentType> => {
  const type: ComponentType = {
    id: `${moduleInfo.id}-kodi`,
    name: "Kodi",
    module: moduleInfo.id,
    description: "",
    icon: "photo-video",
    categories: moduleInfo.categories,
    definition: [
      {
        name: "ip",
        required: true,
        type: FieldType.text,
        placeholder: "10.0.1.24",
        pattern: "^([0-9]{1,3}.){3}[0-9]{1,3}$",
      },
      {
        name: "port",
        required: true,
        type: FieldType.number,
        placeholder: "9090",
      },
      {
        name: "frequency",
        help: "Frequency in second for data retrieving",
        type: FieldType.number,
      },
    ],
    // actions
    actions: [
      { name: "PLAY", description: "Play" },
      { name: "PAUSE", description: "Pause" },
      { name: "STOP", description: "Stop" },
      { name: "PREVIOUS", description: "Previous" },
      { name: "NEXT", description: "Next" },
    ],
    constructor: undefined,
  };
  type.constructor = getConstructor(type);
  return type;
};
