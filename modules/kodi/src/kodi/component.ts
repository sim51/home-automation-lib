import * as util from "util";
import { HAL, Component, ComponentInfo, ComponentType, LogLevel, UPnPClient, EventComponentAction } from "hal";
import KodiClient from "kodi-ws";
import { v4 as uuid } from "uuid";

type Measure = {
  playing_movie: number;
  playing_music: number;
  pause: number;
  status: number;
};

export const getConstructor = (type: ComponentType): ((info: ComponentInfo) => Promise<Component>) => {
  return (info: ComponentInfo): Promise<Component> => {
    // Construct the ComponentInfo
    const component: ComponentInfo = {
      id: info.id || uuid(),
      name: info.name || type.name,
      description: info.description || type.description,
      icon: info.icon || type.icon,
      categories: type.categories,
      // module
      module: type.module,
      // type of the Component (match the id of the type)
      type: type.id,
      //config
      config: info.config,
    };

    let kodi: KodiClient;

    // Storing the last kodi measure
    let lastKodiMeasure: Measure = {
      playing_movie: 0,
      playing_music: 0,
      pause: 0,
      status: 0,
    };

    let timeout = null;
    async function proceedMeasurements(): Promise<void> {
      let measure: Measure = {
        playing_movie: 0,
        playing_music: 0,
        pause: lastKodiMeasure?.pause || 0,
        status: 1,
      };
      try {
        const players = await kodi.Player.GetPlayers();
        const player = players.find((p) => p.playertype === "internal");
        measure = { ...measure, status: 1 };
        if (player) {
          console.log("time", player);
          measure = {
            pause: 0,
            playing_movie: player.playsaudio === "video" ? 1 : 0,
            playing_music: player.playsvideo === "audio" ? 1 : 0,
            status: 1,
          };
        }
      } catch (e) {
        // do nothing
      }
      lastKodiMeasure = measure;
      HAL.componentSendMeasure(component.id, measure);
    }

    /**
     * HAL Start component.
     */
    async function start() {
      HAL.componentLog(component.id, LogLevel.info, `Starting ${component.name}`);
      // Create the client
      kodi = await KodiClient(info.config.ip, info.config.port);

      // Register event for measurement
      kodi.Player.OnPlay((d) => {
        HAL.componentLog(component.id, LogLevel.debug, `Receiving on stop play`, d);
        lastKodiMeasure.pause = 0;
        HAL.componentSendMeasure(component.id, lastKodiMeasure);
      });

      kodi.Player.OnResume((d) => {
        HAL.componentLog(component.id, LogLevel.debug, `Receiving on resume event`, d);
        lastKodiMeasure = {
          pause: 0,
          playing_movie: d.data.item.type === "movie" || d.data.item.type === "episode" ? 1 : 0,
          playing_music: d.data.item.type === "song" ? 1 : 0,
          status: 1,
        };
        HAL.componentSendMeasure(component.id, lastKodiMeasure);
      });

      kodi.Player.OnPause((d) => {
        HAL.componentLog(component.id, LogLevel.debug, `Receiving on pause event`, d);
        lastKodiMeasure.pause = 1;
        HAL.componentSendMeasure(component.id, lastKodiMeasure);
      });

      kodi.Player.OnStop((d) => {
        HAL.componentLog(component.id, LogLevel.debug, `Receiving on stop event`, d);
        lastKodiMeasure = {
          playing_movie: 0,
          playing_music: 0,
          pause: 0,
          status: 1,
        };
        HAL.componentSendMeasure(component.id, lastKodiMeasure);
      });

      kodi.Player.OnAVStart((d) => {
        HAL.componentLog(component.id, LogLevel.debug, `Receiving on avstart event`, d);
        lastKodiMeasure = {
          playing_movie: d.item?.type === "movie" || d.item?.type === "episode" ? 1 : 0,
          playing_music: d.item?.type === "song" ? 1 : 0,
          pause: 0,
          status: 1,
        };
        HAL.componentSendMeasure(component.id, {});
      });

      kodi.Player.OnAVChange((d) => {
        HAL.componentLog(component.id, LogLevel.debug, `Receiving on avschange event`, d);
        lastKodiMeasure = {
          playing_movie: d.item?.type === "movie" || d.item?.type === "episode" ? 1 : 0,
          playing_music: d.item?.type === "song" ? 1 : 0,
          pause: 0,
          status: 1,
        };
        HAL.componentSendMeasure(component.id, {});
      });

      // Register time based measurement
      timeout = setInterval(
        proceedMeasurements,
        component.config.frequency ? component.config.frequency * 1000 : 10000,
      );

      // Register actions
      HAL.componentListenAction(
        component.id,
        "PLAY",
        async (event: EventComponentAction): Promise<void> => {
          await kodi.Player.PlayPause({ playerid: 1, play: true });
        },
      );
      HAL.componentListenAction(
        component.id,
        "PAUSE",
        async (event: EventComponentAction): Promise<void> => {
          await kodi.Player.PlayPause({ playerid: 1, play: false });
        },
      );
      HAL.componentListenAction(
        component.id,
        "STOP",
        async (event: EventComponentAction): Promise<void> => {
          await kodi.Player.Stop({ playerid: 1 });
        },
      );
      HAL.componentListenAction(
        component.id,
        "PREVIOUS",
        async (event: EventComponentAction): Promise<void> => {
          await kodi.Player.GoTo({ playerid: 1, to: "previous" });
        },
      );
      HAL.componentListenAction(
        component.id,
        "NEXT",
        async (event: EventComponentAction): Promise<void> => {
          await kodi.Player.GoTo({ playerid: 1, to: "next" });
        },
      );
    }

    /**
     * HAL Stop component.
     */
    async function stop() {
      kodi = undefined;
      HAL.componentLog(component.id, LogLevel.info, `Stopping ${component.name}`);
    }

    return new Promise((resolve) => {
      resolve({
        ...component,
        start: start,
        stop: stop,
      });
    });
  };
};
