module.exports = function(RED) {
  "use strict";
  var HAL = require("hal").HAL;

  function component(config) {
    // Create node
    RED.nodes.createNode(this, config);

    // Init node properties
    this.name = config.name;
    this.component = config.component;
    this.action = config.action;

    // Send event
    HAL.componentListenAction(this.component, this.action, event => {
      this.send(event);
    });
  }
  RED.nodes.registerType("hal-component-action-out", component);
};
