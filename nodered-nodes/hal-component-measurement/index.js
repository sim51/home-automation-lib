module.exports = function (RED) {
  "use strict";
  var HAL = require("hal").HAL;

  function component(config) {
    // Create node
    RED.nodes.createNode(this, config);

    // Init node properties
    this.name = config.name;
    this.component = config.component;

    // Send event
    HAL.events.on("ComponentMeasure", (event) => {
      if (event.component.id === this.component) {
        this.send(event);
      }
    });
  }

  // Register the node
  RED.nodes.registerType("hal-component-measurement", component);
};
