module.exports = function(RED) {
  "use strict";
  var HAL = require("hal").HAL;

  function component(config) {
    // Create node
    RED.nodes.createNode(this, config);

    // Init node properties
    this.name = config.name;
    this.component = config.component;
    this.action = config.action;
    this.params = config.params || "{}";

    // On  input
    this.on("input", (msg, send, done) => {
      const component = this.component;
      const action = msg.action || this.action;
      const params = msg.params || this.params;

      if (component && component !== "" && action && action !== "") {
        HAL.componentSendAction(component, action, JSON.parse(params));
      } else {
        console.log("bad action");
        done();
      }
    });
  }
  RED.nodes.registerType("hal-component-action-in", component);
};
