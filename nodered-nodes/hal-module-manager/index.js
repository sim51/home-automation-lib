module.exports = function (RED) {
  "use strict";
  var HAL = require("hal").HAL;

  function component(config) {
    // Create node
    RED.nodes.createNode(this, config);

    // Init node properties
    this.name = config.name;
    this.module = config.module;
    this.action = config.action;

    // On  input
    this.on("input", (msg, send, done) => {
      // Check the input action
      if (
        msg.action &&
        msg.action !== "" &&
        msg.action !== "stop" &&
        msg.action !== "start"
      ) {
        done(new Error(`Action ${msg.action} is not a valid value`));
      }

      const action = msg.action ? msg.action : this.action;
      if (action !== "") {
        (action === "start"
          ? HAL.moduleStart(this.module)
          : HAL.moduleStop(this.module)
        )
          .then(() => done())
          .catch((e) => done(e));
      } else {
        done();
      }
    });
  }
  RED.nodes.registerType("hal-module-manager", component);
};
