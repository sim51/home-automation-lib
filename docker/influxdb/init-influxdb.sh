#!/bin/bash
set -e

if [ ! "$(ls -d /var/lib/influxdb/engine 2>/dev/null)" ]; then
  echo "Influxdb setup"
  influxd --reporting-disabled "$@" &
  PID="$!"

  # Waiting until influxdb is not available
  echo "$PID Waiting for server to be ready."
  until $(curl --output /dev/null --silent --head --fail http://localhost:8086); do
      echo '.'
      sleep 1
  done

  echo "Setup"
  influx setup \
    --force \
    --bucket ${INFLUXDB_BUCKET} \
    --org ${INFLUXDB_ORG} \
    --username ${INFLUXDB_ADMIN_USER} \
    --password ${INFLUXDB_ADMIN_PASSWORD} \
    --token ${INFLUXDB_TOKEN}

  echo "Running script in docker-entrypoint-initdb.d"
  for f in /docker-entrypoint-initdb.d/*; do
  		case "$f" in
  			*.sh)     echo "$0: running $f"; . "$f" ;;
  			*)        echo "$0: ignoring $f" ;;
  		esac
  		echo
  	done

  if ! kill -s TERM "$PID" || ! wait "$PID"; then
  	echo >&2 'influxdb init process failed. (Could not stop influxdb)'
  	exit 1
  fi
fi
exit 0
