#!/bin/bash
influx bucket create \
  --name ${INFLUXDB_BUCKET_MONITORING} \
  --org ${INFLUXDB_ORG} \
  --retention ${INFLUXDB_BUCKET_MONITORING_RETENTION}
