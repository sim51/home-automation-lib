#!/bin/bash
set -e

if [ "${1:0:1}" = '-' ]; then
    echo "Staring influxdb"
    set -- influxd "$@"
fi

if [ "$1" = 'influxd' ]; then
  echo "Running init-influxdb.sh"
	/init-influxdb.sh "${@:2}"
fi

echo "Running $@"
exec "$@"
