#!/bin/bash
cd /hal/web

echo "(i) Npm version is $(npm -v)"
echo "(i) Node version is $(node -v)"
echo "(i) Host IP is $DOCKER_HOST_IP"

if [ "$MODE" = "dev" ]; then
  echo "/!\\ Mode is set to DEV /!\\"
else
  echo "/!\\ Mode is set to $MODE /!\\"
fi

echo
echo " ~"
echo " ~ Starting telegraf"
echo " ~"
rm -f /var/run/telegraf/telegraf.pid
/usr/bin/telegraf \
  -pidfile /var/run/telegraf/telegraf.pid \
  -config /etc/telegraf/telegraf.conf \
  -config-directory /etc/telegraf/telegraf.d >/var/log/telegraf/telegraf.log &


echo
echo " ~"
echo " ~ Install dependencies"
echo " ~"
echo
npm install

if [ "$MODE" = "dev" ]; then
  echo
  echo " ~"
  echo " ~ Start the web server"
  echo " ~"
  echo
  export DOLLAR='$'
  envsubst < /etc/nginx/sites-available/nginx.dev.conf > /etc/nginx/sites-available/default
  nginx -c /etc/nginx/nginx.conf

  echo
  echo " ~"
  echo " ~ Start react application"
  echo " ~"
  echo
  npm run start
else
  echo
  echo " ~"
  echo " ~ Building the application"
  echo " ~"
  echo
  npm run build

  echo
  echo " ~"
  echo " ~ Run the production server"
  echo " ~"
  echo
  export DOLLAR='$'
  envsubst < /etc/nginx/sites-available/nginx.prod.conf > /etc/nginx/sites-available/default
  nginx -g 'daemon off;'
fi
