version: "2"
services:
  # Neo4j service
  neo4j:
    image: neo4j:${NEO4J_VERSION}
    restart: ${RESTART_POLICY}
    ulimits:
      nofile:
        soft: ${MAX_OPEN_FILES}
        hard: ${MAX_OPEN_FILES}
    depends_on:
      - influxdb
    volumes:
      - ${PROJECT_PATH}/docker/neo4j/conf:/conf
      - ${PROJECT_PATH}/docker/neo4j/data:/data
      - ${PROJECT_PATH}/docker/neo4j/plugins:/plugins
      - ${PROJECT_PATH}/docker/neo4j/entrypoint.sh:/entrypoint.sh
      - ${PROJECT_PATH}/docker/neo4j/telegraf.conf:/etc/telegraf/telegraf.conf
    environment:
      - "NEO4J_ACCEPT_LICENSE_AGREEMENT=yes"
      - "NEO4J_AUTH=${NEO4J_ADMIN_USER}/${NEO4J_ADMIN_PASSWORD}"
      - "NEO4JLABS_PLUGINS=${NEO4J_PLUGINS}"
      - "EXTENSION_SCRIPT=/entrypoint.sh"
    env_file:
      - ${PROJECT_PATH}/docker/.env

  # InfluxDb service
  influxdb:
    build:
      context: ./influxdb
      dockerfile: ./Dockerfile
      args:
        - INFLUXDB_VERSION=${INFLUXDB_VERSION}
    restart: ${RESTART_POLICY}
    ulimits:
      nofile:
        soft: ${MAX_OPEN_FILES}
        hard: ${MAX_OPEN_FILES}
    volumes:
      - ${PROJECT_PATH}/docker/influxdb/data:/var/lib/influxdb
      - ${PROJECT_PATH}/docker/influxdb/hal-init.sh:/docker-entrypoint-initdb.d/hal-init.sh
    environment:
      - "INFLUXDB_HTTP_AUTH_ENABLED=true"
      - "INFLUXDB_DB=${INFLUXDB_DBNAME}"
      - "INFLUXDB_ADMIN_USER=${INFLUXDB_ADMIN_USER}"
      - "INFLUXDB_ADMIN_PASSWORD=${INFLUXDB_ADMIN_PASSWORD}"
    env_file:
      - ${PROJECT_PATH}/docker/.env

  # Chronograf service
  chronograf:
    image: chronograf:${CHRONOGRAF_VERSION}
    restart: ${RESTART_POLICY}
    depends_on:
      - influxdb
    volumes:
      - ${PROJECT_PATH}/docker/chronograf/data:/var/lib/chronograf
    environment:
      - "INFLUXDB_URL=http://influxdb:8086"
      - "INFLUXDB_USERNAME=${INFLUXDB_ADMIN_USER}"
      - "INFLUXDB_PASSWORD=${INFLUXDB_ADMIN_PASSWORD}"
      - "KAPACITOR_URL=http://kapacitor:9092"
    env_file:
      - ${PROJECT_PATH}/docker/.env

  # Backend server
  server:
    build:
      context: ./server
      dockerfile: ./Dockerfile
    restart: ${RESTART_POLICY}
    ulimits:
      nofile:
        soft: ${MAX_OPEN_FILES}
        hard: ${MAX_OPEN_FILES}
    depends_on:
      - neo4j
      - influxdb
    network_mode: host
    ports:
      - ${SERVER_HOST_PORT}:4000
    volumes:
      - /hal/docker
      - ${PROJECT_PATH}:/hal/
      - ${PROJECT_PATH}/docker/server/telegraf.conf:/etc/telegraf/telegraf.conf
    env_file:
      - ${PROJECT_PATH}/docker/.env

  # Frontend
  web:
    build:
      context: ./web
      dockerfile: ./Dockerfile
    restart: ${RESTART_POLICY}
    depends_on:
      - server
      - influxdb
      - chronograf
    ports:
      - ${HTTP_HOST_PORT}:80
    volumes:
      - /hal/docker
      - ${PROJECT_PATH}:/hal/
      - ${PROJECT_PATH}/docker/web/telegraf.conf:/etc/telegraf/telegraf.conf
      - ${PROJECT_PATH}/docker/web/nginx.dev.conf:/etc/nginx/sites-available/nginx.dev.conf
      - ${PROJECT_PATH}/docker/web/nginx.prod.conf:/etc/nginx/sites-available/nginx.prod.conf
    env_file:
      - ${PROJECT_PATH}/docker/.env
