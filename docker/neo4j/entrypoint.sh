#!/bin/bash
#
# Installing telegraf
#
if ! command -v telegraf &> /dev/null
then
  echo "Installing telegraf"
  apt-get update
  apt-get -y install gnupg procps
  wget -q https://repos.influxdata.com/influxdb.key -O- | apt-key add -
  echo "deb https://repos.influxdata.com/debian buster stable" | tee -a /etc/apt/sources.list
  apt-get update
  apt-get -y install telegraf
fi

rm -f /var/run/telegraf/telegraf.pid
/usr/bin/telegraf \
  -pidfile /var/run/telegraf/telegraf.pid \
  -config /etc/telegraf/telegraf.conf \
  -config-directory /etc/telegraf/telegraf.d > /var/log/telegraf/telegraf.log &
