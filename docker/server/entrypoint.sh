#!/bin/bash
function buildJS {
  echo
  echo "  ~ Building $1"
  cd $1
  echo
  echo "  - Install dependencies"
  echo
  npm install
  echo "  - Building the application"
  npm run build
}

if [ "$MODE" = "dev" ]; then
  echo "/!\\ Mode is set to DEV /!\\"
else
  echo "/!\\ Mode is set to $MODE /!\\"
fi
echo "(i) Npm version is $(npm -v)"
echo "(i) Node version is $(node -v)"

echo
echo " ~"
echo " ~ Starting telegraf"
echo " ~"
rm -f /var/run/telegraf/telegraf.pid
/usr/bin/telegraf \
  -pidfile /var/run/telegraf/telegraf.pid \
  -config /etc/telegraf/telegraf.conf \
  -config-directory /etc/telegraf/telegraf.d > /var/log/telegraf/telegraf.log &

echo
echo " ~"
echo " ~ Hal runtime"
echo " ~"
buildJS /hal/runtime

echo
echo " ~"
echo " ~ Hal modules"
echo " ~"
cd /hal/modules
for module in */; do
  buildJS /hal/modules/$module
done


cd /hal/server
echo
echo " ~"
echo " ~ Hal Server"
echo " ~"
echo
buildJS /hal/server

export configuration="docker"

if [ "$MODE" = "dev" ]; then
  echo
  echo " ~"
  echo " ~ Starting"
  echo " ~"
  echo
  npm run start

else
  echo
  echo " ~"
  echo " ~ Run the server"
  echo " ~"
  echo
  node ./dist/index.js
fi
