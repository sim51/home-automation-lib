import * as assert from "assert";
import { deepMerge, isObject } from "../../src/utils";

describe("Testing `isObject`", () => {
  it("Should return false on a simple types", async () => {
    assert.equal(isObject("Hello World"), false);
    assert.equal(isObject(2), false);
    assert.equal(isObject(false), false);
  });

  it("Should return false on arrays", async () => {
    // on empty array
    assert.equal(isObject([]), false);
    // simple array
    assert.equal(isObject([1, 2, 3]), false);
    // array of object
    assert.equal(isObject([{ test: 1 }, { test: 2 }, { test: 3 }]), false);
  });

  it("Should return true on an empty of object", async () => {
    assert.equal(isObject({}), true);
  });

  it("Should return true on a json", async () => {
    assert.equal(isObject({ test: 1 }), true);
  });

  it("Should return true on a date", async () => {
    assert.equal(isObject(new Date()), true);
  });
});

describe("Testing `deepMerge`", () => {
  it("Should work on disjoincted objects", async () => {
    assert.deepEqual(deepMerge({ a: "a" }, { b: "b" }), { a: "a", b: "b" });
  });
  it("Should work with override", async () => {
    assert.deepEqual(deepMerge({ a: "a1" }, { a: "a2" }), { a: "a2" });
  });
  it("Should work deeply ", async () => {
    assert.deepEqual(
      deepMerge(
        {
          root: {
            test: "bli",
            lvl1: {
              att0: 0,
              att2: [1, 2, 3, 4, 5],
            },
          },
        },
        {
          root: {
            test: "bla",
            lvl1: {
              att1: "a",
              att2: [],
            },
          },
        },
      ),
      {
        root: {
          test: "bla",
          lvl1: {
            att0: 0,
            att1: "a",
            att2: [],
          },
        },
      },
    );
  });
});
