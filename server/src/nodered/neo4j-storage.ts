/*eslint-disable*/
import when from "when";
import neo4j from "neo4j-driver";

// The neo4j driver
let driver;

const convertToNeo4j = (item: any, depth = 0): any => {
  let result: any = item;

  if (depth > 0) {
    // if depth and object, then json stringify
    if (item instanceof Object) {
      result = JSON.stringify(item);
    }
    // if depth = 1 and array, recurisve
    if (depth == 1 && Array.isArray(item)) {
      result = item.map((data) => {
        return convertToNeo4j(data, depth + 1);
      });
    }
  } else {
    if (item instanceof Object) {
      result = Object.assign({}, item);
      Object.keys(item).forEach((key: string) => {
        result[key] = convertToNeo4j(item[key], depth + 1);
      });
    }
    if (Array.isArray(item)) {
      result = convertToNeo4j(item, depth + 1);
    }
  }
  return result;
};

const parseFromNeo4j = (item: any): any => {
  let result: any = item;
  if (typeof item === "string" || item instanceof String) {
    try {
      result = JSON.parse(item.toString());
    } catch (e) {
      //silent exception
    }
  }
  if (Array.isArray(item)) {
    result = item.map((data) => {
      return parseFromNeo4j(data);
    });
  } else {
    if (item instanceof Object) {
      result = Object.assign({}, item);
      Object.keys(item).forEach((key: string) => {
        result[key] = parseFromNeo4j(item[key]);
      });
    }
  }
  return result;
};

const neo4jStorage = {
  init: function (settings) {
    return when.promise(function (resolve, reject) {
      try {
        driver = neo4j.driver(settings.neo4j.url, neo4j.auth.basic(settings.neo4j.login, settings.neo4j.password), {
          disableLosslessIntegers: true,
        });
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  },

  getFlows: function () {
    return when.promise(async function (resolve, reject) {
      const flows = [];
      const session = driver.session();
      try {
        const result = await session.run(
          `
          MATCH (n:FlowItem)
          OPTIONAL MATCH (n)-[r:LINKED_TO]->(m:FlowItem)
          WITH n, r.output AS output, collect(m.id) AS wiresPerOutput ORDER BY id(n), output
          WITH n, collect(wiresPerOutput) AS wires
          RETURN n {
          	.*,
              wires:wires
          }`,
          {},
        );
        result.records.forEach((record) => {
          flows.push(parseFromNeo4j(record.get("n")));
        });
        resolve(flows);
      } catch (e) {
        reject(e);
      } finally {
        session.close();
      }
    });
  },

  saveFlows: function (flows) {
    return when.promise(async function (resolve, reject) {
      const session = driver.session();
      const tx = session.beginTransaction();
      try {
        // Reset
        await tx.run(`MATCH (f:FlowItem) DETACH DELETE f`, {});
        // create flows items
        await tx.run(
          `
            UNWIND $flows AS flow
            MERGE (f:FlowItem {id:flow.id})
            SET f+= apoc.map.clean(flow, ['wires'], [])`,
          {
            flows: flows.map((flow) => {
              return convertToNeo4j(flow);
            }),
          },
        );
        // Wires
        await tx.run(
          `
            UNWIND $flows AS flow
            WITH flow WHERE size(flow.wires) > 0
              UNWIND range(0, size(flow.wires)-1, 1) AS numOuput
                UNWIND flow.wires[numOuput] AS wire
                  MATCH (f1:FlowItem {id:flow.id})
                  MATCH (f2:FlowItem {id:wire})
                  MERGE (f1)-[:LINKED_TO {output:numOuput}]->(f2)
              `,
          {
            flows: flows,
          },
        );
        // Link flow to node red node
        await tx.run(
          `
            MATCH (f:FlowItem) WHERE NOT exists(f.z) WITH f
            MERGE (n:NodeRed)
            MERGE (n)-[:HAS_FLOW]->(f)`,
          {
            flows: flows,
          },
        );
        // Link flowItem to parent
        await tx.run(
          `
            MATCH (f1:FlowItem), (f2:FlowItem)
            WHERE f1.id = f2.z
            WITH f1, f2
            MERGE (f1)-[:HAS_CHILD]->(f2)
            `,
          {
            flows: flows,
          },
        );
        // Link flowItem to Subflow
        await tx.run(
          `
              MATCH (f1:FlowItem) WHERE f1.type STARTS WITH 'subflow:'
              WITH f1, replace(f1.type, 'subflow:', '') AS id
                MATCH (f2:FlowItem {id:id})
                MERGE (f1)-[:IS_SUBFLOW]->(f2)
              `,
          {},
        );
        await tx.commit();
        resolve();
      } catch (e) {
        await tx.rollback();
        reject(e);
      } finally {
        await session.close();
      }
    });
  },

  getCredentials: function () {
    return when.promise(async function (resolve, reject) {
      const session = driver.session();
      try {
        let credential = {};
        const result = await session.run(
          `MATCH (nr:NodeRed) WHERE exists(nr.credentials) RETURN nr.credentials AS credentials`,
          {},
        );
        result.records.forEach((record) => {
          credential = JSON.parse(record.get("credentials"));
        });
        resolve(credential);
      } catch (e) {
        reject(e);
      } finally {
        await session.close();
      }
    });
  },

  saveCredentials: function (credentials) {
    return when.promise(async function (resolve, reject) {
      const session = driver.session();
      try {
        await session.run(`MERGE (nr:NodeRed) SET nr.credentials=$credentials`, {
          credentials: JSON.stringify(credentials),
        });
        resolve();
      } catch (e) {
        reject(e);
      } finally {
        await session.close();
      }
    });
  },

  getSettings: function () {
    return when.promise(async function (resolve, reject) {
      const session = driver.session();
      try {
        const settings: { [key: string]: any } = {};
        const result = await session.run(
          `
          MATCH (nr:NodeRed)
          RETURN
            nr.credentialSecret AS _credentialSecret ,
            [(nr)-[:HAS_USER]->(u:User) | u {.*}] AS users,
            [
                (nr)-[:HAS_MODULE]->(m:NodeModule) |
                  m {
                    .*,
                    nodes: [(m)-[:HAS_NODE]->(n) | n { .*}]
                  }
            ] AS nodes
          `,
          {},
        );
        result.records.forEach((record) => {
          // credential
          settings._credentialSecret = record.get("_credentialSecret");

          // users
          settings.users = {};
          record.get("users").forEach((user) => {
            settings.users[user.id] = parseFromNeo4j(user);
          });

          // modules / nodes
          settings.nodes = {};
          record.get("nodes").forEach((module) => {
            settings.nodes[module.name] = parseFromNeo4j(module);
          });
        });
        resolve(settings);
      } catch (e) {
        reject(e);
      } finally {
        await session.close();
      }
    });
  },

  saveSettings: function (newSettings) {
    return when.promise(async function (resolve, reject) {
      const session = driver.session();
      const tx = session.beginTransaction();
      try {
        // Merge NodeRed with `_credentialSecret`
        if (newSettings._credentialSecret) {
          await tx.run(`MERGE (n:NodeRed) SET n.credentialSecret = $credentialSecret`, {
            credentialSecret: newSettings._credentialSecret,
          });
        }

        // Merge users (how to delete old one ?)
        if (newSettings.users) {
          const users = Object.keys(newSettings.users).map((key) => {
            return convertToNeo4j(newSettings[key]);
          });
          await tx.run(
            `
              MERGE (n:NodeRed) WITH n
              UNWIND $users AS user
                MERGE (u:User {id:user.id})
                SET u+=user
                MERGE (n)-[:HAS_USER]->(u)`,
            { users: users },
          );
          // Delete `old` user
          await tx.run(
            `
                 MATCH (n:NodeRed)-[:HAS_USER]->(u:User)
                 WHERE NOT u.id in $usersId
                 WITH n DETACH DELETE n;`,
            {
              usersId: users.map((user) => {
                return user.id;
              }),
            },
          );
        }

        // Register modules and nodes
        // How to delete old nodes/modules ??
        if (newSettings.nodes) {
          const modules = Object.keys(newSettings.nodes).map((key) => {
            const result = newSettings.nodes[key];
            result.nodes = Object.keys(newSettings.nodes[key].nodes).map((node) => {
              return newSettings.nodes[key].nodes[node];
            });
            return result;
          });
          await tx.run(
            `
              MERGE (nr:NodeRed) WITH nr
              UNWIND $modules AS module
                MERGE (nm:NodeModule { name: module.name})
                SET nm = apoc.map.clean(module, ['nodes'], [])
                MERGE (nr)-[:HAS_MODULE]->(nm)
                WITH nm, module.nodes AS nodes
                UNWIND nodes AS node
                  MERGE (n:Node {name:node.name})
                  SET n += node
                  MERGE (nm)-[:HAS_NODE]->(n)
            `,
            { modules: modules },
          );
        }

        await tx.commit();
        resolve();
      } catch (e) {
        await tx.rollback();
        reject(e);
      } finally {
        await session.close();
      }
    });
  },

  /* eslint-disable @typescript-eslint/no-unused-vars */
  getSessions: () => {
    return when.promise(async (resolve, reject) => {
      resolve({});
    });
  },

  /* eslint-disable @typescript-eslint/no-unused-vars */
  saveSessions: (sessions) => {
    return when.promise(async (resolve, reject) => {
      resolve();
    });
  },

  /* eslint-disable @typescript-eslint/no-unused-vars */
  getLibraryEntry: (type, path) => {
    return when.promise(async (resolve, reject) => {
      resolve({});
    });
  },

  /* eslint-disable @typescript-eslint/no-unused-vars */
  saveLibraryEntry: (type, path, meta, body) => {
    return when.promise(async (resolve, reject) => {
      resolve();
    });
  },
};

export default neo4jStorage;
