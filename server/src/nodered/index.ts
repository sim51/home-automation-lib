import RED from "node-red";
import storage from "./neo4j-storage";
import { config } from "../config";
import { Express } from "express";
import { Server } from "http";
import { getLoggerForLabels } from "../logger";
import { adminAuth } from "./admin-auth";

// logger
const log = getLoggerForLabels(["NODERED"]);

export function register(server: Server, app: Express): RED {
  // Create node red configuration with a custom logger
  const nodeRedConfig = config.nodered;
  nodeRedConfig.logging.customHandler = {
    level: "debug",
    metrics: false,
    audit: false,
    handler: function () {
      // Return the logging function
      return function (msg) {
        // For the level mapping see https://github.com/node-red/node-red/blob/master/packages/node_modules/%40node-red/util/lib/log.js
        switch (msg.level) {
          case 10:
          case 20:
            log.error(msg.msg);
            break;
          case 30:
            log.warn(msg.msg);
            break;
          case 40:
            log.info(msg.msg);
          case 50:
            break;
          case 60:
            log.debug(msg.msg);
            break;
          case 98:
            log.debug(`[Audit]: ${JSON.stringify(msg)}`);
            break;
          case 99:
            log.debug(`[Metric]: ${JSON.stringify(msg)}`);
        }
      };
    },
  };
  RED.init(server, {
    ...nodeRedConfig,
    adminAuth: adminAuth,
    neo4j: config.neo4j,
    storageModule: storage,
  });
  app.use(config.nodered.httpAdminRoot, RED.httpAdmin);
  app.use(config.nodered.httpNodeRoot, RED.httpNode);
  return RED;
}
