import neo4j from "neo4j-driver";
import { config } from "../config";
import { checkToken, getUserToken, login } from "../graphql/neo4j/auth";

// The neo4j driver
const driver = neo4j.driver(config.neo4j.url, neo4j.auth.basic(config.neo4j.login, config.neo4j.password), {
  disableLosslessIntegers: true,
});

interface User {
  username: string;
  permissions: string;
}

const searchUser = async (user: string): Promise<User | null> => {
  const result = await getUserToken(user, driver);
  if (!result) return null;
  return { username: result.user.username, permissions: "*" };
};

const authenticate = async (username: string, password: string): Promise<User | null> => {
  const result = await login(username, password, driver);
  if (!result) return null;
  return { username: result.user.username, permissions: "*" };
};

const tokens = async (token: string): Promise<User | null> => {
  const user = await checkToken(token, driver);
  if (!user) return null;
  return { username: user.username, permissions: "*" };
};

const defaultUser = async (): Promise<User | null> => {
  return null;
};

export const adminAuth = {
  type: "credentials",
  users: searchUser,
  authenticate: authenticate,
  tokens: tokens,
  default: defaultUser,
};
