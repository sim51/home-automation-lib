import express, { Express } from "express";
import http, { Server } from "http";
import { HAL } from "hal";
import { config, getHalConfigurationFromDb } from "./config";
import { deepMerge, listenStopProcess } from "./utils";
import { errorFilter } from "./error";
import { register as initGraphql } from "./graphql";
import { register as initRed } from "./nodered";
import { getLoggerForLabels, Logger } from "./logger";

process.env.JWT_SECRET = config.jwt_secret_key;

// logger
const log = getLoggerForLabels(["Server"]);

/**
 * Launching server function.
 */
const exec = async (): Promise<void> => {
  // Create expressjs app
  const app: Express = express();
  // Register error filter
  app.use(errorFilter);

  // Create a server
  const server: Server = http.createServer(app);

  // Compute HAL config
  const halDbConfig = await getHalConfigurationFromDb(config);
  const halConfig = deepMerge(
    Object.assign({}, config.hal, { logger: Logger }),
    halDbConfig.length > 0 ? { components: halDbConfig } : {},
  );

  // Create GraphQL
  const createSubcriptionServer = await initGraphql(server, app, HAL);

  // Create node red
  const red = await initRed(server, app);

  // Start the server
  server.listen({ port: config.port });

  // Staring node red
  log.info(`Stating node red`);
  red.start();

  // Starting graph subscription server
  log.info(`Stating graphql subscription server`);
  createSubcriptionServer();

  // Starting HAL
  await HAL.init(halConfig);
  await HAL.start();
  listenStopProcess(async () => {
    log.info("Stopping the process");
    try {
      await HAL.stop();
    } catch (e) {
      log.error("Failed to stop HAL", e);
    } finally {
      process.exit();
    }
  });

  log.info(`Server ready at http://localhost:${config.port}`);
};

exec();
