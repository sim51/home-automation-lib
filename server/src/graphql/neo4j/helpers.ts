import * as neo4j from "neo4j-driver";

/**
 * Run a cypher query et return the first record
 *
 * @param {driver} driver Neo4j driver
 * @param {string} query Cypher query to execute
 * @param {object} params Query's parameters
 * @param {string} field of field to get
 * @return {array} The query's result with the given projection
 */
export async function neo4jGetFirstResult<T>(
  driver: neo4j.Driver,
  query: string,
  params: Record<string, unknown>,
  field: string,
): Promise<T | null> {
  const session = driver.session();
  try {
    const result = await session.run(query, params);
    const record = result.records.shift();
    if (!record) {
      return null;
    }
    return record.get(field) as T;
  } finally {
    session.close();
  }
}

/**
 * Run a cypher query
 */
export async function neo4jRunQuery<T>(
  driver: neo4j.Driver,
  query: string,
  params: Record<string, unknown>,
  field = "result",
): Promise<T | null> {
  const session = driver.session();
  try {
    const rs = await session.run(query, params);
    return (rs.records.map((n) => n.get(field)) as unknown) as T;
  } finally {
    session.close();
  }
}

/**
 * Run a query till the result is true, or we reach the number of reties
 */
export async function neo4jRunQueryUntilIsTrue(
  driver: neo4j.Driver,
  query: string,
  params: Record<string, unknown>,
  field = "result",
  retry = 5,
  time = 200,
): Promise<boolean> {
  const session = driver.session();
  return new Promise((resolve, reject) => {
    let nbRetry = 0;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    let timer = setTimeout(function checkNeo4j() {
      nbRetry++;
      neo4jGetFirstResult<boolean>(driver, query, params, field)
        .then((rs) => {
          if (rs !== true && nbRetry <= retry) {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            timer = setTimeout(checkNeo4j, time);
          } else {
            session.close();
            resolve(true);
          }
        })
        .catch((e) => {
          reject(e);
          session.close();
        });
    }, time);
  });
}
