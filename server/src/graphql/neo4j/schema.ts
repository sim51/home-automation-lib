/* eslint-disable @typescript-eslint/no-unused-vars*/
/* eslint-disable @typescript-eslint/no-explicit-any*/
import GraphQLJSON, { GraphQLJSONObject } from "graphql-type-json";
import { GraphQLResolveInfo, GraphQLObjectType } from "graphql";
import gql from "graphql-tag";
import {
  HAL,
  Component,
  ComponentInfo,
  Event,
  EventType,
  EventModule,
  EventComponent,
  MeasurementDefinition,
} from "hal";
import { neo4jRunQueryUntilIsTrue, neo4jRunQuery, neo4jGetFirstResult } from "./helpers";
import { config } from "../../config";
import { ResolverContext } from "../";
import { login, getUserToken } from "./auth";

export const typeDefs = gql`
  #
  # Define custom Graphql types
  #
  scalar JSON
  scalar JSONObject

  enum Role {
    user
    admin
  }

  #
  # Define GraphQl / Neo4j model
  #
  interface Model {
    id: ID! @id
    icon: String!
    name: String!
    description: String
  }

  type User {
    username: ID! @id
    fullname: String!
    avatar: String
    roles: [String]
    isPresent: Boolean
      @cypher(
        statement: "MATCH (this) WHERE exists((this)-[:HAS_PRESENCE_COMPONENT]->(:Component)) RETURN size((this)-[:HAS_PRESENCE_COMPONENT]->(:Component)-[:LAST_MEASURE]->(:Measure {status:1})) > 0"
      )
  }

  type Place implements Model {
    id: ID! @id
    icon: String!
    name: String!
    description: String
    address: String
    postal_code: String
    city: String
    country: String
    location: Point
    rooms: [Room]! @relation(name: "HAS_ROOM", direction: OUT)
    components: [Component] @relation(name: "HAS_COMPONENT", direction: OUT)
  }

  type Room implements Model {
    id: ID! @id
    icon: String!
    name: String!
    description: String
    place: Place! @relation(name: "HAS_ROOM", direction: IN)
    components: [Component] @relation(name: "HAS_COMPONENT", direction: OUT)
  }

  type Category {
    name: ID! @id
  }

  type Module implements Model {
    id: ID! @id
    icon: String!
    name: String!
    description: String
    url: String
    version: String!
    author: String
    license: String!
    started: Boolean!
    startPriority: Int
    isSystemModule: Boolean!
    discovery: Boolean!
    categories: [Category]! @relation(name: "IN_CATEGORY", direction: OUT)
    types: [ComponentType]! @relation(name: "HAS_COMPONENT_TYPE", direction: OUT)
    components: [Component]!
      @cypher(statement: "MATCH (this)-[:HAS_COMPONENT_TYPE]->()-[:HAS_COMPONENT]->(c:Component) RETURN c")
  }

  type Field {
    name: String!
    type: String
    label: String
    help: String
    placeholder: String
    required: Boolean
    pattern: String
    min: Float
    max: Float
    maxLength: Float
    step: Float
  }

  type Action {
    name: String!
    description: String
    params: [Field]! @relation(name: "HAS_PARAM", direction: OUT)
  }

  type MeasureDefinition {
    name: String!
    label: String
    help: String
    unit: String
    min: Float
    max: Float
    colors: [String]
  }

  type ComponentType implements Model {
    id: ID! @id
    icon: String!
    name: String!
    description: String
    definition: [Field]! @relation(name: "HAS_CONFIG", direction: OUT)
    actions: [Action] @relation(name: "HAS_ACTION", direction: OUT)
    measurements: [MeasureDefinition] @relation(name: "HAS_MEASUREMENT", direction: OUT)
    categories: [Category]! @relation(name: "IN_CATEGORY", direction: OUT)
    components: [Component]! @relation(name: "HAS_COMPONENT", direction: OUT)
    module: Module! @relation(name: "HAS_COMPONENT_TYPE", direction: IN)
  }

  type Component implements Model {
    id: ID! @id
    icon: String!
    name: String!
    description: String
    started: Boolean!
    module: Module! @cypher(statement: "MATCH (m:Module)-[:HAS_COMPONENT_TYPE]->()-[:HAS_COMPONENT]->(this) RETURN m")
    type: ComponentType! @relation(name: "HAS_COMPONENT", direction: IN)
    config: JSON @cypher(statement: "RETURN apoc.convert.fromJsonMap(this.config)")
    measure: JSON @cypher(statement: "MATCH (this)-[:LAST_MEASURE]->(m:Measure) RETURN properties(m)")
    room: [Component] @relation(name: "HAS_COMPONENT", direction: IN)
  }

  # This is just a ref to create the constraint
  type Measure {
    id: ID! @id
  }

  # Component info for discovery process
  type ComponentInfo implements Model {
    id: ID! @id
    icon: String!
    name: String!
    description: String
    categories: [String]!
    config: JSON @cypher(statement: "RETURN apoc.convert.fromJsonMap(this.config)")
    module: String!
    type: String!
  }

  # Input type for component mutation
  input ComponentInfoInput {
    id: ID!
    icon: String!
    name: String!
    description: String
    categories: [String]!
    config: JSON
    module: String!
    type: String!
  }

  type Event {
    datetime: String!
    type: String!
    label: String
    data: JSON
  }

  #
  # Query
  #
  type Query {
    whoami: User @isAuthenticated
  }

  #
  # Mutation
  #

  type Mutation {
    #
    # Place mutation
    #

    PlaceCreate(
      id: ID!
      icon: String
      name: String
      description: String
      address: String
      postal_code: String
      city: String
      country: String
      location: _Neo4jPointInput
    ): Place
      @isAuthenticated
      @cypher(
        statement: """
        CREATE (p:Place {
          id: $id,
          icon: $icon,
          name: $name,
          description: $description,
          address: $address,
          postal_code: $postal_code,
          city: $city,
          country: $country,
          location: point($location)
        })
        RETURN p
        """
      )

    PlaceUpdate(
      id: ID!
      icon: String!
      name: String!
      description: String
      address: String
      postal_code: String
      city: String
      country: String
      location: _Neo4jPointInput
    ): Place
      @isAuthenticated
      @cypher(
        statement: """
        MATCH (p:Place { id: $id })
          SET p.icon = $icon,
              p.name = $name,
              p.description = $description,
              p.address = $address,
              p.postal_code = $postal_code,
              p.city = $city,
              p.country = $country,
              p.location = point($location)
        RETURN p
        """
      )

    PlaceDelete(id: ID!): Place
      @isAuthenticated
      @cypher(
        statement: """
        MATCH (p:Place { id: $id })
        DETACH DELETE p
        RETURN p
        """
      )

    #
    # Room mutation
    #

    # Create a component in a specific place
    PlaceAddComponent(id: ID!, component: ID!): Component
      @isAuthenticated
      @cypher(
        statement: """
        MATCH (p:Place {id:$id})
        MATCH (c:Component { id:$component })
        MERGE (p)-[:HAS_COMPONENT]->(c)
        RETURN c
        """
      )

    # Remove a component from a specific place
    PlaceDeleteComponent(id: ID!, component: ID!): Place
      @isAuthenticated
      @cypher(
        statement: """
        MATCH (p:Place { id:$id })-[rel:HAS_COMPONENT]->(c:Component { id:$component })
        DELETE rel
        RETURN p
        """
      )

    # Create a room in a specific place
    CreateRoomInPlace(place: ID!, id: ID!, icon: String, name: String, description: String): Room
      @isAuthenticated
      @cypher(
        statement: """
        MATCH (p:Place {id:$place})
        CREATE (r:Room {id: $id, icon: $icon, name: $name, description: $description})
        CREATE (p)-[:HAS_ROOM]->(r)
        RETURN r
        """
      )

    RoomUpdate(id: ID!, icon: String, name: String, description: String): Room
      @isAuthenticated
      @cypher(
        statement: """
        MATCH (r:Room { id:$id})
        SET r.icon = $icon,
            r.name = $name,
            r.description = $description
        RETURN r
        """
      )

    RoomDelete(id: ID!): Room
      @isAuthenticated
      @cypher(
        statement: """
        MATCH (r:Room { id:$id })
        DETACH DELETE r
        RETURN r
        """
      )

    RoomAddComponent(id: ID!, component: ID!): Room
      @isAuthenticated
      @cypher(
        statement: """
        MATCH (r:Room { id:$id })
        MATCH (c:Component { id:$component })
        MERGE (r)-[:HAS_COMPONENT]->(c)
        RETURN r
        """
      )

    RoomDeleteComponent(id: ID!, component: ID!): Room
      @isAuthenticated
      @cypher(
        statement: """
        MATCH (r:Room { id:$id })-[rel:HAS_COMPONENT]->(c:Component { id:$component })
        DELETE rel
        RETURN r
        """
      )

    #
    # User management
    #

    login(username: ID!, password: String!): String
    refresh: String @isAuthenticated
    updateProfile(password: String = null, fullname: String = null, avatar: String = null): Boolean @isAuthenticated
    UserCreate(
      username: ID!
      password: String!
      fullname: String = ""
      avatar: String = null
      roles: String = "user"
    ): User
      @hasRole(roles: [admin])
      @cypher(
        statement: """
        CREATE (u:User { username: $username, password: apoc.util.md5([$password]), fullname: $fullname, avatar: $avatar, roles:[$roles]})
        RETURN u
        """
      )
    UserUpdate(
      username: ID!
      password: String = null
      fullname: String = null
      avatar: String = null
      roles: String = null
    ): User
      @hasRole(roles: [admin])
      @cypher(
        statement: """
        MATCH (u:User { username: $username })
        SET u.password = CASE WHEN $password IS NULL then u.password ELSE apoc.util.md5([$password]) END,
            u.fullname = $fullname,
            u.avatar = $avatar,
            u.roles = CASE WHEN $roles IS NULL THEN u.roles ELSE [$roles] END
        RETURN u
        """
      )
    UserDelete(username: ID!): Boolean
      @hasRole(roles: [admin])
      @cypher(
        statement: """
        MATCH (u:User { username: $username })
        DETACH DELETE u
        RETURN true
        """
      )

    #
    # Hal management
    #
    moduleStart(module: ID!): Boolean @isAuthenticated
    moduleStop(module: ID!): Boolean @isAuthenticated
    moduleDiscoverComponents(module: ID!): [Component] @isAuthenticated
    componentStart(component: ID!): Boolean @isAuthenticated
    componentStop(component: ID!): Boolean @isAuthenticated
    componentDelete(component: ID!): Boolean @isAuthenticated
    componentCreate(component: ComponentInfoInput!): Boolean @isAuthenticated
    componentUpdate(component: ComponentInfoInput!): Boolean @isAuthenticated
    componentAction(component: ID!, action: String!, params: JSON): Boolean
  }
`;

export const resolvers = {
  // Define custom Graphql types
  JSON: GraphQLJSON,
  JSONObject: GraphQLJSONObject,
  // GraphQL query
  Query: {
    whoami: async (
      parent: GraphQLObjectType,
      params: { [key: string]: any },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<any> => {
      const query = `
        MATCH (u:User { username: $username})
        RETURN {
          username: u.username,
          avatar: u.avatar,
          fullname: u.fullname,
          roles: u.roles,
          isPresent: size((u)-[:HAS_PRESENCE_COMPONENT]->(:Component)-[:LAST_MEASURE]->(:Measure {status:1})) > 0
        } as user`;
      const user = await neo4jGetFirstResult(ctx.driver, query, { username: ctx.user?.username }, "user");
      return user;
    },
  },
  // GraphQL mutation
  Mutation: {
    login: async (
      parent: GraphQLObjectType,
      params: { [key: string]: any },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<string> => {
      const result = await login(params.username, params.password, ctx.driver);
      if (!result) throw new Error("User not found or password incorrect");
      return result.token;
    },
    refresh: async (
      parent: GraphQLObjectType,
      params: { [key: string]: any },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<string> => {
      const result = await getUserToken(ctx.user?.username, ctx.driver);
      if (!result) throw new Error("Can't refresh user token: user not found");
      return result.token;
    },
    updateProfile: async (
      parent: GraphQLObjectType,
      params: { [key: string]: any },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<boolean> => {
      const query = `
        MATCH (u:User { username: $username })
        SET u.avatar = $avatar,
            u.fullname = $fullname,
            u.password = CASE WHEN $password IS NULL THEN u.password ELSE apoc.util.md5([$password]) END
        RETURN true as result`;
      const param = Object.assign({}, params, { username: ctx.user?.username });
      const result = await neo4jGetFirstResult<boolean>(ctx.driver, query, param, "result");
      if (result) {
        return result;
      } else {
        throw new Error("Can't change password on an unknown user");
      }
    },
    moduleStart: async (
      parent: GraphQLObjectType,
      params: { module: string },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<boolean> => {
      // perform the start
      await HAL.moduleStart(params.module);
      // wait that the correspondig event is proceed by Neo4J
      await neo4jRunQueryUntilIsTrue(
        ctx.driver,
        "MATCH (n:Module {id:$module}) RETURN n.started = true AS result",
        params,
      );

      return true;
    },
    moduleStop: async (
      parent: GraphQLObjectType,
      params: { module: string },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<boolean> => {
      // perform the start
      await HAL.moduleStop(params.module);
      // wait that the correspondig event is proceed by Neo4J
      await neo4jRunQueryUntilIsTrue(
        ctx.driver,
        "MATCH (n:Module {id:$module}) RETURN n.started = false AS result",
        params,
      );
      return true;
    },
    moduleDiscoverComponents: async (
      parent: GraphQLObjectType,
      params: { module: string },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<Array<Component>> => {
      // Run the discovery process
      const components = await HAL.moduleDiscoverComponents(params.module);
      const componentsIds = components.map((n) => {
        return n.id;
      });
      // wait that the correspondig event is proceed by Neo4J
      await neo4jRunQueryUntilIsTrue(
        ctx.driver,
        "MATCH (n:Component) WHERE n.id IN $ids WITH collect(n) AS components RETURN size(components) = size($ids) AS result",
        { ids: componentsIds },
      );
      return await neo4jRunQuery(
        ctx.driver,
        `MATCH (t:ComponentType)-[:HAS_COMPONENT]->(n:Component) WHERE n.id IN $ids
        OPTIONAL MATCH (n)-[:LAST_MEASURE]->(m)
         RETURN n {
            id: n.id,
            icon: n.icon,
            name: n.name,
            description: n.description,
            started: n.started,
            config: apoc.convert.fromJsonMap(n.config),
            measure: properties(m),
            type: {
              id: t.id,
              icon: t.icon,
              name: t.name,
              description: t.description,
              definition: t.definition,
              actions: apoc.convert.fromJsonList(t.actions),
              categories: [(t)-[:IN_CATEGORY]->(c) | { name:c.name}]
            }
         } as result`,
        { ids: componentsIds },
      );
    },
    componentStart: async (
      parent: GraphQLObjectType,
      params: { component: string },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<boolean> => {
      // Stoppping the component
      await HAL.componentStart(params.component);
      // wait that the correspondig event is proceed by Neo4J
      await neo4jRunQueryUntilIsTrue(
        ctx.driver,
        "MATCH (n:Component {id:$component}) RETURN n.started = true AS result",
        params,
      );
      return true;
    },
    componentStop: async (
      parent: GraphQLObjectType,
      params: { component: string },
      ctx: { [key: string]: any },
      resolverInfo: GraphQLResolveInfo,
    ): Promise<boolean> => {
      // Stoppping the component
      await HAL.componentStop(params.component);
      // wait that the correspondig event is proceed by Neo4J
      await neo4jRunQueryUntilIsTrue(
        ctx.driver,
        "MATCH (n:Component {id:$component}) RETURN n.started = false AS result",
        params,
      );
      return true;
    },
    componentCreate: async (
      parent: GraphQLObjectType,
      params: { component: ComponentInfo },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<boolean> => {
      // Create the component
      await HAL.componentCreate(params.component);
      // wait that the correspondig event is proceed by Neo4J
      await neo4jRunQueryUntilIsTrue(
        ctx.driver,
        "MATCH (n:Component {id:$component.id}) RETURN true AS result",
        params,
      );
      return true;
    },
    componentUpdate: async (
      parent: GraphQLObjectType,
      params: { component: ComponentInfo },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<boolean> => {
      await HAL.componentUpdate(params.component);
      // wait that the correspondig event is proceed by Neo4J
      await neo4jRunQueryUntilIsTrue(
        ctx.driver,
        "MATCH (n:Component {id:$component.id}) RETURN true AS result",
        params,
      );
      return true;
    },
    componentDelete: async (
      parent: GraphQLObjectType,
      params: { component: string },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<boolean> => {
      await HAL.componentDelete(params.component);
      await neo4jRunQueryUntilIsTrue(
        ctx.driver,
        "OPTIONAL MATCH (n:Component {id:$component}) RETURN coalesce(n, true) AS result",
        params,
      );
      return true;
    },
    componentAction: async (
      parent: GraphQLObjectType,
      params: { component: string; action: string; params?: any },
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<boolean> => {
      HAL.componentSendAction(params.component, params.action, params.params);
      return true;
    },
  },
};

export const gqlConfig = {
  query: {
    exclude: ["ComponentInfo", "LogEntry", "Event", "Category", "Person", "Action", "MeasureDefinition", "Field"],
  },
  mutation: false,
  auth: {
    isAuthenticated: true,
    hasRole: true,
  },
};
