import * as neo4j from "neo4j-driver";
import * as jwt from "jsonwebtoken";
import { neo4jGetFirstResult } from "./helpers";
import { config } from "../../config";

interface User {
  username: string;
  roles: string[];
}

export async function login(
  username: string,
  password: string,
  driver: neo4j.Driver,
): Promise<{ token: string; user: User } | null> {
  const query = `
    MATCH (u:User { username: $username, password: apoc.util.md5([$password])})
    RETURN {
      username: u.username,
      roles: u.roles
    } as user`;
  const user = (await neo4jGetFirstResult(driver, query, { username, password }, "user")) as User;
  if (!user) return null;
  return { token: jwt.sign(user, config.jwt_secret_key), user };
}

export async function getUserToken(
  username: string,
  driver: neo4j.Driver,
): Promise<{ token: string; user: User } | null> {
  const query = `
    MATCH (u:User { username: $username})
    RETURN {
      username: u.username,
      roles: u.roles
    } as user`;
  const user = (await neo4jGetFirstResult(driver, query, { username }, "user")) as User;
  if (!user) return null;
  return { token: jwt.sign(user, config.jwt_secret_key), user };
}

export async function checkToken(token: string, driver: neo4j.Driver): Promise<User | null> {
  try {
    // decode the token
    const userInToken: User = jwt.verify(token, config.jwt_secret_key) as User;
    // try to refresh the token  with the decoded username
    const refreshResult = await getUserToken(userInToken.username, driver);
    if (!refreshResult) return null;
    return refreshResult.user;
  } catch (e) {
    return null;
  }
}
