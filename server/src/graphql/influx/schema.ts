/* eslint-disable @typescript-eslint/no-unused-vars*/
/* eslint-disable @typescript-eslint/no-explicit-any*/
import { GraphQLResolveInfo, GraphQLObjectType } from "graphql";
import gql from "graphql-tag";
import { reverse } from "lodash";
import { config } from "../../config";
import { castToDate, timeWidowFromDuration, execFlux } from "./helpers";
import { LOG_LEVELS } from "../../logger";
import { ResolverContext } from "../";
import { HAL, MeasurementDefinition } from "hal";

export const typeDefs = gql`
  type Measurement {
    module: String!
    type: String!
    id: String!
    time: String!
    field: String!
    value: Float
  }

  type ComponentMeasure {
    name: String!
    label: String
    help: String
    unit: String
    colors: [String]
    min: Float
    max: Float
    data: [Measurement]!
  }

  type LogEntry {
    time: String!
    label: String
    level: String!
    message: String!
  }

  type LogResult {
    data: [LogEntry]
    levels: [String]!
    labels: [String]!
    modules: [String]!
    components: [String]!
  }
  type LogHistogramItem {
    time: String!
    data: [Facet]
  }
  type Facet {
    label: String!
    value: Float!
  }

  type Query {
    getComponentMeasure(id: ID!, start: String = "-1d", stop: String = "now()", field: String): [ComponentMeasure]
    logs(
      start: String = "-1h"
      stop: String = "now()"
      skip: Int = 0
      limit: Int = 50
      level: String
      label: String
      desc: Boolean = true
    ): LogResult
    logsHistogram(start: String = "-1d", stop: String = "now()", level: String, label: String): [LogHistogramItem]
  }
`;

export const resolvers = {
  // GraphQL query
  Query: {
    getComponentMeasure: async (
      parent: GraphQLObjectType,
      params: Record<string, string>,
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<any> => {
      // Retrieve HAL component and create a hashmap of measurement fields
      // -----------------------------------------------------------------------
      const component = HAL.findComponent(params.id);
      const componentType = HAL.findComponentType(component.type);
      const defHash = (componentType.measurements || []).reduce(
        (acc, current) => {
          acc[current.name] = current;
          return acc;
        },
        { status: { name: "status", min: 0, max: 1 } } as { [key: string]: MeasurementDefinition },
      );

      // Query influx for window values
      // -----------------------------------------------------------------------
      const every = timeWidowFromDuration(castToDate(params.start.toString()), castToDate(params.stop.toString()));
      const query = `
        from(bucket:"${config.influxdb.bucketHal}")
          |> range(start:${params.start}, stop:${params.stop})
          |> filter(fn: (r) =>
            r.id == "${params.id}"
            ${params.field ? ' and r._field =="' + params.field + '"' : ""})
          |> drop(columns: ["name"])
          |> aggregateWindow(every: ${every}, fn: mean, createEmpty: true)`;
      const result = await execFlux(query);
      console.log(query);

      // Query influx for the max values
      // -----------------------------------------------------------------------
      const maxQuery = `
        from(bucket:"${config.influxdb.bucketHal}")
          |> range(start:-30d, stop:now())
          |> filter(fn: (r) =>
            r.id == "${params.id}"
            ${params.field ? ' and r._field =="' + params.field + '"' : ""})
          |> max()`;
      const resultMax = await execFlux(maxQuery);
      const maxHash: { [key: string]: number } = result.reduce((acc, table) => {
        if (table.length > 0) {
          acc[table[0].field] = table[0].value as number;
        }
        return acc;
      }, {} as { [key: string]: number });

      // Construct the result
      // -----------------------------------------------------------------------
      return result.map((table) => {
        const field = table[0].field;
        const mDef = defHash[field];
        return {
          name: table[0].field,
          label: mDef?.label || null,
          help: mDef?.help || null,
          unit: mDef?.unit || null,
          colors: mDef?.colors || null,
          min: mDef?.min !== undefined ? mDef.min : 0,
          max: mDef?.max !== undefined ? mDef.max : maxHash[field],
          data: table,
        };
      });
    },
    logs: async (
      parent: GraphQLObjectType,
      params: Record<string, unknown>,
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<any> => {
      const desc = params.desc;
      let levelFilter = LOG_LEVELS;
      if (params.level && params.level !== "") {
        levelFilter = LOG_LEVELS.slice(0, 1 + LOG_LEVELS.findIndex((e) => e === params.level));
      }
      let query = levelFilter
        .map(
          (level) => `
        ${level} = from(bucket: "${config.influxdb.bucketMonitoring}")
          |> range(start:${params.start}, stop:${params.stop})
          |> filter(fn: (r) => r["_measurement"] == "hal-logs")
          |> filter(fn: (r) => r["_field"] == "message")
          |> filter(fn: (r) => r["level"] == "${level}" )
          ${params.label && params.label !== "" ? `|> filter(fn: (r) => r["label"] =~ /${params.label}$/ )` : ""}
          |> keep(columns: ["_time", "_value", "label", "level"])
      `,
        )
        .join("\n");
      if (levelFilter.length > 1) query += `union(tables:[${levelFilter.join(",")}])`;
      query += `
         |> sort(columns:["_time"], desc:${desc})
         |> group()
         |> limit(n:${params.limit}, offset: ${params.skip})
         |> yield()`;
      const result = await execFlux(query);

      // Retrieve labels
      const queryLabels = `
        import "influxdata/influxdb/schema"
        schema.tagValues(bucket: "${config.influxdb.bucketMonitoring}", tag: "label")`;
      const labels = await execFlux(queryLabels);

      return {
        data:
          result.length > 0
            ? result
                .flatMap((table) => table.map((row) => Object.assign(row, { message: row.value })))
                .sort((a, b) => {
                  const asValue = new Date(a.time) < new Date(b.time) ? -1 : 1;
                  return desc ? asValue * -1 : asValue;
                })
            : [],
        levels: LOG_LEVELS,
        labels: labels[0].map((row) => row.value).sort(),
      };
    },
    logsHistogram: async (
      parent: GraphQLObjectType,
      params: Record<string, unknown>,
      ctx: ResolverContext,
      resolverInfo: GraphQLResolveInfo,
    ): Promise<any> => {
      let levelFilter = "";
      if (params.level && params.level !== "") {
        levelFilter = LOG_LEVELS.slice(0, 1 + LOG_LEVELS.findIndex((e) => e === params.level))
          .map((level) => `r.level == "${level}"`)
          .join(" or ");
      }
      const every = timeWidowFromDuration(castToDate(params.start.toString()), castToDate(params.stop.toString()));
      const query = `
        from(bucket: "${config.influxdb.bucketMonitoring}")
          |> range(start:${params.start}, stop:${params.stop})
          |> filter(fn: (r) =>
            ${levelFilter !== "" ? `(${levelFilter}) and ` : ""}
            ${params.label && params.label !== "" ? "r.label =~ /" + params.label + ".*$/ and" : ""}
            r["_measurement"] == "hal-logs" and r["_field"] == "message")
          |> group(columns: ["level"])
          |> aggregateWindow(every: ${every}, fn: count, createEmpty: true)`;
      const result = await execFlux(query);
      return (
        Object.values(
          // Flatten the result, because there is one table per level
          result
            .flatMap((table) =>
              table.map((row) => {
                return { time: row.time, level: row.level, value: row.value };
              }),
            )
            // make a hashmap per time with the result item
            .reduce((acc, curr) => {
              if (!acc[curr.time]) acc[curr.time] = { time: curr.time, data: [] };
              acc[curr.time].data.push({ label: curr.level, value: curr.value });
              return acc;
            }, {}),
        )
          // order the data array based on labels (debug at first, error at last)
          .map((row) => {
            row["data"].sort((a, b) =>
              LOG_LEVELS.findIndex((e) => e === a.label) > LOG_LEVELS.findIndex((e) => e === b.label) ? -1 : 1,
            );
            return row;
          })
      );
    },
  },
};
