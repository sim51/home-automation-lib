import axios from "axios";
import Papa from "papaparse";
import { isFinite, toNumber } from "lodash";
import { config } from "../../config";
import { getLoggerForLabels } from "../../logger";

// Logger
const log = getLoggerForLabels(["influxdb"]);

/**
 * Execute a flux query on the server specified by the component
 */
export async function execFlux(query: string): Promise<Array<Array<{ [key: string]: any }>>> {
  log.debug(`Executing flux query ${query}`);
  const queryResult = await axios.post(
    // Url
    `${config.influxdb.url}/api/v2/query?org=${config.influxdb.organization}`,
    // Data
    {
      dialect: { annotations: ["group", "datatype", "default"] },
      query: query,
    },
    //headers
    {
      headers: {
        Authorization: `Token ${config.influxdb.token}`,
        Accept: "application/csv",
        "Content-type": "application/json",
      },
    },
  );
  const data = Papa.parse(queryResult.data);
  const result: {
    datatypes: Array<Array<string>>;
    headers: Array<Array<string>>;
    tables: Array<Array<{ [key: string]: any }>>;
  } = data.data.reduce(
    (result, row) => {
      // if the line declare a datatype
      if (row[0] === "#datatype") {
        result.datatypes.push(row.slice(3));
      }
      // if row is an header, we create a new table
      if (row[1] === "result") {
        result.headers.push(row.slice(3).map((value) => (value.startsWith("_") ? value.replace("_", "") : value)));
        result.tables.push([]);
      }

      // if we have a table number
      if (row[2] !== "" && isFinite(toNumber(row[2])) && row[0] !== "#default") {
        const tableIndex = toNumber(row[2]);
        // check if the table index already exist
        // if not : create it, + create header & datatype with the previous one
        if (result.tables.length !== tableIndex + 1) {
          result.tables.push([]);
          result.headers.push(result.headers[result.headers.length - 1]);
          result.datatypes.push(result.datatypes[result.datatypes.length - 1]);
        }
        // we push the data to the table, but before we transform it with the header+type we've got
        result.tables[tableIndex].push(
          row.slice(3).reduce((obj, colValue, colIndex) => {
            const colName = result.headers[tableIndex][colIndex];
            const colType = result.datatypes[tableIndex][colIndex];
            switch (colType) {
              case "long":
              case "double":
                obj[colName] = colValue && colValue !== "" ? toNumber(colValue) : null;
                break;
              default:
                obj[colName] = colValue;
                break;
            }
            return obj;
          }, {}),
        );
      }
      return result;
    },
    {
      datatypes: [],
      headers: [],
      tables: [],
    },
  );
  return result.tables;
}

export function castToDate(value: string): Date {
  let result: Date | null = null;
  if (value.startsWith("-")) {
    const today = Date.now();
    const matches = value.match(/\-([0-9]+)([y|mo|w|d|h|m|s|ms])/);
    if (matches) {
      const valueToSubstract = parseInt(matches[1]);
      switch (matches[2]) {
        case "y":
          result = new Date(today - valueToSubstract * 1000 * 60 * 60 * 24 * 365);
          break;
        case "mo":
          result = new Date(today - valueToSubstract * 1000 * 60 * 60 * 24 * 30);
          break;
        case "w":
          result = new Date(today - valueToSubstract * 1000 * 60 * 60 * 24 * 7);
          break;
        case "d":
          result = new Date(today - valueToSubstract * 1000 * 60 * 60 * 24);
          break;
        case "h":
          result = new Date(today - valueToSubstract * 1000 * 60 * 60);
          break;
        case "m":
          result = new Date(today - valueToSubstract * 1000 * 60);
          break;
        case "s":
          result = new Date(today - valueToSubstract * 1000);
          break;
        case "ms":
          result = new Date(today - valueToSubstract);
          break;
      }
    }
  } else if (value.toLowerCase() === "now()") {
    result = new Date();
  } else {
    result = new Date(value);
  }
  if (result === null) {
    throw new Error(`String ${value} can't be cast to date`);
  }
  return result;
}

export function timeWidowFromDuration(start: Date, stop: Date): string {
  const duration: number = stop.getTime() - start.getTime();
  let result = "1d";
  // d < 15min
  if (duration <= 15 * 60 * 1000) {
    result = "10s";
    // d < 1h
  } else if (duration <= 60 * 60 * 1000) {
    result = "30s";
    // d < 1d
  } else if (duration <= 24 * 60 * 60 * 1000) {
    result = "5m";
    // d < 3d
  } else if (duration <= 3 * 24 * 60 * 60 * 1000) {
    result = "1h";
    // d < 1w
  } else if (duration <= 7 * 24 * 60 * 60 * 1000) {
    result = "2h";
  }
  return result;
}
