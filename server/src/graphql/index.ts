import { Express } from "express";
import { ApolloServer } from "apollo-server-express";
// import { SubscriptionServer } from "subscriptions-transport-ws";
import { makeAugmentedSchema, assertSchema } from "neo4j-graphql-js";
import { IHal } from "hal";
import { mergeResolvers, mergeTypeDefs } from "@graphql-tools/merge";
import { IsAuthenticatedDirective } from "graphql-auth-directives";
import { Server } from "http";
import * as neo4j from "neo4j-driver";
import { config } from "../config";
import { resolvers as neoResolvers, typeDefs as neoTypeDef, gqlConfig } from "./neo4j/schema";
import { resolvers as influxResolvers, typeDefs as influxTypeDef } from "./influx/schema";

export type ResolverContext = { driver: neo4j.Driver; user?: { username: string; avatar: string } };

export async function register(server: Server, app: Express, hal: IHal): Promise<() => void> {
  const moduleWithGqlSchema = hal.modules
    .filter((m) => {
      return m.graphql !== undefined;
    })
    .map((m) => {
      return m.graphql;
    });

  // create the  graphql schema
  const schema = makeAugmentedSchema({
    typeDefs: mergeTypeDefs([neoTypeDef, influxTypeDef].concat(moduleWithGqlSchema.map((n) => n.typeDefs))),
    resolvers: mergeResolvers(
      [neoResolvers, influxResolvers].concat(
        moduleWithGqlSchema.filter((m) => m.resolvers !== undefined).map((n) => n.resolvers),
      ),
    ),
    config: gqlConfig,
    schemaDirectives: {
      isAuthenticated: IsAuthenticatedDirective,
    },
  });

  // create the neo4j driver
  const driver = neo4j.driver(config.neo4j.url, neo4j.auth.basic(config.neo4j.login, config.neo4j.password));

  // create the graphql server with apollo
  const serverGraphql = new ApolloServer({
    schema,
    context: ({ req }) => {
      return {
        driver,
        req,
      };
    },
  });

  // Sync the Neo4j schema (ie. indexes, constraints)
  await assertSchema({ schema, driver, debug: true });

  // Register the graphql server to express
  serverGraphql.applyMiddleware({ app });

  return (): void => {
    // new SubscriptionServer(
    //   {
    //     execute,
    //     subscribe,
    //     schema: schema,
    //   },
    //   {
    //     server: server,
    //     path: "/subscriptions",
    //   },
    // );
  };
}
