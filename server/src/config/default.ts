import { Config } from "./type";

export const config_default: Config = {
  port: 4000,
  error_with_stack: true,
  jwt_secret_key: "shhhhhhhuuuuuuuuuuuuut",
  logs: {
    console_level: "info",
    file_level: "error",
    file_maxsize: "250m",
    file_retention: "1",
    file_path: "./",
  },
  nodered: {
    credentialSecret: false,
    httpAdminRoot: "/red/ui",
    httpNodeRoot: "/red/api",
    userDir: ".nodered/",
    functionGlobalContext: {},
    logging: {
      // Console logging
      console: {
        level: "off",
        metrics: false,
        audit: false,
      },
    },
  },
  neo4j: {
    url: "bolt://localhost:7687",
    login: process.env.NEO4J_ADMIN_USER || "neo4j",
    password: process.env.NEO4J_ADMIN_PASSWORD || "admin",
  },
  influxdb: {
    url: "http://localhost:8086",
    token: process.env.INFLUXDB_TOKEN || "",
    organization: process.env.INFLUXDB_ORG || "HAL",
    bucketHal: process.env.INFLUXDB_BUCKET || "hal",
    bucketMonitoring: process.env.INFLUXDB_BUCKET_MONITORING || "monitoring",
  },
  hal: {
    module_path: "../modules",
    components: [
      {
        id: "1",
        name: "Neo4j Storage",
        // module
        module: "hal-module-neo4j-data-storage",
        // type of the device (match the id of the type)
        type: "hal-module-neo4j-data-storage-component",
        //config
        config: {
          url: "bolt://localhost:7687",
          login: process.env.NEO4J_ADMIN_USER || "neo4j",
          password: process.env.NEO4J_ADMIN_PASSWORD || "admin",
        },
      },
      {
        id: "2",
        name: "Influxdb Storage",
        // module&
        module: "hal-module-influxdb-data-storage",
        // type of the device (match the id of the type)
        type: "hal-module-influxdb-data-storage-component",
        //config
        config: {
          url: "http://localhost:8086",
          token: process.env.INFLUXDB_TOKEN || "",
          organization: process.env.INFLUXDB_ORG || "HAL",
          bucketHal: process.env.INFLUXDB_BUCKET_HAL || "hal",
          bucketMonitoring: process.env.INFLUXDB_BUCKET_MONITORING || "monitoring",
        },
      },
    ],
  },
};
