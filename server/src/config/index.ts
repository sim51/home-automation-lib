import neo4j from "neo4j-driver";
import { deepMerge } from "../utils";
import { Settings as config_hal } from "hal";
import { Config } from "./type";
import { config_default } from "./default";
import * as config_dev from "./dev.json";
import * as config_test from "./test.json";
import * as config_docker from "./docker.json";
import * as config_production from "./production.json";

let config: Config;

switch (process.env.configuration) {
  case "test":
    config = deepMerge(config_default, config_test);
    break;
  case "docker":
    config = deepMerge(config_default, config_docker);
    break;
  case "production":
    config = deepMerge(config_default, config_production);
    break;
  default:
    config = deepMerge(config_default, config_dev);
    break;
}

export async function getHalConfigurationFromDb(config: Config): Promise<config_hal["components"]> {
  const driver = neo4j.driver(config.neo4j.url, neo4j.auth.basic(config.neo4j.login, config.neo4j.password));
  const session = driver.session();
  try {
    const rs = await session.run(`
      MATCH (c:Component)<-[:HAS_COMPONENT]-(ct:ComponentType)<-[:HAS_COMPONENT_TYPE]-(m:Module)
      RETURN  {
  	     id: c.id,
         name: c.name,
         icon: c.icon,
         type: ct.id,
         module: m.id,
         config: apoc.convert.fromJsonMap(c.config)
       } as data
    `);
    return rs.records.map((record) => record.get("data"));
  } finally {
    session.close();
    driver.close();
  }
}

export { config, Config };
