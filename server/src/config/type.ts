import { Settings as config_hal } from "hal";

type LogLevel = "error" | "warning" | "info" | "debug" | "off";

interface config_log {
  console_level: LogLevel;
  file_level: LogLevel;
  file_maxsize: string;
  file_retention: string;
  file_path: string;
}

interface config_nodered {
  httpAdminRoot: string;
  httpNodeRoot: string;
  userDir: string;
  functionGlobalContext: Record<string, unknown>;
  credentialSecret: string | boolean;
  logging: {
    // Console logging
    console: {
      level: LogLevel;
      metrics: boolean;
      audit: boolean;
    };
    customHandler?: {
      level: LogLevel;
      metrics: boolean;
      audit: boolean;
      handler: () => (msg: { level: number; msg: string }) => void;
    };
  };
}

interface config_neo4j {
  url: string;
  login: string;
  password: string;
  options?: { [key: string]: string | boolean | number };
}

interface config_influxdb {
  url: string;
  token: string;
  organization: string;
  bucketMonitoring: string;
  bucketHal: string;
}

export interface Config {
  port: number;
  error_with_stack: boolean;
  jwt_secret_key: string;
  logs: config_log;
  nodered: config_nodered;
  neo4j: config_neo4j;
  influxdb: config_influxdb;
  hal: config_hal;
}
