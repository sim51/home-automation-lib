import TransportStream from "winston-transport";
import axios from "axios";
import { LogEntry } from "./../index";
import { escape } from "./influxdb-escape";

interface InfluxConfig {
  url: string;
  organization: string;
  bucketMonitoring: string;
  token: string;
}

/**
 * Custom transport that send logs to influxdb.
 */
export class InfluxdbTransport extends TransportStream {
  logs: Array<LogEntry> = [];
  job: any;
  config: InfluxConfig;

  constructor(config: InfluxConfig, opt: TransportStream.TransportStreamOptions) {
    super(opt);
    this.config = config;
    this.job = setInterval(() => {
      this.sendLogsToInflux();
    }, 5000);
  }

  log(info: LogEntry, callback: () => void): void {
    setImmediate(() => {
      this.emit("logged", info);
    });
    // Adding log into the stack
    this.logs.push(info);
    callback();
  }

  async sendLogsToInflux(): Promise<void> {
    if (this.logs.length > 0) {
      try {
        await axios.post(
          // url
          `${this.config.url}/api/v2/write?org=${this.config.organization}&bucket=${this.config.bucketMonitoring}&precision=ms`,
          // data
          this.logs.map((log) => this.logToInflux(log)).join("\n"),
          // headers
          { headers: { Authorization: `Token ${this.config.token}` } },
        );
        this.logs = [];
      } catch (e) {
        console.log(`[INFLUXDB]: Failed to inject point`, e.message);
      }
    }
  }

  private logToInflux(log: LogEntry): string {
    // measurement
    let measure = `${escape.measurement("hal-logs")},`;
    // tags
    measure += ["label", "level"]
      .map((key: string) => {
        return `${escape.tag(key)}=${escape.tag(log[key].toUpperCase())}`;
      })
      .join(",");
    // message field
    measure += ` ${escape.tag("message")}=${escape.quoted(log.message)}`;
    // time
    measure += " " + new Date(log.timestamp).getTime();
    return measure;
  }

  close(): void {
    clearInterval(this.job);
  }
}
