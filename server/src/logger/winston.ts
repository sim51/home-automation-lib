import * as winston from "winston";
import DailyRotateFile from "winston-daily-rotate-file";
import * as path from "path";
import * as util from "util";
import { config } from "./../config";
import { InfluxdbTransport } from "./transport/influxdb";

/**
 * Define log levels with the associated color
 */
export const logLevels = {
  levels: {
    error: 0,
    warn: 1,
    info: 2,
    debug: 3,
  },
  colors: {
    error: "red",
    warn: "yellow",
    info: "cyan",
    debug: "green",
  },
};
winston.addColors(logLevels.colors);

/**
 * Function to stringify labels.
 */
const labelsStringify = (labels: string[]) => labels.map((e) => e.toUpperCase()).join(" > ");

/**
 * Winston log format.
 * NB: label is already stringify
 */
const logFormat = winston.format.printf(({ level, message, label, timestamp }) => {
  return `${timestamp} - ${level.toUpperCase()} - [${label}] - ${message}`;
});

/**
 * Class wrapper on top of Winston logger.
 */
export class WinstonLogger {
  logger: winston.Logger;

  constructor(logger: winston.Logger) {
    this.logger = logger;
  }

  error(message: string, params: unknown = undefined): void {
    this.logger.log("error", message + ` ${params ? util.inspect(params, false, null) : ""}`);
  }
  warn(message: string, params: unknown = undefined): void {
    this.logger.log("warn", message + ` ${params ? util.inspect(params, false, null) : ""}`);
  }
  info(message: string, params: unknown = undefined): void {
    this.logger.log("info", message + ` ${params ? util.inspect(params, false, null) : ""}`);
  }
  debug(message: string, params: unknown = undefined): void {
    this.logger.log("debug", message + ` ${params ? util.inspect(params, false, null) : ""}`);
  }
}

/**
 * Create a logger instance.
 */
export function getLoggerForLabels(labels: string[]): WinstonLogger {
  if (!winston.loggers.has(labelsStringify(labels))) {
    winston.loggers.add(labelsStringify(labels), {
      levels: logLevels.levels,
      transports: [
        // In console we log everything with colors
        new winston.transports.Console({
          level: config.logs.console_level,
          format: winston.format.combine(
            winston.format.colorize({ all: true }),
            winston.format.label({ label: labelsStringify(labels) }),
            winston.format.timestamp(),
            logFormat,
          ),
        }),
        // There is also a combined log file
        new DailyRotateFile({
          filename: path.join(config.logs.file_path, "server-%DATE%.log"),
          datePattern: "YYYY-MM-DD",
          createSymlink: false,
          symlinkName: "server.log",
          zippedArchive: true,
          maxSize: config.logs.file_maxsize,
          maxFiles: config.logs.file_retention,
          level: config.logs.file_level,
          format: winston.format.combine(
            winston.format.splat(),
            winston.format.timestamp(),
            winston.format.label({ label: labelsStringify(labels) }),
            logFormat,
          ),
        }),
        // log in influx
        new InfluxdbTransport(config.influxdb, {
          level: "debug",
          format: winston.format.combine(
            winston.format.splat(),
            winston.format.timestamp(),
            winston.format.label({ label: labelsStringify(labels) }),
            logFormat,
          ),
        }),
      ],
    });
  }
  return new WinstonLogger(winston.loggers.get(labelsStringify(labels)));
}
