import { Logger as HalLogger } from "hal";
import { getLoggerForLabels, WinstonLogger, logLevels } from "./winston";

const LOG_LEVELS: string[] = Object.keys(logLevels.levels).map((e) => e.toUpperCase());
/**
 * Definition of a log entry.
 */
interface LogEntry {
  timestamp: string;
  labels: string[];
  level: string;
  message: string;
}

const Logger: HalLogger = {
  error: (labels: string[], message: string, params: unknown): void => {
    getLoggerForLabels(labels).error(message, params);
  },
  warn: (labels: string[], message: string, params: unknown): void => {
    getLoggerForLabels(labels).warn(message, params);
  },
  info: (labels: string[], message: string, params: unknown): void => {
    getLoggerForLabels(labels).info(message, params);
  },
  debug: (labels: string[], message: string, params: unknown): void => {
    getLoggerForLabels(labels).debug(message, params);
  },
};

export { LOG_LEVELS, getLoggerForLabels, WinstonLogger as LoggerForLabels, Logger, LogEntry };
