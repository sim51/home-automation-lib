/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

/**
 * Test if the given param is an object.
 */
export function isObject(item: any): boolean {
  return item && typeof item === "object" && !Array.isArray(item) && item !== null;
}

/**
 * Make a deep merge of the source object into the target one.
 * @returns The merge object
 */
export function deepMerge<T>(target: any, source: any): T {
  const output = Object.assign({}, target);

  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach((key) => {
      if (isObject(source[key])) {
        if (!(key in output)) Object.assign(output, { [key]: source[key] });
        else output[key] = deepMerge(target[key], source[key]);
      } else {
        Object.assign(output, { [key]: source[key] });
      }
    });
  }

  return output as T;
}

export function listenStopProcess(fn: () => Promise<void>): void {
  //catch ctrl+c event
  process.on("SIGINT", async () => {
    try {
      await fn();
    } catch (e) {
      process.exit();
    }
  });
  // catch "kill pid"
  process.on("SIGTERM", async () => {
    try {
      await fn();
    } catch (e) {
      process.exit();
    }
  });
}
